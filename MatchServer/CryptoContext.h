#ifndef _AURI_CRYPTO_CONTEXT_H_
#define _AURI_CRYPTO_CONTEXT_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <wincrypt.h>

#pragma comment(lib,"Crypt32")

BOOLEAN GetCryptoContext(_Out_ HCRYPTPROV* hCryptProv);

#endif