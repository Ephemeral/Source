#include "CryptoContext.h"

BOOLEAN GetCryptoContext(_Out_ HCRYPTPROV* hCryptProv)
{
	BOOLEAN Status = TRUE;
	if(!CryptAcquireContextA(hCryptProv,0,MS_ENHANCED_PROV_A,PROV_RSA_FULL,0))
	{
		Status = FALSE;
		if(GetLastError() == NTE_BAD_KEYSET)
		{
			if(CryptAcquireContext(
				hCryptProv, 
				NULL, 
				MS_ENHANCED_PROV, 
				PROV_RSA_FULL, 
				CRYPT_NEWKEYSET))
			{
				Status = TRUE;
			}
		}
	}
	return Status;
}