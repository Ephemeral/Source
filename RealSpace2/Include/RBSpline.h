#pragma once
//#include "RTypes.h"

class RBSpline
{
	typedef vector<RVector>	lsPos;
	typedef lsPos::iterator	itPos;

public:
	lsPos		vPt		;						// Points
	RVector*	pCrv	;						// for Curve
	float		fS		;						// Step
	RMatrix		mtCd	;						// cardinal Matrix

	int			iC		;						// current vertex
	bool		bC		;

	float		m_fTime	;
	float		m_fLifeTime;

	int				iN;
	RTestVertex*	pVtx;

public:
	RBSpline(void);
	virtual ~RBSpline(void);

	void		Destroy();

	//void		Generate(bool loop, std::vector<REmitterVariable> &vectorVars);
	void		Generate(int nCount, RVector* pVec);
	void		CurveUP();

	INT			VtxUp();
	RVector		Update(float fElapseTime);
	//void		Render(RDevice* pDevice);
};