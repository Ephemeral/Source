class ServerShield {
	private:
		static ServerShield *m_pInstance;
		std::vector<MTD_FileHash *> m_pFilehashList;

		void parseFileHashes();

	public:
		static ServerShield *getInstance();

		void initialize();
		std::vector<MTD_FileHash *> getFileHashes();
		void onFileHashReceived(MUID uidSender, void *pBlob, int nHashCount);
		void messagePlayer(MMatchObject *pObj, char *pszMessage);
};