#include "stdafx.h"
#include <sstream>
#include <Wincrypt.h>

#include "ClientShield.h"
#include "MMatchClient.h"

MemoryCheck::MemoryCheck(DWORD dwAddress) {
	m_dwAddress = dwAddress;
	m_nSize = 0;

	while (true) {
		unsigned char uchTemp = ((unsigned char *)dwAddress)[m_nSize++];

		if (uchTemp == 0xC2) {
			m_nSize += 2;
			break;
		}

		if (uchTemp == 0xC3) {
			break;
		}
	}

	m_byChecksum = new unsigned char[m_nSize];
	memcpy(m_byChecksum, (void *)dwAddress, m_nSize);
}

ClientShield *ClientShield::m_pInstance(new ClientShield());

ClientShield *ClientShield::getInstance() {
	return m_pInstance;
}

void ClientShield::initialize() {
	// Create memory checksum
	memcpy(m_rgbyMemoryChecksum, GetModuleHandle(NULL), CODE_SECTION_SIZE);

	// Add functions to check memory
	HMODULE hKernel32 = GetModuleHandleA("kernel32.dll");

	m_pMemoryCheckList.push_back(new MemoryCheck((DWORD)GetProcAddress(hKernel32, "QueryPerformanceCounter"))); //Underclocking
	m_pMemoryCheckList.push_back(new MemoryCheck((DWORD)GetProcAddress(hKernel32, "GetTickCount"))); //Underclocking
}

void ClientShield::onUpdate() {
	unsigned long dwTime = timeGetTime();

	// Check memory checksum
	if ((m_dwLastMemoryCheck < (dwTime - MEMORY_CHECK_INTERVAL))) {
		if (memcmp(m_rgbyMemoryChecksum, GetModuleHandle(NULL), CODE_SECTION_SIZE) != 0) {
			reportToServer();
		}

		// Check memory check list
		for (std::vector<class MemoryCheck *>::iterator pMemoryCheckListIt = m_pMemoryCheckList.begin(); pMemoryCheckListIt != m_pMemoryCheckList.end(); pMemoryCheckListIt++) {
			MemoryCheck *pMemoryCheck = (*pMemoryCheckListIt);

			if (memcmp(pMemoryCheck->m_byChecksum, (void *)pMemoryCheck->m_dwAddress, pMemoryCheck->m_nSize) != 0) {
				reportToServer();
			}
		}

		m_dwLastMemoryCheck = dwTime;
	}
}

void ClientShield::reportToServer() {
	MClient *pClient = MMatchClient::GetInstance();
	pClient->Post(pClient->CreateCommand(MC_REQUEST_GIVE_ONESELF_UP, pClient->GetServerUID()));
}

std::string ClientShield::GetHash(const void *pData, int nSize, HashType nHashType) {
	HCRYPTPROV dwProvider = NULL;

	if (!CryptAcquireContext(&dwProvider, NULL, NULL, PROV_RSA_AES, CRYPT_VERIFYCONTEXT)) {
		return "Error 1";
	}

	HCRYPTPROV dwHash = NULL;

	if (!(CryptCreateHash(dwProvider, (nHashType == HashType::HASHTYPE_MD5 ? CALG_MD5 : CALG_SHA1), 0, 0, &dwHash))) {
		CryptReleaseContext(dwProvider, NULL);
		return "Error 2";
	}

	if (!(CryptHashData(dwHash, static_cast<const unsigned char *>(pData), nSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 3";
	}

	unsigned long dwHashSize = 0, dwCount = sizeof(unsigned long);

	if (!(CryptGetHashParam(dwHash, HP_HASHSIZE, (unsigned char *)&dwHashSize, &dwCount, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 4";
	}

	std::vector<unsigned char> pBufferVector(dwHashSize);

	if (!(CryptGetHashParam(dwHash, HP_HASHVAL, reinterpret_cast<unsigned char *>(&pBufferVector[0]), &dwHashSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 5";
	}

	// Fuck it, string stream!
	std::ostringstream strOutput;

	for (std::vector<unsigned char>::const_iterator pIterator = pBufferVector.begin(); pIterator != pBufferVector.end();
		pIterator++) {
			strOutput.fill('0');
			strOutput.width(2);
			strOutput << std::hex << static_cast<const int>(*pIterator);
		}

	CryptDestroyHash(dwHash);
	CryptReleaseContext(dwProvider, NULL);

	return strOutput.str();
}