#ifndef _MQUEST_NPC_H
#define _MQUEST_NPC_H

#include "MQuestConst.h"
#include "MTypes.h"

/// NPC 종류
enum MQUEST_NPC
{
	// Not the absolute value of the constant change. xml also be replaced with.

	NPC_NONE					= 0,	///< N/A

	NPC_GOBLIN					= 11,	///< Goblin Soldier
	NPC_GOBLIN_GUNNER			= 12,	///< Goblin Gunner
	NPC_GOBLIN_WIZARD			= 13,	///< Goblin Wizard
	NPC_GOBLIN_COMMANDER		= 14,	///< Goblin colon
	NPC_GOBLIN_CHIEF			= 15,	///< Goblin Chieftain
	NPC_GOBLIN_KING				= 16,	///< Goblin King

	NPC_KOBOLD					= 21,	///< Kobold
	NPC_KOBOLD_SHAMAN			= 22,	///< Kobold shaman
	NPC_KOBOLD_COMMANDER		= 23,	///< Kobold captain
	NPC_KOBOLD_KING				= 24,	///< Kobold King
	NPC_KOBOLD_KING_BOSS		= 25,	///< Kobold King (Boss)
	NPC_BROKEN_GOLEM			= 26,	///< Failed Golem

	NPC_SKELETON				= 31,	///< Skeleton
	NPC_SKELETON_MAGE			= 32,	///< Skeletal Mage
	NPC_SKELETON_COMMANDER		= 33,	///< Skeleton colon
	NPC_GIANT_SKELETON			= 34,	///< Giant Skeletons
	NPC_THE_UNHOLY				= 35,	///< Corpse accursed
	NPC_LICH_BABY				= 36,	///< Rich Baby
	NPC_GIANT_SKELETON_BOSS		= 37,	///< Giant Skeletons (Boss)
	NPC_THE_UNHOLY_BOSS			= 38,	///< Cursed body (Boss)
	NPC_LICH					= 39,	///< Rich

	NPC_PALMPOW_BABY			= 41,	///< Palm Poe Baby
	NPC_PALMPOA					= 42,	///< Palm Mirepoix
	NPC_PALMPOA_COMMANDER		= 43,	///< Colon Palm Poe
	NPC_PALMPOW					= 44,	///< Palm Poe
	NPC_CURSED_PALMPOW			= 45,	///< Poe cursed farm

	NPC_SCRIDER					= 51,	///< Rider disk


	//Should be separately registered for the event.
	NPC_GOBLIN_EVENT			= 111,	///< Goblin Soldier for the event.
	NPC_SKELETON_EVENT			= 131,	///< Skeleton for the event.

	// For survival
	NPCS_GOBLIN					= 151,	///< Goblin Soldier
	NPCS_GOBLIN_GUNNER			= 152,	///< Goblin Gunner
	NPCS_GOBLIN_WIZARD			= 153,	///< Goblin Wizard
	NPCS_GOBLIN_COMMANDER		= 154,	///< Goblin colon
	NPCS_GOBLIN_CHIEF			= 155,	///< Goblin Chieftain
	NPCS_GOBLIN_KING			= 156,	///< Goblin King
	NPCS_LITTLE_GOBLIN_KING_JR	= 157, ///< Dwarf Goblin King, Jr.
	NPCS_THUNDER_GOBLIN_KING_JR	= 158, ///< Thunder Goblin King, Jr.
	NPCS_THUNDER_GOBLIN_KING	= 159, ///< Thunder Goblin King

	NPCS_KOBOLD					= 161,	///< Kobold
	NPCS_KOBOLD_SHAMAN			= 162,	///< Kobold shaman
	NPCS_KOBOLD_COMMANDER		= 163,	///< Kobold captain
	NPCS_KOBOLD_KING			= 164,	///< Kobold King
	NPCS_KOBOLD_KING_BOSS		= 165,	///< Kobold King (Boss)
	NPCS_BROKEN_GOLEM			= 166,	///< Failed Golem

	NPCS_SKELETON				= 171,	///< Skeleton
	NPCS_SKELETON_MAGE			= 172,	///< Skeletal Mage
	NPCS_SKELETON_COMMANDER		= 173,	///< Skeleton colon
	NPCS_GIANT_SKELETON			= 174,	///< Giant Skeletons
	NPCS_THE_UNHOLY				= 175,	///< Corpse accursed
	NPCS_LICH_BABY				= 176,	///< Rich Phone
	NPCS_GIANT_SKELETON_BOSS	= 177,	///< Hyperion shoe
	NPCS_THE_UNHOLY_BOSS		= 178,	///< Arne Ramon (Boss)
	NPCS_LICH					= 179,	///< Rich (Boss)

	NPCS_LICH_RITUAL			= 254,
};

/// NPC Rating
enum MQUEST_NPC_GRADE
{
	NPC_GRADE_REGULAR = 0,			///< General
	NPC_GRADE_VETERAN,				///< Veteran
	NPC_GRADE_ELITE,				///< Elite
	NPC_GRADE_BOSS,					///< Boseugeup
	NPC_GRADE_LEGENDARY,			///< Special boseugeup
	NPC_GRADE_END
};

/// NPC attack type, edit the 1Byte MQuestNPCInfo.nNPCAttackTypes should be greater than the face.
enum MQUEST_NPC_ATTACK
{
	NPC_ATTACK_NONE			= 0,		///< N/A
	NPC_ATTACK_MELEE		= 0x1,		///< Melee
	NPC_ATTACK_RANGE		= 0x2,		///< Perspective attack
	NPC_ATTACK_MAGIC		= 0x4,		///< Magic Attack
};

/// NPC Action type
enum MQUEST_NPC_BEHAVIOR
{

};


/// NPC Sound
enum MQUEST_NPC_SOUND
{
	NPC_SOUND_ATTACK = 0,		// When the attack
	NPC_SOUND_WOUND,			// When hit
	NPC_SOUND_DEATH,			// When he died
	NPC_SOUND_END
};

/// Quest NPC information
struct MQuestNPCInfo
{
	// Basic Information
	MQUEST_NPC			nID;				///< id
	MQUEST_NPC_GRADE	nGrade;				///< 등급
	char				szName[256];		///< 이름
	char				szDesc[256];		///< 설명
	BYTE				nNPCAttackTypes;	///< 공격타입	- MQUEST_NPC_ATTACK형의 SET
	float				fSpeed;				///< 속도
	float				fWeight;			///< 몸무게
	int					nMaxHP;				///< 최대 HP. 클라이언트는 사용하지 않는다. 서버에서 보내준다.
	int					nMaxAP;				///< 최대 AP. 클라이언트는 사용하지 않는다. 서버에서 보내준다.
	int					nDC;				///< 난이도 상수
	char				szMeshName[256];	///< 메쉬이름
	int					nWeaponDamage;		///< 갖고 있는 무기 공격력
	float				fRotateSpeed;		///< 방향 전환 속도 (초당 회전 각도임, 예를 들어 3.14159이면 초당 반바퀴 회전함. -값이면 바로 회전)
	float				fCollRadius;		///< 충돌 반지름
	float				fCollHeight;		///< 충돌 높이
	bool				bColPick;			///< 원거리 피격시 반지름말고 피킹여부로 피격체크할지 여부(주로 보스만 true)
	MVector				vScale;				///< 크기
	MVector				vColor;				///< 색
	unsigned char		nSpawnWeight;		///< 등장 가중치
	unsigned long int	nWeaponItemID;		///< 장비하고 있는 무기 ID(Zitem에 있는거)
	float				fDyingTime;			///< 죽는 애니메이션 시간(기본은 5초)
	float				fTremble;			///< 피격시 몸 떠는 정도(기본은 30)
	int					nOffenseType;		///< 공격 특성( 1:melee, 2:range)
	bool				bFriendly;			///< 우호적인지...
	// int					nDBIndex;			///< DB와 NPC와 Matching되는 인덱스.

	// Sound
	char				szSoundName[NPC_SOUND_END][128];	///< 사운드 파일 이름

	// AI 관련 파라메타
	char				nIntelligence;			///< 지능(1 ~ 5단계) 1이 가장 좋다. - 길찾기 업데이트 속도와 관련있다.
	char				nAgility;				///< 민첩성(1 ~ 5단계) 1이 가장 좋다. - 공격 업데이트 속도와 관련있다.

	float				fAttackRange;			///< 기본 공격범위
	float				fAttackRangeAngle;		///< 기본 공격 시야 각도
	float				fAttackHitRate;			///< 기본 공격 명중률(특히 원거리)
	float				fAttackCoolTime;		///< 기본 공격 쿨타임(특히 원거리)
	float				fCollisionCheckSpeed;	///< 충돌체크 속도
	float				fViewAngle;				///< 시야 각도(라디안)
	float				fViewDistance;			///< 시야 거리
	float				fViewDistance360;		///< 전방향 시야거리
	
	// 플래그
	bool				bNeverBlasted;				///< 칼로 띄워올리기 등 적용 여부
	bool				bMeleeWeaponNeverDamaged;	///< 근접 공격에 맞는지 여부
	bool				bRangeWeaponNeverDamaged;	///< 원근 공격에 맞는지 여부
	bool				bShadow;					///< 그림자가 있는지 여부
	bool				bNeverPushed;				///< 공격 받았을때 밀릴것인가?
	bool				bNeverAttackCancel;			///< 공격 받았을때 하던 공격이 취소되는가?

	int					nSkills;				///< 가진 스킬 개수
	int					nSkillIDs[MAX_SKILL];	///< 가지고있는 스킬

	// 서버만 사용하는 파라메타
	int					nDropTableID;			///< drop table index - 서버만 사용한다.
	char				szDropTableName[8];		///< drop table name

	/////////////////////////////////////////////

	/// 초기화
	void SetDefault()
	{
		nID					= NPC_GOBLIN;
		nGrade				= NPC_GRADE_REGULAR;
		strcpy(szName, "Noname");
		szDesc[0]			= 0;
		nNPCAttackTypes		= NPC_ATTACK_MELEE;
		fSpeed				= 300.0f;
		fWeight				= 1.0f;
		nMaxHP				= 100;
		nMaxAP				= 0;
		nDC					= 5;
		szMeshName[0]		= 0;
		nWeaponDamage		= 5;
		fRotateSpeed		= 6.28318f;
		fCollRadius			= 35.0f;
		fCollHeight			= 180.0f;
		bColPick			= false;
		vScale				= MVector(1.0f,1.0f,1.0f);
		vColor				= MVector(0.6f,0.6f,0.6f);
		nSpawnWeight		= 100;
		nWeaponItemID		= 300000;		// 고블린 단검
		fTremble			= 30.0f;		// 기본은 캐릭터값이랑 동일
		
		nIntelligence		= 3;
		nAgility			= 3;
		fDyingTime			= 5.0f;

		fAttackRange		= 130.0f;
		fAttackRangeAngle	= 1.570796f;		// 90도
		fAttackHitRate		= 1.0f;			// 기본은 100% 명중 (원근 공격 NPC만 이 수치를 사용)
		fAttackCoolTime		= 0.0f;
		fCollisionCheckSpeed = 0.0f;


		fViewAngle			= 3.14159f;		// 180도
		fViewDistance		= 800.0f;
		fViewDistance360	= 800.0f;

		bNeverBlasted				= false;
		bMeleeWeaponNeverDamaged	= false;
		bRangeWeaponNeverDamaged	= false;
		bShadow						= true;
		bNeverPushed				= false;
		bNeverAttackCancel			= false;

		nSkills				= 0;

		nDropTableID		= 0;
		szDropTableName[0]	= 0;

		nOffenseType		= 1;
		bFriendly			= false;

		for (int i = 0; i < NPC_SOUND_END; i++) szSoundName[i][0] = 0;
	}

	MQuestNPCSpawnType GetSpawnType();		///< 스폰 타입 반환
};


#define NPC_INTELLIGENCE_STEPS		5
#define NPC_AGILITY_STEPS			5

struct MQuestNPCGlobalAIValue
{
	// shaking ratio
	float		m_fPathFinding_ShakingRatio;
	float		m_fAttack_ShakingRatio;
	float		m_fSpeed_ShakingRatio;

	// update time
	float		m_fPathFindingUpdateTime[NPC_INTELLIGENCE_STEPS];
	float		m_fAttackUpdateTime[NPC_AGILITY_STEPS];
};

/// NPC 정보 관리자 클래스
class MQuestNPCCatalogue : public map<MQUEST_NPC, MQuestNPCInfo*>
{
private:
	MQuestNPCGlobalAIValue			m_GlobalAIValue;

	// 함수
	void ParseNPC(MXmlElement& element);
	void Insert(MQuestNPCInfo* pNPCInfo);
	void ParseGlobalAIValue(MXmlElement& element);

	// 몬스터 도감.
	map< int, MQUEST_NPC > m_MonsterBibleCatalogue;

public :
	MQuestNPCInfo* GetIndexInfo( int nIndex );

public:
	MQuestNPCCatalogue();													///< 생성자
	~MQuestNPCCatalogue();													///< 소멸자

	bool ReadXml(const char* szFileName);									///< xml로부터 npc정보를 읽는다.
	bool ReadXml(MZFileSystem* pFileSystem,const char* szFileName);			///< xml로부터 npc정보를 읽는다.
	void Clear();															///< 초기화

	MQuestNPCInfo* GetInfo(MQUEST_NPC nNpc);								///< NPC 정보 반환
	MQuestNPCInfo* GetPageInfo( int nPage);									///< 페이지로부터 NPC 정보 반환

	MQuestNPCGlobalAIValue* GetGlobalAIValue() { return &m_GlobalAIValue; }
};

/////////////////////////////////////////////////////////////////////////////////////////////

/// NPC Set의 NPC정보
struct MNPCSetNPC
{
	MQUEST_NPC		nNPC;					///< NPC 정보
	int				nMinRate;				///< 등장할 최소 비율(퍼센트)
	int				nMaxRate;				///< 등장할 최대 비율
	int				nMaxSpawnCount;			///< 등장할 수 있는 최대 개수

	/// 생성자
	MNPCSetNPC()
	{
		nNPC = NPC_NONE;
		nMinRate = 0;
		nMaxRate = 0;
		nMaxSpawnCount = 0;
	}
};

/// NPC Set
struct MQuestNPCSetInfo
{
	int					nID;				///< ID
	char				szName[16];			///< 이름
	MQUEST_NPC			nBaseNPC;			///< 기본 베이스 NPC
	vector<MNPCSetNPC>	vecNPCs;			///< 등장할 NPC
};

/// NPC Set 정보 관리자 클래스
class MQuestNPCSetCatalogue : public map<int, MQuestNPCSetInfo*>
{
private:
	map<string, MQuestNPCSetInfo*>		m_NameMap;
	// 함수
	void Clear();
	void ParseNPCSet(MXmlElement& element);
	void Insert(MQuestNPCSetInfo* pNPCSetInfo);
public:
	MQuestNPCSetCatalogue();											///< 생성자
	~MQuestNPCSetCatalogue();											///< 소멸자

	bool ReadXml(const char* szFileName);								///< xml로부터 npc정보를 읽는다.
	bool ReadXml(MZFileSystem* pFileSystem,const char* szFileName);		///< xml로부터 npc정보를 읽는다.

	MQuestNPCSetInfo* GetInfo(int nID);									///< NPC Set 정보 반환
	MQuestNPCSetInfo* GetInfo(const char* szName);						///< NPC Set 정보 반환
};


#endif