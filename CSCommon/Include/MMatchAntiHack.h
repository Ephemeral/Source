#ifndef _MMATCH_ANTIHACK_H
#define _MMATCH_ANTIHACK_H


#include "MUID.h"

#include <list>
#include <string>
using namespace std;

class MMatchAntiHack
{
private:
	static list<unsigned int>	m_clientFileListCRC;
    static string Updates;

public:
	MMatchAntiHack()		{}
	~MMatchAntiHack()		{}

	static size_t			GetFielCRCSize();
	static string			GetUpdateString();
	static void				ClearClientFileList() { m_clientFileListCRC.clear(); }
	static void				InitClientFileList();
	static void				InitClientUpdateList();
	static bool				CheckClientFileListCRC(unsigned int crc, const MUID& uidUser );
};

#endif
