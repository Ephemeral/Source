#define	CODE_SECTION_SIZE		0x00263000
#define	MEMORY_CHECK_INTERVAL	5000		// Milliseconds

class MemoryCheck {
	public:
		unsigned long m_dwAddress;
		unsigned char *m_byChecksum;
		int m_nSize;

		MemoryCheck(DWORD dwAddress);
};

class ClientShield {
	private:
		static ClientShield *m_pInstance;
		unsigned long m_rgbyMemoryChecksum[CODE_SECTION_SIZE];
		unsigned long m_dwLastMemoryCheck;
		std::vector<class MemoryCheck *> m_pMemoryCheckList;

	public:
		enum HashType {
			HASHTYPE_MD5,
			HASHTYPE_SHA1,
			HASHTYPE_END
		};

		static ClientShield *getInstance();

		void initialize();
		void onUpdate();
		void reportToServer();
		std::string GetHash(const void *pData, int nSize, HashType nHashType);
};