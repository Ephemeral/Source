#include "stdafx.h"
#include "MMatchServer.h"
#include "MSharedCommandTable.h"
#include "MErrorTable.h"
#include "MBlobArray.h"
#include "MObject.h"
#include "MMatchObject.h"
#include "MMatchItem.h"
#include "MAgentObject.h"
#include "MMatchNotify.h"
#include "Msg.h"
#include "MMatchObjCache.h"
#include "MMatchStage.h"
#include "MMatchTransDataType.h"
#include "MMatchFormula.h"
#include "MMatchConfig.h"
#include "MCommandCommunicator.h"
#include "MMatchShop.h"
#include "MMatchTransDataType.h"
#include "MDebug.h"
#include "MMatchAuth.h"
#include "MMatchStatus.h"
#include "MAsyncDBJob.h"
#include "MMatchTransDataType.h"
#include "MMatchAntiHack.h"


void MMatchServer::OnAdminTerminal(const MUID& uidAdmin, const char* szText)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsHighAdmin(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

	char szOut[32768]; szOut[0] = 0;

	if (m_Admin.Execute(uidAdmin, szText))
	{
		MCommand* pNew = CreateCommand(MC_ADMIN_TERMINAL, MUID(0,0));
		pNew->AddParameter(new MCmdParamUID(MUID(0,0)));
		pNew->AddParameter(new MCmdParamStr(szOut));
		RouteToListener(pObj, pNew);
	}
}

void MMatchServer::OnAdminAnnounce(const MUID& uidAdmin, const char* szChat, unsigned long int nType)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{	
		return;
	}

	//Box for HighAdminOnly
	if (nType == 1) 
	{
		if (!IsHighAdmin(pObj))
		{
			return;
		}
	}

	char szMsg[256];
	char *szName = (pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminIncognito)) ? pObj->GetCharInfo()->m_szIncognitoName : pObj->GetCharInfo()->m_szName;
	sprintf(szMsg,"^1[!]%s : ^2%s",szName,szChat);
	//strcpy(szMsg, szChat);
	MCommand* pCmd = CreateCommand(MC_ADMIN_ANNOUNCE, MUID(0,0));
	pCmd->AddParameter(new MCmdParamUID(uidAdmin));
	pCmd->AddParameter(new MCmdParamStr(szMsg));
	pCmd->AddParameter(new MCmdParamUInt(nType));

	RouteToAllClient(pCmd);

	CTime theTime = CTime::GetCurrentTime();
	CString szTime = theTime.Format( "[%c] " );

	/*if(strstr(szMsg, "%"))
	{
		LOG(LOG_PROG, "Admin %s tried to crash the server\n", pObj->GetCharInfo()->m_szName);
	} 
	else*/
	{
		dglog("AdminLog//announcements.txt", "%s : %s(%s) announced (type: %s) : %s\n",szTime, pObj->GetCharInfo()->m_szName, pObj->GetIPString(), nType ? "Box" : "Msg", szMsg);
		LOG(LOG_PROG, "Admin %s announced (type: %s): %s\n", pObj->GetCharInfo()->m_szName, nType ? "Box" : "Msg", szMsg);
	}
}



void MMatchServer::OnAdminRequestServerInfo(const MUID& uidAdmin)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

	// 서버 정보 보여주는것 아직 안넣었음
/*
	MCommand* pNew = CreateCommand(MC_MATCH_ANNOUNCE, MUID(0,0));
	pNew->AddParameter(new MCmdParamUInt(0));

	RouteToListener(pObj, pNew);
*/
}
void MMatchServer::OnAdminServerHalt(const MUID& uidAdmin)
{
	LOG(LOG_PROG, "OnAdminServerHalt(...) Called");

	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	MMatchUserGradeID nGrade = pObj->GetAccountInfo()->m_nUGrade;

	// 관리자 권한을 가진 사람이 아니면 무시.
	if ((nGrade != MMUG_ADMIN && nGrade != MMUG_HCODER)) return;

	// Shutdown 시작	
	m_MatchShutdown.Start(GetGlobalClockCount());	
}

// 서버에서 메뉴로만 쓰는 명령어..
void MMatchServer::OnAdminServerHalt()
{
	LOG(LOG_PROG, "OnAdminServerHalt() Called");

	// Shutdown 시작	
	m_MatchShutdown.Start(GetGlobalClockCount());	
}

void MMatchServer::OnAdminRequestUpdateAccountUGrade(const MUID& uidAdmin, const char* szPlayer)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

	int nRet = MOK;

	if ((strlen(szPlayer)) < 2) return;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);
	if (pTargetObj == NULL) return;




	/*MCommand* pNew = CreateCommand(MC_ADMIN_REQUEST_UPDATE_ACCOUNT_UGRADE, MUID(0,0));
	pNew->AddParameter(new MCmdParamUInt(nRet));
	RouteToListener(pObj, pNew);*/

}

void MMatchServer::OnAdminPingToAll(const MUID& uidAdmin)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

	MCommand* pNew = CreateCommand(MC_NET_PING, MUID(0,0));
	pNew->AddParameter(new MCmdParamUInt(GetGlobalClockCount()));
	RouteToAllConnection(pNew);
}


void MMatchServer::OnAdminRequestSwitchLadderGame(const MUID& uidAdmin, const bool bEnabled)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (!IsEnabledObject(pObj)) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

	
	MGetServerConfig()->SetEnabledCreateLadderGame(bEnabled);


	char szMsg[256] = "설정되었습니다.";
	Announce(pObj, szMsg);
}

void MMatchServer::OnAdminHide(const MUID& uidAdmin)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (!IsEnabledObject(pObj)) return;

	// 관리자 권한을 가진 사람이 아니면 연결을 끊는다.
	if (!IsAdminGrade(pObj))
	{
//		DisconnectObject(uidAdmin);		
		return;
	}

#if defined(LOCALE_NHNUSA) || defined(_DEBUG)
	m_HackingChatList.Init();
	mlog( "reload hacking chat list.\n" );
#endif

	if (pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminHide)) {
		pObj->SetPlayerFlag(MTD_PlayerFlags_AdminHide, false);
		Announce(pObj, "Now Revealing...");
	} else {
		pObj->SetPlayerFlag(MTD_PlayerFlags_AdminHide, true);
		Announce(pObj, "Now Hiding...");
	}
}

void MMatchServer::OnAdminIncognito(const MUID& uidAdmin, char *pszNewName)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (!pObj || !IsEnabledObject(pObj)) return;

	if (!IsAdminGrade(pObj)) return;

	if (pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminIncognito)) {
		pObj->SetPlayerFlag(MTD_PlayerFlags_AdminIncognito, false);
		Announce(pObj, "Removing disguise...");
		MCommand* pCmd = CreateCommand(MC_ADMIN_RESPONSE_INCOGNITO, MUID(0,0));
		pCmd->AddParameter(new MCmdParamUInt(pObj->GetAccountInfo()->m_nUGrade));
		RouteToListener(pObj, pCmd);
	} else {
		pObj->SetPlayerFlag(MTD_PlayerFlags_AdminIncognito, true);
		if (IsHighAdmin(pObj))
			pObj->GetAccountInfo()->m_nIncognitoGrade = MMUG_HIDDENADMIN;
		else
			pObj->GetAccountInfo()->m_nIncognitoGrade = MMUG_HIDDENGM;

		bool bAlphaNum = true;

		if (ValidateMakingName(pszNewName, MIN_CHARNAME, MAX_CHARNAME) != MOK)
			bAlphaNum = false;

		for(SIZE_T nStr = 0; nStr < strlen(pszNewName); nStr++)
		{
			if(!isalnum(pszNewName[nStr]))
			{
				if(pszNewName[nStr] != '[' || pszNewName[nStr] != ']')
				{
					bAlphaNum = false;
					break;
				}
			}
		}
		if (!bAlphaNum && pObj->GetAccountInfo()->m_nUGrade != MMUG_HCODER)
		{
			Announce(pObj, "That name is invalid.");
			return;
		}

		strcpy(pObj->GetCharInfo()->m_szIncognitoName,pszNewName);
		char msg[256];
		sprintf(msg,"Going incognito as %s",pszNewName);
		Announce(pObj, msg);
		MCommand* pCmd = CreateCommand(MC_ADMIN_RESPONSE_INCOGNITO, MUID(0,0));
		pCmd->AddParameter(new MCmdParamUInt(pObj->GetAccountInfo()->m_nIncognitoGrade));
		RouteToListener(pObj, pCmd);
	}
}

void MMatchServer::OnAdminResetAllHackingBlock( const MUID& uidAdmin )
{
	MMatchObject* pObj = GetObject( uidAdmin );
	if( (0 != pObj) && IsAdminGrade(pObj) )
	{
		GetDBMgr()->AdminResetAllHackingBlock();
	}
}

void MMatchServer::OnAdminRequestKickPlayer(const MUID& uidAdmin, const char* szPlayer)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL)			return;
	if (!IsAdminGrade(pObj) || pObj->GetAccountInfo()->m_nUGrade == MMUG_DEVELOPER)	return;
	if ((strlen(szPlayer)) < 2) return;

	int nRet = MOK;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);
	if (pTargetObj != NULL) 
	{
		if (IsHighAdmin(pTargetObj) && !IsHighAdmin(pObj)) return;

		CTime theTime = CTime::GetCurrentTime();
		CString szTime = theTime.Format( "[%c] " );

		dglog("AdminLog//disconnection.txt", "%s : %s(%s) disconnected %s\n", szTime, pObj->GetCharInfo()->m_szName, pObj->GetIPString(), szPlayer);
		LOG(LOG_PROG, "Admin %s disconnected %s\n",pObj->GetCharInfo()->m_szName, szPlayer);	

		char szKickMessage[256];
		sprintf(szKickMessage,"^2You have been disconnected by ^1%s",pObj->GetName());
		MGetMatchServer()->AnnounceBox(pTargetObj,szKickMessage);

		pTargetObj->GetDisconnStatusInfo().SetStatus(MMDS_DISCONNECT);
	} else {
		nRet = MERR_ADMIN_NO_TARGET;
	}

	MCommand* pNew = CreateCommand(MC_ADMIN_RESPONSE_KICK_PLAYER, MUID(0,0));
	pNew->AddParameter(new MCmdParamInt(nRet));
	RouteToListener(pObj, pNew);
}
void MMatchServer::OnRequestChangeTeam(const MUID& Admin,PCHAR Player,LONG TeamId)
{
	MMatchObject* Sender = GetObject(Admin);
	MMatchObject* Target = GetPlayerByName(Player);
	MCommand* NewCommand = NULL;

	if(!Sender)
		return;

	if(!Target)
		return;

	if(TeamId < 0 || TeamId > 3)
		return;

	if(!IsAdminGrade(Sender))
		return;

	MUID TargetUID = Target->GetUID();

	if(!TargetUID.Low)
		return;

	NewCommand = CreateCommand(MC_MATCH_GAME_RESPONSE_CHANGE_TEAM,MUID(0,0));
	if(!NewCommand)
		return;

	NewCommand->AddParameter(new MCmdParamUID(TargetUID));
	NewCommand->AddParameter(new MCmdParamInt(TeamId));

	RouteToBattle(Sender->GetStageUID(),NewCommand);
}
void MMatchServer::OnRequestTeleportOtherPlayer(const MUID& Admin,const char* Player,float X,float Y,float Z)
{
	MMatchObject* GradeObject = GetObject(Admin);
	if(!GradeObject)
		return;

	if(!IsCoderOnly(GradeObject))
		return;

	MMatchObject* pTargetObj = GetPlayerByName(Player);
	if(!pTargetObj)
		return;
	MUID TargetUID = pTargetObj->GetUID();
	if(!TargetUID.Low)
		return;

	MCommand* pNew = CreateCommand(MC_MATCH_GAME_RESPONSE_TELEPORT,MUID(0,0));
	pNew->AddParameter(new MCmdParamUID(TargetUID));
	pNew->AddParameter(new MCmdParamFloat(X));
	pNew->AddParameter(new MCmdParamFloat(Y));
	pNew->AddParameter(new MCmdParamFloat(Z));

	RouteToBattle(GradeObject->GetStageUID(),pNew);

}
void MMatchServer::OnAdminRequestMutePlayer(const MUID& uidAdmin, const char* szPlayer, const int nPenaltyHour)
{
	return; //disabled

	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL)			return;	
	if (!IsAdminGrade(pObj))	return;
	if ((strlen(szPlayer)) < 2) return;

	int nRet = MOK;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);	
	if (pTargetObj != NULL) 
	{
		pTargetObj->GetAccountPenaltyInfo()->SetPenaltyInfo(MPC_CHAT_BLOCK, nPenaltyHour);
		
		const MPenaltyInfo* pPenaltyInfo = pTargetObj->GetAccountPenaltyInfo()->GetPenaltyInfo(MPC_CHAT_BLOCK);
		if( m_MatchDBMgr.InsertAccountPenaltyInfo(pTargetObj->GetAccountInfo()->m_nAID
			, pPenaltyInfo->nPenaltyCode, nPenaltyHour, pObj->GetAccountName()) == false ) 
		{
			pTargetObj->GetAccountPenaltyInfo()->ClearPenaltyInfo(MPC_CHAT_BLOCK);
			nRet = MERR_ADMIN_CANNOT_PENALTY_ON_DB;
		}
	} 
	else 
	{
		nRet = MERR_ADMIN_NO_TARGET;
	}

	MCommand* pNew = CreateCommand(MC_ADMIN_RESPONSE_MUTE_PLAYER, MUID(0,0));
	pNew->AddParameter(new MCmdParamInt(nRet));
	
	if( nRet == MOK ) {
		RouteToListener(pTargetObj, pNew->Clone());
	}

	RouteToListener(pObj, pNew);
}

void MMatchServer::OnAdminRequestBlockPlayer(const MUID& uidAdmin, const char* szPlayer, const int nPenaltyHour)
{
	return; //disabled

	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL)			return;	
	if (!IsAdminGrade(pObj))	return;
	if ((strlen(szPlayer)) < 2) return;

	int nRet = MOK;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);	
	if (pTargetObj != NULL) 
	{
		pTargetObj->GetAccountPenaltyInfo()->SetPenaltyInfo(MPC_CONNECT_BLOCK, nPenaltyHour);

		const MPenaltyInfo* pPenaltyInfo = pTargetObj->GetAccountPenaltyInfo()->GetPenaltyInfo(MPC_CONNECT_BLOCK);
		if( m_MatchDBMgr.InsertAccountPenaltyInfo(pTargetObj->GetAccountInfo()->m_nAID
			, pPenaltyInfo->nPenaltyCode, nPenaltyHour, pObj->GetAccountName()) == false ) 
		{
			pTargetObj->GetAccountPenaltyInfo()->ClearPenaltyInfo(MPC_CONNECT_BLOCK);
			nRet = MERR_ADMIN_CANNOT_PENALTY_ON_DB;
		}
	} 
	else 
	{
		nRet = MERR_ADMIN_NO_TARGET;
	}

	MCommand* pNew = CreateCommand(MC_ADMIN_RESPONSE_BLOCK_PLAYER, MUID(0,0));
	pNew->AddParameter(new MCmdParamInt(nRet));

	if( nRet == MOK ) {
		Disconnect(pTargetObj->GetUID());
	}

	RouteToListener(pObj, pNew);
}

void MMatchServer::OnAdminRequestEventBanPlayer(const MUID& uidAdmin, const char* szPlayer, const int nPenaltyHour)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL)			return;	
	if (!IsDGAdminGrade(pObj))	return;
	if ((strlen(szPlayer)) < 2) return;

	int nRet = MOK;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);	
	if (pTargetObj != NULL && pTargetObj->GetAccountInfo()->m_nUGrade != MMUG_HCODER) 
	{
		if( m_MatchDBMgr.SetPGrade(pTargetObj->GetAccountInfo()->m_nAID,MMPG_EVENTBAN))
		{
			nRet = MERR_ADMIN_CANNOT_PENALTY_ON_DB;
		}
	} 
	else 
	{
		nRet = MERR_ADMIN_NO_TARGET;
	}

	if( nRet == MOK ) {
		char szKickMessage[256];
		sprintf(szKickMessage,"^2You have been banned from events by ^1%s",pObj->GetName());
		MGetMatchServer()->AnnounceBox(pTargetObj,szKickMessage);
		MGetMatchServer()->Announce(pObj,"Player has been banned events");
		Disconnect(pTargetObj->GetUID());
	}
}

void MMatchServer::OnAdminRequestEventUnbanPlayer(const MUID& uidAdmin, const char* szPlayer, const int nPenaltyHour)
{
	MMatchObject* pObj = GetObject(uidAdmin);
	if (pObj == NULL)			return;	
	if (!IsDGAdminGrade(pObj))	return;
	if ((strlen(szPlayer)) < 2) return;

	int nRet = MOK;
	MMatchObject* pTargetObj = GetPlayerByName(szPlayer);	
	if (pTargetObj != NULL && pTargetObj->GetAccountInfo()->m_nPGrade == MMPG_EVENTBAN) 
	{
		if( m_MatchDBMgr.SetPGrade(pTargetObj->GetAccountInfo()->m_nAID,MMPG_FREE))
		{
			nRet = MERR_ADMIN_CANNOT_PENALTY_ON_DB;
		}
	} 
	else 
	{
		nRet = MERR_ADMIN_NO_TARGET;
	}

	if( nRet == MOK ) {
		char szKickMessage[256];
		sprintf(szKickMessage,"^2You have been unbanned from events by ^1%s",pObj->GetName());
		MGetMatchServer()->AnnounceBox(pTargetObj,szKickMessage);
		MGetMatchServer()->Announce(pObj,"Player has been unbanned events");
		Disconnect(pTargetObj->GetUID());
	}
}