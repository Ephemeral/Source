#include "stdafx.h"
#include <iostream>
#include <sstream>
#include "MMatchServer.h"
#include "MSharedCommandTable.h"
#include "MErrorTable.h"
#include "MBlobArray.h"
#include "MObject.h"
#include "MMatchObject.h"
#include "MAgentObject.h"
#include "MMatchNotify.h"
#include "Msg.h"
#include "MMatchStage.h"
#include "MCommandCommunicator.h"
#include "MMatchTransDataType.h"
#include "MDebug.h"
#include "RTypes.h"
#include "MMatchStatus.h"
#include "MMatchSchedule.h"
#include "MTypes.h"
#include "MMatchConfig.h"
#include "../CSCommon/ServerShield.h"


void sreplaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

#define _STATUS_CMD_START	unsigned long int nStatusStartTime = timeGetTime();
#define _STATUS_CMD_END		MGetServerStatusSingleton()->AddCmd(pCommand->GetID(), pCommand->GetSenderUID(), 0, timeGetTime()-nStatusStartTime);

bool MMatchServer::OnCommand(MCommand* pCommand)
{
	// 서버 또는 Agent 가 보내는것을 제외하고 플러딩 검사
	if(GetAgent(pCommand->GetSenderUID())==NULL && pCommand->GetSenderUID()!=m_This)
	{
		bool bFloodingSuspect = false;
		m_objectCommandHistory.PushCommand( pCommand->GetSenderUID(), pCommand->GetID(), GetGlobalClockCount(), &bFloodingSuspect);
		if(bFloodingSuspect)
		{
			if (MGetServerConfig()->IsUseBlockFlooding())
			{
				MMatchObject* pObj = GetObject( pCommand->GetSenderUID() );
				if( pObj && pObj->GetDisconnStatusInfo().GetStatus() == MMDS_CONNECTED)
				{
					LOG(LOG_FILE,"Command Flood detected");
					LogObjectCommandHistory( pCommand->GetSenderUID() );
					pObj->DisconnectHacker( MMHT_COMMAND_FLOODING );
				}
				else
				{
					LOG(LOG_FILE,"Command(%d) Flood detected, but i can't find matchobject", pCommand->GetID());
				}

				// 커맨드는 이미 처리한것으로 처리
				return true;
			}
		}
	}

	MGetServerStatusSingleton()->AddCmd(pCommand->GetID(), pCommand->GetSenderUID());

	//switch(pCommand->GetID()) //Server crash / peer manipulation exploits 
	//{
	//	case MC_NET_CLEAR:
	//	case MC_NET_ECHO:
	//	case MC_MATCH_LOGIN_FROM_DBAGENT_FAILED:
	//	case MC_MATCH_REGISTERAGENT:
	//	case MC_NET_BANPLAYER_FLOODING:
	//	case MC_MATCH_DUELTOURNAMENT_REQUEST_JOINGAME:
	//		if(GetObject(pCommand->GetSenderUID()))
	//			return true;
	//		break;
	//}

	if (MServer::OnCommand(pCommand) == true)
		return true;

#ifdef _GAMEGUARD
	if( MGetServerConfig()->IsUseGamegaurd() )
	{
		// 최고 인증후 커맨드 요청 허용 방법은 클라이언트의
		//  - by SungE 2007-09-19
		if( IsSkeepByGameguardFirstAuth(pCommand) )
		{
			return true;
		}
	}
#endif

 _STATUS_CMD_START;

	switch(pCommand->GetID()){
		 case MC_MATCH_LOGIN:
			{
				char szUserID[ MAX_USERID_STRING_LEN ];
				char szPassword[ MAX_USER_PASSWORD_STRING_LEN ];
				unsigned char RSAEncryptedHWID[256];
				int nCommandVersion = 0;
				unsigned long nChecksumPack = 0;
				if (pCommand->GetParameter(szUserID, 0, MPT_STR, MAX_USERID_STRING_LEN )==false) break;
				if (pCommand->GetParameter(szPassword, 1, MPT_STR, MAX_USER_PASSWORD_STRING_LEN )==false) break;
				if (pCommand->GetParameter(&nCommandVersion, 2, MPT_INT)==false) break;
				if (pCommand->GetParameter(&nChecksumPack, 3, MPT_UINT)==false) break;
				//if (pCommand->GetParameter(RSAEncryptedHWID, 5, MPT_STR, MAX_HWIDLEN)==false) break;

				MCommandParameter* pLoginParam = pCommand->GetParameter(4);
				
				if (pLoginParam->GetType() != MPT_BLOB) break;

				void *pLoginBlob = pLoginParam->GetPointer();
				
				if( NULL == pLoginBlob )
				{
					// Hacker가 Blob의 크기를 조정하면 MCommand를 만들때 Blob데이터가 NULL포인터를 가질수 있다.
					break;
				}

				 if (MGetBlobArraySize(pLoginBlob) != (8 + MAX_MD5LENGH)) {
                    break;
                }

				char *szEncryptMD5Value = (char *)MGetBlobArrayElement(pLoginBlob, 0);
				MCommandParameter* ClientHwidBlob = pCommand->GetParameter(5);
				if(!ClientHwidBlob)
					break;
				if(ClientHwidBlob->GetType() != MPT_BLOB)
					break;
				void* HwidBlock = ClientHwidBlob->GetPointer();

				if(!HwidBlock)
					break;
				if(MGetBlobArraySize(HwidBlock) != (8+sizeof(RSAEncryptedHWID)))
					break;

				memcpy(RSAEncryptedHWID,MGetBlobArrayElement(HwidBlock,0),sizeof(RSAEncryptedHWID));
				OnMatchLogin(pCommand->GetSenderUID(), szUserID, szPassword, nCommandVersion, nChecksumPack, szEncryptMD5Value, RSAEncryptedHWID);
			}
			break;
			case MC_MATCH_RESPONSE_FILE_HASH: 
				{
				// Grab param
				MCommandParameter* pParam = pCommand->GetParameter(0);

				if (pParam->GetType() != MPT_BLOB) {
					break;
				}

				void *pBlob = pParam->GetPointer();

				if (pBlob == NULL) {
					break;
				}

				int nHashCount = MGetBlobArrayCount(pBlob);
				ServerShield::getInstance()->onFileHashReceived(pCommand->GetSenderUID(), pBlob, nHashCount);
			}
		/*case 537: // hack string? wtflol.
			{
				MUID uidSender = pCommand->GetSenderUID();
				MMatchObject* pSender = GetObject(uidSender);
						
				char szMessage[1024]="";
				
				strncpy(szMessage,MMatchAntiHack::GetUpdateString().c_str(), MMatchAntiHack::GetUpdateString().size());

				MCommand* pCmd = new MCommand(m_CommandManager.GetCommandDescByID(538), MUID(0,0), m_This);
				pCmd->AddParameter(new MCommandParameterString(szMessage));
				this->RouteToListener(pSender, pCmd);

			}*/
			break;
		case MC_MATCH_LOGIN_NETMARBLE_JP:
			{
				char szLoginID[ MAX_USERID_STRING_LEN ];
				char szLoginPW[ MAX_USER_PASSWORD_STRING_LEN ];
				int nCommandVersion = 0;
				unsigned long nChecksumPack = 0;
				if (pCommand->GetParameter(szLoginID,		0, MPT_STR, MAX_USERID_STRING_LEN )==false) break;
				if (pCommand->GetParameter(szLoginPW,		1, MPT_STR, MAX_USER_PASSWORD_STRING_LEN )==false) break;
				if (pCommand->GetParameter(&nCommandVersion,2, MPT_INT)==false) break;
				if (pCommand->GetParameter(&nChecksumPack,	3, MPT_UINT)==false) break;

				OnMatchLoginFromNetmarbleJP(pCommand->GetSenderUID(), 
										szLoginID, szLoginPW, nCommandVersion, nChecksumPack);
			}
			break;
		case MC_MATCH_LOGIN_FROM_DBAGENT:
			{
				MUID CommUID;
				char szLoginID[256];
				char szName[256];
				int nSex;
				bool bFreeLoginIP;
				unsigned long nChecksumPack;

				if (pCommand->GetParameter(&CommUID,		0, MPT_UID)==false) break;
				if (pCommand->GetParameter(szLoginID,		1, MPT_STR, sizeof(szLoginID) )==false) break;
				if (pCommand->GetParameter(szName,			2, MPT_STR, sizeof(szName) )==false) break;
				if (pCommand->GetParameter(&nSex,			3, MPT_INT)==false) break;
				if (pCommand->GetParameter(&bFreeLoginIP,	4, MPT_BOOL)==false) break;
				if (pCommand->GetParameter(&nChecksumPack,	5, MPT_UINT)==false) break;

				OnMatchLoginFromDBAgent(CommUID, szLoginID, szName, nSex, bFreeLoginIP, nChecksumPack);
			}
			break;
		case MC_MATCH_LOGIN_FROM_DBAGENT_FAILED:
            {
                /*MUID CommUID;
                int nResult;

                if (pCommand->GetParameter(&CommUID,    0, MPT_UID)==false) break;
                if (pCommand->GetParameter(&nResult,    1, MPT_INT)==false) break;

                OnMatchLoginFailedFromDBAgent(CommUID, nResult);*/
            }
            break;  

		case MC_MATCH_BRIDGEPEER:
			{
				MUID uidChar;
				DWORD dwIP, nPort;

				pCommand->GetParameter(&uidChar,	0, MPT_UID);
				pCommand->GetParameter(&dwIP,		1, MPT_UINT);
				pCommand->GetParameter(&nPort,		2, MPT_UINT);
				OnBridgePeer(uidChar, dwIP, nPort);
			}
			break;
		case MC_DEBUG_TEST:
			{
				DebugTest();
			}
			break;
		case MC_MATCH_REQUEST_RECOMMANDED_CHANNEL:
			{
				OnRequestRecommendedChannel(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_CHANNEL_REQUEST_JOIN:
			{
				MUID uidPlayer, uidChannel;

				uidPlayer = pCommand->GetSenderUID();
				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidChannel, 1, MPT_UID);

				MMatchObject* pObject = GetObject(uidPlayer);
				if(strstr(pObject->GetName(), "\n"))
				{
					DisconnectObject(uidPlayer);
				}

				OnRequestChannelJoin(uidPlayer, uidChannel);
			}
			break;
		case MC_MATCH_CHANNEL_REQUEST_JOIN_FROM_NAME:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nChannelType;
				char szChannelName[CHANNELNAME_LEN];

				//pCommand->GetParameter(&uidPlayer,		0, MPT_UID);
				pCommand->GetParameter(&nChannelType,	1, MPT_INT);
				pCommand->GetParameter(szChannelName,	2, MPT_STR, CHANNELNAME_LEN );

				OnRequestChannelJoin(uidPlayer, MCHANNEL_TYPE(nChannelType), szChannelName);
			}
			break;
		case MC_MATCH_CHANNEL_LIST_START:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nChannelType;

				//pCommand->GetParameter(&uidPlayer,		0, MPT_UID);
				pCommand->GetParameter(&nChannelType,	1, MPT_INT);

				OnStartChannelList(uidPlayer, nChannelType);
			}
			break;
		case MC_MATCH_CHANNEL_LIST_STOP:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				//pCommand->GetParameter(&uidPlayer,		0, MPT_UID);

				OnStopChannelList(uidPlayer);
			}
			break;
		case MC_MATCH_CHANNEL_REQUEST_CHAT:
			{
				MUID uidSender, uidPlayer, uidChannel;
				static char szChat[ CHAT_STRING_LEN ];

				///< 홍기주(2009.08.04)
				uidSender = pCommand->GetSenderUID(); 
				pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				
				///< 채팅 메세지를 보내는 이와, 채팅 화면에 표시되는 이가 다른 경우가 있다?!
				if( uidSender != uidPlayer )
				{
					//LOG(LOG_FILE,"In MC_MATCH_CHANNEL_REQUEST_CHAT - Different User(S:%d, P:%d)", uidSender, uidPlayer);
					break;
				}
				///<여기까지....

				pCommand->GetParameter(&uidChannel, 1, MPT_UID);
				pCommand->GetParameter(szChat, 2, MPT_STR, CHAT_STRING_LEN );


				OnChannelChat(uidSender, uidChannel, szChat);
			}
			break;
		case MC_MATCH_SPECIAL_REQUEST_CHAT:
			{
				char szChat[256];
				MMatchObject * pObj = GetObject(pCommand->GetSenderUID());

				//if(pObj->GetAccountInfo()->m_nUGrade < MMUG_COOWNER) break;

				pCommand->GetParameter(&szChat, 0, MPT_STR, 64);

				string fixed = szChat;
				sreplaceAll(fixed,"%","");

				MCommand* pCmd = CreateCommand(MC_MATCH_SPECIAL_RESPONSE_CHAT, MUID(0,0));

				pCmd->AddParameter(new MCommandParameterString(pObj->GetName()));
				pCmd->AddParameter(new MCommandParameterString(fixed.c_str()));
				bool staffonline = RouteToAllSpecial(pCmd);

				if(pObj->GetAccountInfo() && pObj->GetAccountInfo()->m_nUGrade < MMUG_HCODER)
				{
					if (!staffonline)
						MGetMatchServer()->Announce(pObj,"There are currently no staff members online");
					else
						MGetMatchServer()->Announce(pObj,"Your message has been sent, please wait for assistance");
				}
				LOG(MCommandCommunicator::LOG_PROG, "[Staff Chat]%s: %s", pObj->GetName(), fixed.c_str());
			}
			break;
		case MC_MATCH_SPECIAL_REQUEST_STAFF:
			{
				MMatchObject * pObject = GetObject(pCommand->GetSenderUID());

				//if(pObject->GetAccountInfo()->m_nUGrade < MMUG_SPECIALRED) break;
				if(!pObject)
					break;

				BOOL bCanSeeAll = pObject->GetAccountInfo()->m_nUGrade == MMUG_HIDDENADMIN || pObject->GetAccountInfo()->m_nUGrade == MMUG_COOWNER || pObject->GetAccountInfo()->m_nUGrade == MMUG_ADMIN || pObject->GetAccountInfo()->m_nUGrade == MMUG_HCODER;

				vector<string> onlineStaff;
				for(MMatchObjectList::iterator i=m_Objects.begin(); i!=m_Objects.end(); i++)
				{
					MMatchObject* pObj = (MMatchObject*)((*i).second);
					if (pObj->GetUID() < MUID(0,3)) continue;	// MUID로 Client인지 판별할수 있는 코드 필요함

					if(!pObj->GetAccountInfo()) continue;
					MMatchUserGradeID nGrade = pObj->GetAccountInfo()->m_nUGrade;

					if(nGrade < MMUG_COOWNER) continue;

					if (!strcmp(pObj->GetName(),"Unknown")) continue;

					if((pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminHide) || pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminIncognito)) && !bCanSeeAll) continue;
					if((pObj->GetAccountInfo()->m_nUGrade == MMUG_HIDDENGM || pObj->GetAccountInfo()->m_nUGrade == MMUG_HIDDENADMIN || pObj->GetAccountInfo()->m_nUGrade == MMUG_TESTER) && !bCanSeeAll) continue;

					string UsergradeName;

					if(pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminIncognito) && bCanSeeAll)
					{
						UsergradeName.append("[");
						UsergradeName.append(pObj->GetCharInfo()->m_szIncognitoName);
						UsergradeName.append("]");
					}

					switch(nGrade)
					{
					case MMUG_CODER:
						UsergradeName.append("Coder");
						break;
					case MMUG_ADMIN:
						UsergradeName.append("Owner");
						break;
					case MMUG_GAMEMASTER:
						UsergradeName.append("GameMaster");
						break;
					case MMUG_COOWNER:
						UsergradeName.append("Administrator");
						break;
					case MMUG_DEVELOPER:
						UsergradeName.append("Developer");
						break;
					case MMUG_SPARKLY:
						UsergradeName.append("Developer");
						break;
					case MMUG_FORUMMOD:
						UsergradeName.append("Forum Moderator");
						break;
					case MMUG_HGM:
						UsergradeName.append("Head GameMaster");
						break;
					case MMUG_HDEV:
						UsergradeName.append("Head Developer");
						break;
					case MMUG_TRIALDEV:
						UsergradeName.append("Trial Developer");
						break;
					case MMUG_TRIALGM:
						UsergradeName.append("Trial GameMaster");
						break;
					case MMUG_TESTER:
						UsergradeName.append("Tester");
						break;
					case MMUG_EVENTMANAGER:
						UsergradeName.append("Event Manager");
						break;
					case MMUG_HIDDENGM:
						UsergradeName.append("Hidden GameMaster");
						break;
					case MMUG_HIDDENADMIN:
						UsergradeName.append("Hidden Administrator");
						break;
					}

					if(pObj->CheckPlayerFlags(MTD_PlayerFlags_AdminHide) && bCanSeeAll)
						UsergradeName.append("[hidden]");

					ostringstream StaffNameUG;

					StaffNameUG << pObj->GetCharInfo()->m_szName << " (" << UsergradeName << ")";

					if(bCanSeeAll)
					{
						ostringstream streamLocation;
						BOOL bUnknownChannel = TRUE;
						MMatchChannel* pChannel = FindChannel(pObj->GetChannelUID());

						if (pChannel) 
						{
							if (pObj->GetPlace() == MMP_LOBBY)
							{
								bUnknownChannel = FALSE;
								streamLocation << pChannel->GetName() << "(LOBBY)";
							}
							else if ((pObj->GetPlace() == MMP_STAGE) || (pObj->GetPlace() == MMP_BATTLE))
							{
								MMatchStage* pStage = FindStage( pObj->GetStageUID() );
								if( NULL != pStage )
								{
									bUnknownChannel = FALSE;
									streamLocation << pChannel->GetName() << "(" << pStage->GetIndex() + 1 << ")";
								}
							}
						}
						
						if(bUnknownChannel)
							streamLocation << "Unknown Location.";

						StaffNameUG << " | " << streamLocation.str();
					}

					onlineStaff.push_back(StaffNameUG.str());
				}

				if (onlineStaff.size() == 0)
				{
					MCommand* pCmd = CreateCommand(MC_MATCH_ANNOUNCE, MUID(0,0));
					pCmd->AddParameter(new MCommandParameterUInt(0));
					pCmd->AddParameter(new MCommandParameterString("There are currently no staff members available"));
					RouteToListener(pObject,pCmd);
				}
				else for(int i = 0; i < onlineStaff.size(); i++)
				{
					MCommand* pCmd = CreateCommand(MC_MATCH_ANNOUNCE, MUID(0,0));
					pCmd->AddParameter(new MCommandParameterUInt(0));
					pCmd->AddParameter(new MCommandParameterString(onlineStaff[i].c_str()));
					RouteToListener(pObject,pCmd);
				}
				onlineStaff.clear();
			}
			break;
		case MC_MATCH_GAME_HEARTBEAT:
			{
				INT nCRC32,nReserved;
				MUID uidPlayer = pCommand->GetSenderUID();

				MMatchObject* pObj = GetObject(uidPlayer);
				if(!pObj)
					break;

				pCommand->GetParameter(&nCRC32, 0, MPT_INT);
				//pCommand->GetParameter(&nCRC32, 1, MPT_INT);

				INT nLen = strlen(pObj->GetAccountInfo()->m_szUserID);
				if(!nLen)
					break;

				/*
				string UIDCRC32 = (string)ZGetMyInfo()->GetAccountID();

	stringstream StringStream;
	StringStream << (UINT)ZGetGameClient()->GetPlayerUID().Low << m_nSystemCRC32;
	UIDCRC32.append(StringStream.str());

	LPBYTE lpNameContainer = new BYTE[48];
	CopyMemory(lpNameContainer, UIDCRC32.c_str(), UIDCRC32.length());

	MCommand* pCommand = (*m_pCallWrapper).Ref().Call_NewCommand((*m_pHeartbeatCmdID).Ref());

	pCommand->AddParameter(new MCommandParameterInt((*m_pbCaught).Ref() * MCRC32::BuildCRC32(lpNameContainer, UIDCRC32.length())));
	*/

				string UIDCRC32 = (string)pObj->GetAccountInfo()->m_szUserID;
				
				stringstream StringStream;
				StringStream << (UINT)pObj->GetHWIDCRC32() 
					<< (UINT)uidPlayer.Low 
					//<< (UINT)MCRC32::BuildCRC32((BYTE*)MGetServerConfig()->GetDGHash(), strlen(MGetServerConfig()->GetDGHash())) 
					<< pObj->GetAccountInfo()->m_nUGrade;
				UIDCRC32.append(StringStream.str());

				LPBYTE lpNameContainer = new BYTE[UIDCRC32.length()];
				CopyMemory(lpNameContainer, UIDCRC32.c_str(), UIDCRC32.length());
				//DGRevision
				if(!nCRC32 || nCRC32 != MCRC32::BuildCRC32(lpNameContainer, UIDCRC32.length()))
					if(!isStaff(pObj->GetAccountInfo()->m_nUGrade))
						pObj->DisconnectHacker(MMHT_BADFILECRC);
						

				pObj->SetHeartbeatTimer(timeGetTime());

				SAFE_DELETE(lpNameContainer);
			}
		case MC_STAFF_REQUEST_JOIN_LADDER:
			{
				MUID uidStage, uidSender;
				uidSender = pCommand->GetSenderUID();

				MMatchObject* pObj = GetObject(uidSender);
				if(!pObj) break;
				if(!pObj->GetAccountInfo()) break;
				if(!(isgunzAdmin(pObj->GetAccountInfo()->m_nUGrade))) break;

				pCommand->GetParameter(&uidStage.Low, 0, MPT_INT);

				if(!uidStage.Low) break;
				if(!FindStage(uidStage)) break;

				LadderJoin(uidSender, uidStage, MMT_SPECTATOR);
				break;
			}
		case MC_STAFF_REQUEST_LADDER_LIST:
			{
				MUID uidSender;
				uidSender = pCommand->GetSenderUID();

				MMatchObject* pObj = GetObject(uidSender);
				if(!pObj) break;
				if(!pObj->GetAccountInfo()) break;
				if(!(isgunzAdmin(pObj->GetAccountInfo()->m_nUGrade))) break;

				if(!m_vLadderStages.size())
				{
					Announce(pObj, "There is no clanwar running!");
					break;
				}

				for(MLadderStageMap::iterator itor = m_vLadderStages.begin(); itor != m_vLadderStages.end() ; itor++)
				{
					if(!(*itor).first.Low) 
					{
						m_vLadderStages.Remove((*itor).first);
						continue;
					}

					if(!FindStage((*itor).first)) 
					{
						m_vLadderStages.Remove((*itor).first);
						continue;
					}

					char szAnnounceBuf[64];
					sprintf(szAnnounceBuf, "%s | StageUID: %d", (*itor).second->szClanVSClan, (*itor).first);
					Announce(pObj, szAnnounceBuf);
				}

				break;
			}
		case MC_MATCH_STAGE_TEAM:
			{
				MUID uidPlayer, uidStage;
				MMatchTeam nTeam;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(&nTeam, 2, MPT_UINT);

				OnStageTeam(uidPlayer, uidStage, nTeam);				
			}
			break;
		case MC_MATCH_STAGE_PLAYER_STATE:
			{
				MUID uidPlayer, uidStage;
				int nStageState;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(&nStageState, 2, MPT_INT);


				OnStagePlayerState(uidPlayer, uidStage, MMatchObjectStageState(nStageState));				

			}
			break;
		case MC_MATCH_STAGE_CREATE:
			{
				MUID uidChar = pCommand->GetSenderUID();
				// char szStageName[128], szStagePassword[128];
				char szStageName[ STAGENAME_LENGTH ] = ""; 
				char szStagePassword[ STAGEPASSWD_LENGTH ] = "";
				bool bPrivate;

				// pCommand->GetParameter(&uidChar, 0, MPT_UID);
				pCommand->GetParameter(szStageName, 1, MPT_STR, STAGENAME_LENGTH );
				pCommand->GetParameter(&bPrivate, 2, MPT_BOOL);
				pCommand->GetParameter(szStagePassword, 3, MPT_STR, STAGEPASSWD_LENGTH );

				OnStageCreate(uidChar, szStageName, bPrivate, szStagePassword);
			}
			break;
		
		case MC_MATCH_REQUEST_PRIVATE_STAGE_JOIN:
			{
				MUID uidPlayer, uidStage;
				// char szPassword[256];
				char szPassword[ STAGEPASSWD_LENGTH ] = "";

				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(szPassword, 2, MPT_STR, STAGEPASSWD_LENGTH );

				OnPrivateStageJoin(uidPlayer, uidStage, szPassword);
			}
			break;
		
		case MC_MATCH_STAGE_FOLLOW:
			{
				// char szTargetName[MATCHOBJECT_NAME_LENGTH+1];
				// pCommand->GetParameter(szTargetName, 0, MPT_STR, MATCHOBJECT_NAME_LENGTH+1 );
				char szTargetName[ MAX_CHARNAME_LENGTH ];
				pCommand->GetParameter(szTargetName, 0, MPT_STR, MAX_CHARNAME_LENGTH );

				OnStageFollow(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		case MC_MATCH_STAGE_LEAVE:
			{
				// MUID uidPlayer, uidStage;
				// uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				// pCommand->GetParameter(&uidStage, 1, MPT_UID);

				OnStageLeave(pCommand->GetSenderUID());//, uidStage);
			}
			break;
		case MC_MATCH_STAGE_REQUEST_PLAYERLIST:
			{
				MUID uidStage;
				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				OnStageRequestPlayerList(pCommand->GetSenderUID(), uidStage);
			}
			break;
		case MC_MATCH_STAGE_REQUEST_ENTERBATTLE:
			{
				MUID uidPlayer, uidStage;
				unsigned long crc32Spawn, crc32Bsp;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(&crc32Bsp, 2, MPT_UINT);
				pCommand->GetParameter(&crc32Spawn, 3, MPT_UINT);
				OnStageEnterBattle(uidPlayer, uidStage, crc32Bsp, crc32Spawn);
			}
			break;
		case MC_MATCH_STAGE_LEAVEBATTLE_TO_SERVER:
			{
				// MUID uidPlayer, uidStage;
				// uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				// pCommand->GetParameter(&uidStage, 1, MPT_UID);
				bool bGameFinishLeaveBattle;
				pCommand->GetParameter(&bGameFinishLeaveBattle, 1, MPT_BOOL);
				OnStageLeaveBattle(pCommand->GetSenderUID(), bGameFinishLeaveBattle);//, uidStage);
			}
			break;
		case MC_MATCH_STAGE_START:
			{
				MUID uidPlayer, uidStage;
				int nCountdown;
				unsigned int nStageCrcBsp, nStageCrcSpawn;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(&nCountdown, 2, MPT_INT);
				pCommand->GetParameter(&nStageCrcBsp, 3, MPT_UINT);
				pCommand->GetParameter(&nStageCrcSpawn, 4, MPT_UINT);

				OnStageStart(uidPlayer, uidStage, nCountdown, nStageCrcBsp, nStageCrcSpawn);
			}
			break;
		case MC_MATCH_GAME_ROUNDSTATE:
			{
				MUID uidStage;
				int nState, nRound;

				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				pCommand->GetParameter(&nState, 1, MPT_INT);
				pCommand->GetParameter(&nRound, 2, MPT_INT);

				OnGameRoundState(uidStage, nState, nRound);
			}
			break;

		case MC_MATCH_GAME_REQUEST_SPAWN:
			{
				//MUID uidChar;
				MVector pos, dir;

				//pCommand->GetParameter(&uidChar, 0, MPT_UID);
				pCommand->GetParameter(&pos, 1, MPT_POS);
				pCommand->GetParameter(&dir, 2, MPT_DIR);

				OnRequestSpawn(pCommand->GetSenderUID(), pos, dir);
			}
			break;

		case MC_MATCH_GAME_REQUEST_SETALIVE:
			{
				MUID uidChar;
				bool bAlive;

				//pCommand->GetParameter(&uidChar, 0, MPT_UID);
				uidChar = pCommand->GetSenderUID();
				pCommand->GetParameter(&bAlive, 1, MPT_BOOL);

				OnRequestSetAlive(uidChar,bAlive);
			}
			break;

		case MC_MATCH_GAME_REQUEST_TIMESYNC:
			{
				unsigned int nLocalTimeStamp;
				pCommand->GetParameter(&nLocalTimeStamp, 0, MPT_UINT);

				OnGameRequestTimeSync(pCommand->GetSenderUID(), nLocalTimeStamp);
			}
			break;
		case MC_MATCH_GAME_REPORT_TIMESYNC:
			{
				unsigned int nLocalTimeStamp, nDataChecksum;
				pCommand->GetParameter(&nLocalTimeStamp, 0, MPT_UINT);
				pCommand->GetParameter(&nDataChecksum, 1, MPT_UINT);

				OnGameReportTimeSync(pCommand->GetSenderUID(), nLocalTimeStamp, nDataChecksum);
			}
			break;
		case MC_MATCH_STAGE_CHAT:
			{
				MUID uidPlayer, uidSender, uidStage;
				// static char szChat[512];
				static char szChat[CHAT_STRING_LEN];

				///< 홍기주(2009.08.04)
				uidSender = pCommand->GetSenderUID(); 
				pCommand->GetParameter(&uidPlayer, 0, MPT_UID);

				///< 채팅 메세지를 보내는 이와, 채팅 화면에 표시되는 이가 다른 경우가 있다?!				
				if( uidSender != uidPlayer )
				{
					//LOG(LOG_FILE,"In MC_MATCH_STAGE_CHAT - Different User(S:%d, P:%d)", uidSender, uidPlayer);
					break;
				}
				///< 여기까지

				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				pCommand->GetParameter(szChat, 2, MPT_STR, CHAT_STRING_LEN );

				OnStageChat(uidPlayer, uidStage, szChat);
			}
			break;
		case MC_MATCH_STAGE_REQUEST_QUICKJOIN:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				MCommandParameter* pQuickJoinParam = pCommand->GetParameter(1);
				if(pQuickJoinParam->GetType()!=MPT_BLOB) break;

				void* pQuickJoinBlob = pQuickJoinParam->GetPointer();
				if( NULL == pQuickJoinBlob )
					break;

				OnRequestQuickJoin(uidPlayer, pQuickJoinBlob);

			}
			break;
		case MC_MATCH_STAGE_GO:
			{
				unsigned int nRoomNo;

				pCommand->GetParameter(&nRoomNo, 0, MPT_UINT);
				OnStageGo(pCommand->GetSenderUID(), nRoomNo);
			}
			break;
		case MC_MATCH_STAGE_SUMMON:
			{
				char szTargetName[256] = {0,};

				pCommand->GetParameter(szTargetName, 0, MPT_STR, 32);
				OnStageSummon(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		case MC_MATCH_STAGE_LIST_START:
			{
				OnStartStageList(pCommand->GetSenderUID());

			}
			break;
		case MC_MATCH_STAGE_LIST_STOP:
			{
				OnStopStageList(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_REQUEST_STAGE_LIST:
			{
				MUID uidPlayer, uidChannel;
				int nStageCursor;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidChannel, 1, MPT_UID);
				pCommand->GetParameter(&nStageCursor, 2, MPT_INT);

				OnStageRequestStageList(uidPlayer, uidChannel, nStageCursor);
			}
			break;
		case MC_MATCH_CHANNEL_REQUEST_PLAYER_LIST:
            {
                MUID /*uidPlayer, */uidChannel;
                int nPage;

                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
                pCommand->GetParameter(&uidChannel, 1, MPT_UID);
                pCommand->GetParameter(&nPage, 2, MPT_INT);

                OnChannelRequestPlayerList(pCommand->GetSenderUID(), uidChannel, nPage);
            }
            break;  
		case MC_MATCH_STAGE_MAP:
			{
				MUID uidPlayer,uidStage;
				// 클라이언트에서 맵 이름만 전송이 되기때문에 _MAX_DIR은 필요 없음. - by SungE 2007-04-02
				char szMapName[ MAPNAME_LENGTH ] = {0,};

				uidPlayer = pCommand->GetSenderUID();
				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				pCommand->GetParameter(szMapName, 1, MPT_STR, MAPNAME_LENGTH );

				OnStageMap(uidPlayer, uidStage, szMapName);
			}
			break;
		case MC_MATCH_STAGE_RELAY_MAP_ELEMENT_UPDATE:
			{
				MUID uidStage;
				int nType = 0;
				int nRepeatCount = 0;

				pCommand->GetParameter(&uidStage, 0, MPT_UID );
				pCommand->GetParameter(&nType, 1, MPT_INT );
				pCommand->GetParameter(&nRepeatCount, 2, MPT_INT );

				OnStageRelayMapElementUpdate(uidStage, nType, nRepeatCount);
			}
			break;
		case MC_MATCH_STAGE_RELAY_MAP_INFO_UPDATE:
			{
				MUID uidStage;
				int nType = 0;
				int nRepeatCount = 0;
				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				pCommand->GetParameter(&nType, 1, MPT_INT );
				pCommand->GetParameter(&nRepeatCount, 2, MPT_INT );
				MCommandParameter* pParam = pCommand->GetParameter(3);
				if (pParam->GetType() != MPT_BLOB)
					break;
				void* pRelayMapListBlob = pParam->GetPointer();
				if( NULL == pRelayMapListBlob )
					break;

				OnStageRelayMapListUpdate(uidStage, nType, nRepeatCount, pRelayMapListBlob);
			}
			break;
		case MC_MATCH_STAGESETTING:
			{
				MUID uidPlayer, uidStage;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);

				MCommandParameter* pStageParam = pCommand->GetParameter(2);
				if(pStageParam->GetType()!=MPT_BLOB) break;
				void* pStageBlob = pStageParam->GetPointer();
				if( NULL == pStageBlob )
					break;

				int nStageCount = MGetBlobArrayCount(pStageBlob);

				OnStageSetting(uidPlayer, uidStage, pStageBlob, nStageCount);
			}
			break;
		case MC_MATCH_REQUEST_STAGESETTING:
			{
				MUID uidStage;
				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				OnRequestStageSetting(pCommand->GetSenderUID(), uidStage);

				MUID uidPlayer = pCommand->GetSenderUID();

				MMatchObject* pObject = GetObject(uidPlayer);
				if(strstr(pObject->GetName(), "\n"))
				{
					DisconnectObject(uidPlayer);
				}
			}
			break;
		case MC_MATCH_REQUEST_PEERLIST:
			{
				MUID uidStage;
				// pCommand->GetParameter(&uidChar, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				OnRequestPeerList(pCommand->GetSenderUID(), uidStage);
			}
			break;
		case MC_MATCH_REQUEST_GAME_INFO:
			{
				MUID uidStage;
				// pCommand->GetParameter(&uidChar, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				OnRequestGameInfo(pCommand->GetSenderUID(), uidStage);
			}
			break;
		case MC_MATCH_LOADING_COMPLETE:
			{
				
				int nPercent;
				// pCommand->GetParameter(&uidChar, 0, MPT_UID);
				pCommand->GetParameter(&nPercent, 1, MPT_INT);
				OnMatchLoadingComplete(pCommand->GetSenderUID(), nPercent);
			}
			break;
		case MC_MATCH_REQUEST_PEER_RELAY:
			{
				MUID  uidPeer;

				// if (pCommand->GetParameter(&uidChar, 0, MPT_UID) == false) break;
				if (pCommand->GetParameter(&uidPeer, 1, MPT_UID) == false) break;

				OnRequestRelayPeer(pCommand->GetSenderUID(), uidPeer);
			}
			break;
		case MC_AGENT_PEER_READY:
			{
				MUID uidChar;
				MUID uidPeer;

				if (pCommand->GetParameter(&uidChar, 0, MPT_UID) == false) break;
				if (pCommand->GetParameter(&uidPeer, 1, MPT_UID) == false) break;

				OnPeerReady(uidChar, uidPeer);
			}
			break;
		case MC_MATCH_REGISTERAGENT:
            {
                char szIP[128];
                int nTCPPort, nUDPPort;

                if (pCommand->GetParameter(&szIP, 0, MPT_STR, sizeof(szIP) ) == false) break;
                if (pCommand->GetParameter(&nTCPPort, 1, MPT_INT) == false) break;
                if (pCommand->GetParameter(&nUDPPort, 2, MPT_INT) == false) break;

                // Not the best way to patch, but working for now
                if (strstr(szIP, "%")) {
                    break;
                }
                
                OnRegisterAgent(pCommand->GetSenderUID(), szIP, nTCPPort, nUDPPort);
            }  
			break;
		case MC_MATCH_UNREGISTERAGENT:
			{
				OnUnRegisterAgent(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_AGENT_REQUEST_LIVECHECK:
			{
				unsigned long nTimeStamp;
				unsigned long nStageCount;
				unsigned long nUserCount;

				if (pCommand->GetParameter(&nTimeStamp, 0, MPT_UINT) == false) break;
				if (pCommand->GetParameter(&nStageCount, 1, MPT_UINT) == false) break;
				if (pCommand->GetParameter(&nUserCount, 2, MPT_UINT) == false) break;

				OnRequestLiveCheck(pCommand->GetSenderUID(), nTimeStamp, nStageCount, nUserCount);
			}
			break;
		case MC_AGENT_STAGE_READY:
			{
				MUID uidStage;
				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				OnAgentStageReady(pCommand->GetSenderUID(), uidStage);
			}
			break;

		case MC_MATCH_REQUEST_ACCOUNT_CHARINFO:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				char nCharNum;
				pCommand->GetParameter(&nCharNum, 0, MPT_CHAR);


				OnRequestAccountCharInfo(uidPlayer, nCharNum);
			}
			break;
		case MC_MATCH_REQUEST_SELECT_CHAR:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				unsigned long int nCharIndex;

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nCharIndex, 1, MPT_UINT);

				OnRequestSelectChar(uidPlayer, nCharIndex);
			}
			break;
		case MC_MATCH_REQUEST_DELETE_CHAR:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				unsigned long int nCharIndex;
				// char szCharName[MAX_CHARNAME];
				char szCharName[ MAX_CHARNAME_LENGTH ];

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nCharIndex, 1, MPT_UINT);
				pCommand->GetParameter(szCharName, 2, MPT_STR, MAX_CHARNAME_LENGTH );

				OnRequestDeleteChar(uidPlayer, nCharIndex, szCharName);
			}
			break;
		case MC_MATCH_REQUEST_CREATE_CHAR:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				unsigned long int nCharIndex;
				unsigned long int nSex, nHair, nFace, nCostume;

				// char szCharName[MAX_CHARNAME];
				char szCharName[ MAX_CHARNAME_LENGTH ];

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nCharIndex, 1, MPT_UINT);
				if (pCommand->GetParameter(szCharName, 2, MPT_STR, MAX_CHARNAME_LENGTH )==false) break;
				pCommand->GetParameter(&nSex, 3, MPT_UINT);
				pCommand->GetParameter(&nHair, 4, MPT_UINT);
				pCommand->GetParameter(&nFace, 5, MPT_UINT);
				pCommand->GetParameter(&nCostume, 6, MPT_UINT);

				int nResult = -1;
				bool bAlphaNum = true;

				nResult = ValidateMakingName(szCharName, MIN_CHARNAME, MAX_CHARNAME);
				if (nResult != MOK)
					bAlphaNum = false;

				for(SIZE_T nStr = 0; nStr < strlen(szCharName); nStr++)
				{
					if(!isalnum(szCharName[nStr]))
					{
						if(szCharName[nStr] != '[' || szCharName[nStr] != ']')
						{
							bAlphaNum = false;
							break;
						}
					}
				}
				if (!bAlphaNum)
				{
					MMatchObject* pTargetObj = GetObject(uidPlayer);
					if(!pTargetObj) break;

					MCommand* pNewCmd = CreateCommand(MC_MATCH_RESPONSE_CREATE_CHAR, MUID(0,0));
					pNewCmd->AddParameter(new MCommandParameterInt(MERR_WRONG_WORD_NAME));			// result
					pNewCmd->AddParameter(new MCommandParameterString(szCharName));		// 만들어진 캐릭터 이름
					RouteToListener(pTargetObj, pNewCmd);

					break;
				}

				OnRequestCreateChar(uidPlayer, nCharIndex, szCharName, nSex, nHair, nFace, nCostume);
			}
			break;
		case MC_MATCH_ROUND_FINISHINFO:
			{
				MUID uidStage, uidChar;

				pCommand->GetParameter(&uidStage, 0, MPT_UID);
				pCommand->GetParameter(&uidChar, 1, MPT_UID);
				OnUpdateFinishedRound(uidStage, uidChar, NULL, NULL);
			}
			break;
		

		case MC_MATCH_STAGE_REQUEST_FORCED_ENTRY:
			{
				MUID uidPlayer, uidStage;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&uidStage, 1, MPT_UID);
				
				OnRequestForcedEntry(uidStage, uidPlayer);
			}
			break;
		case MC_MATCH_FRIEND_ADD:
			{
				// char szArg[MAX_CHARNAME];
				char szArg[ MAX_CHARNAME_LENGTH ];
				pCommand->GetParameter(szArg, 0, MPT_STR, MAX_CHARNAME_LENGTH );

				OnFriendAdd(pCommand->GetSenderUID(), szArg);
			}
			break;
		case MC_MATCH_FRIEND_REMOVE:
			{
				// char szArg[MAX_CHARNAME];
				char szArg[ MAX_CHARNAME_LENGTH ];
				pCommand->GetParameter(szArg, 0, MPT_STR, MAX_CHARNAME_LENGTH );

				OnFriendRemove(pCommand->GetSenderUID(), szArg);
			}
			break;
		case MC_MATCH_FRIEND_LIST:
			{
				OnFriendList(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_FRIEND_MSG:
			{
				char szArg[CHAT_STRING_LEN];
				pCommand->GetParameter(szArg, 0, MPT_STR, CHAT_STRING_LEN );

				OnFriendMsg(pCommand->GetSenderUID(), szArg);
			}
			break;
		case MC_ADMIN_ANNOUNCE:
			{
				MUID uidAdmin;
				static char szChat[ ANNOUNCE_STRING_LEN ];
				unsigned long int nMsgType = 0;

				pCommand->GetParameter(&uidAdmin, 0, MPT_UID);
				pCommand->GetParameter(szChat, 1, MPT_STR, ANNOUNCE_STRING_LEN );
				pCommand->GetParameter(&nMsgType, 2, MPT_UINT);

				// OnAdminAnnounce(uidAdmin, szChat, nMsgType);
				OnAdminAnnounce(pCommand->GetSenderUID(), szChat, nMsgType);
			}
			break;
		case MC_ADMIN_TERMINAL:
			{
				// MUID uidAdmin;
				char szText[1024];
				
				// pCommand->GetParameter(&uidAdmin, 0, MPT_UID);
				pCommand->GetParameter(szText, 1, MPT_STR, sizeof(szText) );

				//OnAdminTerminal(uidAdmin, szText);
				OnAdminTerminal(pCommand->GetSenderUID(), szText);

			}
			break;
		case MC_ADMIN_REQUEST_DF:
			{
				MUID Admin = pCommand->GetSenderUID();
				MMatchObject* GradeObject = GetObject(Admin);
				if(!GradeObject)
					break;

				if(!IsGunzHighAdminIncludeEMGM(GradeObject->GetAccountInfo()->m_nUGrade))
					break;

				MCommand* NewCommand = CreateCommand(MC_ADMIN_RESPONSE_DF,MUID(0,0));
				NewCommand->AddParameter(new MCommandParameterUID(Admin));
				
				if(!GradeObject->GetStageUID().Low)
					break;

				RouteToBattle(GradeObject->GetStageUID(),NewCommand);


			}
		case MC_ADMIN_REQUEST_SERVER_INFO:
			{
				// MUID uidAdmin;
				// pCommand->GetParameter(&uidAdmin, 0, MPT_UID);

				//OnAdminRequestServerInfo(uidAdmin);
				OnAdminRequestServerInfo(pCommand->GetSenderUID());
			}
			break;
		case MC_ADMIN_SERVER_HALT:
			{
				MUID uidAdmin = pCommand->GetSenderUID();
				LOG(LOG_PROG, "Command(MC_ADMIN_SERVER_HALT) Received - Shutdown Start(%d%d)", uidAdmin.High, uidAdmin.Low);
				OnAdminServerHalt(uidAdmin);
			}
			break;
		case MC_ADMIN_REQUEST_KICK_PLAYER:
			{
				char szPlayer[512];
				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer) );

				OnAdminRequestKickPlayer(pCommand->GetSenderUID(), szPlayer);
			}
			break;
		case MC_MATCH_GAME_REQUEST_CHANGE_TEAM:
			{
				CHAR Player[512];
				LONG TeamId;

				pCommand->GetParameter(Player,0,MPT_STR,sizeof(Player));
				pCommand->GetParameter(&TeamId,1,MPT_INT);

				OnRequestChangeTeam(pCommand->GetSenderUID(),Player,TeamId);
			}
		case MC_MATCH_GAME_REQUEST_TELEPORT:
			{
				char Player[512] = {0};
				float X;
				float Y;
				float Z;
				pCommand->GetParameter(Player, 0, MPT_STR, sizeof(Player) );
				pCommand->GetParameter(&X, 1, MPT_FLOAT);
				pCommand->GetParameter(&Y, 2, MPT_FLOAT);
				pCommand->GetParameter(&Z, 3, MPT_FLOAT);
				OnRequestTeleportOtherPlayer(pCommand->GetSenderUID(),Player,X,Y,Z);
				
			}
			break;
		case MC_ADMIN_REQUEST_MUTE_PLAYER:
			{
				int nPunishHour;
				char szPlayer[512];

				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer) );
				pCommand->GetParameter(&nPunishHour, 1, MPT_INT);

				OnAdminRequestMutePlayer(pCommand->GetSenderUID(), szPlayer, nPunishHour);
			}
			break;

		case MC_ADMIN_REQUEST_BLOCK_PLAYER:
			{
				int nPunishHour;
				char szPlayer[512];

				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer) );
				pCommand->GetParameter(&nPunishHour, 1, MPT_INT);

				OnAdminRequestBlockPlayer(pCommand->GetSenderUID(), szPlayer, nPunishHour);
			}
			break;

		case MC_ADMIN_REQUEST_EVENTBAN_PLAYER:
			{
				int nPunishHour;
				char szPlayer[512];

				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer) );
				pCommand->GetParameter(&nPunishHour, 1, MPT_INT);

				OnAdminRequestEventBanPlayer(pCommand->GetSenderUID(), szPlayer, nPunishHour);
			}
			break;

		case MC_ADMIN_REQUEST_EVENTUNBAN_PLAYER:
			{
				int nPunishHour;
				char szPlayer[512];

				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer) );
				pCommand->GetParameter(&nPunishHour, 1, MPT_INT);

				OnAdminRequestEventUnbanPlayer(pCommand->GetSenderUID(), szPlayer, nPunishHour);
			}
			break;

		case MC_ADMIN_REQUEST_UPDATE_ACCOUNT_UGRADE:
			{
				// MUID uidAdmin;
				char szPlayer[512];

				// pCommand->GetParameter(&uidAdmin, 0, MPT_UID);
				pCommand->GetParameter(szPlayer, 1, MPT_STR, sizeof(szPlayer) );
			
				//OnAdminRequestUpdateAccountUGrade(uidAdmin, szPlayer);
				OnAdminRequestUpdateAccountUGrade(pCommand->GetSenderUID(), szPlayer);
			}
			break;
		case MC_ADMIN_REQUEST_SWITCH_LADDER_GAME:
			{
				// MUID uidAdmin;
				bool bEnabled;

				// pCommand->GetParameter(&uidAdmin, 0, MPT_UID);
				pCommand->GetParameter(&bEnabled, 1, MPT_BOOL);
			
				//OnAdminRequestSwitchLadderGame(uidAdmin, bEnabled);
				OnAdminRequestSwitchLadderGame(pCommand->GetSenderUID(), bEnabled);
			}
			break;
		case MC_ADMIN_HIDE:
			{
				OnAdminHide(pCommand->GetSenderUID());
			}
			break;
		case MC_ADMIN_INCOGNITO:
			{
				char szPlayer[256];
				pCommand->GetParameter(szPlayer, 0, MPT_STR, sizeof(szPlayer));
				OnAdminIncognito(pCommand->GetSenderUID(),szPlayer);
			}
			break;
		case MC_ADMIN_PING_TO_ALL:
			{
				OnAdminPingToAll(pCommand->GetSenderUID());
			}
			break;
		case MC_ADMIN_RESET_ALL_HACKING_BLOCK :
			{
				OnAdminResetAllHackingBlock( pCommand->GetSenderUID() );
			}
			break;
		case MC_EVENT_CHANGE_MASTER:
			{
				OnEventChangeMaster(pCommand->GetSenderUID());
			}
			break;
		case MC_EVENT_CHANGE_PASSWORD:
			{
				char szPassword[ STAGEPASSWD_LENGTH ]="";
				pCommand->GetParameter(szPassword, 0, MPT_STR, STAGEPASSWD_LENGTH );

				OnEventChangePassword(pCommand->GetSenderUID() ,szPassword);
			}
			break;
		case MC_STAGE_REQUEST_CHANGE_TITLE:
			{
				char szTitle[ STAGENAME_LENGTH ]="";
				pCommand->GetParameter(szTitle, 0, MPT_STR, STAGENAME_LENGTH );

				OnStageChangeTitle(pCommand->GetSenderUID() ,szTitle);
			}
			break;
		case MC_EVENT_REQUEST_JJANG:
			{
				char szTargetName[128]="";
				pCommand->GetParameter(szTargetName, 0, MPT_STR, sizeof(szTargetName) );
				OnEventRequestJjang(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		case MC_EVENT_REMOVE_JJANG:
			{
				char szTargetName[128]="";
				pCommand->GetParameter(szTargetName, 0, MPT_STR, sizeof(szTargetName) );
				OnEventRemoveJjang(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		
		case MC_MATCH_REQUEST_ACCOUNT_ITEMLIST:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				OnRequestAccountItemList(uidPlayer);
			}
			break;

		///< AccountItem 관련하여 추가된 커맨드
		// 홍기주(2010-03-09)
		/*
		case MC_MATCH_REQUEST_MOVE_ACCOUNTITEM_IN_BANK : 
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				int nAIIDFrom = 0;
				int nAIIDTo = 0;
				int nCount = 0;

				pCommand->GetParameter(&nAIIDFrom, 0, MPT_INT);
				pCommand->GetParameter(&nAIIDTo, 1, MPT_INT);
				pCommand->GetParameter(&nCount, 2, MPT_INT);

				OnRequestAccountItemMoveInBank(uidPlayer, nAIIDFrom, nAIIDTo, nCount);
			}
			break;
		*/
		///< 여기까지
		
		case MC_MATCH_REQUEST_SUICIDE:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);

				OnRequestSuicide(uidPlayer);
			}
			break;
		case MC_MATCH_REQUEST_OBTAIN_WORLDITEM:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nItemUID = 0;

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nItemUID, 1, MPT_INT);

				OnRequestObtainWorldItem(uidPlayer, nItemUID);
			}
			break;
		case MC_MATCH_REQUEST_FLAG_CAP:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nItemID = 0;

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nItemID, 0, MPT_INT);

				OnRequestFlagCap(uidPlayer, nItemID);
			}
			break;
		case MC_MATCH_REQUEST_SPAWN_WORLDITEM:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nItemID = 0;
				rvector pos;
				rvector dir;
				float nDropDelayTime = 0.f;

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(&nItemID, 1, MPT_INT);
				pCommand->GetParameter(&pos, 2, MPT_POS);
				pCommand->GetParameter(&dir, 3, MPT_DIR);
				pCommand->GetParameter(&nDropDelayTime, 4, MPT_FLOAT);
				OnRequestSpawnWorldItem(uidPlayer, nItemID, pos.x, pos.y, pos.z, dir.x, dir.y, dir.z, nDropDelayTime);
			}
			break;
		case MC_MATCH_NOTIFY_THROW_TRAPITEM:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nItemID = 0;
				pCommand->GetParameter(&nItemID, 0, MPT_SHORT);

				OnNotifyThrowTrapItem(uidPlayer, nItemID);
			}
			break;
		case MC_MATCH_NOTIFY_ACTIVATED_TRAPITEM:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int nItemID = 0;
				MShortVector s_pos;
				pCommand->GetParameter(&nItemID, 0, MPT_SHORT);
				pCommand->GetParameter(&s_pos, 1, MPT_SVECTOR);

				OnNotifyActivatedTrapItem(uidPlayer, nItemID, MVector3(s_pos.x, s_pos.y, s_pos.z));
			}
			break;

		case MC_MATCH_USER_WHISPER:
			{
				// char szSenderName[128]="";
				// char szTargetName[128]="";
				// char szMessage[1024]="";
				char szSenderName[ MAX_CHARNAME_LENGTH ]="";
				char szTargetName[ MAX_CHARNAME_LENGTH ]="";
				char szMessage[CHAT_STRING_LEN]="";
				
				if (pCommand->GetParameter(szSenderName, 0, MPT_STR, MAX_CHARNAME_LENGTH ) == false) break;
				if (pCommand->GetParameter(szTargetName, 1, MPT_STR, MAX_CHARNAME_LENGTH ) == false) break;
				if (pCommand->GetParameter(szMessage, 2, MPT_STR, CHAT_STRING_LEN ) == false) break;

				OnUserWhisper(pCommand->GetSenderUID(), szSenderName, szTargetName, szMessage);
			}
			break;
		case MC_MATCH_USER_WHERE:
			{
				// char szTargetName[MAX_CHARNAME]="";
				char szTargetName[ MAX_CHARNAME_LENGTH ]="";
				pCommand->GetParameter(szTargetName, 0, MPT_STR, MAX_CHARNAME_LENGTH );

				OnUserWhere(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		case MC_MATCH_USER_OPTION:
			{
				unsigned long nOptionFlags=0;
				pCommand->GetParameter(&nOptionFlags, 0, MPT_UINT);

				OnUserOption(pCommand->GetSenderUID(), nOptionFlags);
			}
			break;
		case MC_MATCH_CHATROOM_CREATE:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				char szChatRoomName[MAX_CHATROOMNAME_STRING_LEN];

				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				pCommand->GetParameter(szChatRoomName, 1, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );

				OnChatRoomCreate(uidPlayer, szChatRoomName);
			}
			break;
		case MC_MATCH_CHATROOM_JOIN:
			{
				// char szPlayerName[MAX_CHARNAME];
				char szPlayerName[ MAX_CHARNAME_LENGTH ];
				char szChatRoomName[MAX_CHATROOMNAME_STRING_LEN];
				
				pCommand->GetParameter(szPlayerName, 0, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(szChatRoomName, 1, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );

				OnChatRoomJoin(pCommand->GetSenderUID(), szPlayerName, szChatRoomName);
			}
			break;
		case MC_MATCH_CHATROOM_LEAVE:
			{
				// char szPlayerName[MAX_CHARNAME];
				char szPlayerName[ MAX_CHARNAME_LENGTH ];
				char szChatRoomName[MAX_CHATROOMNAME_STRING_LEN];
				
				pCommand->GetParameter(szPlayerName, 0, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(szChatRoomName, 1, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );

				OnChatRoomLeave(pCommand->GetSenderUID(), szPlayerName, szChatRoomName);
			}
			break;
		case MC_MATCH_CHATROOM_SELECT_WRITE:
			{
				char szChatRoomName[MAX_CHATROOMNAME_STRING_LEN];

				pCommand->GetParameter(szChatRoomName, 0, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );

				OnChatRoomSelectWrite(pCommand->GetSenderUID(), szChatRoomName);
			}
			break;
		case MC_MATCH_CHATROOM_INVITE:
			{
				char szSenderName[ MAX_CHARNAME_LENGTH ]="";
				char szTargetName[MAX_CHARNAME_LENGTH ]="";
				char szRoomName[MAX_CHATROOMNAME_STRING_LEN]="";

				pCommand->GetParameter(szSenderName, 0, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(szTargetName, 1, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(szRoomName, 2, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );

				OnChatRoomInvite(pCommand->GetSenderUID(), szTargetName);
			}
			break;
		case MC_MATCH_CHATROOM_CHAT:
			{
				char szRoomName[MAX_CHATROOMNAME_STRING_LEN];
				// char szPlayerName[MAX_CHARNAME];
				char szPlayerName[ MAX_CHARNAME_LENGTH ];
				// char szMessage[256];
				char szMessage[CHAT_STRING_LEN];

				pCommand->GetParameter(szRoomName, 0, MPT_STR, MAX_CHATROOMNAME_STRING_LEN );
				pCommand->GetParameter(szPlayerName, 1, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(szMessage, 2, MPT_STR, CHAT_STRING_LEN);

				OnChatRoomChat(pCommand->GetSenderUID(), szMessage);
			}
			break;
		case MC_MATCH_REQUEST_MY_SIMPLE_CHARINFO:
            {
                //MUID uidPlayer;
                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
                OnRequestMySimpleCharInfo(pCommand->GetSenderUID());
            }
            break;  
		case MC_MATCH_REQUEST_COPY_TO_TESTSERVER:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				//pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
				OnRequestCopyToTestServer(uidPlayer);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_CREATE_CLAN:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				char szClanName[CLAN_NAME_LENGTH];

				//pCommand->GetParameter(&uidPlayer,			0, MPT_UID);
				pCommand->GetParameter(szClanName,			1, MPT_STR, CLAN_NAME_LENGTH );

				OnClanRequestCreateClan(uidPlayer, NULL, szClanName);
			}
			break;
		case MC_MATCH_CLAN_ANSWER_SPONSOR_AGREEMENT:
			{
				MUID uidClanMaster;
				int nRequestID;
				bool bAnswer;
				// char szCharName[MAX_CHARNAME];
				char szCharName[ MAX_CHARNAME_LENGTH ];

				pCommand->GetParameter(&nRequestID,		0, MPT_INT);
				pCommand->GetParameter(&uidClanMaster,	1, MPT_UID);
				pCommand->GetParameter(szCharName,		2, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(&bAnswer,		3, MPT_BOOL);

				OnClanAnswerSponsorAgreement(nRequestID, uidClanMaster, szCharName, bAnswer);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_AGREED_CREATE_CLAN:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				char szClanName[CLAN_NAME_LENGTH];
				char szSponsorNames[CLAN_SPONSORS_COUNT][256];
				char* sncv[CLAN_SPONSORS_COUNT];

				//pCommand->GetParameter(&uidPlayer,			0, MPT_UID);
				pCommand->GetParameter(szClanName,			1, MPT_STR, CLAN_NAME_LENGTH );

				// 발기인 숫자만큼 인자를 받는다. - 4명
				for (int i = 0; i < CLAN_SPONSORS_COUNT; i++)
				{
					pCommand->GetParameter(szSponsorNames[i],	2+i, MPT_STR, sizeof(szSponsorNames[i]) );

					sncv[i] = szSponsorNames[i];
				}

				OnClanRequestAgreedCreateClan(uidPlayer, szClanName, sncv);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_CLOSE_CLAN:
			{
				MUID uidClanMaster;
				char szClanName[CLAN_NAME_LENGTH];

				uidClanMaster = pCommand->GetSenderUID();

				// pCommand->GetParameter(&uidClanMaster,		0, MPT_UID);
				pCommand->GetParameter(szClanName, 1, MPT_STR, CLAN_NAME_LENGTH );

				OnClanRequestCloseClan(uidClanMaster, szClanName);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_JOIN_CLAN:
			{
				// MUID uidClanAdmin;
				char szClanName[CLAN_NAME_LENGTH];
				// char szJoiner[MAX_CHARNAME];
				char szJoiner[ MAX_CHARNAME_LENGTH ];

				// pCommand->GetParameter(&uidClanAdmin,	0, MPT_UID);
				pCommand->GetParameter(szClanName,		1, MPT_STR, CLAN_NAME_LENGTH );
				pCommand->GetParameter(szJoiner,		2, MPT_STR, MAX_CHARNAME_LENGTH );

				OnClanRequestJoinClan(pCommand->GetSenderUID(), szClanName, szJoiner);
			}
			break;
		case MC_MATCH_CLAN_ANSWER_JOIN_AGREEMENT:
			{
				MUID uidClanAdmin;
				bool bAnswer;
				// char szJoiner[MAX_CHARNAME];
				char szJoiner[ MAX_CHARNAME_LENGTH ];

				// uidClanAdmin = pCommand->GetSenderUID();

				pCommand->GetParameter(&uidClanAdmin,	0, MPT_UID);
				pCommand->GetParameter(szJoiner,		1, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(&bAnswer,		2, MPT_BOOL);

				OnClanAnswerJoinAgreement(uidClanAdmin, szJoiner, bAnswer);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_AGREED_JOIN_CLAN:
			{
				///< 이 커맨드는 건들지 말자...		

				 MUID uidClanAdmin;
				char szClanName[CLAN_NAME_LENGTH];
				// char szJoiner[MAX_CHARNAME];
				char szJoiner[ MAX_CHARNAME_LENGTH ];

				 pCommand->GetParameter(&uidClanAdmin,	0, MPT_UID);
				pCommand->GetParameter(szClanName,		1, MPT_STR, CLAN_NAME_LENGTH );
				// 가입자 구분을 왜 이름으로 하는지 모르겠음...-_-;;
				pCommand->GetParameter(szJoiner,		2, MPT_STR, MAX_CHARNAME_LENGTH );

				OnClanRequestAgreedJoinClan(uidClanAdmin, szClanName, szJoiner);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_LEAVE_CLAN:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer,	0, MPT_UID);

				OnClanRequestLeaveClan(uidPlayer);
			}
			break;
		case MC_MATCH_CLAN_MASTER_REQUEST_CHANGE_GRADE:
			{
				MUID uidClanMaster;
				// char szMember[MAX_CHARNAME];
				char szMember[ MAX_CHARNAME_LENGTH ];
				int nClanGrade;

				uidClanMaster = pCommand->GetSenderUID();

				// pCommand->GetParameter(&uidClanMaster,	0, MPT_UID);
				pCommand->GetParameter(szMember,		1, MPT_STR, MAX_CHARNAME_LENGTH );
				pCommand->GetParameter(&nClanGrade,		2, MPT_INT);

				OnClanRequestChangeClanGrade(uidClanMaster, szMember, nClanGrade);
			}
			break;
		case MC_MATCH_CLAN_ADMIN_REQUEST_EXPEL_MEMBER:
			{
				MUID uidClanAdmin;
				// char szMember[MAX_CHARNAME];
				char szMember[ MAX_CHARNAME_LENGTH ];

				uidClanAdmin = pCommand->GetSenderUID();
				// pCommand->GetParameter(&uidClanAdmin,	0, MPT_UID);
				pCommand->GetParameter(szMember,		1, MPT_STR, MAX_CHARNAME_LENGTH );

				OnClanRequestExpelMember(uidClanAdmin, szMember);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_MSG:
			{
				MUID uidSender;
				char szMsg[MSG_STRING_LEN];

				uidSender = pCommand->GetSenderUID();
				// pCommand->GetParameter(&uidSender,	0, MPT_UID);
				pCommand->GetParameter(szMsg,		1, MPT_STR, MSG_STRING_LEN );

				OnClanRequestMsg(uidSender, szMsg);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_MEMBER_LIST:
			{
				MUID uidChar;

				uidChar = pCommand->GetSenderUID();
				// pCommand->GetParameter(&uidChar,	0, MPT_UID);

				OnClanRequestMemberList(uidChar);
			}
			break;
		case MC_MATCH_CLAN_REQUEST_CLAN_INFO:
			{
				MUID uidChar;
				char szClanName[CLAN_NAME_LENGTH] = "";

				uidChar = pCommand->GetSenderUID();
				// pCommand->GetParameter(&uidChar,	0, MPT_UID);
				
				pCommand->GetParameter(szClanName,	1, MPT_STR, CLAN_NAME_LENGTH );

				OnClanRequestClanInfo(uidChar, szClanName);

			}
			break;
		case MC_MATCH_CHANNEL_REQUEST_ALL_PLAYER_LIST:
			{
				MUID uidPlayer, uidChannel;
				unsigned long int nPlaceFilter;
				unsigned long int nOptions;
				uidPlayer = pCommand->GetSenderUID();

				//pCommand->GetParameter(&uidPlayer,		0, MPT_UID);
				pCommand->GetParameter(&uidChannel,		1, MPT_UID);
				pCommand->GetParameter(&nPlaceFilter,	2, MPT_UINT);
				pCommand->GetParameter(&nOptions,		3, MPT_UINT);

				OnChannelRequestAllPlayerList(uidPlayer, uidChannel, nPlaceFilter, nOptions);
			}
			break;
		case MC_MATCH_REQUEST_CHARINFO_DETAIL:
			{
				// MUID uidChar;
				char szCharName[ MAX_CHARNAME_LENGTH ];

				// pCommand->GetParameter(&uidChar,	0, MPT_UID);
				pCommand->GetParameter(szCharName,	1, MPT_STR, MAX_CHARNAME_LENGTH );

				OnRequestCharInfoDetail(pCommand->GetSenderUID(), szCharName);
			}
			break;


		case MC_MATCH_LADDER_REQUEST_CHALLENGE:
			{
				int nMemberCount;
				unsigned long int nOptions, nGladiator, nEquality;

				pCommand->GetParameter(&nMemberCount,		0, MPT_INT);
				pCommand->GetParameter(&nOptions,			1, MPT_UINT);

				MCommandParameter* pMemberNamesBlobParam = pCommand->GetParameter(2);
				if(pMemberNamesBlobParam->GetType()!=MPT_BLOB) break;

				void* pMemberNamesBlob = pMemberNamesBlobParam->GetPointer();
				if( NULL == pMemberNamesBlob )
					break;

				pCommand->GetParameter(&nGladiator, 3, MPT_UINT);
				pCommand->GetParameter(&nEquality, 4, MPT_UINT);

				OnLadderRequestChallenge(pCommand->GetSenderUID(), pMemberNamesBlob, nOptions, nGladiator, nEquality);
			}
			break;
		case MC_MATCH_LADDER_REQUEST_CANCEL_CHALLENGE:
			{
				OnLadderRequestCancelChallenge(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_REQUEST_PROPOSAL:
			{
				MUID uidChar;
				int nProposalMode, nRequestID, nReplierCount;

				uidChar = pCommand->GetSenderUID();
				// pCommand->GetParameter(&uidChar,			0, MPT_UID);

				pCommand->GetParameter(&nProposalMode,		1, MPT_INT);
				pCommand->GetParameter(&nRequestID,			2, MPT_INT);
				pCommand->GetParameter(&nReplierCount,		3, MPT_INT);

				MCommandParameter* pReplierNamesParam = pCommand->GetParameter(4);
				if(pReplierNamesParam->GetType()!=MPT_BLOB) break;

				void* pReplierNamesBlob = pReplierNamesParam->GetPointer();
				if( NULL == pReplierNamesBlob )
					break;

				OnRequestProposal(uidChar, nProposalMode, nRequestID, nReplierCount, pReplierNamesBlob);
			}
			break;
		case MC_MATCH_REPLY_AGREEMENT:
			{
				MUID uidProposer, uidReplier;
				char szReplierName[256];
				int nProposalMode, nRequestID;
				bool bAgreement;

				pCommand->GetParameter(&uidProposer,	0, MPT_UID);

				// pCommand->GetParameter(&uidReplier,		1, MPT_UID);
				uidReplier = pCommand->GetSenderUID();

				pCommand->GetParameter(szReplierName,	2, MPT_STR, sizeof(szReplierName) );
				pCommand->GetParameter(&nProposalMode,	3, MPT_INT);
				pCommand->GetParameter(&nRequestID,		4, MPT_INT);
				pCommand->GetParameter(&bAgreement,		5, MPT_BOOL);

				OnReplyAgreement(uidProposer, uidReplier, szReplierName, nProposalMode, nRequestID, bAgreement);
			}
			break;
		case MC_MATCH_CALLVOTE:
			{
				char szDiscuss[ VOTE_DISCUSS_STRING_LEN ]="";
				char szArg[ VOTE_ARG_STRING_LEN ]="";
				
				pCommand->GetParameter(szDiscuss, 0, MPT_STR, VOTE_DISCUSS_STRING_LEN );
				pCommand->GetParameter(szArg, 1, MPT_STR, VOTE_ARG_STRING_LEN );
				OnVoteCallVote(pCommand->GetSenderUID(), szDiscuss, szArg);
			}
			break;
		case MC_MATCH_PERMKICK:
			{
				MMatchObject* pObj = GetObject(pCommand->GetSenderUID());
				if(!pObj) break;

				if(!IsAdminGrade(pObj) || pObj->GetAccountInfo()->m_nUGrade == MMUG_DEVELOPER) break;

				MMatchStage* pStage = FindStage(pObj->GetStageUID());
				if(!pStage) break;

				char szName[32];
				pCommand->GetParameter(szName, 0, MPT_STR, 32);

				MMatchObject *pTarget = GetPlayerByName(szName);
				if (pTarget == NULL || (IsHighAdmin(pTarget) && !IsHighAdmin(pObj))) break;

				if (pStage->KickBanPlayer(szName))
				{
					char szKickMessage[256];
					sprintf(szKickMessage,"^2You have been banned from room %.3i by ^1%s",pStage->GetIndex(),pObj->GetName());
					MGetMatchServer()->AnnounceBox(pTarget,szKickMessage);
					MGetMatchServer()->Announce(pObj,"Player banned from the room");
				}
				else
					MGetMatchServer()->Announce(pObj,"Player is already banned from the room");
			}
			break;
			case MC_MATCH_REMOVEKICK:
			{
				MMatchObject* pObj = GetObject(pCommand->GetSenderUID());
				if(!pObj) break;

				if(!IsAdminGrade(pObj) || pObj->GetAccountInfo()->m_nUGrade == MMUG_DEVELOPER) break;

				MMatchStage* pStage = FindStage(pObj->GetStageUID());
				if(!pStage) break;

				MMatchChannel* pChannel = FindChannel(pObj->GetChannelUID());
				bool bEvent = pChannel && strcmp(pChannel->GetName(), "DG Event")==0;

				if (bEvent && !IsDGAdminGrade(pObj))
					break;

				char szName[32];
				pCommand->GetParameter(szName, 0, MPT_STR, 32);

				MMatchObject *pTarget = GetPlayerByName(szName);
				if (pTarget == NULL) break;

				if (pStage->UnbanPlayer(szName))
				{
					char szPardonMessage[256];
					sprintf(szPardonMessage,"^2You have been unbanned from room %.3i by ^1%s",pStage->GetIndex(),pObj->GetName());
					MGetMatchServer()->AnnounceBox(pTarget,szPardonMessage);
					MGetMatchServer()->Announce(pObj,"Player unbanned from the room");
				}
				else
					MGetMatchServer()->Announce(pObj,"Player is already unbanned from the room");
			}
			break;
		case MC_MATCH_GAME_INFECTED:
			{
				MMatchObject* pObj = GetObject(pCommand->GetSenderUID());
				if(!pObj) break;

				MMatchStage* pStage = FindStage(pObj->GetStageUID());
				if(!pStage || pObj->GetEnterBattle()) break;

				if(pStage->GetRule()->GetGameType() != MMATCH_GAMETYPE_INFECTION) break;

				MCommand* pCmd = CreateCommand(MC_MATCH_RESET_TEAM_MEMBERS, MUID(0,0));
	
				int nMemberCount = pStage->GetObjInBattleCount();;
				void* pTeamMemberDataArray = MMakeBlobArray(sizeof(MTD_ResetTeamMembersData), nMemberCount);

				int nCounter = 0;
				for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
				{
					MMatchObject* pObject = (MMatchObject*)(*i).second;

					if ((pObject->GetEnterBattle() == false) && (!pObject->IsLaunchedGame()))
						continue;

					MTD_ResetTeamMembersData* pNode = (MTD_ResetTeamMembersData*)MGetBlobArrayElement(pTeamMemberDataArray, nCounter);
					pNode->m_uidPlayer = pObject->GetUID();

					pStage->PlayerTeam(pObject->GetUID(), pObject->GetUID() == pObj->GetUID() && pObj->GetTeam() == MMT_BLUE ? MMT_RED /*true*/ : pObject->GetTeam() /*false*/);
					pNode->nTeam = pObject->GetTeam();

					nCounter++;
				}

				pCmd->AddParameter(new MCommandParameterBool(true));
				pCmd->AddParameter(new MCommandParameterBlob(pTeamMemberDataArray, MGetBlobArraySize(pTeamMemberDataArray)));
	
				MEraseBlobArray(pTeamMemberDataArray);
	
				RouteToBattle(pStage->GetUID(), pCmd);
			}
			break;
		case MC_MATCH_VOTE_YES:
			{
				OnVoteYes(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_VOTE_NO:
			{
				OnVoteNo(pCommand->GetSenderUID());
			}
			break;
		case MC_MATCH_CLAN_REQUEST_EMBLEMURL:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				MCommandParameter* pParam = pCommand->GetParameter(0);
				if (pParam->GetType() != MPT_BLOB)
				{
					break;
				}
				void* pEmblemURLListBlob = pParam->GetPointer();
				if( NULL == pEmblemURLListBlob )
					break;

				OnClanRequestEmblemURL(uidPlayer, pEmblemURLListBlob);
			}
			break;
		
		case MC_QUEST_REQUEST_NPC_DEAD:
			{
				MUID uidKiller, uidNPC;
				MShortVector s_pos;
				pCommand->GetParameter(&uidKiller,	0, MPT_UID);
				pCommand->GetParameter(&uidNPC,		1, MPT_UID);
				pCommand->GetParameter(&s_pos,		2, MPT_SVECTOR);

				MUID uidSender = pCommand->GetSenderUID();
				MVector pos = MVector((float)s_pos.x, (float)s_pos.y, (float)s_pos.z);

				OnRequestNPCDead(uidSender, uidKiller, uidNPC, pos);

#ifdef _DEBUG
				static int a = 0;
				mlog( "npc dead : %d.\n", a++ );
#endif
			}
			break;
		case MC_MATCH_QUEST_REQUEST_DEAD:
			{
				MUID uidVictim = pCommand->GetSenderUID();
				OnQuestRequestDead(uidVictim);
			}
			break;
		case MC_QUEST_TEST_REQUEST_NPC_SPAWN:
			{
				int nNPCType, nNPCCount;

				pCommand->GetParameter(&nNPCType,	0, MPT_INT);
				pCommand->GetParameter(&nNPCCount,	1, MPT_INT);
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestTestRequestNPCSpawn(uidPlayer, nNPCType, nNPCCount);
			}
			break;
		case MC_QUEST_TEST_REQUEST_CLEAR_NPC:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestTestRequestClearNPC(uidPlayer);
			}
			break;
		case MC_QUEST_TEST_REQUEST_SECTOR_CLEAR:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestTestRequestSectorClear(uidPlayer);
			}
			break;
		case MC_QUEST_TEST_REQUEST_FINISH:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestTestRequestQuestFinish(uidPlayer);
			}
			break;
		case MC_QUEST_REQUEST_MOVETO_PORTAL:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestRequestMovetoPortal(uidPlayer);
			}
			break;
		case MC_QUEST_READYTO_NEWSECTOR:
			{
				MUID uidPlayer = pCommand->GetSenderUID();

				OnQuestReadyToNewSector(uidPlayer);
			}
			break;

		case MC_QUEST_PONG:
			{
				MUID uidPlayer = pCommand->GetSenderUID();
//				unsigned long int nTimeStamp;
//				
//				pCommand->GetParameter(&nTimeStamp, 0, MPT_UINT);

				OnQuestPong(uidPlayer);
			}
			break;

		case MC_MATCH_REQUEST_CHAR_QUEST_ITEM_LIST :
			{
				MUID uidChar;

				pCommand->GetParameter( &uidChar, 0, MPT_UID );

				OnRequestCharQuestItemList( uidChar );
			}
			break;

		case MC_MATCH_REQUEST_DROP_SACRIFICE_ITEM :
			{
				// MUID	uidQuestItemOwner;
				int		nSlotIndex;
				int		nItemID;

				// pCommand->GetParameter( &uidQuestItemOwner, 0, MPT_UID );
				pCommand->GetParameter( &nSlotIndex, 1, MPT_INT );
				pCommand->GetParameter( &nItemID, 2, MPT_INT );

				OnRequestDropSacrificeItemOnSlot( pCommand->GetSenderUID(), nSlotIndex, nItemID );
			}
			break;

		case MC_MATCH_REQUEST_CALLBACK_SACRIFICE_ITEM :
			{
				MUID uidPlayer = pCommand->GetSenderUID();
				int		nSlotIndex;
				int		nItemID;

				//pCommand->GetParameter( &uidPlayer, 0, MPT_UID );
				pCommand->GetParameter( &nSlotIndex, 1, MPT_INT );
				pCommand->GetParameter( &nItemID, 2, MPT_INT );

				OnRequestCallbackSacrificeItem( uidPlayer, nSlotIndex, nItemID );
			}
			break;

		case MC_MATCH_REQUEST_BUY_QUEST_ITEM :
			{
				int	nItemID;
				int	nItemCount;

				pCommand->GetParameter( &nItemID, 1, MPT_INT );
				pCommand->GetParameter( &nItemCount, 2, MPT_INT );

				OnRequestBuyQuestItem( pCommand->GetSenderUID(), nItemID, nItemCount );
			}
			break;

		case MC_MATCH_REQUEST_SELL_QUEST_ITEM :
			{
				int		nItemID;
				int		nCount;

				pCommand->GetParameter( &nItemID, 1, MPT_INT );
				pCommand->GetParameter( &nCount, 2, MPT_INT );

				OnRequestSellQuestItem( pCommand->GetSenderUID(), nItemID, nCount );
			}
			break;

		case MC_QUEST_REQUEST_QL :
			{
				// MUID uidSender;

				// pCommand->GetParameter( &uidSender, 0, MPT_UID );

				OnRequestQL( pCommand->GetSenderUID() );
			}
			break;

		case MC_MATCH_REQUEST_SLOT_INFO :
			{
				//MUID uidSender;

				//pCommand->GetParameter( &uidSender, 0, MPT_UID );

				OnRequestSacrificeSlotInfo( pCommand->GetSenderUID() );
			}
			break;

		case MC_QUEST_STAGE_MAPSET :
			{
				MUID uidStage;
				char nMapsetID;

				pCommand->GetParameter( &uidStage,	0, MPT_UID );
				pCommand->GetParameter( &nMapsetID, 1, MPT_CHAR );

				OnQuestStageMapset(uidStage, (int)nMapsetID );
			}
			break;
		case MC_MATCH_REQUEST_MONSTER_BIBLE_INFO :
			{
				MUID uidSender;

				pCommand->GetParameter( &uidSender, 0, MPT_UID );

				OnRequestMonsterBibleInfo( uidSender );
			}
			break;

		case MC_RESPONSE_RESOURCE_CRC32 :
			{
				DWORD dwResourceXORCache = 0;
				DWORD dwResourceCRC32Cache = 0;

				pCommand->GetParameter( &dwResourceCRC32Cache, 0, MPT_UINT );
				pCommand->GetParameter( &dwResourceXORCache, 1, MPT_UINT );

				//_ASSERT( 0 != dwResourceXORCache );
				//_ASSERT( 0 != dwResourceCRC32Cache );

				MMatchObject* pObj = GetObject( pCommand->GetSenderUID() );
				if( NULL == pObj )
				{
					return false;
				}

				MMatchStage* pStage = FindStage( pObj->GetStageUID() );
				if( NULL == pStage )
				{
					// 유저를 로비나 방으로 보내주어야 한다.
					//_ASSERT( 0 );
					return false;
				}

				if( !pStage->IsValidResourceCRC32Cache(pCommand->GetSenderUID(), dwResourceCRC32Cache, dwResourceXORCache) )
				{
					// 유저를 로비나 방으로 보내주어야 한다.
					StageLeave( pObj->GetUID());//, pObj->GetStageUID() );
					return false;
				}
			}
			break;


		///< 홍기주(2009.08.04)
		///< CommandBuilder 단에서 만든 Flooding 관련 유저에 대한 Command 처리이다.
		///< 이 부분에 Handler를 두기가 좀 싫긴 하지만.. 어쩔 수 없이.. 일단은 처리하자!
		///< 무조건 MC_NET_CLEAR보다 먼저 실행되어야 Abuse를 시킬 수 있다.		
		case MC_NET_BANPLAYER_FLOODING :
            {
                /*MUID uidPlayer;
                
                pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
                if (MGetServerConfig()->IsUseBlockFlooding())
                {
                    MMatchObject* pObj = GetObject( uidPlayer );
                    if( pObj && pObj->GetDisconnStatusInfo().GetStatus() == MMDS_CONNECTED)
                    {
                        if( pObj->GetAccountName() ) {
                            LOG(LOG_PROG,"Ban Player On Flooding - (MUID:%d%d, ID:%s)"
                                , uidPlayer.High, uidPlayer.Low, pObj->GetAccountName());
                        } else {
                            LOG(LOG_PROG,"Ban Player On Flooding - (MUID:%d%d, ID:%s)"
                                , uidPlayer.High, uidPlayer.Low);
                        }
                        
                        pObj->DisconnectHacker( MMHT_COMMAND_FLOODING );
                    }
                    else
                    {
                        LOG(LOG_PROG,"Ban Player On Flooding - Can't Find Object");
                    }
                }*/
            }
            break;  
		///< 여기까지

		case MC_MATCH_DUELTOURNAMENT_REQUEST_JOINGAME :
            {
                //MUID uidPlayer;
                MDUELTOURNAMENTTYPE nType;

                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
                pCommand->GetParameter(&nType, 1, MPT_INT);

                if( MGetServerConfig()->IsEnabledDuelTournament() )    {
                    ResponseDuelTournamentJoinChallenge(pCommand->GetSenderUID(), nType);
                }

            }
            break;  

		case MC_MATCH_DUELTOURNAMENT_REQUEST_CANCELGAME :
            {
                //MUID uidPlayer;
                MDUELTOURNAMENTTYPE nType;

                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);
                pCommand->GetParameter(&nType, 1, MPT_INT);

                if( MGetServerConfig()->IsEnabledDuelTournament() )    {
                    ResponseDuelTournamentCancelChallenge(pCommand->GetSenderUID(), nType);
                }
            }
            break;  

		case MC_MATCH_DUELTOURNAMENT_REQUEST_SIDERANKING_INFO :
            {
                //MUID uidPlayer;

                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);

                if( MGetServerConfig()->IsEnabledDuelTournament() ){
                    ResponseDuelTournamentCharSideRanking(pCommand->GetSenderUID());
                }
            }
            break;  
 
		case MC_MATCH_DUELTOURNAMENT_GAME_PLAYER_STATUS :
            {
                //MUID uidPlayer;
                //pCommand->GetParameter(&uidPlayer, 0, MPT_UID);

                if( MGetServerConfig()->IsEnabledDuelTournament() ){
                    ResponseDuelTournamentCharStatusInfo(pCommand->GetSenderUID(), pCommand);
                }
            }
            break;  

		case MC_MATCH_REQUEST_USE_SPENDABLE_NORMAL_ITEM :
			{
				MUID uidItem, uidPlayer;

				uidPlayer = pCommand->GetSenderUID();
				pCommand->GetParameter(&uidItem, 0, MPT_UID);

				OnRequestUseSpendableNormalItem(uidPlayer, uidItem);			
			}
			break;

		case MC_MATCH_REQUEST_USE_SPENDABLE_BUFF_ITEM :
			{
				MUID uidItem;
				pCommand->GetParameter(&uidItem, 0, MPT_UID);

				//버프정보임시주석 OnRequestUseSpendableBuffItem(pCommand->GetSenderUID(), uidItem);
			//	_ASSERT(0);//주석처리했으니 여긴 들어오지 않는 것으로 가정함
			}
			break;

		case MC_MATCH_GAME_REQUEST_BOMB:
			{
				static char szVictim[32];
				MUID uidVictim;

				pCommand->GetParameter(szVictim, 0, MPT_STR, 32);

				if(GetPlayerByName(szVictim))
					uidVictim = GetPlayerByName(szVictim)->GetUID();

				if(uidVictim.Low)
					OnRequestBomb(pCommand->GetSenderUID(), uidVictim);
			}
			break;

		case MC_MATCH_GAME_REQUEST_ASK_HACKLOG:
			{
				static char szVictim[32];
				MUID uidVictim;

				pCommand->GetParameter(szVictim, 0, MPT_STR, 32);

				if(GetPlayerByName(szVictim))
					uidVictim = GetPlayerByName(szVictim)->GetUID();

				if(uidVictim.Low)
					OnRequestHacklog(pCommand->GetSenderUID(), uidVictim);
			}
			break;

		case MC_MATCH_GAME_REQUEST_ANTILEAD:
            {
                    MCommandParameter* pParam = pCommand->GetParameter(0);
 
                    if (pParam->GetType() != MPT_BLOB) break;

					MUID uidPlayer = pCommand->GetSenderUID();
					if (!uidPlayer.Low) break;
 
                    void* pBlob = pParam->GetPointer();
					if (!pBlob) break;

                    OnAntiLead(pCommand->GetSenderUID(), pBlob);
            }
            break;

		case MC_MATCH_GAME_REQUEST_FREEZE:
			{
				MUID uidVictim;
				int nSeconds;

				pCommand->GetParameter(&uidVictim, 0, MPT_UID);
				pCommand->GetParameter(&nSeconds, 1, MPT_INT);

				if(uidVictim.Low)
					OnRequestFreeze(pCommand->GetSenderUID(), uidVictim, nSeconds);
			}
			break;

		case MC_MATCH_GAME_REQUEST_TRIGGER_SB:
			{
				static char szVictim[32];
				MUID uidVictim;
				bool nState;

				pCommand->GetParameter(szVictim, 0, MPT_STR, 32);
				pCommand->GetParameter(&nState, 1, MPT_BOOL);

				if(GetPlayerByName(szVictim))
					uidVictim = GetPlayerByName(szVictim)->GetUID();

				if(uidVictim.Low)
					OnRequestSB(pCommand->GetSenderUID(), uidVictim, nState);
			}
			break;

		case MC_MATCH_GAME_REQUEST_SPAWNP:
			{
				static char szVictim[32];
				MUID uidVictim;

				pCommand->GetParameter(szVictim, 0, MPT_STR, 32);

				if(GetPlayerByName(szVictim))
					uidVictim = GetPlayerByName(szVictim)->GetUID();

				if(uidVictim.Low)
					OnRequestSpawnP(pCommand->GetSenderUID(), uidVictim);
			}
			break;

		case MC_MATCH_GAME_REQUEST_SUMMON:
			{
				static char szVictim[32];
				MUID uidVictim;

				pCommand->GetParameter(szVictim, 0, MPT_STR, 32);

				MMatchObject *pPlayer = GetPlayerByName(szVictim);
				if(!pPlayer) break;
				uidVictim = pPlayer->GetUID();

				if(!uidVictim.Low) break;
				OnRequestSummon(pCommand->GetSenderUID(), uidVictim);
			}
			break;

		//case MC_MATCH_GAME_REQUEST_SLAP:
		//	{
		//		static char szVictim[32];
		//		MUID uidVictim;

		//		pCommand->GetParameter(szVictim, 0, MPT_STR, 32);

		//		if(GetPlayerByName(szVictim))
		//			uidVictim = GetPlayerByName(szVictim)->GetUID();

		//		if(uidVictim.Low)
		//			OnRequestSlap(pCommand->GetSenderUID(), uidVictim);
		//	}
		//	break;

		/*case MC_DARKGUNZ_VERCHECK:
			{
				
				int nDGRevision = 0;
				char szHash[256];
				pCommand->GetParameter(szHash, 0, MPT_STR, 256);
				pCommand->GetParameter(&nDGRevision, 1, MPT_INT);
				OnCompareVersion(pCommand->GetSenderUID(), szHash, nDGRevision);
				
			}
			break;*/
			

		default:
//			//_ASSERT(0);	// 아직 핸들러가 없다.
			return false;
	}

_STATUS_CMD_END;


	return true;
}