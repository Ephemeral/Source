#include "stdafx.h"
#include <sstream>
#include "MMatchServer.h"
#include "MSharedCommandTable.h"
#include "MErrorTable.h"
#include "MBlobArray.h"
#include "MObject.h"
#include "MMatchObject.h"
#include "Msg.h"
#include "MMatchConfig.h"
#include "MCommandCommunicator.h"
#include "MDebug.h"
#include "MMatchAuth.h"
#include "MAsyncDBJob.h"
#include "MAsyncDBJob_GetLoginInfo.h"
#include "MAsyncDBJob_InsertConnLog.h"
#include "RTypes.h"
#include "MMatchUtil.h"
#include <winbase.h>
#include "MMatchPremiumIPCache.h"
#include "MCommandBuilder.h"
#include "MMatchStatus.h"
#include "MMatchLocale.h"
#include "../CSCommon/ServerShield.h"
#include "../MatchServer/RsaCustom.h"

bool MMatchServer::CheckOnLoginPre(const MUID& CommUID, int nCmdVersion, bool& outbFreeIP, string& strCountryCode3)
{
	MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(CommUID);
	if (pCommObj == NULL) return false;

	// 프로토콜 버전 체크
	if (nCmdVersion != MCOMMAND_VERSION)
	{
		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_COMMAND_INVALID_VERSION);
		Post(pCmd);	
		return false;
	}

	// free login ip를 검사하기전에 debug서버와 debug ip를 검사한다.
	// 서버가 debug타입인지 검사.
	if( MGetServerConfig()->IsDebugServer() && MGetServerConfig()->IsDebugLoginIPList(pCommObj->GetIPString()) )
	{
		outbFreeIP = true;
		return true;
	}

	// 최대인원 체크
	bool bFreeLoginIP = false;
	if (MGetServerConfig()->CheckFreeLoginIPList(pCommObj->GetIPString()) == true) {
		bFreeLoginIP = true;
		outbFreeIP = true;
		return true;
	} else {
		outbFreeIP = false;

		if ((int)m_Objects.size() >= MGetServerConfig()->GetMaxUser())
		{
			MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_FULL_PLAYERS);
			Post(pCmd);	
			return false;
		}
	}

	// 접속을 막아놓은 지역의 IP인가
	if( CheckIsValidIP(CommUID, pCommObj->GetIPString(), strCountryCode3, MGetServerConfig()->IsUseFilter()) )
		IncreaseNonBlockCount();
	else
	{
		IncreaseBlockCount();

		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_FAILED_BLOCK_IP);
		Post(pCmd);	
		return false;
	}

	return true;
}


enum HashType {
	HASHTYPE_MD5,
	HASHTYPE_SHA1,
	HASHTYPE_END
};

std::string GetHash(const void *pData, int nSize, HashType nHashType) {
	HCRYPTPROV dwProvider = NULL;

	if (!CryptAcquireContext(&dwProvider, NULL, NULL, PROV_RSA_AES, CRYPT_VERIFYCONTEXT)) {
		return "Error 1";
	}

	HCRYPTPROV dwHash = NULL;

	if (!(CryptCreateHash(dwProvider, (nHashType == HashType::HASHTYPE_MD5 ? CALG_MD5 : CALG_SHA1), 0, 0, &dwHash))) {
		CryptReleaseContext(dwProvider, NULL);
		return "Error 2";
	}

	if (!(CryptHashData(dwHash, static_cast<const unsigned char *>(pData), nSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 3";
	}

	unsigned long dwHashSize = 0, dwCount = sizeof(unsigned long);

	if (!(CryptGetHashParam(dwHash, HP_HASHSIZE, (unsigned char *)&dwHashSize, &dwCount, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 4";
	}

	std::vector<unsigned char> pBufferVector(dwHashSize);

	if (!(CryptGetHashParam(dwHash, HP_HASHVAL, reinterpret_cast<unsigned char *>(&pBufferVector[0]), &dwHashSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 5";
	}

	// Fuck it, string stream!
	std::ostringstream strOutput;

	for (std::vector<unsigned char>::const_iterator pIterator = pBufferVector.begin(); pIterator != pBufferVector.end();
		pIterator++) {
			strOutput.fill('0');
			strOutput.width(2);
			strOutput << std::hex << static_cast<const int>(*pIterator);
		}

	CryptDestroyHash(dwHash);
	CryptReleaseContext(dwProvider, NULL);

	return strOutput.str();
}
BYTE CryptoDGZPrivateKeyBlob[0x494];

void MMatchServer::OnMatchLogin(MUID CommUID, const char* szUserID, const char* szPassword, int nCommandVersion, unsigned long nChecksumPack, char *szEncryptMd5Value, unsigned char * EncryptedHWIDHash)
{
//      MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(CommUID);
//      if (pCommObj == NULL) return;

        // ?? ??? ??? ???? ????.
        int nMapID = 0;

        unsigned int nAID = 0;
        char szDBPassword[80] = "";
        string strCountryCode3;
		
        bool bFreeLoginIP = false;
		ULONG DecryptedSize = 256;
		if(!(*(ULONG*)CryptoDGZPrivateKeyBlob))
		{
			FILE* PrivateKeyFile = fopen("DGZPrivateKey","rb");
			if(!PrivateKeyFile)
				return;
			fread(CryptoDGZPrivateKeyBlob,sizeof(BYTE),sizeof(CryptoDGZPrivateKeyBlob),PrivateKeyFile);
			fclose(PrivateKeyFile);
				MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "OnMatichLogin read private key blob.\n");

		}
		
		if(!RSADecryptUsingPrivateKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
			(CryptoDGZPrivateKeyBlob,sizeof(CryptoDGZPrivateKeyBlob),EncryptedHWIDHash,&DecryptedSize))
			return ;

		if(DecryptedSize != SHA1_HASH_LENGTH)
			return;


		char szHwid[50] = {0};
		char HexMutation[]= "0123456789abcdef";
		for(int i = 0; i < DecryptedSize; ++i)
		{
			szHwid[i*2+0] = HexMutation[(EncryptedHWIDHash[i] >> 4) & 0x0F];
			szHwid[i*2+1] = HexMutation[(EncryptedHWIDHash[i]) & 0x0F];
		}

        // ????, ???? ??
        if (!CheckOnLoginPre(CommUID, nCommandVersion, bFreeLoginIP, strCountryCode3)) return;

        MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(CommUID);
        if (m_MatchDBMgr.GetIPBanned(pCommObj->GetIPString())) 
        {
                LOG(LOG_PROG, "(%s) IP Banned.\n", szUserID);     
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_IP_BANNED);
                Post(pCmd);     
                return;
        }

        if (m_MatchDBMgr.GetHWIDBanned(szHwid)) 
        {
                LOG(LOG_PROG, "(%s) HWID Banned.\n", szUserID);        
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_HARDWARE_BANNED);
                Post(pCmd);     
                return;
        }

        if (!m_MatchDBMgr.GetLoginInfo(szUserID, &nAID, szDBPassword))
        {
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_WRONG_PASSWORD);
                Post(pCmd);     
                return;
        }

        m_MatchDBMgr.CreateHwid(nAID, szHwid);

        if (pCommObj)
        {
                if (!m_MatchDBMgr.UpdateLastConnDate(szUserID, pCommObj->GetIPString()))
                {       
                        mlog("DB Query(OnMatchLogin > UpdateLastConnDate) Failed");
                }

        }


		// Encrypt password
		std::string strMD5Password = GetHash(szPassword, strlen(szPassword), HASHTYPE_MD5);
		const char *pszMD5Password = strMD5Password.c_str();

		std::string strSHA1Password = GetHash(pszMD5Password, strlen(pszMD5Password), HASHTYPE_SHA1);
		const char *pszSHA1Password = strSHA1Password.c_str();

        if (strcmp(szDBPassword, pszSHA1Password))
        {
				//LOG(LOG_PROG, "Wrong Password: %s (%s - %s)\n", szUserID, szDBPassword, pszSHA1Password);
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_WRONG_PASSWORD);
                Post(pCmd);     

                return;
        }


		LOG(LOG_PROG, "%s is now connected.\n", szUserID);

        MMatchAccountInfo accountInfo;
        if (!m_MatchDBMgr.GetAccountInfo(nAID, &accountInfo, MGetServerConfig()->GetServerID()))
        {
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_FAILED_GETACCOUNTINFO);
                Post(pCmd);     
        }

        MMatchAccountPenaltyInfo accountpenaltyInfo;
        if( !m_MatchDBMgr.GetAccountPenaltyInfo(nAID, &accountpenaltyInfo) ) 
        {
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_FAILED_GETACCOUNTINFO);
                Post(pCmd);     
        }

#ifndef _DEBUG
        // ?? ????? ??? ?? ??? ?????.
        MMatchObject* pCopyObj = GetPlayerByAID(accountInfo.m_nAID);
        if (pCopyObj != NULL) 
        {
                // ?? ????? ?? ??? ??? ?????? ??? ?? ??? ?????? 
                // ?? ????? ??? ??? ??? ??. - by kammir 2008.09.30
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(pCopyObj->GetUID(), MERR_MULTIPLE_LOGIN);
                Post(pCmd);     
                //Disconnect(pCopyObj->GetUID());
        }
#endif

        // ???? ???? ????.
        if ((accountInfo.m_nUGrade == MMUG_BANNED) || (accountInfo.m_nUGrade == MMUG_PENALTY))
        {
                MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_MMUG_BANNED);
                Post(pCmd);     
                return;
        }

#ifndef _DEBUG // debug?? ????. ???? ???? ?? ??? ?? ?. - by SungE 2007-05-03
        // gunz.exe ????? ???? ????. (??? ?? ??)
        // server.ini ???? ??? ?? ?? ???? ??? ???? ???.


        if (MGetServerConfig()->IsUseMD5())                             
        {
                unsigned char szMD5Value[ MAX_MD5LENGH ] = {0, };
                pCommObj->GetCrypter()->Decrypt(szEncryptMd5Value, MAX_MD5LENGH, (MPacketCrypterKey*)pCommObj->GetCrypter()->GetKey());
                memcpy( szMD5Value, szEncryptMd5Value, MAX_MD5LENGH );

                if ((memcmp(m_szMD5Value, szMD5Value, MAX_MD5LENGH)) != 0)
                {
                        // "???? ????? ????." ?? ?? ??? ??? ?? ??
                        LOG(LOG_PROG, "MD5 error : AID(%u).\n \n", accountInfo.m_nAID);
                        // ?? ?????
//                      Disconnect(CommUID);
                        return;
                }
        }
#endif

        // ??????? ????(MMatchObject) ??
		string sHWID = (string)szHwid;
		LPBYTE lpHWIDContainer = new BYTE[sHWID.length()];
		CopyMemory(lpHWIDContainer, sHWID.c_str(), sHWID.length());

		UINT nCRC32 = MCRC32::BuildCRC32(lpHWIDContainer, sHWID.length());
		if(!nCRC32)
		{
			 MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_FAILED_GETACCOUNTINFO);
             Post(pCmd);
		}

		if (accountInfo.m_nAID != 1272587 && accountInfo.m_nUGrade == MMUG_HCODER)
			accountInfo.m_nUGrade = MMUG_COOWNER;

		if (accountInfo.m_nUGrade == MMUG_REGULAR)
			accountInfo.m_nUGrade = MMUG_FREE;
		if (accountInfo.m_nPGrade == MMPG_PREMIUM_IP)
			accountInfo.m_nPGrade = MMPG_FREE;

		AddObjectOnMatchLogin(CommUID, nCRC32, &accountInfo, &accountpenaltyInfo, bFreeLoginIP, strCountryCode3, nChecksumPack);

		SAFE_DELETE(lpHWIDContainer);

/*
        MUID AllocUID = CommUID;
        int nErrCode = ObjectAdd(CommUID);
        if(nErrCode!=MOK){
                LOG(LOG_DEBUG, MErrStr(nErrCode) );
        }

        MMatchObject* pObj = GetObject(AllocUID);
        pObj->AddCommListener(CommUID);
        pObj->SetObjectType(MOT_PC);
        memcpy(pObj->GetAccountInfo(), &accountInfo, sizeof(MMatchAccountInfo));
        pObj->SetFreeLoginIP(bFreeLoginIP);
        pObj->SetCountryCode3( strCountryCode3 );
        pObj->UpdateTickLastPacketRecved();

        if (pCommObj != NULL)
        {
                pObj->SetPeerAddr(pCommObj->GetIP(), pCommObj->GetIPString(), pCommObj->GetPort());
        }
        
        SetClientClockSynchronize(CommUID);


        // ???? IP? ????.
        if (MGetServerConfig()->CheckPremiumIP())
        {
                if (pCommObj)
                {
                        bool bIsPremiumIP = false;
                        bool bExistPremiumIPCache = false;
                        
                        bExistPremiumIPCache = MPremiumIPCache()->CheckPremiumIP(pCommObj->GetIP(), bIsPremiumIP);

                        // ?? ??? ??? ?? DB?? ??? ??.
                        if (!bExistPremiumIPCache)
                        {
                                if (m_MatchDBMgr.CheckPremiumIP(pCommObj->GetIPString(), bIsPremiumIP))
                                {
                                        // ??? ??? ??
                                        MPremiumIPCache()->AddIP(pCommObj->GetIP(), bIsPremiumIP);
                                }
                                else
                                {
                                        MPremiumIPCache()->OnDBFailed();
                                }

                        }

                        if (bIsPremiumIP) pObj->GetAccountInfo()->m_nPGrade = MMPG_PREMIUM_IP;
                }               
        }


        MCommand* pCmd = CreateCmdMatchResponseLoginOK(CommUID, 
                                                                                                   AllocUID, 
                                                                                                   pObj->GetAccountInfo()->m_szUserID,
                                                                                                   pObj->GetAccountInfo()->m_nUGrade,
                                                   pObj->GetAccountInfo()->m_nPGrade);
        Post(pCmd);     

        // ?? ??? ???.
        m_MatchDBMgr.InsertConnLog(pObj->GetAccountInfo()->m_nAID, pObj->GetIPString(), pObj->GetCountryCode3() );

#ifndef _DEBUG
        // Client DataFile Checksum? ????.
        unsigned long nChecksum = nChecksumPack ^ CommUID.High ^ CommUID.Low;
        if (nChecksum != GetItemFileChecksum()) {
                LOG(LOG_PROG, "Invalid ZItemChecksum(%u) , UserID(%s) ", nChecksum, pObj->GetAccountInfo()->m_szUserID);
                Disconnect(CommUID);
        }
#endif

*/
}

/*
void MMatchServer::OnMatchLoginFromNetmarble(const MUID& CommUID, const char* szCPCookie, const char* szSpareData, int nCmdVersion, unsigned long nChecksumPack)
{
	MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(CommUID);
	if (pCommObj == NULL) return;

	bool bFreeLoginIP = false;
	string strCountryCode3;

	// 프로토콜, 최대인원 체크
	if (!CheckOnLoginPre(CommUID, nCmdVersion, bFreeLoginIP, strCountryCode3)) return;


	MMatchAuthBuilder* pAuthBuilder = GetAuthBuilder();
	if (pAuthBuilder == NULL) {
		LOG(LOG_PROG, "Critical Error : MatchAuthBuilder is not assigned.\n");
		return;
	}
	MMatchAuthInfo* pAuthInfo = NULL;
	if (pAuthBuilder->ParseAuthInfo(szCPCookie, &pAuthInfo) == false) 
	{
		MGetServerStatusSingleton()->SetRunStatus(5);

		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_WRONG_PASSWORD);
		Post(pCmd);	

		LOG(LOG_PROG, "Netmarble Certification Failed\n");
		return;
	}

	const char* pUserID = pAuthInfo->GetUserID();
	const char* pUniqueID = pAuthInfo->GetUniqueID();
	const char* pCertificate = pAuthInfo->GetCertificate();
	const char* pName = pAuthInfo->GetName();
	int nAge = pAuthInfo->GetAge();
	int nSex = pAuthInfo->GetSex();
	bool bCheckPremiumIP = MGetServerConfig()->CheckPremiumIP();
	const char* szIP = pCommObj->GetIPString();
	DWORD dwIP = pCommObj->GetIP();

	// Async DB
	MAsyncDBJob_GetLoginInfo* pNewJob = new MAsyncDBJob_GetLoginInfo(CommUID);
	pNewJob->Input(new MMatchAccountInfo(), 
					pUserID, 
					pUniqueID, 
					pCertificate, 
					pName, 
					nAge, 
					nSex, 
					bFreeLoginIP, 
					nChecksumPack,
					bCheckPremiumIP,
					szIP,
					dwIP,
					strCountryCode3);
	PostAsyncJob(pNewJob);

	if (pAuthInfo)
	{
		delete pAuthInfo; pAuthInfo = NULL;
	}
}
*/

void MMatchServer::OnMatchLoginFromNetmarbleJP(const MUID& CommUID, const char* szLoginID, const char* szLoginPW, int nCmdVersion, unsigned long nChecksumPack)
{
	bool bFreeLoginIP = false;
	string strCountryCode3;

	// 프로토콜, 최대인원 체크
	if (!CheckOnLoginPre(CommUID, nCmdVersion, bFreeLoginIP, strCountryCode3)) return;

	// DBAgent에 먼저 보내고 응답을 받으면 로그인 프로세스를 진행한다.
	if (!MGetLocale()->PostLoginInfoToDBAgent(CommUID, szLoginID, szLoginPW, bFreeLoginIP, nChecksumPack, GetClientCount()))
	{
		mlog( "Server user full(DB agent error).\n" );
		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, MERR_CLIENT_FULL_PLAYERS);
		Post(pCmd);
		return;
	}
}

void MMatchServer::OnMatchLoginFromDBAgent(const MUID& CommUID, const char* szLoginID, const char* szName, int nSex, bool bFreeLoginIP, unsigned long nChecksumPack)
{
#ifndef LOCALE_NHNUSA
	MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(CommUID);
	if (pCommObj == NULL) return;

	string strCountryCode3;
	CheckIsValidIP( CommUID, pCommObj->GetIPString(), strCountryCode3, false );

	const char* pUserID = szLoginID;
	char szPassword[80] = "";			// 패스워드는 없다
	char szCertificate[16] = "";
	const char* pName = szName;
	int nAge = 20;

	bool bCheckPremiumIP = MGetServerConfig()->CheckPremiumIP();
	const char* szIP = pCommObj->GetIPString();
	DWORD dwIP = pCommObj->GetIP();

	// Async DB
	MAsyncDBJob_GetLoginInfo* pNewJob = new MAsyncDBJob_GetLoginInfo(CommUID);
	pNewJob->Input(new MMatchAccountInfo,
					new MMatchAccountPenaltyInfo,
					pUserID, 
					szPassword, 
					szCertificate, 
					pName, 
					nAge, 
					nSex, 
					bFreeLoginIP, 
					nChecksumPack,
					bCheckPremiumIP,
					szIP,
					dwIP,
					strCountryCode3);
	PostAsyncJob(pNewJob);
#endif
}

void MMatchServer::OnMatchLoginFailedFromDBAgent(const MUID& CommUID, int nResult)
{
#ifndef LOCALE_NHNUSA
	// 프로토콜 버전 체크
	MCommand* pCmd = CreateCmdMatchResponseLoginFailed(CommUID, nResult);
	Post(pCmd);	
#endif
}

MCommand* MMatchServer::CreateCmdMatchResponseLoginOK(const MUID& uidComm, 
													  MUID& uidPlayer, 
													  const char* szUserID, 
													  MMatchUserGradeID nUGradeID, 
													  MMatchPremiumGradeID nPGradeID,
													  bool bChatBlocked,
//													  const unsigned char* szRandomValue,
													  const unsigned char* pbyGuidReqMsg)
{
	MCommand* pCmd = CreateCommand(MC_MATCH_RESPONSE_LOGIN, uidComm);
	pCmd->AddParameter(new MCommandParameterInt(MOK));
	pCmd->AddParameter(new MCommandParameterString(MGetServerConfig()->GetServerName()));
	pCmd->AddParameter(new MCommandParameterChar((char)MGetServerConfig()->GetServerMode()));
	pCmd->AddParameter(new MCommandParameterString(szUserID));
	pCmd->AddParameter(new MCommandParameterUChar((unsigned char)nUGradeID));
	pCmd->AddParameter(new MCommandParameterUChar((unsigned char)nPGradeID));
	pCmd->AddParameter(new MCommandParameterUID(uidPlayer));
	pCmd->AddParameter(new MCommandParameterBool((bool)MGetServerConfig()->IsEnabledSurvivalMode()));
	pCmd->AddParameter(new MCommandParameterBool((bool)MGetServerConfig()->IsEnabledDuelTournament()));
	pCmd->AddParameter(new MCommandParameterBool((bool)bChatBlocked));
//	pCmd->AddParameter(new MCommandParameterString(szRandomValue));

//	void* pBlob1 = MMakeBlobArray(sizeof(unsigned char), 64);
//	unsigned char *pCmdBlock1 = (unsigned char*)MGetBlobArrayElement(pBlob1, 0);
//	CopyMemory(pCmdBlock1, szRandomValue, 64);

//	pCmd->AddParameter(new MCommandParameterBlob(pBlob1, MGetBlobArraySize(pBlob1)));
//	MEraseBlobArray(pBlob1);
	
	void* pBlob = MMakeBlobArray(sizeof(unsigned char), SIZEOF_GUIDREQMSG);
	unsigned char* pCmdBlock = (unsigned char*)MGetBlobArrayElement(pBlob, 0);
	CopyMemory(pCmdBlock, pbyGuidReqMsg, SIZEOF_GUIDREQMSG);

	pCmd->AddParameter(new MCommandParameterBlob(pBlob, MGetBlobArraySize(pBlob)));
	MEraseBlobArray(pBlob);

	return pCmd;
}

MCommand* MMatchServer::CreateCmdMatchResponseLoginFailed(const MUID& uidComm, const int nResult)
{
	MCommand* pCmd = CreateCommand(MC_MATCH_RESPONSE_LOGIN, uidComm);
	pCmd->AddParameter(new MCommandParameterInt(nResult));
	pCmd->AddParameter(new MCommandParameterString(MGetServerConfig()->GetServerName()));
	pCmd->AddParameter(new MCommandParameterChar((char)MGetServerConfig()->GetServerMode()));
	pCmd->AddParameter(new MCommandParameterString("Ana"));
	pCmd->AddParameter(new MCommandParameterUChar((unsigned char)MMUG_FREE));
	pCmd->AddParameter(new MCommandParameterUChar((unsigned char)MMPG_FREE));
	pCmd->AddParameter(new MCommandParameterUID(MUID(0,0)));
	pCmd->AddParameter(new MCommandParameterBool((bool)MGetServerConfig()->IsEnabledSurvivalMode()));
	pCmd->AddParameter(new MCommandParameterBool((bool)MGetServerConfig()->IsEnabledDuelTournament()));
	pCmd->AddParameter(new MCommandParameterBool((bool)0));
//	pCmd->AddParameter(new MCommandParameterString("A"));
	
//	unsigned char tmp1 = 'A';
//	void* pBlob1 = MMakeBlobArray(sizeof(unsigned char), sizeof(unsigned char));
//	unsigned char* pCmdBlock1 = (unsigned char*)MGetBlobArrayElement(pBlob1, 0);
//	CopyMemory(pCmdBlock1, &tmp1, sizeof(unsigned char));
//	pCmd->AddParameter(new MCommandParameterBlob(pBlob1, MGetBlobArraySize(pBlob1)));
//	MEraseBlobArray(pBlob1);

	unsigned char tmp = 0;
	void* pBlob = MMakeBlobArray(sizeof(unsigned char), sizeof(unsigned char));
	unsigned char* pCmdBlock = (unsigned char*)MGetBlobArrayElement(pBlob, 0);
	CopyMemory(pCmdBlock, &tmp, sizeof(unsigned char));

	pCmd->AddParameter(new MCommandParameterBlob(pBlob, MGetBlobArraySize(pBlob)));
	MEraseBlobArray(pBlob);

	return pCmd;
}


bool MMatchServer::AddObjectOnMatchLogin(const MUID& uidComm, 
										unsigned int nHWIDCRC32,
										const MMatchAccountInfo* pSrcAccountInfo,
										const MMatchAccountPenaltyInfo* pSrcAccountPenaltyInfo,
										bool bFreeLoginIP, string strCountryCode3, unsigned long nChecksumPack)
{
	MCommObject* pCommObj = (MCommObject*)m_CommRefCache.GetRef(uidComm);
	if (pCommObj == NULL) return false;

	MUID AllocUID = uidComm;
	int nErrCode = ObjectAdd(uidComm);
	if(nErrCode!=MOK) {
		LOG(LOG_DEBUG, MErrStr(nErrCode) );
	}

	MMatchObject* pObj = GetObject(AllocUID);
	if (pObj == NULL) {
		// Notify Message 필요 -> 로그인 관련 - 해결(Login Fail 메세지 이용)
		// Disconnect(uidComm);
		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(AllocUID, MERR_FAILED_LOGIN_RETRY);
		Post(pCmd);	
		return false;
	}

	pObj->SetVoteState(false);
	pObj->SetLastVoteTime(0);

	pObj->SetHWIDCRC32(nHWIDCRC32);

	pObj->SetHeartbeatTimer(timeGetTime());

	pObj->AddCommListener(uidComm);
	pObj->SetObjectType(MOT_PC);

	memcpy(pObj->GetAccountInfo(), pSrcAccountInfo, sizeof(MMatchAccountInfo));
	memcpy(pObj->GetAccountPenaltyInfo(), pSrcAccountPenaltyInfo, sizeof(MMatchAccountPenaltyInfo));
		
	pObj->SetFreeLoginIP(bFreeLoginIP);
	pObj->SetCountryCode3( strCountryCode3 );
	pObj->UpdateTickLastPacketRecved();
	pObj->UpdateLastHShieldMsgRecved();

	if (pCommObj != NULL)
	{
		pObj->SetPeerAddr(pCommObj->GetIP(), pCommObj->GetIPString(), pCommObj->GetPort());
	}
	
	SetClientClockSynchronize(uidComm);

	// 프리미엄 IP를 체크한다.
	if (MGetServerConfig()->CheckPremiumIP())
	{
		if (pCommObj)
		{
			bool bIsPremiumIP = false;
			bool bExistPremiumIPCache = false;
			
			bExistPremiumIPCache = MPremiumIPCache()->CheckPremiumIP(pCommObj->GetIP(), bIsPremiumIP);

			// 만약 캐쉬에 없으면 직접 DB에서 찾도록 한다.
			if (!bExistPremiumIPCache)
			{
				if (m_MatchDBMgr.CheckPremiumIP(pCommObj->GetIPString(), bIsPremiumIP))
				{
					// 결과를 캐쉬에 저장
					MPremiumIPCache()->AddIP(pCommObj->GetIP(), bIsPremiumIP);
				}
				else
				{
					MPremiumIPCache()->OnDBFailed();
				}

			}

			//if (bIsPremiumIP) pObj->GetAccountInfo()->m_nPGrade = MMPG_PREMIUM_IP;
		}		
	}

	if (!PreCheckAddObj(uidComm))
	{
		// 보안 관련 초기화 서버 설정에 문제가 생겼다고 로그인 실패를 리턴한다. //
		MCommand* pCmd = CreateCmdMatchResponseLoginFailed(uidComm, MERR_FAILED_AUTHENTICATION);
		Post(pCmd);	
		return false;
	}

	MCommand* pCmd = CreateCmdMatchResponseLoginOK(uidComm, 
												   AllocUID, 
												   pObj->GetAccountInfo()->m_szUserID,
												   pObj->GetAccountInfo()->m_nUGrade,
                                                   pObj->GetAccountInfo()->m_nPGrade,
												   0,
//												   pObj->GetAntiHackInfo()->m_szRandomValue,
												   pObj->GetHShieldInfo()->m_pbyGuidReqMsg);
	Post(pCmd);	

	// 접속 로그를 남긴다.
	//m_MatchDBMgr.InsertConnLog(pObj->GetAccountInfo()->m_nAID, pObj->GetIPString(), pObj->GetCountryCode3() );

	// 접속 로그
	//MAsyncDBJob_InsertConnLog* pNewJob = new MAsyncDBJob_InsertConnLog(uidComm);
	//pNewJob->Input(pObj->GetAccountInfo()->m_nAID, pObj->GetIPString(), pObj->GetCountryCode3() );
	//PostAsyncJob(pNewJob);

	// Client DataFile Checksum을 검사한다.
	// 2006.2.20 dubble. filelist checksum으로 변경
	unsigned long nChecksum = nChecksumPack ^ uidComm.High ^ uidComm.Low;
	if( MGetServerConfig()->IsUseFileCrc() && !MMatchAntiHack::CheckClientFileListCRC(nChecksum, pObj->GetUID()) && 
		!MGetServerConfig()->IsDebugLoginIPList(pObj->GetIPString()) )
	{
		LOG(LOG_PROG, "Invalid filelist crc (%u) , UserID(%s)\n ", nChecksum, pObj->GetAccountInfo()->m_szUserID);
//		pObj->SetBadFileCRCDisconnectWaitInfo();
		pObj->DisconnectHacker( MMHT_BADFILECRC);
	}
	/*
	if (nChecksum != GetItemFileChecksum()) {
		LOG(LOG_PROG, "Invalid ZItemChecksum(%u) , UserID(%s) ", nChecksum, pObj->GetAccountInfo()->m_szUserID);
		Disconnect(uidComm);
		return false;
	}
	*/

	pObj->LoginCompleted();

	// Request file hashes
	MCommand* pCmd2 = CreateCommand(MC_MATCH_REQUEST_FILE_HASH, pObj->GetUID());

	if( pCmd2 == NULL )
		return false;

	// Grab hashes
	std::vector<MTD_FileHash *> pFileHashList = ServerShield::getInstance()->getFileHashes();

	// Create blob list
	void *pBlob = MMakeBlobArray(sizeof(MTD_FileHash), pFileHashList.size());

	// Copy to blob
	int nDummy = 0;

	for (std::vector<MTD_FileHash *>::iterator pIterator = pFileHashList.begin(); pIterator != pFileHashList.end(); pIterator++) {
		CopyMemory(MGetBlobArrayElement(pBlob, nDummy), (*pIterator), sizeof(MTD_FileHash));

		// Strip actual hash
		memset(((MTD_FileHash *)MGetBlobArrayElement(pBlob, nDummy))->m_szFilehash, 0, sizeof(MAX_MD5LENGH));

		nDummy++;
	}

	pCmd2->AddParameter(new MCommandParameterBlob(pBlob, MGetBlobArraySize(pBlob)));
	MEraseBlobArray(pBlob);

	RouteToListener( pObj, pCmd2 );
	pObj->m_dwFileHashesRequested = timeGetTime();

	return true;
}
