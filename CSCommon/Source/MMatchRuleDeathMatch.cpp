#include "stdafx.h"
#include "MMatchRuleDeathMatch.h"
#include "MMatchTransDataType.h"
#include "MBlobArray.h"
#include "MMatchFormula.h"

// TEAM DEATH RULE ///////////////////////////////////////////////////////////////
MMatchRuleTeamDeath::MMatchRuleTeamDeath(MMatchStage* pStage) : MMatchRule(pStage)
{
}

void MMatchRuleTeamDeath::OnBegin()
{
}

void MMatchRuleTeamDeath::OnEnd()
{
}

bool MMatchRuleTeamDeath::OnRun()
{
	bool ret = MMatchRule::OnRun();


	return ret;
}

void MMatchRuleTeamDeath::OnRoundBegin()
{
	MMatchRule::OnRoundBegin();
}

void MMatchRuleTeamDeath::OnRoundEnd()
{
	if (m_pStage != NULL)
	{
		switch(m_nRoundArg)
		{
			case MMATCH_ROUNDRESULT_BLUE_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED);break;
			case MMATCH_ROUNDRESULT_RED_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_REDWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED); break;
			case MMATCH_ROUNDRESULT_BLUEWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_DRAW: break;
		}
	}

	MMatchRule::OnRoundEnd();
}

bool MMatchRuleTeamDeath::OnCheckEnableBattleCondition()
{
	// 선승제일 경우는 Free상태가 안된다.
	if (m_pStage->GetStageSetting()->IsTeamWinThePoint() == true)
	{
		return true;
	}

	int nRedTeam = 0, nBlueTeam = 0;
	int nPreRedTeam = 0, nPreBlueTeam = 0;
	int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
		{
			nStageObjects++;
			continue;
		}

		if (pObj->GetTeam() == MMT_RED)
		{
			nRedTeam++;
		}
		else if (pObj->GetTeam() == MMT_BLUE)
		{
			nBlueTeam++;
		}
	}

	if ( nRedTeam == 0 || nBlueTeam == 0)
	{
		return false;
	}

	return true;
}

// 만약 레드팀이나 블루팀에서 팀원이 0명일 경우는 false 반환 , true,false 모두 AliveCount 반환
bool MMatchRuleTeamDeath::GetAliveCount(int* pRedAliveCount, int* pBlueAliveCount)
{
	int nRedCount = 0, nBlueCount = 0;
	int nRedAliveCount = 0, nBlueAliveCount = 0;
	(*pRedAliveCount) = 0;
	(*pBlueAliveCount) = 0;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if (pObj->GetEnterBattle() == false) continue;	// 배틀참가하고 있는 플레이어만 체크

		if (pObj->GetTeam() == MMT_RED)
		{
			nRedCount++;
			if (pObj->CheckAlive()==true)
			{
				nRedAliveCount++;
			}
		}
		else if (pObj->GetTeam() == MMT_BLUE)
		{
			nBlueCount++;
			if (pObj->CheckAlive()==true)
			{
				nBlueAliveCount++;
			}
		}
	}

	(*pRedAliveCount) = nRedAliveCount;
	(*pBlueAliveCount) = nBlueAliveCount;

	if ((nRedAliveCount == 0) || (nBlueAliveCount == 0))
	{
		return false;
	}
	return true;
}

bool MMatchRuleTeamDeath::OnCheckRoundFinish()
{
	int nRedAliveCount = 0;
	int nBlueAliveCount = 0;

	// 팀원이 0명인 팀이 있으면 false반환
	if (GetAliveCount(&nRedAliveCount, &nBlueAliveCount) == false)
	{
		int nRedTeam = 0, nBlueTeam = 0;
		int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

		MMatchStage* pStage = GetStage();

		for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
		{
			MMatchObject* pObj = (MMatchObject*)(*i).second;
			if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			{
				nStageObjects++;
				continue;
			}

			if (pObj->GetTeam() == MMT_RED)		nRedTeam++;
			else if (pObj->GetTeam() == MMT_BLUE)	nBlueTeam++;
		}

		if( nBlueTeam ==0 && (pStage->GetTeamScore(MMT_BLUE) > pStage->GetTeamScore(MMT_RED)) )
			SetRoundArg(MMATCH_ROUNDRESULT_BLUE_ALL_OUT);
		else if( nRedTeam ==0 && (pStage->GetTeamScore(MMT_RED) > pStage->GetTeamScore(MMT_BLUE)) )
			SetRoundArg(MMATCH_ROUNDRESULT_RED_ALL_OUT);
		else if ( (nRedAliveCount == 0) && (nBlueAliveCount == 0) )
			SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
		else if (nRedAliveCount == 0)
			SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
		else if (nBlueAliveCount == 0)
			SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
	}

	if (nRedAliveCount==0 || nBlueAliveCount==0) return true;
	else return false;
}

void MMatchRuleTeamDeath::OnRoundTimeOut()
{
	int nRedAliveCount = 0;
	int nBlueAliveCount = 0;
	GetAliveCount(&nRedAliveCount, &nBlueAliveCount);

	if (nRedAliveCount > nBlueAliveCount)
		SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
	else if (nBlueAliveCount > nRedAliveCount)
		SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
	else SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}

// 반환값이 false이면 게임이 끝난다.
bool MMatchRuleTeamDeath::RoundCount() 
{
	if (m_pStage == NULL) return false;

	int nTotalRound = m_pStage->GetStageSetting()->GetRoundMax();
	m_nRoundCount++;

	if (m_pStage->GetStageSetting()->IsTeamWinThePoint() == false)
	{
		// 선승제가 아닐 경우
		if (m_nRoundCount < nTotalRound) return true;

	}
	else
	{
		// 선승제일 경우 

		// 팀원이 0명인 팀이 있어도 게임이 끝난다.
		int nRedTeamCount=0, nBlueTeamCount=0;
		m_pStage->GetTeamMemberCount(&nRedTeamCount, &nBlueTeamCount, NULL, true);

		if ((nRedTeamCount == 0) || (nBlueTeamCount == 0))
		{
			return false;
		}

		int nRedScore = m_pStage->GetTeamScore(MMT_RED);
		int nBlueScore = m_pStage->GetTeamScore(MMT_BLUE);
		
		// 래더게임에서 먼저 4승인 팀이 승리
		const int LADDER_WINNING_ROUNT_COUNT = 4;


		// 두팀이 모두 4승이 아니면 true반환
		if ((nRedScore < LADDER_WINNING_ROUNT_COUNT) && (nBlueScore < LADDER_WINNING_ROUNT_COUNT))
		{
			return true;
		}
	}

	return false;
}

void MMatchRuleTeamDeath::CalcTeamBonus(MMatchObject* pAttacker, MMatchObject* pVictim,
								int nSrcExp, int* poutAttackerExp, int* poutTeamExp)
{
	if (m_pStage == NULL)
	{
		*poutAttackerExp = nSrcExp;
		*poutTeamExp = 0;
		return;
	}

	*poutTeamExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamBonusExpRatio);
	*poutAttackerExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamMyExpRatio);
}

//Infection
MMatchRuleInfection::MMatchRuleInfection(MMatchStage* pStage) : MMatchRule(pStage)
{
	bStart = false;
	m_uidSource = MUID(0,0);
}

void MMatchRuleInfection::OnBegin()
{
}

void MMatchRuleInfection::OnEnd()
{
}

void MMatchRuleInfection::OnAssignNumber(int nTotalPlayers, int *nPlayerID)
{
	 MTime MRandomGenerator;

	 *nPlayerID = MRandomGenerator.MakeNumber(0, nTotalPlayers-1);
}

bool MMatchRuleInfection::OnAssignInfected()
{
	int nAssignedInfected = 0;
	int nTotalObjects = 0;
	int nCounter = 0;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			continue;
	
		nTotalObjects++;
	}

	if(nTotalObjects < 2)
		return false;

	OnAssignNumber(nTotalObjects, &nAssignedInfected);

	MCommand* pCmd = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_RESET_TEAM_MEMBERS, MUID(0,0));
	
	int nMemberCount = pStage->GetObjInBattleCount();;
	void* pTeamMemberDataArray = MMakeBlobArray(sizeof(MTD_ResetTeamMembersData), nMemberCount);

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;

		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			continue;

		MTD_ResetTeamMembersData* pNode = (MTD_ResetTeamMembersData*)MGetBlobArrayElement(pTeamMemberDataArray, nCounter);
		pNode->m_uidPlayer = pObj->GetUID();

		if(nCounter == nAssignedInfected)
		{
			pStage->PlayerTeam(pObj->GetUID(), MMT_RED);
			m_uidSource = pObj->GetUID();
		}
		else
			pStage->PlayerTeam(pObj->GetUID(), MMT_BLUE);

		pNode->nTeam = pObj->GetTeam();

		nCounter++;
	}

	pCmd->AddParameter(new MCommandParameterBool(true));
	pCmd->AddParameter(new MCommandParameterBlob(pTeamMemberDataArray, MGetBlobArraySize(pTeamMemberDataArray)));
	
	MEraseBlobArray(pTeamMemberDataArray);
	
	MMatchServer::GetInstance()->RouteToBattle(pStage->GetUID(), pCmd);

	pCmd = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_GAME_SOURCE, MUID(0,0));
	pCmd->AddParameter(new MCommandParameterUID(m_uidSource));

	MMatchServer::GetInstance()->RouteToBattle(pStage->GetUID(), pCmd);

	return true;
}

void MMatchRuleInfection::OnInfected(const MUID& uidVictim)
{
	int nCounter = NULL;

	MMatchObject* pObject = MMatchServer::GetInstance()->GetObject(uidVictim);
	if(!pObject) return;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return;

	MCommand* pCmd = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_RESET_TEAM_MEMBERS, MUID(0,0));
	
	int nMemberCount = pStage->GetObjInBattleCount();;
	void* pTeamMemberDataArray = MMakeBlobArray(sizeof(MTD_ResetTeamMembersData), nMemberCount);

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;

		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			continue;

		MTD_ResetTeamMembersData* pNode = (MTD_ResetTeamMembersData*)MGetBlobArrayElement(pTeamMemberDataArray, nCounter);
		pNode->m_uidPlayer = pObj->GetUID();

		if(uidVictim.Low == pObj->GetUID().Low)
			pStage->PlayerTeam(pObj->GetUID(),MMT_RED);

		pNode->nTeam = pObj->GetTeam();

		nCounter++;
	}

	pCmd->AddParameter(new MCommandParameterBool(true));
	pCmd->AddParameter(new MCommandParameterBlob(pTeamMemberDataArray, MGetBlobArraySize(pTeamMemberDataArray)));
	
	MEraseBlobArray(pTeamMemberDataArray);
	
	MMatchServer::GetInstance()->RouteToBattle(pStage->GetUID(), pCmd);
}

bool MMatchRuleInfection::OnRun()
{
	bool ret = MMatchRule::OnRun();


	return ret;
}

void MMatchRuleInfection::OnRoundBegin()
{
	MMatchRule::OnRoundBegin();
}

void MMatchRuleInfection::OnRoundEnd()
{
	if (m_pStage != NULL)
	{
		switch(m_nRoundArg)
		{
			case MMATCH_ROUNDRESULT_BLUE_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED);break;
			case MMATCH_ROUNDRESULT_RED_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_REDWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED); break;
			case MMATCH_ROUNDRESULT_BLUEWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_DRAW: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
		}
	}

	MMatchRule::OnRoundEnd();
}

bool MMatchRuleInfection::OnCheckEnableBattleCondition()
{
	int nTotalObjects = 0;
	int nRedTeam = 0, nBlueTeam = 0;
	int nPreRedTeam = 0, nPreBlueTeam = 0;
	int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
		{
			nStageObjects++;
			continue;
		}

		if (pObj->GetTeam() == MMT_RED)
		{
			nRedTeam++;
		}
		else if (pObj->GetTeam() == MMT_BLUE)
		{
			nBlueTeam++;
		}

		nTotalObjects++;
	}

	if (nTotalObjects < 2)
	{
		return false;
	}

	if(!bStart)
	{
		bStart = true;
		OnAssignInfected();

		return true;
	}

	if(nRedTeam == 0)
	{
		OnAssignInfected();
	}
	return true;
}

bool MMatchRuleInfection::OnCheckRoundFinish()
{
		int nTotalObjects = 0;
		int nRedTeam = 0, nBlueTeam = 0;
		int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

		MMatchStage* pStage = GetStage();

		for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
		{
			MMatchObject* pObj = (MMatchObject*)(*i).second;
			if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			{
				nStageObjects++;
				continue;
			}

			if (pObj->GetTeam() == MMT_RED)		nRedTeam++;
			else if (pObj->GetTeam() == MMT_BLUE)	nBlueTeam++;

			nTotalObjects++;
		}

		if(nTotalObjects >= 2 && nBlueTeam == 0 && nRedTeam > 0)
		{
			SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
			return true;
		}
		return false;
}

void MMatchRuleInfection::OnRoundTimeOut()
{
	SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
}

// 반환값이 false이면 게임이 끝난다.
bool MMatchRuleInfection::RoundCount() 
{
	if (m_pStage == NULL) return false;

	int nTotalRound = m_pStage->GetStageSetting()->GetRoundMax();
	m_nRoundCount++;

	if (m_nRoundCount < nTotalRound) 
		return true;

	return false;
}

void MMatchRuleInfection::CalcTeamBonus(MMatchObject* pAttacker, MMatchObject* pVictim,
								int nSrcExp, int* poutAttackerExp, int* poutTeamExp)
{
	if (m_pStage == NULL)
	{
		*poutAttackerExp = nSrcExp;
		*poutTeamExp = 0;
		return;
	}

	*poutTeamExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamBonusExpRatio);
	*poutAttackerExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamMyExpRatio);
}
//////////////////////////////////////////////////////////////////////////////////
// MMatchRuleSoloDeath ///////////////////////////////////////////////////////////
MMatchRuleSoloDeath::MMatchRuleSoloDeath(MMatchStage* pStage) : MMatchRule(pStage)
{

}

void MMatchRuleSoloDeath::OnBegin()
{

}
void MMatchRuleSoloDeath::OnEnd()
{
}

bool MMatchRuleSoloDeath::RoundCount()
{
	if (++m_nRoundCount < 1) return true;
	return false;
}

bool MMatchRuleSoloDeath::CheckKillCount(MMatchObject* pOutObject)
{
	MMatchStage* pStage = GetStage();
	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if (pObj->GetEnterBattle() == false) continue;

		if (pObj->GetKillCount() >= (unsigned int)pStage->GetStageSetting()->GetRoundMax())
		{
			pOutObject = pObj;
			return true;
		}
	}
	return false;
}

bool MMatchRuleSoloDeath::OnCheckRoundFinish()
{
	MMatchObject* pObject = NULL;

	if (CheckKillCount(pObject))
	{
		return true;
	}
	return false;
}

void MMatchRuleSoloDeath::OnRoundTimeOut()
{
	SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}
//////////////////////////////////////////////////////////////////////////////////
// MMatchRuleGunGame ///////////////////////////////////////////////////////////
MMatchRuleGunGame::MMatchRuleGunGame(MMatchStage* pStage) : MMatchRule(pStage)
{

}

void MMatchRuleGunGame::OnBegin()
{

}
void MMatchRuleGunGame::OnEnd()
{
}

void MMatchRuleGunGame::GetGearSetLevel(MTD_CharInfo& pInfo, int nLvl)
{
	int kills = nLvl;

	MTD_CharInfo* pInfoTemp = new MTD_CharInfo();

	switch(kills)
	{
		case 1:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500001;//Advencture Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4001;//Raptor 50
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 2:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 31;//  Iron Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4005;// Minic 567
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 3:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 51;// Giant Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4004;//Raptor 120 x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 5001;//Rendard
			break;
		case 4:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500004;//Riddic Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4008;//Zaurus A x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4004;//Raptor 120 x2
			break;
		case 5:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502007;//Golden Dragon Dual Swords
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5001;//Renard
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4502;//Dynax 7000 x2
			break;
		case 6:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502001;//Adventure Long Sword	
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5004;//Walcom S5 x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4005;//Minic 567
			break;
		case 7:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1;//Rusty Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4506;//Phantom Cruise
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 8:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502012;//Black Dragon Dual Swords
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5014;//Walcom XL x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4503;//Minic 567 Limited
			break;
		case 9:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502002;//Salamanda
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5015;//Renard VI
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 5017;//Walcom XLS
			break;
		case 10:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 63;//Black Iron Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4516;//Dynax 800C x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 11:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 71;//Black Iron Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6002;//Breaker 5
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 12:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502003;//Dark Kharma
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6003;//Breaker 6
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6004;//Breaker 7
			break;
		case 13:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500006;//Shadow Vane
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6006;//Breaker 8
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6008;//Breaker 9P
			break;
		case 14:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502014;//Justice
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6009;//Iron Crow Shotgun Limited
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6010;//Cruelizon Limited
			break;
		case 15:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502015;//Silver Fang
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7002;//Nico R5
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7003;//Walcom Warrior
			break;
		case 16:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1040;//Assassin Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7004;//maxwell LX30
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 17:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502016;//Harvester
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7005;//Nico R6
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7006;//Maxwell LX44
			break;
		case 18:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502026;//Shark Fang
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8001;//Nico MG-K8
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7002;//Nico R5
			break;
		case 19:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333022;//Kingdom Key
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7003;//Walcom Warrior
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 8002;//Nico MG-K9
			break;
		case 20:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1036;//Golden Ruby
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8004;//Nico MG-K11
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 21:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 190294;//Golden Chaos Axe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 190286;//P37
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287570;//Gun Blade (revolver)
			break;
		case 22:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 481518;//Raster's Edge
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8000;//Donation Revolver
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333034;//District-9
			break;
		case 23:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 300040;//Infection Katana
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333033;//Dragonite
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333043;//LEGO Shooter
			break;
		case 24:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50018;//Mercury Hammer
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1019;//Cerberus
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1020;//Raging Bull
			break;
		case 25:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50000;//Valoran Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7000;//Donation Rifle
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 26:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50001;//Fusion Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287573;//XM8=Carbine
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7025;//Donation Rocket
			break;
		case 27:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50002;//Sinister Blades
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287577;//Torgue Rocket Launcher
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 200132;//Jaw's Launcher
			break;
		case 28:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50003;//Ork Axes
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 2014;//Event Winner's Rocket
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 900022;//Bisfed3
			break;
		case 29:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50004;//Carbon Ripper
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 900004;//Body Counter
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1025;//TakeDown
			break;
		case 30:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50005;//Jarvin's Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 190289;//Locust
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 9456;//Donator Shotgun Black
			break;
		case 31:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50006;//Targon's Spear
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 9459;//Donator Shotgun Pink
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 9461;//Donator Shotgun Red
			break;
		case 32:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50007;//Viscero Spear
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71693;//Gauss Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 33:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50008;//Darkin Blade
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71692;//PPZ30 Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 34:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50009;//Noxian Axe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 40413;//River Death
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 40417;//Deathly Blow
			break;
		case 35:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50010;//Saltwater Cutlass
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1008;//Impulse
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4567;//Dual Silver Shotguns
			break;
		case 36:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50011;//Glamdring
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4568;//Dual Golden Shotguns
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1016;//Reflex
			break;
		case 37:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50012;//Blade of Justice
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1027;//Blunderbuss
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 71699;//The Marshall
			break;
		case 38:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50013;//Boneshiver
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287572;//MX-26
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287578;//Carnage
			break;
		case 39:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50014;//Urukhai Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1022;//Fracture
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333052;//PULSE RX-A1
			break;
		case 40:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50015;//Wuju Blade
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 37862;//Hunting Rifle
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 40412;//Punk Shotgun
			break;
		case 41:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50016;//Nimbus
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333050;//ATLAS 77-A
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 2047;//Event Winner's Rifle
			break;
		case 42:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50017;//Shadow Scythe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 378650;//XEM-8 PT
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 378652;//Mutilator
			break;
		case 43:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333100;//Crystal Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71694;//Krylov
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 71695;//CR320 Battle Rifle
			break;
		case 44:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333106;//Bakuya
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333029;//Thumper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333030;//RPG-7X-22A
			break;
		case 45:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333108;//Hidden Scorpion
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1009;//Omni
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 200132;//Jaw's Launcher
			break;
		case 46:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 200118;//Devil's Glare
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71698;//Oriental Shotgun
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287572;//MX-26
			break;
		case 47:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333000;//Soul Edge
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1;
			break;
		case 48:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 900010;//Tensa Zangetsu Rebirth
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 13371;//Staff Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1;
			break;
		case 49:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 0;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 900017;//Admin Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 0;
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			pInfoTemp->nEquipedItemDesc[MMCIP_CUSTOM1] = 2034;//The Ultimate Nades
			break;
		case 50:
		default:
			break;
	}
	delete pInfoTemp;

}

void MMatchRuleGunGame::OnNewGearSet(const MUID& pAttacker)
{
	int nCounter = NULL;

	MMatchObject* pObject = MMatchServer::GetInstance()->GetObject(pAttacker);
	if(!pObject) return;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return;

	MCommand* pCmd = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_NEWGEARSET, MUID(0,0));
	
	
	void* pNewCharacter = MMakeBlobArray(sizeof(MTD_CharInfo), 0);


	MTD_CharInfo* pInfo = (MTD_CharInfo*)pObject->GetCharInfo();
	GetGearSetLevel(*pInfo,(int)pObject->GetKillCount());

	pNewCharacter = (void*)pInfo;

	pCmd->AddParameter(new MCommandParameterUID(pAttacker));
	pCmd->AddParameter(new MCommandParameterBlob(pNewCharacter, MGetBlobArraySize(pInfo)));
	
	MEraseBlobArray(pNewCharacter);
	
	MMatchServer::GetInstance()->RouteToBattle(pStage->GetUID(), pCmd);

}

bool MMatchRuleGunGame::RoundCount()
{
	if (++m_nRoundCount < 1) return true;
	return false;
}

bool MMatchRuleGunGame::CheckKillCount(MMatchObject* pOutObject)
{
	MMatchStage* pStage = GetStage();
	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if (pObj->GetEnterBattle() == false) continue;

		if (pObj->GetKillCount() >= (unsigned int)pStage->GetStageSetting()->GetRoundMax())
		{
			pOutObject = pObj;
			return true;
		}
	}
	return false;
}

bool MMatchRuleGunGame::OnCheckRoundFinish()
{
	MMatchObject* pObject = NULL;

	if (CheckKillCount(pObject))
	{
		return true;
	}
	return false;
}

void MMatchRuleGunGame::OnRoundTimeOut()
{
	SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}




// 무한 팀데스매치 - 추가 by 동섭
//////////////////////////////////////////////////////////////////////////
MMatchRuleTeamDeath2::MMatchRuleTeamDeath2(MMatchStage* pStage) : MMatchRule(pStage)
{
}

void MMatchRuleTeamDeath2::OnBegin()
{
	m_pStage->InitTeamKills();
}

void MMatchRuleTeamDeath2::OnEnd()
{
}

bool MMatchRuleTeamDeath2::OnRun()
{
	bool ret = MMatchRule::OnRun();


	return ret;
}

void MMatchRuleTeamDeath2::OnRoundBegin()
{
	MMatchRule::OnRoundBegin();
}

void MMatchRuleTeamDeath2::OnRoundEnd()
{
	if (m_pStage != NULL)
	{
		if (m_nRoundArg == MMATCH_ROUNDRESULT_REDWON) 
		{
			m_pStage->OnRoundEnd_FromTeamGame(MMT_RED);
		} 
		else if (m_nRoundArg == MMATCH_ROUNDRESULT_BLUEWON) 
		{
			m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE);
		} 
		else if (m_nRoundArg == MMATCH_ROUNDRESULT_DRAW) 
		{ 
			// Do Nothing
		}
	}

	MMatchRule::OnRoundEnd();
}

// 만약 레드팀이나 블루팀에서 팀원이 0명일 경우는 false 반환 , true,false 모두 AliveCount 반환
void MMatchRuleTeamDeath2::GetTeamScore(int* pRedTeamScore, int* pBlueTeamScore)
{
	(*pRedTeamScore) = 0;
	(*pBlueTeamScore) = 0;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return;

	(*pRedTeamScore) = pStage->GetTeamKills(MMT_RED);
	(*pBlueTeamScore) = pStage->GetTeamKills(MMT_BLUE);

	return;
}

bool MMatchRuleTeamDeath2::OnCheckRoundFinish()
{
	int nRedScore, nBlueScore;
	GetTeamScore(&nRedScore, &nBlueScore);

	MMatchStage* pStage = GetStage();

	if (nRedScore >= pStage->GetStageSetting()->GetRoundMax())
	{
		SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
		return true;
	}
	else if (nBlueScore >= pStage->GetStageSetting()->GetRoundMax())
	{
		SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
		return true;
	}

	return false;
}

void MMatchRuleTeamDeath2::OnRoundTimeOut()
{
	if (!OnCheckRoundFinish())
		SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}

// 반환값이 false이면 게임이 끝난다.
bool MMatchRuleTeamDeath2::RoundCount() 
{
	if (++m_nRoundCount < 1) return true;
	return false;
}

void MMatchRuleTeamDeath2::CalcTeamBonus(MMatchObject* pAttacker, MMatchObject* pVictim,
										int nSrcExp, int* poutAttackerExp, int* poutTeamExp)
{
	if (m_pStage == NULL)
	{
		*poutAttackerExp = nSrcExp;
		*poutTeamExp = 0;
		return;
	}

	*poutTeamExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamBonusExpRatio);
	*poutAttackerExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamMyExpRatio);
}




void MMatchRuleTeamDeath2::OnGameKill(const MUID& uidAttacker, const MUID& uidVictim)
{
	MMatchObject* pAttacker = MMatchServer::GetInstance()->GetObject(uidAttacker);
	MMatchObject* pVictim = MMatchServer::GetInstance()->GetObject(uidVictim);

	if (m_pStage != NULL)
	{
//		if (pAttacker->GetTeam() != pVictim->GetTeam())
//		{
//			m_pStage->AddTeamKills(pAttacker->GetTeam());
//		}

		m_pStage->AddTeamKills(pVictim->GetTeam() == MMT_BLUE ? MMT_RED : MMT_BLUE);		// 죽은사람 반대편팀 킬수 올림
	}
}
// 무한 팀데스매치 - 추가 by 동섭
//////////////////////////////////////////////////////////////////////////
MMatchRuleTeamCTF::MMatchRuleTeamCTF(MMatchStage* pStage) : MMatchRule(pStage)
{
	SetBlueFlagObtained(false);
	SetRedFlagObtained(false);
	SetBlueCarrier(MUID(0,0));
	SetRedCarrier(MUID(0,0));
}

void MMatchRuleTeamCTF::OnBegin()
{
	m_pStage->InitTeamKills();
}

void MMatchRuleTeamCTF::OnEnd()
{
	SetBlueFlagObtained(false);
	SetRedFlagObtained(false);
	SetBlueCarrier(MUID(0,0));
	SetRedCarrier(MUID(0,0));
}

bool MMatchRuleTeamCTF::OnRun()
{
	bool ret = MMatchRule::OnRun();


	return ret;
}

void MMatchRuleTeamCTF::OnRoundBegin()
{
	MMatchRule::OnRoundBegin();
}

void MMatchRuleTeamCTF::OnRoundEnd()
{
	if (m_pStage != NULL)
	{
		if (m_nRoundArg == MMATCH_ROUNDRESULT_REDWON) 
		{
			m_pStage->OnRoundEnd_FromTeamGame(MMT_RED);
		} 
		else if (m_nRoundArg == MMATCH_ROUNDRESULT_BLUEWON) 
		{
			m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE);
		} 
		else if (m_nRoundArg == MMATCH_ROUNDRESULT_DRAW) 
		{ 
			// Do Nothing
		}
	}

	MMatchRule::OnRoundEnd();
}

// 만약 레드팀이나 블루팀에서 팀원이 0명일 경우는 false 반환 , true,false 모두 AliveCount 반환
void MMatchRuleTeamCTF::GetTeamScore(int* pRedTeamScore, int* pBlueTeamScore)
{
	(*pRedTeamScore) = 0;
	(*pBlueTeamScore) = 0;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return;

	(*pRedTeamScore) = pStage->GetTeamKills(MMT_RED);
	(*pBlueTeamScore) = pStage->GetTeamKills(MMT_BLUE);

	return;
}

bool MMatchRuleTeamCTF::OnCheckRoundFinish()
{
	int nRedScore, nBlueScore;
	GetTeamScore(&nRedScore, &nBlueScore);

	MMatchStage* pStage = GetStage();

	if (nRedScore >= pStage->GetStageSetting()->GetRoundMax())
	{
		SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
		return true;
	}
	else if (nBlueScore >= pStage->GetStageSetting()->GetRoundMax())
	{
		SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
		return true;
	}

	return false;
}

void MMatchRuleTeamCTF::OnRoundTimeOut()
{
	if (!OnCheckRoundFinish())
		SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}

// 반환값이 false이면 게임이 끝난다.
bool MMatchRuleTeamCTF::RoundCount() 
{
	if (++m_nRoundCount < 1) return true;
	return false;
}
void MMatchRuleTeamCTF::CalcTeamBonus(MMatchObject* pAttacker, MMatchObject* pVictim,
										int nSrcExp, int* poutAttackerExp, int* poutTeamExp)
{
	if (m_pStage == NULL)
	{
		*poutAttackerExp = nSrcExp;
		*poutTeamExp = 0;
		return;
	}

	*poutTeamExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamBonusExpRatio);
	*poutAttackerExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamMyExpRatio);
}

void MMatchRuleTeamCTF::OnObtainWorldItem(MMatchObject* pObj, int nItemID, int* pnExtraValues)
{
	if( 0 == pObj )
		return;

	if (m_pStage == NULL)
	{
		SetBlueFlagObtained(false);
		SetRedFlagObtained(false);
		SetBlueCarrier(MUID(0,0));
		SetRedCarrier(MUID(0,0));
		return;
	}

	MMatchObject* pObtainer = pObj;
	MMatchTeam nTeam = pObj->GetTeam();

	int nBlueTeamCount = 0;
	int nRedTeamCount = 0;
	for (MUIDRefCache::iterator i=m_pStage->GetObjBegin(); i!=m_pStage->GetObjEnd(); i++) {
		MMatchObject* pTeamObj = (MMatchObject*)(*i).second;
		if (pTeamObj->GetEnterBattle() == true)
		{
				if (pTeamObj->GetTeam() == MMT_RED) 
				{
					nRedTeamCount++;
				}
				else if (pTeamObj->GetTeam() == MMT_BLUE) 
				{
					nBlueTeamCount++;
				}
		}
	}

	if(MMT_BLUE == nTeam)
	{
		if(IsRedFlagTaken() == false && nItemID == CTF_RED_ITEM_ID)
		{
			MUID obtainerUID = pObtainer->GetUID();
			// this is a grab
			SetRedFlagObtained(true);
			m_pStage->m_WorldItemManager.ChangeFlagState(false, MMT_RED);
			SetBlueCarrier(obtainerUID);
			RouteAssignFlag(obtainerUID, nTeam);
			SendAssignState();
		}
		else if(IsBlueFlagTaken() == false && nItemID == CTF_BLUE_ITEM_ID && IsRedFlagTaken() == true)
		{
			SetBlueFlagObtained(false);
			SetRedFlagObtained(false);
			SetBlueCarrier(MUID(0,0));
			SetRedCarrier(MUID(0,0));
			m_pStage->AddTeamKills(nTeam);
			m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_RED);
			if(pObj->GetCharInfo() && nBlueTeamCount > NUM_APPLYED_TEAMBONUS_TEAM_PLAYERS && nRedTeamCount > NUM_APPLYED_TEAMBONUS_TEAM_PLAYERS)
			{
			unsigned long int nGettingExp = (MMatchFormula::GetGettingExp(pObj->GetCharInfo()->m_nLevel, pObj->GetCharInfo()->m_nLevel) * 3);
			MMatchServer::GetInstance()->ApplyObjectTeamBonus(pObj, nGettingExp * 3);
			m_pStage->OnApplyTeamBonus(nTeam);
			}
			m_pStage->ResetTeamBonus();
			RouteAssignCap(nTeam);
			SendAssignState();
			//this is a cap
		}
		else
		{
			return;
		}
	}
	else if(MMT_RED == nTeam) //reverse logic
	{
		if(IsBlueFlagTaken() == false && nItemID == CTF_BLUE_ITEM_ID)
		{
			MUID obtainerUID = pObtainer->GetUID();
			// this is a grab
			SetBlueFlagObtained(true);
			m_pStage->m_WorldItemManager.ChangeFlagState(false, MMT_BLUE);
			SetRedCarrier(obtainerUID);
			RouteAssignFlag(obtainerUID, nTeam);
			SendAssignState();
		}
		else if(IsBlueFlagTaken() == true && nItemID == CTF_RED_ITEM_ID && IsRedFlagTaken() == false)
		{
			SetBlueFlagObtained(false);
			SetRedFlagObtained(false);
			SetBlueCarrier(MUID(0,0));
			SetRedCarrier(MUID(0,0));
			m_pStage->AddTeamKills(nTeam);
			m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_BLUE);
			if(pObj->GetCharInfo() && nBlueTeamCount > NUM_APPLYED_TEAMBONUS_TEAM_PLAYERS && nRedTeamCount > NUM_APPLYED_TEAMBONUS_TEAM_PLAYERS)
			{
			unsigned long int nGettingExp = (MMatchFormula::GetGettingExp(pObj->GetCharInfo()->m_nLevel, pObj->GetCharInfo()->m_nLevel) * 3);
			MMatchServer::GetInstance()->ApplyObjectTeamBonus(pObj, nGettingExp * 3);
			m_pStage->OnApplyTeamBonus(nTeam);
			}
			m_pStage->ResetTeamBonus();
			RouteAssignCap(nTeam);
			SendAssignState();
			//this is a cap
		}
		else
		{
			return;
		}
	}
	else 
	{
		return; //haxxors!
	}

}


void MMatchRuleTeamCTF::RouteAssignFlag(MUID& uidFlagBearer, int nTeam)
{	MCommand* pNew = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_EFFECT, MUID(0, 0));
	pNew->AddParameter(new MCmdParamUID(uidFlagBearer));
	pNew->AddParameter(new MCmdParamInt(nTeam));
	MMatchServer::GetInstance()->RouteToBattle(m_pStage->GetUID(), pNew);
}

void MMatchRuleTeamCTF::RouteAssignFlagToJoiner(MUID& uidFlagBearer, MUID& uidSendTo, int nTeam)
{	
	MCommand* pNew = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_EFFECT, MUID(0, 0));
	pNew->AddParameter(new MCmdParamUID(uidFlagBearer));
	pNew->AddParameter(new MCmdParamInt(nTeam));
	MMatchServer::GetInstance()->RouteToObjInStage(m_pStage->GetUID(), uidSendTo, pNew);
}

void MMatchRuleTeamCTF::RouteAssignCap(int nTeam)
{	MCommand* pNew = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_CAP, MUID(0, 0));
	pNew->AddParameter(new MCmdParamInt(nTeam));
	MMatchServer::GetInstance()->RouteToBattle(m_pStage->GetUID(), pNew);
}

void MMatchRuleTeamCTF::RouteAssignState(MUID uidSendTo)
{
			MMatchRuleTeamCTF* pTeamCTF = (MMatchRuleTeamCTF*)this;

			//Route Blue Flag
			MCommand* pCmdBlue = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_STATE, MUID(0,0));
			pCmdBlue->AddParameter(new MCmdParamInt(CTF_BLUE_ITEM_ID));
			pCmdBlue->AddParameter(new MCmdParamShortVector(pTeamCTF->GetBlueFlagPosition().x, pTeamCTF->GetBlueFlagPosition().y, pTeamCTF->GetBlueFlagPosition().z ));
			pCmdBlue->AddParameter(new MCmdParamInt(pTeamCTF->IsBlueFlagTaken()));
			pCmdBlue->AddParameter(new MCmdParamUID(pTeamCTF->GetBlueCarrier()));
			MMatchServer::GetInstance()->RouteToObjInStage(m_pStage->GetUID(), uidSendTo, pCmdBlue);

			//Route Red Flag
			MCommand* pCmdRed = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_STATE, MUID(0,0));
			pCmdRed->AddParameter(new MCmdParamInt(CTF_RED_ITEM_ID));
			pCmdRed->AddParameter(new MCmdParamShortVector(pTeamCTF->GetRedFlagPosition().x, pTeamCTF->GetRedFlagPosition().y, pTeamCTF->GetRedFlagPosition().z ));
			pCmdRed->AddParameter(new MCmdParamInt(pTeamCTF->IsRedFlagTaken()));
			pCmdRed->AddParameter(new MCmdParamUID(pTeamCTF->GetRedCarrier()));
			MMatchServer::GetInstance()->RouteToObjInStage(m_pStage->GetUID(), uidSendTo, pCmdRed);
}

void MMatchRuleTeamCTF::SendAssignState()
{
			MMatchRuleTeamCTF* pTeamCTF = (MMatchRuleTeamCTF*)this;

			//Route Blue Flag
			MCommand* pCmdBlue = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_STATE, MUID(0,0));
			pCmdBlue->AddParameter(new MCmdParamInt(CTF_BLUE_ITEM_ID));
			pCmdBlue->AddParameter(new MCmdParamShortVector(pTeamCTF->GetBlueFlagPosition().x, pTeamCTF->GetBlueFlagPosition().y, pTeamCTF->GetBlueFlagPosition().z ));
			pCmdBlue->AddParameter(new MCmdParamInt(pTeamCTF->IsBlueFlagTaken()));
			pCmdBlue->AddParameter(new MCmdParamUID(pTeamCTF->GetBlueCarrier()));
			MMatchServer::GetInstance()->RouteToBattle(m_pStage->GetUID(), pCmdBlue);

			//Route Red Flag
			MCommand* pCmdRed = MMatchServer::GetInstance()->CreateCommand(MC_MATCH_FLAG_STATE, MUID(0,0));
			pCmdRed->AddParameter(new MCmdParamInt(CTF_RED_ITEM_ID));
			pCmdRed->AddParameter(new MCmdParamShortVector(pTeamCTF->GetRedFlagPosition().x, pTeamCTF->GetRedFlagPosition().y, pTeamCTF->GetRedFlagPosition().z ));
			pCmdRed->AddParameter(new MCmdParamInt(pTeamCTF->IsRedFlagTaken()));
			pCmdRed->AddParameter(new MCmdParamUID(pTeamCTF->GetRedCarrier()));
			MMatchServer::GetInstance()->RouteToBattle(m_pStage->GetUID(), pCmdRed);
}

void MMatchRuleTeamCTF::OnLeaveBattle(MUID& uidChar)
{
	if (uidChar == GetBlueCarrier())
	{
	MUID m_uidBearer = MUID(0, 0);
	SetBlueCarrier(m_uidBearer);
	SetRedFlagObtained(false);
	m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_RED);
	RouteAssignFlag(m_uidBearer, MMT_BLUE);
	}

	else if (uidChar == GetRedCarrier())
	{
	MUID m_uidBearer = MUID(0, 0);
	SetRedCarrier(m_uidBearer);
	SetBlueFlagObtained(false);
	m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_BLUE);
	RouteAssignFlag(m_uidBearer, MMT_RED);
	}

	SendAssignState();
}

void MMatchRuleTeamCTF::OnEnterBattle(MUID& uidChar)
{
	RouteAssignFlagToJoiner(GetBlueCarrier(), uidChar, MMT_ALL);
	RouteAssignFlagToJoiner(GetRedCarrier(), uidChar, MMT_END);
	RouteAssignState(uidChar);
}


void MMatchRuleTeamCTF::OnGameKill(const MUID& uidAttacker, const MUID& uidVictim)
{
	MMatchObject* pVictim = MMatchServer::GetInstance()->GetObject(uidVictim);

	if (m_pStage != NULL)
	{
		MUID uidChar = pVictim->GetUID();

		if (uidChar == GetBlueCarrier())
		{
		MUID m_uidBearer = MUID(0, 0);
		SetBlueCarrier(m_uidBearer);
		SetRedFlagObtained(false);
		m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_RED);
		RouteAssignFlag(m_uidBearer, MMT_BLUE);
		SendAssignState();
		}

		else if (uidChar == GetRedCarrier())
		{
		MUID m_uidBearer = MUID(0, 0);
		SetRedCarrier(m_uidBearer);
		SetBlueFlagObtained(false);
		m_pStage->m_WorldItemManager.ChangeFlagState(true, MMT_BLUE);
		RouteAssignFlag(m_uidBearer, MMT_RED);
		SendAssignState();
		}
	}
}

// TEAM DEATH RULE ///////////////////////////////////////////////////////////////
MMatchRuleTeamTraining::MMatchRuleTeamTraining(MMatchStage* pStage) : MMatchRule(pStage)
{
}

void MMatchRuleTeamTraining::OnBegin()
{
}

void MMatchRuleTeamTraining::OnEnd()
{
}

bool MMatchRuleTeamTraining::OnRun()
{
	bool ret = MMatchRule::OnRun();


	return ret;
}

void MMatchRuleTeamTraining::OnRoundBegin()
{
	MMatchRule::OnRoundBegin();
}

void MMatchRuleTeamTraining::OnRoundEnd()
{
	if (m_pStage != NULL)
	{
		switch(m_nRoundArg)
		{
			case MMATCH_ROUNDRESULT_BLUE_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED);break;
			case MMATCH_ROUNDRESULT_RED_ALL_OUT: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_REDWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_RED); break;
			case MMATCH_ROUNDRESULT_BLUEWON: m_pStage->OnRoundEnd_FromTeamGame(MMT_BLUE); break;
			case MMATCH_ROUNDRESULT_DRAW: break;
		}
	}

	MMatchRule::OnRoundEnd();
}

bool MMatchRuleTeamTraining::OnCheckEnableBattleCondition()
{
	// 선승제일 경우는 Free상태가 안된다.
	if (m_pStage->GetStageSetting()->IsTeamWinThePoint() == true)
	{
		return true;
	}

	int nRedTeam = 0, nBlueTeam = 0;
	int nPreRedTeam = 0, nPreBlueTeam = 0;
	int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
		{
			nStageObjects++;
			continue;
		}

		if (pObj->GetTeam() == MMT_RED)
		{
			nRedTeam++;
		}
		else if (pObj->GetTeam() == MMT_BLUE)
		{
			nBlueTeam++;
		}
	}

	if ( nRedTeam == 0 || nBlueTeam == 0)
	{
		return false;
	}

	return true;
}

// 만약 레드팀이나 블루팀에서 팀원이 0명일 경우는 false 반환 , true,false 모두 AliveCount 반환
bool MMatchRuleTeamTraining::GetAliveCount(int* pRedAliveCount, int* pBlueAliveCount)
{
	int nRedCount = 0, nBlueCount = 0;
	int nRedAliveCount = 0, nBlueAliveCount = 0;
	(*pRedAliveCount) = 0;
	(*pBlueAliveCount) = 0;

	MMatchStage* pStage = GetStage();
	if (pStage == NULL) return false;

	for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
	{
		MMatchObject* pObj = (MMatchObject*)(*i).second;
		if (pObj->GetEnterBattle() == false) continue;	// 배틀참가하고 있는 플레이어만 체크

		if (pObj->GetTeam() == MMT_RED)
		{
			nRedCount++;
			if (pObj->CheckAlive()==true)
			{
				nRedAliveCount++;
			}
		}
		else if (pObj->GetTeam() == MMT_BLUE)
		{
			nBlueCount++;
			if (pObj->CheckAlive()==true)
			{
				nBlueAliveCount++;
			}
		}
	}

	(*pRedAliveCount) = nRedAliveCount;
	(*pBlueAliveCount) = nBlueAliveCount;

	if ((nRedAliveCount == 0) || (nBlueAliveCount == 0))
	{
		return false;
	}
	return true;
}

bool MMatchRuleTeamTraining::OnCheckRoundFinish()
{
	int nRedAliveCount = 0;
	int nBlueAliveCount = 0;

	// 팀원이 0명인 팀이 있으면 false반환
	if (GetAliveCount(&nRedAliveCount, &nBlueAliveCount) == false)
	{
		int nRedTeam = 0, nBlueTeam = 0;
		int nStageObjects = 0;		// 게임안에 없고 스테이지에 있는 사람

		MMatchStage* pStage = GetStage();

		for (MUIDRefCache::iterator i=pStage->GetObjBegin(); i!=pStage->GetObjEnd(); i++) 
		{
			MMatchObject* pObj = (MMatchObject*)(*i).second;
			if ((pObj->GetEnterBattle() == false) && (!pObj->IsLaunchedGame()))
			{
				nStageObjects++;
				continue;
			}

			if (pObj->GetTeam() == MMT_RED)		nRedTeam++;
			else if (pObj->GetTeam() == MMT_BLUE)	nBlueTeam++;
		}

		if( nBlueTeam ==0 && (pStage->GetTeamScore(MMT_BLUE) > pStage->GetTeamScore(MMT_RED)) )
			SetRoundArg(MMATCH_ROUNDRESULT_BLUE_ALL_OUT);
		else if( nRedTeam ==0 && (pStage->GetTeamScore(MMT_RED) > pStage->GetTeamScore(MMT_BLUE)) )
			SetRoundArg(MMATCH_ROUNDRESULT_RED_ALL_OUT);
		else if ( (nRedAliveCount == 0) && (nBlueAliveCount == 0) )
			SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
		else if (nRedAliveCount == 0)
			SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
		else if (nBlueAliveCount == 0)
			SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
	}

	if (nRedAliveCount==0 || nBlueAliveCount==0) return true;
	else return false;
}

void MMatchRuleTeamTraining::OnRoundTimeOut()
{
	int nRedAliveCount = 0;
	int nBlueAliveCount = 0;
	GetAliveCount(&nRedAliveCount, &nBlueAliveCount);

	if (nRedAliveCount > nBlueAliveCount)
		SetRoundArg(MMATCH_ROUNDRESULT_REDWON);
	else if (nBlueAliveCount > nRedAliveCount)
		SetRoundArg(MMATCH_ROUNDRESULT_BLUEWON);
	else SetRoundArg(MMATCH_ROUNDRESULT_DRAW);
}

// 반환값이 false이면 게임이 끝난다.
bool MMatchRuleTeamTraining::RoundCount() 
{
	if (m_pStage == NULL) return false;

	int nTotalRound = m_pStage->GetStageSetting()->GetRoundMax();
	m_nRoundCount++;

	if (m_pStage->GetStageSetting()->IsTeamWinThePoint() == false)
	{
		// 선승제가 아닐 경우
		if (m_nRoundCount < nTotalRound) return true;

	}
	else
	{
		// 선승제일 경우 

		// 팀원이 0명인 팀이 있어도 게임이 끝난다.
		int nRedTeamCount=0, nBlueTeamCount=0;
		m_pStage->GetTeamMemberCount(&nRedTeamCount, &nBlueTeamCount, NULL, true);

		if ((nRedTeamCount == 0) || (nBlueTeamCount == 0))
		{
			return false;
		}

		int nRedScore = m_pStage->GetTeamScore(MMT_RED);
		int nBlueScore = m_pStage->GetTeamScore(MMT_BLUE);
		
		// 래더게임에서 먼저 4승인 팀이 승리
		const int LADDER_WINNING_ROUNT_COUNT = 4;


		// 두팀이 모두 4승이 아니면 true반환
		if ((nRedScore < LADDER_WINNING_ROUNT_COUNT) && (nBlueScore < LADDER_WINNING_ROUNT_COUNT))
		{
			return true;
		}
	}

	return false;
}

void MMatchRuleTeamTraining::CalcTeamBonus(MMatchObject* pAttacker, MMatchObject* pVictim,
								int nSrcExp, int* poutAttackerExp, int* poutTeamExp)
{
	if (m_pStage == NULL)
	{
		*poutAttackerExp = nSrcExp;
		*poutTeamExp = 0;
		return;
	}

	*poutTeamExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamBonusExpRatio);
	*poutAttackerExp = (int)(nSrcExp * m_pStage->GetStageSetting()->GetCurrGameTypeInfo()->fTeamMyExpRatio);
}