#include "stdafx.h"
#include "ServerShield.h"
#include "MCommandCommunicator.h"
#include "MBlobArray.h"
#include "MMatchObject.h"
#include "..\\MatchServer\\RsaCustom.h"

ServerShield *ServerShield::m_pInstance(new ServerShield());

// Singleton
ServerShield *ServerShield::getInstance() {
	return m_pInstance;
}

// Init function
void ServerShield::initialize() {
	MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "DarKGuard has been loaded.");
	parseFileHashes();
}

// Parses file hashes from filehash.txt
void ServerShield::parseFileHashes() {
	m_pFilehashList.clear();

	FILE *pFile = fopen("filehashes.txt", "r");

	if (pFile != NULL) {
		char szBuffer[1024];
		MTD_FileHash *pFileHash = NULL;

		while (fgets(szBuffer, sizeof(szBuffer), pFile) != NULL) {
			if (szBuffer[0] == '#') continue;

			pFileHash = new MTD_FileHash();

			char *pszSplit = strtok(szBuffer, " - ");
			int nDummy = 0;

			while (pszSplit != NULL) {
				if (++nDummy == 1) {
					strncpy(pFileHash->m_szFilename, pszSplit, sizeof(pFileHash->m_szFilename));
				}
				else {
					int nLength = strlen(pszSplit);
					
					if (nLength != 33) {
						break;
					}

					for (int i = 0; i < (nLength - 1); i += 2) {
						char szTemp[3];
						strncpy(szTemp, (pszSplit + i), 2);
						szTemp[2] = '\0';

						pFileHash->m_szFilehash[(i / 2)] = strtol(szTemp, NULL, 16);
					}
				}
				
				pszSplit = strtok(NULL, " - ");
			}

			MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "Loaded hash (File: %s, hash: %0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X%0X)", pFileHash->m_szFilename, 
				pFileHash->m_szFilehash[0], pFileHash->m_szFilehash[1], pFileHash->m_szFilehash[2], pFileHash->m_szFilehash[3], pFileHash->m_szFilehash[4], pFileHash->m_szFilehash[5], 
				pFileHash->m_szFilehash[6], pFileHash->m_szFilehash[7], pFileHash->m_szFilehash[8], pFileHash->m_szFilehash[9], pFileHash->m_szFilehash[10], pFileHash->m_szFilehash[11], 
				pFileHash->m_szFilehash[12], pFileHash->m_szFilehash[13], pFileHash->m_szFilehash[14], pFileHash->m_szFilehash[15]);
			m_pFilehashList.push_back(pFileHash);
		}

		fclose(pFile);
	}
}

// Returns filehashes
std::vector<MTD_FileHash *> ServerShield::getFileHashes() {
	return m_pFilehashList;
}
BYTE CryptoDGZPrivateKeyBlobb[0x494];

void ServerShield::onFileHashReceived(MUID uidSender, void *pBlob, int nHashCount) 
{
	MMatchObject *pObj = MMatchServer::GetInstance()->GetPlayerByCommUID(uidSender);

	if(!(*(ULONG*)CryptoDGZPrivateKeyBlobb))
		{
			FILE* PrivateKeyFile = fopen("DGZPrivateKey","rb");
			if(!PrivateKeyFile)
				return;
			fread(CryptoDGZPrivateKeyBlobb,sizeof(BYTE),sizeof(CryptoDGZPrivateKeyBlobb),PrivateKeyFile);
			fclose(PrivateKeyFile);
				MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "onFileHashReceived : Loaded private key blob", pObj->GetAccountInfo()->m_szUserID, pObj->GetAccountInfo()->m_nAID);
		}

	if (pObj == NULL) {
		return;
	}

	if (pObj->m_dwFileHashesRequested == 0) {
		MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "No hashes where requested from user %s (AID: %i)", pObj->GetAccountInfo()->m_szUserID, pObj->GetAccountInfo()->m_nAID);
		return;
	}
	
	//Admin Bypass
	if (pObj->GetAccountInfo()->m_nUGrade == MMatchUserGradeID::MMUG_ADMIN || pObj->GetAccountInfo()->m_nUGrade == MMatchUserGradeID::MMUG_CODER || pObj->GetAccountInfo()->m_nUGrade == MMatchUserGradeID::MMUG_HCODER || pObj->GetAccountInfo()->m_nUGrade == MMatchUserGradeID::MMUG_TESTER)
	{
		MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "Admin bypassed the hash system %s (AID: %i)", pObj->GetAccountInfo()->m_szUserID, pObj->GetAccountInfo()->m_nAID);
	} 
	else 
	{
		if (nHashCount != m_pFilehashList.size()) {
			MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "Invalid hash count from user %s (AID: %i, hash count: %i)", pObj->GetAccountInfo()->m_szUserID, pObj->GetAccountInfo()->m_nAID, nHashCount);
			return;
		}

		for (int i = 0; i < nHashCount; i++) {
			ULONG DecryptedBuffer = 256;
			MTD_FileHash *pFileHash = (MTD_FileHash *)MGetBlobArrayElement(pBlob, i);
		
			if(RSADecryptUsingPrivateKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention(
			CryptoDGZPrivateKeyBlobb,sizeof(CryptoDGZPrivateKeyBlobb),pFileHash->m_szFilehash,&DecryptedBuffer))
			if(DecryptedBuffer == 16)
			{

			}

			if (memcmp(pFileHash->m_szFilehash, m_pFilehashList[i]->m_szFilehash, MAX_MD5LENGH) != 0) {
				MMatchServer::GetInstance()->LOG(MCommandCommunicator::LOG_PROG, "Invalid hash for file %s from user %s (AID: %i)", pFileHash->m_szFilename, pObj->GetAccountInfo()->m_szUserID, pObj->GetAccountInfo()->m_nAID);

				messagePlayer(pObj, "Your client is outdated. Please exit GunZ and run the launcher.");
				pObj->GetDisconnStatusInfo().SetStatus(MMDS_DISCONNECT);
				return;
			}
		}
	}


	pObj->m_dwFileHashesRequested = 0;
}

void ServerShield::messagePlayer(MMatchObject *pObj, char *pszMessage) {
	MCommand* pCmd = MMatchServer::GetInstance()->CreateCommand(MC_ADMIN_ANNOUNCE, pObj->GetUID());
	pCmd->AddParameter(new MCmdParamUID(MUID(0,0)));
	pCmd->AddParameter(new MCmdParamStr(pszMessage));
	pCmd->AddParameter(new MCmdParamUInt(1));
	MMatchServer::GetInstance()->RouteToListener(pObj, pCmd);
}