; ****************************************************************************
; Module: asm_macros_x64.asm
; Description: This module helps x64 to call Themida/WinLicense macros
;              without having to link with external SecureEngineSDK library
;
; Authors: Oreans Technologies 
; (c) 2011 Oreans Technologies
; ****************************************************************************


; ****************************************************************************
;                               Data Segment
; ****************************************************************************

.DATA             


; ****************************************************************************
;                                 Constants
; ****************************************************************************

.CONST 

VM_START_ID             =   1
VM_END_ID               =   2
CODEREPLACE_START_ID    =   3
CODEREPLACE_END_ID      =   4
REGISTERED_START_ID     =   5
REGISTERED_END_ID       =   6
ENCODE_START_ID         =   7
ENCODE_END_ID           =   8
CLEAR_START_ID          =   9
CLEAR_END_ID            =   10
UNREGISTERED_START_ID   =   11
UNREGISTERED_END_ID     =   12
REGISTEREDVM_START_ID   =   13
REGISTEREDVM_END_ID     =   14
UNPROTECTED_START_ID    =   15   
UNPROTECTED_END_ID      =   16   
CHECK_PROTECTION_ID     =   17
CHECK_CODE_INTEGRITY_ID =   18
CHECK_REGISTRATION_ID   =   19
CHECK_VIRTUAL_PC_ID     =   20
MUTATE_START_ID         =   21
MUTATE_END_ID           =   22
STR_ENCRYPT_START_ID    =   23
STR_ENCRYPT_END_ID      =   24
STR_ENCRYPTW_START_ID   =   27
STR_ENCRYPTW_END_ID     =   28


; ****************************************************************************
;                               Code Segment
; ****************************************************************************
                  
.CODE            
                  
VMStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, VM_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

VMStart ENDP

VMEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, VM_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

VMEnd ENDP

CodeReplaceStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CODEREPLACE_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

CodeReplaceStart ENDP

CodeReplaceEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CODEREPLACE_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

CodeReplaceEnd ENDP

RegisteredStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, REGISTERED_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

RegisteredStart ENDP

RegisteredEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, REGISTERED_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

RegisteredEnd ENDP

EncodeStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, ENCODE_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

EncodeStart ENDP

EncodeEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, ENCODE_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

EncodeEnd ENDP

ClearStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CLEAR_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

ClearStart ENDP

ClearEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CLEAR_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

ClearEnd ENDP

MutateStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, MUTATE_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

MutateStart ENDP

MutateEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, MUTATE_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

MutateEnd ENDP

StrEncryptStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, STR_ENCRYPT_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

StrEncryptStart ENDP

StrEncryptEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, STR_ENCRYPT_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

StrEncryptEnd ENDP

StrEncryptWStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, STR_ENCRYPTW_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

StrEncryptWStart ENDP

StrEncryptWEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, STR_ENCRYPTW_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

StrEncryptWEnd ENDP

UnregisteredStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, UNREGISTERED_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

UnregisteredStart ENDP

UnregisteredEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, UNREGISTERED_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

UnregisteredEnd ENDP

RegisteredVMStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, REGISTEREDVM_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

RegisteredVMStart ENDP

RegisteredVMEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, REGISTEREDVM_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

RegisteredVMEnd ENDP

UnprotectedStart PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, UNPROTECTED_START_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

UnprotectedStart ENDP

UnprotectedEnd PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, UNPROTECTED_END_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    ret

UnprotectedEnd ENDP

SECheckProtection PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CHECK_PROTECTION_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    mov     dword ptr [rcx], edx
    ret

SECheckProtection ENDP

SECheckCodeIntegrity PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CHECK_CODE_INTEGRITY_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    mov     dword ptr [rcx], edx
    ret

SECheckCodeIntegrity ENDP

SECheckRegistration PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CHECK_REGISTRATION_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    mov     dword ptr [rcx], edx
    ret

SECheckRegistration ENDP

SECheckVirtualPC PROC

    push    rax
    push    rbx
    push    rcx 

    mov     eax, 'TMWL'
    mov     ebx, CHECK_VIRTUAL_PC_ID
    mov     ecx, 'TMWL'
    add     ebx, eax
    add     ecx, eax

    pop     rcx
    pop     rbx
    pop     rax
    mov     dword ptr [rcx], edx
    ret

SECheckVirtualPC ENDP

END 

