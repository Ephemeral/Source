////Example and usage


#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>

#include "Rsa.h"


int main()
{
	HCRYPTPROV CryptoContext = 0;
	HCRYPTKEY KeyContext = 0;
	PBYTE PrivateKeyBlob = NULL;
	ULONG PrivateKeyBlobSize = 0;
	PBYTE PublicKeyBlob = NULL;
	ULONG PublicKeyBlobSize = 0;
	/*BYTE BufferToBeEncrypted[] = "Hello World!";*/
	BYTE BufferToBeEncrypted[MAX_BUFFER_SIZE_FOR_ENCRYPTION] = "Hello World!";

	//PBYTE EncryptedBuffer = NULL;
	ULONG EncryptedBufferSize = 0;

	if(!GetCryptoContext(&CryptoContext))
		goto CleanUp;

	if(!RSAGenerateKeyPair(CryptoContext,&KeyContext))
		goto CleanUp;

	if(!RSAExportPublicKeyToBlob(KeyContext,&PublicKeyBlob,&PublicKeyBlobSize))
		goto CleanUp;

	if(!RSAExportPrivateKeyToBlob(KeyContext,&PrivateKeyBlob,&PrivateKeyBlobSize))
		goto CleanUp;

	WriteShellToFile("DGZPrivateKey.h","DGZPrivateKey.cpp","CryptoDGZPrivateKeyBlob","PRIVATE_KEY_BLOB",PrivateKeyBlob,PrivateKeyBlobSize);
	
	WriteShellToFile("DGZPublicKey.h","DGZPublicKey.cpp","CryptoDGZPublicKeyBlob","PUBLIC_KEY_BLOB",PublicKeyBlob,PublicKeyBlobSize);

	RSADestroyKeyHandle(KeyContext);
	KeyContext = 0;

	DestroyCryptoContext(CryptoContext);
	CryptoContext = 0;

	if(!RSAEncryptUsingPublicKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
		(PublicKeyBlob,PublicKeyBlobSize,BufferToBeEncrypted,13/*,&EncryptedBuffer*/,&EncryptedBufferSize))
		goto CleanUp;

	//printf("Encrypted:%s\n\n",/*EncryptedBuffer*/BufferToBeEncrypted);

	Dump(BufferToBeEncrypted,EncryptedBufferSize);

	if(!RSADecryptUsingPrivateKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
		(PrivateKeyBlob,PrivateKeyBlobSize,/*EncryptedBuffer*/BufferToBeEncrypted,&EncryptedBufferSize))
		goto CleanUp;

	printf("Decrypted:%s\n",/*EncryptedBuffer*/BufferToBeEncrypted);
	
	EncryptedBufferSize = sizeof(BufferToBeEncrypted);


	if(!CalculateSHA1(BufferToBeEncrypted,13,BufferToBeEncrypted,&EncryptedBufferSize))
		goto CleanUp;

	Dump(BufferToBeEncrypted,EncryptedBufferSize);

	//printf("SHA:%s\n",BufferToBeEncrypted);

CleanUp:
	
	/*RSADestroyAllocatedBuffer(EncryptedBuffer);*/


	RSADestroyExportedBlob(PublicKeyBlob);
	
	RSADestroyExportedBlob(PrivateKeyBlob);

	if(KeyContext)
		RSADestroyKeyHandle(KeyContext);
	
	if(CryptoContext)
		DestroyCryptoContext(CryptoContext);


	fgetc(stdin);

	return 0;
}
