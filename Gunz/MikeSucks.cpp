#include "StdAfx.h"
#include "MikeSucks.h"
//#include "EnumWindowsHooks.h"
/*
HHOOK MouseHook;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		MSLLHOOKSTRUCT *info=(MSLLHOOKSTRUCT*)lParam;     
		if(((MSLLHOOKSTRUCT *)lParam)->flags == LLMHF_INJECTED)
		{
			ZGetApplication()->Exit();
			//ExitProcess(-1);
		}
	}
	
	LRESULT lRes = CallNextHookEx(MouseHook,nCode,wParam,lParam);

	return lRes;
}

VOID StartBackgroundMode(VOID)
{
	OSVERSIONINFO osver;

	ZeroMemory(&osver,sizeof(OSVERSIONINFO));
	osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	GetVersionEx( &osver );

	if( osver.dwMajorVersion >= 6 )
	{
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_LOWEST);
		SetThreadPriority(GetCurrentThread(), THREAD_MODE_BACKGROUND_BEGIN);
	// Vista or above
	}
	else
	{
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_LOWEST);
	// XP or below
	}

	ZApplication* pApplication = ZGetApplication();
	pApplication = pApplication->GetInstance();

	if(pApplication->GetTestServer())
		return;

	HINSTANCE hInstance = GetModuleHandle(NULL);
	MouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProc, hInstance, NULL);
}

VOID EndBackgroundMode(VOID)
{
	OSVERSIONINFO osver;

	ZeroMemory(&osver,sizeof(OSVERSIONINFO));
	osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	GetVersionEx( &osver );

	if( osver.dwMajorVersion >= 6 )
	{
		SetThreadPriority(GetCurrentThread(), THREAD_MODE_BACKGROUND_END);
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);
	// Vista or above
	}
	else
	{
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);
	// XP or below
	}

	UnhookWindowsHookEx(MouseHook);
}

VOID MikeSucks(VOID)
{
	//StartBackgroundMode();
	HINSTANCE hInstance = GetModuleHandle(NULL);
	MouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProc, hInstance, NULL);

	MSG message;
	while(GetMessage(&message, NULL, NULL, NULL)) 
	{
        TranslateMessage(&message);
        DispatchMessage(&message);
    }

	UnhookWindowsHookEx(MouseHook);
	//EndBackgroundMode();
}*/

/*VOID EnumHooks(VOID)
{
	NTSTATUS NtStatus = STATUS_UNSUCCESSFUL;

    PVOID ImageBase;
    ULONG DllCharacteristics = 0;
    PVOID User32InitializeImmEntryTable = NULL;

	UNICODE_STRING DllName;
    ANSI_STRING ProcedureName;
    
    ULONG i;
    ULONG UserDelta = 0;
    ULONG HandleEntries = 0;

    SHARED_INFO *SharedInfo = NULL;
    HANDLE_ENTRY *UserHandleTable = NULL;
    HOOK *HookInfo = NULL;
    
    
	__try
    {
        RtlInitUnicodeString(
			                 &DllName,
			                 L"user32");

		NtStatus = LdrLoadDll(
							  NULL,                // DllPath
							  &DllCharacteristics, // DllCharacteristics
							  &DllName,            // DllName
							  &ImageBase);         // DllHandle

		if(NtStatus == STATUS_SUCCESS)
		{
			RtlInitAnsiString(
				              &ProcedureName,
				              "User32InitializeImmEntryTable");

			NtStatus = LdrGetProcedureAddress(
											  ImageBase,                               // DllHandle
											  &ProcedureName,                          // ProcedureName
											  0,                                       // ProcedureNumber OPTIONAL
											  (PVOID*)&User32InitializeImmEntryTable); // ProcedureAddress

			if(NtStatus == STATUS_SUCCESS)
			{
				__asm
				{
					mov esi, User32InitializeImmEntryTable
					test esi, esi
					jz __exit2
					mov ecx, 0x80
						
				__loop:
					dec ecx
					test ecx, ecx
					jz __exit1

					lodsb
					cmp al, 0x50
					jnz __loop

					lodsb
					cmp al, 0x68
					jnz __loop

					lodsd
					mov SharedInfo, eax

                    jmp __exit2

				__exit1:
					mov SharedInfo, ecx
					
				__exit2:
					sub eax, eax
					mov eax, fs:[eax+0x18]
					lea eax, [eax+0x06CC]
					mov eax, [eax+0x001C]
					mov UserDelta, eax
				}

                HandleEntries = *((ULONG *)((ULONG)SharedInfo->ServerInfo + 8));

              /*  printf(
                       " +--------------------------------------------------------------------+\n"
                       " | SHARED_INFO - %.8X                                             |\n"
                       " +--------------------------------------------------------------------+\n"
                       " | ServerInfo - %.8X                                              |\n"
                       " | HandleEntryList - %.8X                                         |\n"
                       " | HandleEntries - %.8X                                           |\n"
                       " | DisplayInfo - %.8X                                             |\n"
                       " | SharedDelta - %.8X                                             |\n"
                       " | UserDelta - %.8X                                               |\n"
                       " +--------------------------------------------------------------------+\n\n",
                       SharedInfo,
                       SharedInfo->ServerInfo,
                       SharedInfo->HandleEntryList,
                       HandleEntries,
                       SharedInfo->DisplayInfo,
                       SharedInfo->SharedDelta,
                       UserDelta);

                UserHandleTable = (HANDLE_ENTRY *)SharedInfo->HandleEntryList;
                
                for(i=0; i<HandleEntries; i++)
                {
                    if(UserHandleTable[i].Type == TYPE_HOOK)
                    {
                        __try
                        {
                            HookInfo = (HOOK *)((ULONG)UserHandleTable[i].Head - UserDelta);

                            printf(
                                   " +--------------------------------------------------------------------+\n"
                                   " | HOOK - %.8X                                                    |\n"
                                   " +--------------------------------------------------------------------+\n"
                                   " | Handle - %.8X                                                  |\n"
                                   " | LockObj - %.8X                                                 |\n"
                                   " | ThreadInfo- %.8X                                               |\n"
                                   " | Desktop1 - %.8X                                                |\n"
                                   " | Self - %.8X                                                    |\n"
                                   " | NextHook - %.8X                                                |\n"
                                   " | HookType - %.8X                                                |\n"
                                   " | FunctionAddress - %.8X                                         |\n"
                                   " | Flags - %.8X                                                   |\n"
                                   " | ModuleHandle - %.8X                                            |\n"
                                   " | Hooked - %.8X                                                  |\n"
                                   " | Desktop2 - %.8X                                                |\n"
                                   " +--------------------------------------------------------------------+\n\n",
                                   (ULONG)UserHandleTable[i].Head - UserDelta,
                                   HookInfo->Handle,
                                   HookInfo->LockObj,
                                   HookInfo->ThreadInfo,
                                   HookInfo->Desktop1,
                                   HookInfo->Self,
                                   HookInfo->NextHook,
                                   HookInfo->HookType,
                                   HookInfo->FunctionAddress,
                                   HookInfo->Flags,
                                   HookInfo->ModuleHandle,
                                   HookInfo->Hooked,
                                   HookInfo->Desktop2);
                        }
                        __except(EXCEPTION_EXECUTE_HANDLER) {}
                    }
                }
            }
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        printf(">> main - %.8X\n", GetExceptionCode());

        return GetExceptionCode();
    }

    return FALSE;
}*/