#ifndef _POINTS_H
#define _POINTS_H

typedef rvector Point;

struct CamSpline
{
	bool m_bTimes;
	float m_nValue;
	int m_nTimes;

	bool m_bRevert;
	bool m_bORevert;
};

#endif