#include "stdafx.h"
#include "CRC32.h"
unsigned long r3dCRC32(const unsigned char* data, unsigned long size)
{
  unsigned long crc32 = 0xFFFFFFFF;

  #define UPDC32(octet,crc) (crc_32_tab[((crc) ^ ((unsigned char)octet)) & 0xff] ^ ((crc) >> 8))

  for(; size; --size, ++data) 
  {
    crc32 = UPDC32(*data, crc32);
  }
  #undef UPDC32
  return ~crc32;
}