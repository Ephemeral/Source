#include "stdafx.h"
#include "GeoIP.h"

using namespace std;

DWORD __stdcall GeoIP_GetCountry(LPVOID lpThreadParameter)
{
		GEOIP_Params *params = (GEOIP_Params *)lpThreadParameter;
        char *country = "Unknown";
        SOCKET hSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        sockaddr_in sockinfo;
        sockinfo.sin_family = AF_INET;
        sockinfo.sin_port = htons(80);
        hostent *ent = gethostbyname("freegeoip.net");
        sockinfo.sin_addr.S_un.S_addr = *(unsigned long*)(ent->h_addr_list[0]);

        if (connect(hSocket,(sockaddr*)&sockinfo,sizeof(sockinfo)) == 0)
        {
                char request[256];
				sprintf(request, "GET /csv/%s HTTP/1.1\r\nHost: freegeoip.net\r\nUser-Agent: DarkGunz\r\nConnection: close\r\n\r\n",params->peerip);
				int rlen = strlen(request);

				if (send(hSocket,request,strlen(request),0) > 0)
                {
                        char tempbuf[1024];
                        memset(tempbuf,0,1024);
                        if (recv(hSocket,tempbuf,1024,0) > 0)
                        {
                                char *content,*junk,*ip;
                                content = strstr(tempbuf,"\r\n\r\n");
                                if (content)
                                {
                                        content += 4;
                                        ip = strtok(content,",");
                                        if (!ip) { closesocket(hSocket); return 0; }
										

                                        junk = strtok(NULL,",");
                                        if (!junk) { closesocket(hSocket); return 0; }

										country = strtok (NULL,",");

                                        if (!country) { closesocket(hSocket); return 0; }
                                        
                                        int n = strlen(country);
                                        if (n <= 0) { closesocket(hSocket); return 0; }
                                }
                        }
                }
        }
		closesocket(hSocket);
		if (strstr(params->szCountry,"Unknown") && ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)
			strcpy(params->szCountry,params->peerip);
		else
			strcpy(params->szCountry,country);
		return 0;
}