#include "stdafx.h"

#include "ZChat.h"
#include "MChattingFilter.h"
#include "ZApplication.h"
#include "ZGameInterface.h"
#include "ZPost.h"
#include "ZPostLocal.h"
#include "ZCombatChat.h"
#include "ZCombatInterface.h"
#include "ZIDLResource.h"
#include "MListBox.h"
#include "MLex.h"
#include "MTextArea.h"
#include "ZMyInfo.h"
#include "ZMessages.h"
#include "MUtil.h"
#include "ZConfiguration.h"
#include "ZInterfaceListener.h"
#include "ZNetRepository.h"
#include "ZPlayerListBox.h"

#include "ZApplication.h"
#include "ZChat_CmdID.h"

void ChatCmd_Test(const char* line, const int argc, char **const argv);				// Testing

void ChatCmd_Help(const char* line, const int argc, char **const argv);				// 도움말
void ChatCmd_Go(const char* line, const int argc, char **const argv);					// 게임방 이동
void ChatCmd_Whisper(const char* line, const int argc, char **const argv);			// 귓속말
void ChatCmd_CreateChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 개설
void ChatCmd_JoinChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 참여
void ChatCmd_LeaveChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 탈퇴
void ChatCmd_SelectChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 선택
void ChatCmd_InviteChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 초대
void ChatCmd_VisitChatRoom(const char* line, const int argc, char **const argv);		// 채팅방 초대 참석
void ChatCmd_ChatRoomChat(const char* line, const int argc, char **const argv);		// 채팅
void ChatCmd_CopyToTestServer(const char* line, const int argc, char **const argv);	// 캐릭터전송 - 테스트 서버로 캐릭터 전송
void ChatCmd_StageFollow(const char* line, const int argc, char **const argv);		// 플레이어 추적해서 게임참가
void ChatCmd_Friend(const char* line, const int argc, char **const argv);				// 친구 관련
void ChatCmd_Clan(const char* line, const int argc, char **const argv);				// 클란 관련
void ChatCmd_Where(const char* line, const int argc, char **const argv);

void ChatCmd_RequestQuickJoin(const char* line, const int argc, char **const argv);		// 퀵조인
void ChatCmd_Report119(const char* line, const int argc, char **const argv);				// 신고
void ChatCmd_RequestPlayerInfo(const char* line, const int argc, char **const argv);		// 사용자 정보보기
void ChatCmd_Macro(const char* line,const int argc, char **const argv);

// 게임안에서만 가능한 명령어
void ChatCmd_EmotionTaunt(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionBow(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionWave(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionLaugh(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionCry(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionDance(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionLoginWalk(const char* line,const int argc, char **const argv);
void ChatCmd_EmotionLoginIntro(const char* line,const int argc, char **const argv);
void ChatCmd_Suicide(const char* line,const int argc, char **const argv);
void ChatCmd_Callvote(const char* line,const int argc, char **const argv);
void ChatCmd_VoteYes(const char* line,const int argc, char **const argv);
void ChatCmd_VoteNo(const char* line,const int argc, char **const argv);
void ChatCmd_Kick(const char* line,const int argc, char **const argv);
void ChatCmd_MouseSensitivity(const char* line,const int argc, char **const argv);
void ChatCmd_AddFriend(const char* line,const int argc, char **const argv);
void ChatCmd_AFK(const char* line,const int argc, char **const argv);
void ChatCmd_HPCheck(const char* line,const int argc, char **const argv);
void ChatCmd_TeamHPCheck(const char* line,const int argc, char **const argv);
void ChatCmd_Time(const char* line, const int argc, char **const argv);
void ChatCmd_IgnoreList(const char* line,const int argc, char **const argv);
void ChatCmd_Ignore(const char* line,const int argc, char **const argv);
void ChatCmd_Unignore(const char* line,const int argc, char **const argv);

void ChatCmd_TestBomb(const char* line,const int argc, char **const argv);
void ChatCmd_Hacklog(const char* line,const int argc, char **const argv);
void ChatCmd_TeleTo(const char* line,const int argc, char **const argv);
void ChatCmd_Tele(const char* line, const int argc, char **const argv);

void ChatCmd_Teleother(const char* line, const int argc, char **const argv);

void ChatCmd_SetTarget(const char* line, const int argc, char **const argv);
void ChatCmd_Summon(const char* line,const int argc, char **const argv);
void ChatCmd_SummonAll(const char* line,const int argc, char **const argv);
void ChatCmd_Spawn(const char* line,const int argc, char **const argv);
void ChatCmd_Slap(const char* line,const int argc, char **const argv);
void ChatCmd_PeerIcon(const char* line,const int argc, char **const argv);
void ChatCmd_PeerChangeItem(const char* line,const int argc, char **const argv);
void ChatCmd_GetCharItems(const char* line,const int argc, char **const argv);
void ChatCmd_SearchItems(const char* line,const int argc, char **const argv);
void ChatCmd_Design(const char* line,const int argc, char **const argv);
void ChatCmd_Freeze(const char* line,const int argc, char **const argv);
void ChatCmd_FreezeAll(const char* line, const int argc, char **const argv);
void ChatCmd_SlapAll(const char* line,const int argc, char **const argv);
void ChatCmd_KillAll(const char* line,const int argc, char **const argv);
void ChatCmd_SpawnAll(const char* line, const int argc, char **const argv);
void ChatCmd_AutoQuest(const char* line,const int argc, char **const argv);
// test
void ChatCmd_LadderInvite(const char* line,const int argc, char **const argv);
void ChatCmd_LadderTest(const char* line,const int argc, char **const argv);
void ChatCmd_LaunchTest(const char* line,const int argc, char **const argv);

// QuestTest Admin Commands
void ChatCmd_QUESTTEST_God(const char* line,const int argc, char **const argv);				// 무적모드
void ChatCmd_QUESTTEST_SpawnNPC(const char* line,const int argc, char **const argv);		// NPC 스폰
void ChatCmd_QUESTTEST_NPCClear(const char* line,const int argc, char **const argv);		// NPC 클리어
void ChatCmd_QUESTTEST_Reload(const char* line,const int argc, char **const argv);			// 리소스 리로드
void ChatCmd_QUESTTEST_SectorClear(const char* line,const int argc, char **const argv);		// 섹터 클리어
void ChatCmd_QUESTTEST_Finish(const char* line,const int argc, char **const argv);			// 퀘스트 클리어
void ChatCmd_QUESTTEST_LocalSpawnNPC(const char* line,const int argc, char **const argv);	// npc 스폰 (local)
void ChatCmd_QUESTTEST_WeakNPCs(const char* line,const int argc, char **const argv);		// NPC 에너지를 1로

// admin 명령
void ChatCmd_AdminKickPlayer(const char* line, const int argc, char **const argv);		// 디스커넥트 시킴
void ChatCmd_ChangeTeam(const char* line, const int argc, char **const argv);
void ChatCmd_AdminEventBanPlayer(const char* line, const int argc, char **const argv);
void ChatCmd_AdminEventUnbanPlayer(const char* line, const int argc, char **const argv);
void ChatCmd_AdminRequestLadderList(const char* line, const int argc, char **const argv);
void ChatCmd_AdminRequestLadderJoin(const char* line, const int argc, char **const argv);

void ChatCmd_AdminPingToAll(const char* line, const int argc, char **const argv);		// Garbage Connection 청소 확인
void ChatCmd_AdminHalt(const char* line, const int argc, char **const argv);
void ChatCmd_AdminAnnounce(const char* line, const int argc, char **const argv);		// 전체 공지
void ChatCmd_AdminBox(const char* line, const int argc, char **const argv);		// 전체 공지
void ChatCmd_AdminSwitchCreateLadderGame(const char* line, const int argc, char **const argv);		// 클랜전 여부 설정
void ChatCmd_AdminReloadClientHash(const char* line, const int argc, char **const argv);		// XTrap 클라이언트 Hash 리로드
void ChatCmd_AdminResetAllHackingBlock( const char* line, const int argc, char **const argv );
void ChatCmd_AdminReloadGambleitem( const char* line, const int argc, char **const argv );
void ChatCmd_AdminDumpGambleitemLog( const char* line, const int argc, char **const argv );
void ChatCmd_AdminAssasin( const char* line, const int argc, char **const argv );
void ChatCmd_AdminGod( const char* line, const int argc, char **const argv );
void ChatCmd_Reflect( const char* line, const int argc, char **const argv );
void ChatCmd_ClanInfo( const char* line, const int argc, char **const argv );
void ChatCmd_Check( const char* line, const int argc, char **const argv );
void ChatCmd_df( const char* line, const int argc, char **const argv );
void ChatCmd_Camera( const char* line, const int argc, char **const argv );
void ChatCmd_Start( const char* line, const int argc, char **const argv );
void ChatCmd_Clanwar( const char* line, const int argc, char **const argv );
void ChatCmd_DevMode( const char* line, const int argc, char **const argv );
void ChatCmd_GetUGrade( const char* line, const int argc, char **const argv );
void ChatCmd_Ghost( const char* line, const int argc, char **const argv );
//DevMode Commands
void ChatCmd_SetMine(const char* line, const int argc, char **const argv);
void ChatCmd_SetGravity(const char* line, const int argc, char **const argv);
void ChatCmd_SetSpeed(const char* line, const int argc, char **const argv);
void ChatCmd_SetVelocity(const char* line, const int argc, char **const argv);
void ChatCmd_SetDelay(const char* line, const int argc, char **const argv);
void ChatCmd_SetRange(const char* line, const int argc, char **const argv);
void ChatCmd_SetAccuracy(const char* line, const int argc, char **const argv);
void ChatCmd_SetPelletCount(const char* line, const int argc, char **const argv);
void ChatCmd_Wireframe(const char* line, const int argc, char **const argv);
void ChatCmd_Collision(const char* line, const int argc, char **const argv);
//End Dev Commands


void ChatCmd_StaffLobbyChat( const char* line, const int argc, char **const argv );
void ChatCmd_PermKick( const char* line, const int argc, char **const argv );
void ChatCmd_RemoveKick(const char* line, const int argc, char **const argv);
void ChatCmd_StageSummon(const char* line, const int argc, char **const argv);

void ChatCmd_ShotbotTest( const char* line, const int argc, char **const argv );
void ChatCmd_DamageCounter( const char* line, const int argc, char **const argv );

// event 진행운영자 명령
void ChatCmd_ChangeMaster(const char* line, const int argc, char **const argv);			// 방장권한 빼앗어옴
void ChatCmd_ChangePassword(const char* line, const int argc, char **const argv);		// 방 패스워드 바꿈
void ChatCmd_ChangeTitle(const char* line, const int argc, char **const argv);
void ChatCmd_AdminHide(const char* line, const int argc, char **const argv);			// 투명인간
void ChatCmd_AdminIncognito(const char* line, const int argc, char **const argv);
void ChatCmd_RequestJjang(const char* line, const int argc, char **const argv);
void ChatCmd_RemoveJjang(const char* line, const int argc, char **const argv);

void ChatCmd_ToggleClanUI(const char* line, const int argc, char **const argv);

// Darkgunz extra
void ChatCmd_Info(const char* line, const int argc, char **const argv);

///////////////////////////////////////////////////////////////////////////////////////////////

void _AddCmdFromXml(ZChatCmdManager* pCmdManager, ZCmdXmlParser* pParser, 
				int nCmdID, ZChatCmdProc* fnProc, unsigned long int flag,
				int nMinArgs, int nMaxArgs, bool bRepeatEnabled)
{
	ZCmdXmlParser::_CmdStr* pCmdStr = pParser->GetStr(nCmdID);
	if (pCmdStr)
	{
		pCmdManager->AddCommand(nCmdID, pCmdStr->szName, fnProc, flag, nMinArgs, nMaxArgs, bRepeatEnabled, 
			pCmdStr->szUsage, pCmdStr->szHelp);

		for (int i = 0; i < (int)pCmdStr->vecAliases.size(); i++)
		{
			pCmdManager->AddAlias(pCmdStr->vecAliases[i].c_str(), pCmdStr->szName);
		}
	}
}

#define _CC_AC(X1,X2,X3,X4,X5,X6,X7,X8)		m_CmdManager.AddCommand(0,X1,X2,X3,X4,X5,X6,X7,X8)
#define _CC_ALIAS(NEW,TAR)					m_CmdManager.AddAlias(NEW,TAR)
#define _CC_ACX(X1,X2,X3,X4,X5,X6)			_AddCmdFromXml(&m_CmdManager,&parser,X1,X2,X3,X4,X5,X6)

// 채팅명령어 사용법은 http://iworks.maietgames.com/mdn/wiki.php/건즈;채팅명령어 에 적어주세요.. by bird

void ZChat::InitCmds()
{	
	ZCmdXmlParser parser;
	if (!parser.ReadXml(ZApplication::GetFileSystem(), FILENAME_CHATCMDS))
	{
		MLog("Error while Read Item Descriptor %s", "system/chatcmds.xml");
	}

	_CC_ACX(CCMD_ID_HELP,				&ChatCmd_Help,				CCF_ALL, ARGVNoMin, ARGVNoMax, true);
	_CC_ACX(CCMD_ID_WHISPER,			&ChatCmd_Whisper,			CCF_ALL, ARGVNoMin, 1, false);
	_CC_ACX(CCMD_ID_REPORT119,			&ChatCmd_Report119,			CCF_ALL, ARGVNoMin, ARGVNoMax, true);
	_CC_ACX(CCMD_ID_FRIEND,				&ChatCmd_Friend,			CCF_ALL, ARGVNoMin, 1, false);
	_CC_ACX(CCMD_ID_CLAN,				&ChatCmd_Clan,				CCF_ALL, ARGVNoMin, ARGVNoMax, false);
	_CC_ACX(CCMD_ID_REQUEST_QUICK_JOIN,	&ChatCmd_RequestQuickJoin,	CCF_LOBBY, ARGVNoMin, ARGVNoMax, true);
	_CC_ACX(CCMD_ID_EMOTION_TAUNT,		&ChatCmd_EmotionTaunt,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_EMOTION_BOW,		&ChatCmd_EmotionBow  ,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_EMOTION_WAVE,		&ChatCmd_EmotionWave ,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_EMOTION_LAUGH,		&ChatCmd_EmotionLaugh,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_EMOTION_CRY,		&ChatCmd_EmotionCry  ,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_EMOTION_DANCE,		&ChatCmd_EmotionDance,		CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_AC("walk",						&ChatCmd_EmotionLoginWalk,	CCF_GAME, ARGVNoMin, ARGVNoMax, true, "/walk", "");
	_CC_AC("intro",						&ChatCmd_EmotionLoginIntro,	CCF_GAME, ARGVNoMin, ARGVNoMax, true, "/intro", "");
	_CC_ACX(CCMD_ID_MACRO,				&ChatCmd_Macro,				CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_SUICIDE,			&ChatCmd_Suicide,			CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_CALLVOTE,			&ChatCmd_Callvote,			CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_VOTEYES,			&ChatCmd_VoteYes,			CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_VOTENO,				&ChatCmd_VoteNo,			CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_KICK,				&ChatCmd_Kick,				CCF_GAME, ARGVNoMin, ARGVNoMax,true);
	_CC_ACX(CCMD_ID_CREATE_CHATROOM,	&ChatCmd_CreateChatRoom,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_JOIN_CHATROOM,		&ChatCmd_JoinChatRoom  ,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_LEAVE_CHATROOM,		&ChatCmd_LeaveChatRoom ,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_SELECT_CHATROOM,	&ChatCmd_SelectChatRoom,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_INVITE_CHATROOM,	&ChatCmd_InviteChatRoom,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_VISIT_CHATROOM,		&ChatCmd_VisitChatRoom ,	CCF_ALL, ARGVNoMin, 1,true);
	_CC_ACX(CCMD_ID_CHAT_CHATROOM,		&ChatCmd_ChatRoomChat  ,	CCF_ALL, ARGVNoMin, 1,false);
	_CC_ACX(CCMD_ID_MOUSE_SENSITIVITY,	&ChatCmd_MouseSensitivity,	CCF_GAME, ARGVNoMin, 1,true);

	_CC_AC("friend", &ChatCmd_Friend, CCF_ALL, ARGVNoMin, 1, false, "/friend <cmd> <rest>", "");
	_CC_AC("where",	&ChatCmd_Where,	CCF_ALL, ARGVNoMin, 1, true, "/where <name>", "");
	_CC_AC("follow",	&ChatCmd_StageFollow,	CCF_LOBBY, ARGVNoMin, 1, true, "/follow <name>", "");

	_CC_AC("counter",	&ChatCmd_DamageCounter,	CCF_GAME , ARGVNoMin, ARGVNoMax , true, "/counter", "");
	_CC_ALIAS("damage","counter");
////////////////////////////////////////////////////////////////////
	// admin
	_CC_AC("dc",				&ChatCmd_AdminKickPlayer,		CCF_ALL, ARGVNoMin, 1, true, "/dc <charname>", "");			// 2010-08-09 수정됨 - 홍기주
	
	_CC_AC("ChangeTeam",		&ChatCmd_ChangeTeam,			CCF_ALL, ARGVNoMin, 2, true, "/ChangeTeam <charname> <0 = None, 1=Inspect, 2= Red, 3 = Blue>", "");
	_CC_AC("halt",				&ChatCmd_AdminHalt,				CCF_ALL, ARGVNoMin, ARGVNoMax , true,"/halt", "");
	_CC_AC("shout",				&ChatCmd_AdminAnnounce,			CCF_ALL, ARGVNoMin, 1 , true,"/shout <message>", "");
	_CC_AC("box",				&ChatCmd_AdminBox,				CCF_ALL, ARGVNoMin, 1 , true,"/box <message>", "");
	_CC_AC("changemaster",		&ChatCmd_ChangeMaster,			CCF_ADMIN|CCF_STAGE|CCF_GAME, ARGVNoMin, ARGVNoMax, true,"/changemaster", "");
	_CC_AC("changepassword",	&ChatCmd_ChangePassword,		CCF_ADMIN|CCF_STAGE|CCF_GAME, ARGVNoMin, ARGVNoMax, true,"/changepassword <password>", "");
	_CC_AC("changetitle",		&ChatCmd_ChangeTitle,			CCF_ADMIN|CCF_STAGE|CCF_GAME, ARGVNoMin, ARGVNoMax , true,"/changetitle <newname>", "");
	_CC_AC("hide",				&ChatCmd_AdminHide,				CCF_ADMIN|CCF_LOBBY, ARGVNoMin, ARGVNoMax, true,"/hide", "");
	_CC_AC("incognito",			&ChatCmd_AdminIncognito,		CCF_ADMIN|CCF_LOBBY, ARGVNoMin, ARGVNoMax, true,"/incognito <name>", "");
	_CC_AC("eventban",			&ChatCmd_AdminEventBanPlayer,	CCF_ADMIN|CCF_ALL, ARGVNoMin, 1, true, "/eventban <charname>", "");	// 2010-08-09 수정됨 - 홍기주
	_CC_AC("eventunban",		&ChatCmd_AdminEventUnbanPlayer,	CCF_ADMIN|CCF_ALL, ARGVNoMin, 1, true, "/eventunban <charname>", "");	// 2010-08-09 수정됨 - 홍기주

	//_CC_AC("admin_switch_laddergame",		&ChatCmd_AdminSwitchCreateLadderGame,	CCF_ALL, ARGVNoMin, ARGVNoMax, true,"/admin_switch_laddergame 1", "");		
	//_CC_AC("admin_reset_all_hacking_block", &ChatCmd_AdminResetAllHackingBlock,		CCF_ALL, ARGVNoMin, ARGVNoMax, true, "/admin_reset_all_hacking_block", "");
	//_CC_AC("admin_reload_gambleitem",		&ChatCmd_AdminReloadGambleitem,			CCF_ALL, ARGVNoMin, ARGVNoMax, true, "/admin_reload_gambleitem", "");
	//_CC_AC("admin_dump_gambleitem_log",		&ChatCmd_AdminDumpGambleitemLog,		CCF_ALL, ARGVNoMin, ARGVNoMax, true, "/admin_dump_gambleitem_log", "");
	//_CC_AC("admin_commander",				&ChatCmd_AdminAssasin,					CCF_ALL|CCF_GAME, ARGVNoMin, ARGVNoMax, true, "/admin_commander", "");
	//_CC_AC("ladderlist",					&ChatCmd_AdminRequestLadderList,					CCF_ALL|CCF_GAME, ARGVNoMin, ARGVNoMax, true, "/ladderlist", "");
	//_CC_AC("joinladder",					&ChatCmd_AdminRequestLadderJoin,					CCF_ALL|CCF_LOBBY, ARGVNoMin, 1, true, "/joinladder <id>", "");
	
	
	_CC_AC("toggle_clan_ui",	 &ChatCmd_ToggleClanUI, CCF_ALL,				ARGVNoMin, ARGVNoMax, true, "/toggle_clan_ui", "");
	_CC_AC("god",				&ChatCmd_AdminGod,		CCF_ADMIN,				ARGVNoMin, 2    , true,"/god <name> <state>", "");
	_CC_ALIAS("gtgod","god");
	_CC_AC("reflect",			&ChatCmd_Reflect,		CCF_ADMIN,				ARGVNoMin, 2    , true,"/reflect", "");
	_CC_AC("claninfo",			&ChatCmd_ClanInfo,		CCF_GAME,				ARGVNoMin, 1    , true,"/claninfo username", ""); //Is it this?
	_CC_AC("check",				&ChatCmd_Check,			CCF_GAME,				ARGVNoMin, 1    , true,"/check <name>", "");
	
	_CC_AC("camera",			&ChatCmd_Camera,			CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/camera <distance>", "");
	_CC_AC("start",				&ChatCmd_Start,			CCF_STAGE | CCF_ADMIN,	ARGVNoMin, ARGVNoMax, true,"/start", "");
	_CC_AC("df",				&ChatCmd_df,			CCF_GAME  | CCF_ADMIN,	ARGVNoMin, ARGVNoMax, true,"/df", "");
	_CC_AC("clanwar",			&ChatCmd_Clanwar,		CCF_LOBBY,				ARGVNoMin, ARGVNoMax, true,"/clanwar", "");
	//Dev Mode Commands
	_CC_AC("devmode",			&ChatCmd_DevMode,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/devmode", "");
	_CC_AC("GetUGrade",			&ChatCmd_GetUGrade,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/GetUGrade", "");//temporary
	//_CC_AC("ghost",				&ChatCmd_Ghost,			CCF_GAME | CCF_ADMIN,	ARGVNoMin, 1    , true,"/ghost", ""); 
	_CC_AC("gravity",			&ChatCmd_SetGravity,	CCF_GAME | CCF_ALL,		ARGVNoMin, 1	, true,"/gravity <percent>", "");
	_CC_AC("speed",				&ChatCmd_SetSpeed,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1	, true,"/speed <percent>", "");
	//_CC_AC("setmine",			&ChatCmd_SetMine,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/setmine <type>", ""); 
	_CC_AC("changevelocity",	&ChatCmd_SetVelocity,	CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/changevelocity <velocity>", ""); 
	_CC_AC("changedelay",		&ChatCmd_SetDelay,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/changedelay <delay>", "");
	_CC_AC("changerange",		&ChatCmd_SetRange,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/changerange <range>", "");
	_CC_AC("changeaccuracy",	&ChatCmd_SetAccuracy,	CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/changeaccuracy <accuracy>", "");
	_CC_AC("changepelletcount",	&ChatCmd_SetPelletCount,CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/changepelletcount <pellets>", "");
	_CC_AC("wireframe",			&ChatCmd_Wireframe,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/wireframe", "");
	_CC_AC("collision",			&ChatCmd_Collision,		CCF_GAME | CCF_ALL,		ARGVNoMin, 1    , true,"/collision", "");
	//End DevMode Commands



	_CC_AC("sbtest",	&ChatCmd_ShotbotTest,	CCF_GAME			, ARGVNoMin, ARGVNoMax, true, "/sbtest <name> <state>", "");
	_CC_AC("bomb",		&ChatCmd_TestBomb,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/bomb <name>", "");
	//_CC_AC("hacklog",	&ChatCmd_Hacklog,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/hacklog <name>", "");
	_CC_AC("goto",		&ChatCmd_TeleTo,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/goto <name>", "");
	_CC_AC("ignorelist",&ChatCmd_IgnoreList,	CCF_ALL, ARGVNoMin, ARGVNoMax , true, "/ignorelist", "");
	_CC_AC("ignore",	&ChatCmd_Ignore,		CCF_ALL, ARGVNoMin, 1 , true, "/ignore <name>", "");
	_CC_AC("unignore",	&ChatCmd_Unignore,		CCF_ALL, ARGVNoMin, 1 , true, "/unignore <name>", "");
	_CC_AC("tele",		&ChatCmd_Tele,			CCF_GAME | CCF_ADMIN, ARGVNoMin, 3 , true, "/tele <x> <y> <z>", "");

	_CC_AC("teleother",	&ChatCmd_Teleother,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 4 , true, "/teleother <name> <x> <y> <z>", "");
	
	_CC_AC("target",	&ChatCmd_SetTarget,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/target <name>", "");
	_CC_AC("summon",	&ChatCmd_Summon,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/summon <name>", "");
	_CC_AC("summonall",	&ChatCmd_SummonAll,		CCF_GAME | CCF_ADMIN, ARGVNoMin, ARGVNoMax , true, "/summonall", "");
	_CC_AC("spawn",		&ChatCmd_Spawn,			CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/spawn <name>", "");
	_CC_AC("slap",		&ChatCmd_Slap,			CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/slap <name>", "");
	//_CC_AC("icon",		&ChatCmd_PeerIcon,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/icon <name>", "");
	_CC_AC("changeitem",&ChatCmd_PeerChangeItem,CCF_GAME | CCF_ADMIN, ARGVNoMin, 2 , true, "/changeitem <name> <ItemID>", "");
	_CC_AC("getitems",  &ChatCmd_GetCharItems,	CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/getitems <name>", "");
	_CC_AC("search",	&ChatCmd_SearchItems,	CCF_GAME | CCF_ADMIN, ARGVNoMin, 1 , true, "/search <ItemName>", "");
	_CC_AC("design",	&ChatCmd_Design,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 2 , true, "/design <number>", "");
	_CC_AC("freeze",	&ChatCmd_Freeze,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 3 , true, "/freeze <name> <?seconds?>", "");
	_CC_AC("freezeall",	&ChatCmd_FreezeAll,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 2 , true, "/freezeall <seconds>", "");
	_CC_AC("slapall",   &ChatCmd_SlapAll,		CCF_GAME | CCF_ADMIN, ARGVNoMin, ARGVNoMax, true, "/slapall", "");
	_CC_AC("killall",   &ChatCmd_KillAll,		CCF_GAME | CCF_ADMIN, ARGVNoMin, ARGVNoMax, true, "/killall", "");
	_CC_AC("spawnall",  &ChatCmd_SpawnAll,		CCF_GAME | CCF_ADMIN, ARGVNoMin, ARGVNoMax, true, "/spawnall", "");
	
	_CC_AC("permkick",  &ChatCmd_PermKick,		CCF_GAME | CCF_ADMIN, ARGVNoMin, 1, true, "/permkick <name>", "");
	_CC_AC("removekick",&ChatCmd_RemoveKick,	CCF_GAME | CCF_ADMIN, ARGVNoMin, 1, true, "/removekick <name>", "");
	_CC_AC("stagesummon",&ChatCmd_StageSummon,	CCF_ALL  | CCF_ADMIN, ARGVNoMin, 1, true, "/stagesummon <name>", "");
	//_CC_AC("autoquest",	&ChatCmd_AutoQuest, CCF_ALL, ARGVNoMin,  ARGVNoMax    , true,"/autoquest", "");
	_CC_AC("add",		&ChatCmd_AddFriend,		CCF_ALL, ARGVNoMin, 1, true,"/add <name>", "");
	_CC_AC("afk",		&ChatCmd_AFK,			CCF_ALL, ARGVNoMin, ARGVNoMax, true,"/afk <message>", "");
	_CC_AC("hp",		&ChatCmd_HPCheck,		CCF_GAME | CCF_ALL, ARGVNoMin, ARGVNoMax, true,"/hp", "");
	_CC_AC("hpteam",	&ChatCmd_TeamHPCheck,	CCF_GAME | CCF_ALL, ARGVNoMin, ARGVNoMax, true,"/hpteam", "");
	_CC_AC("time",		&ChatCmd_Time,			CCF_ALL, ARGVNoMin, ARGVNoMax, true,"/time", "");

	_CC_AC("lobbychat", &ChatCmd_StaffLobbyChat,CCF_ALL, ARGVNoMin, ARGVNoMax, true, "/lobbychat", "");

#ifdef _DEBUG
	//_CC_AC("캐릭터전송", &ChatCmd_CopyToTestServer, CCF_LOBBY, ARGVNoMin, 0, "/캐릭터전송", "캐릭터 정보를 테스트서버에 복사합니다.");
	_CC_AC("team", &ChatCmd_LadderInvite, CCF_LOBBY, ARGVNoMin, ARGVNoMax, true, "", "");
	_CC_AC("test", &ChatCmd_Test, CCF_ALL, ARGVNoMin, 1, true ,"/test <name>", "테스트를 수행합니다.");
	_CC_AC("laddertest", &ChatCmd_LadderTest, CCF_ALL|CCF_ALL, ARGVNoMin, ARGVNoMax, true ,"/laddertest", "래더테스트를 요청합니다.");
	_CC_AC("launchtest", &ChatCmd_LaunchTest, CCF_ALL|CCF_ALL, ARGVNoMin, ARGVNoMax, true ,"/laddertest", "래더테스트를 요청합니다.");
#endif

	_CC_AC("go",		&ChatCmd_Go, CCF_LOBBY, ARGVNoMin, 1, true, "/go Room number "," to go directly to the game room.");


	// 아직 다 구현되지 않았음
	_CC_AC("charinfo",		&ChatCmd_RequestPlayerInfo, CCF_LOBBY|CCF_STAGE, ARGVNoMin, 2, true ,"/charinfo <name>", "");
	_CC_ALIAS("profile","pf");

	// 테스트 전용
	//_CC_AC("qgtgod",			&ChatCmd_QUESTTEST_God,				CCF_ADMIN | CCF_GAME, ARGVNoMin, 1    , true,"/gtgod", "");
	//_CC_AC("qgtspn",			&ChatCmd_QUESTTEST_SpawnNPC,		CCF_ADMIN | CCF_GAME, ARGVNoMin, 2    , true,"/gtspn <NPC타입> <NPC수>", "");
	//_CC_AC("qgtclear",		&ChatCmd_QUESTTEST_NPCClear,	CCF_ADMIN | 	CCF_GAME, ARGVNoMin, 1    , true,"/gtclear", "");
	//_CC_AC("qgtreload",		&ChatCmd_QUESTTEST_Reload,			CCF_ADMIN | CCF_GAME, ARGVNoMin, 1    , true,"/gtreload", "");
	//_CC_AC("qgtsc",			&ChatCmd_QUESTTEST_SectorClear,		CCF_ADMIN | CCF_GAME, ARGVNoMin, 1    , true,"/gtsc", "");
	//_CC_AC("qgtfin",			&ChatCmd_QUESTTEST_Finish,		CCF_ADMIN | 	CCF_GAME, ARGVNoMin, 1    , true,"/gtfin", "");
	//_CC_AC("qgtlspn",			&ChatCmd_QUESTTEST_LocalSpawnNPC,CCF_ADMIN | 	CCF_GAME, ARGVNoMin, 2    , true,"/gtlspn <NPC타입> <NPC수>", "");
	//_CC_AC("qgtweaknpcs",	&ChatCmd_QUESTTEST_WeakNPCs,		CCF_ADMIN | CCF_GAME, ARGVNoMin, 1    , true,"/gtweaknpcs", "");

	//Darkgunz extra
	_CC_AC("info", &ChatCmd_Info, CCF_ALL, ARGVNoMin, ARGVNoMax, true, "/info", "");
}

void OutputCmdHelp(const char* cmd)
{
	ZChatCmdManager* pCCM = ZGetGameInterface()->GetChat()->GetCmdManager();
	ZChatCmd* pCmd = pCCM->GetCommandByName(cmd);
	if (pCmd == NULL) return;

	if ( (pCmd->GetFlag() & CCF_ALL) && !ZGetMyInfo()->IsAdminGrade())
		return;

    char szBuf[512];
	sprintf(szBuf, "%s: %s", pCmd->GetName(), pCmd->GetHelp());
	ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
}

void OutputCmdUsage(const char* cmd)
{
	ZChatCmdManager* pCCM = ZGetGameInterface()->GetChat()->GetCmdManager();
	ZChatCmd* pCmd = pCCM->GetCommandByName(cmd);
	if (pCmd == NULL) return;

	if ( (pCmd->GetFlag() & CCF_ALL) && !ZGetMyInfo()->IsAdminGrade())
		return;

    char szBuf[512];
	sprintf(szBuf, "%s: %s", ZMsg(MSG_WORD_USAGE), pCmd->GetUsage());
	ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
}

void OutputCmdWrongArgument(const char* cmd)
{
	ZChatOutput( ZMsg(MSG_WRONG_ARGUMENT) );
	OutputCmdUsage(cmd);
}

unsigned long ul_tDCounter = 0;
void ChatCmd_DamageCounter(const char* line, const int argc, char **const argv)
{
	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
		return;

	if(ul_tDCounter < timeGetTime() - 5000 || !ul_tDCounter)
	{
		ul_tDCounter = timeGetTime();
		ZGetGameClient()->PeerChat("I have caused %d damage.", /*ZGetGame()->m_pMyCharacter->GetStatus().Ref().nDamageTaken,*/ ZGetGame()->m_pMyCharacter->GetStatus().Ref().nDamageCaused);
	}
}


void ChatCmd_Wireframe(const char* line, const int argc, char **const argv)
{
	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;
	if(!bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER)
		return;

	if (ZGetGameClient()->m_DevMode) 
	{
		ZGetGame()->m_bShowWireframe =! ZGetGame()->m_bShowWireframe;
		if (ZGetMyInfo()->GetUGradeID() != MMUG_HCODER)
		{
			if(ZGetGame()->m_bShowWireframe)
				ZGetGameClient()->PeerChat("I have toggled Wireframe mode! If this is an event, I'm a cheater!");
			else
				ZGetGameClient()->PeerChat("I have disabled Wireframe mode!");
		}
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_Collision(const char* line, const int argc, char **const argv)
{
	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;
	if(!bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER)
		return;

	if (ZGetGameClient()->m_DevMode) 
	{
		ZGetGame()->m_bCollision =! ZGetGame()->m_bCollision;
		if (ZGetMyInfo()->GetUGradeID() != MMUG_HCODER)
		{
			if(ZGetGame()->m_bCollision)
				ZGetGameClient()->PeerChat("I have toggled Collision mode!");
			else
				ZGetGameClient()->PeerChat("I have disabled Collision mode!");
		}
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}

}

void ChatCmd_SetGravity(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) 
	{

		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}
		int grav = atoi(argv[1]);

		ZGetGame()->SetGravityMod(grav / 100.0f);

		char szBuf[512];
		sprintf(szBuf, "Gravity is now: %d%%",grav);
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}

}

void ChatCmd_SetSpeed(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) 
	{

		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}
		int speed = atoi(argv[1]);

		ZGetGame()->SetSpeedMod(speed / 100.0f);

		char szBuf[512];
		sprintf(szBuf, "Speed is now: %d%%",speed);
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}

}

void ChatCmd_SetMine(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) 
	{

		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}
		int mine = atoi(argv[1]);

		if(mine <= 0)
		{
			mine = 1;
		}

		ZGetGameClient()->m_CZmine = mine;

		char szBuf[512];
		sprintf(szBuf, "Mine Type is Now: %d",mine);
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}

}

void ChatCmd_SetVelocity(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) {
		if (argc < 2) 
			{
				OutputCmdWrongArgument(argv[0]);
				return;
			}
			int velocity = atoi(argv[1]);
		if(velocity <= 0)
			{
				velocity = 1;
			}
			MGetMatchItemDescMgr()->GetItemDesc(ZGetGame()->m_pMyCharacter->GetItems()->GetSelectedWeapon()->GetDescID())->m_fRocketVelocity.Set_CheckCrc(velocity);
			ZGetGameClient()->m_CZitem = true;
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_SetPelletCount(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) 
	{
		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}

		int pelletc = atoi(argv[1]);

		if(pelletc <= 0)
		{
			pelletc = 1;
		}
		MGetMatchItemDescMgr()->GetItemDesc(ZGetGame()->m_pMyCharacter->GetItems()->GetSelectedWeapon()->GetDescID())->m_nPelletCount.Set_CheckCrc(pelletc);
		ZGetGameClient()->m_CZitem = true;
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_SetDelay(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) {
		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}

		int delay = atoi(argv[1]);

		if(delay <= 0)
		{
			delay = 1;
		}
		MGetMatchItemDescMgr()->GetItemDesc(ZGetGame()->m_pMyCharacter->GetItems()->GetSelectedWeapon()->GetDescID())->m_nDelay.Set_CheckCrc(delay);
		ZGetGameClient()->m_CZitem = true;
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_SetRange(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) {
		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}

		int range = atoi(argv[1]);

		if(range <= 0)
		{
			range = 1;
		}
		MGetMatchItemDescMgr()->GetItemDesc(ZGetGame()->m_pMyCharacter->GetItems()->GetSelectedWeapon()->GetDescID())->m_nRange.Set_CheckCrc(range);
		ZGetGameClient()->m_CZitem = true;
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_SetAccuracy(const char* line, const int argc, char **const argv)
{
	if (ZGetGameClient()->m_DevMode) {
		if (argc < 2) 
		{
			OutputCmdWrongArgument(argv[0]);
			return;
		}

		int accuracy = atoi(argv[1]);

		if(accuracy < 0)
			accuracy = 0;
		if (accuracy > 100)
			accuracy = 100;
		MGetMatchItemDescMgr()->GetItemDesc(ZGetGame()->m_pMyCharacter->GetItems()->GetSelectedWeapon()->GetDescID())->m_nControllability.Set_CheckCrc(accuracy);
		ZGetGameClient()->m_CZitem = true;
	}
	else
	{
		char szBuf[512];
		sprintf(szBuf, "Only Usable During Devmode.");
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
}

void ChatCmd_ShotbotTest( const char* line, const int argc, char **const argv )
{
	if (!ZGetMyInfo()->IsAdminGrade()) return;

	if (argc < 3) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszPlayerName = argv[1];

	bool bState = 0;

	if( stricmp(argv[2], "1")==0 ) bState = true;
	if( stricmp(argv[2], "0")==0 ) bState = false;
	if( stricmp(argv[2], "on")==0 ) bState = true;
	if( stricmp(argv[2], "off")==0 ) bState = false;
	if( stricmp(argv[2], "true")==0 ) bState = true;
	if( stricmp(argv[2], "false")==0 ) bState = false;
	ZPostAdminShotBot(pszPlayerName, bState);
}

void ChatCmd_ToggleClanUI(const char* line, const int argc, char ** const argv)
{
	if(ZGetGameClient()->GetChannelType() == MCHANNEL_TYPE_CLAN)
	{
		ZGameInterface* pInterface = ZGetGameInterface();
		pInterface->InitClanLobbyUI(!pInterface->IsShowClanUI());
	}
}

void ChatCmd_TestBomb(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_BOMB, MCmdParamStr(pszName));
}

void ChatCmd_IgnoreList(const char* line, const int argc, char **const argv)
{

	ZGameClient *pClient = ZGetGameClient();
	for (int i = 0; i < pClient->m_iIgnoreCount; i++)
	{
		ZChatOutput(pClient->m_szIgnoreList[i]);
	}
}

void ChatCmd_Ignore(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	if (stricmp(pszName,ZGetMyInfo()->GetCharName()) == 0) return;

	if (ZGetGameClient()->m_iIgnoreCount >= 255)
	{
		ZGetGameClient()->OutputMessage("Your ignore list is full");
		return;
	}

	if (ZGetGameClient()->AddIgnore(pszName))
		ZGetGameClient()->OutputMessage_Sub("%s was added to your ignore list",pszName);
	else
		ZGetGameClient()->OutputMessage_Sub("%s is already on your ignore list",pszName);
}

void ChatCmd_Unignore(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	if (ZGetGameClient()->RemoveIgnore(pszName))
		ZGetGameClient()->OutputMessage_Sub("%s was removed from ignore list",pszName);
	else
		ZGetGameClient()->OutputMessage_Sub("%s is not in your ignore list",pszName);
}

void ChatCmd_Hacklog(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_ASK_HACKLOG, MCmdParamStr(pszName));
}

void ChatCmd_Slap(const char* line, const int argc, char **const argv)
{
	if (!ZGetGame() || !ZGetGame()->m_pMyCharacter) return;
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];
	MUID uidTarget = MUID(0,0);

	if(!stricmp(ZGetGame()->m_pMyCharacter->GetUserName(),pszName))
	{
		uidTarget = ZGetMyUID();
	}
	else
	{
		for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
		{
			MMatchPeerInfo * tInfo = (*itor).second;
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			if (peerChar)
			{
				if (!stricmp(peerChar->GetUserNameA(),pszName))
				{
					uidTarget = peerChar->GetUID();
				}
			}
		}
	}

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SLAP, MCmdParamUID(uidTarget));
}

void ChatCmd_PeerIcon(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];
	MUID uidTarget = MUID(0,0);

	if(!stricmp(ZGetGame()->m_pMyCharacter->GetUserNameA(),pszName))
	{
		uidTarget = ZGetMyUID();
	}
	else
	{
		for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
		{
			MMatchPeerInfo * tInfo = (*itor).second;
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			if (peerChar)
			{
				if (!stricmp(peerChar->GetUserNameA(),pszName))
				{
					uidTarget = peerChar->GetUID();
				}
			}
		}
	}

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_ICON, MCmdParamUID(uidTarget));
}

void ChatCmd_PeerChangeItem(const char* line, const int argc, char **const argv)
{
	if (argc < 3) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];
	int nItemID = atoi(argv[2]);
	MUID uidTarget = MUID(0,0);

	if(!stricmp(ZGetGame()->m_pMyCharacter->GetUserNameA(),pszName))
	{
		uidTarget = ZGetMyUID();
	}
	else
	{
		for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
		{
			MMatchPeerInfo * tInfo = (*itor).second;
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			if (peerChar)
			{
				if (!stricmp(peerChar->GetUserNameA(),pszName))
				{
					uidTarget = peerChar->GetUID();
				}
			}
		}
	}

	ZPOSTCMD2(MC_MATCH_GAME_REQUEST_CHANGE_ITEM, MCmdParamUID(uidTarget),MCmdParamInt(nItemID));
}

void ChatCmd_GetCharItems(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	if (!stricmp(ZGetMyInfo()->GetCharName(),pszName))
	{
		ZCharacterItem* pItems = ZGetGame()->m_pMyCharacter->GetItems();
				
		for(int i = MMCIP_HEAD; i <= MMCIP_AVATAR; i++)
		{
			MMatchItemDesc* pDesc = pItems->GetItem(MMatchCharItemParts(i))->GetDesc();
			if (!pDesc)
			{
				ZGetGameClient()->OutputMessage_Sub("%s == NULL.",GetCharItemPartsStr(MMatchCharItemParts(i)));
				continue;
			}
			if(MMatchCharItemParts(i) == MMCIP_FINGERL || MMatchCharItemParts(i) == MMCIP_FINGERR)
				continue;

			ZGetGameClient()->OutputMessage_Sub("[%d]%s - %s",pDesc->m_nID,pDesc->m_pMItemName->Ref().m_szItemName,GetCharItemPartsStr(MMatchCharItemParts(i)));
		}
		return;
	}

	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar)
		{
			if (!stricmp(peerChar->GetUserNameA(),pszName))
			{
				ZCharacterItem* pItems = peerChar->GetItems();
				
				for(int i = MMCIP_HEAD; i <= MMCIP_AVATAR; i++)
				{
					MMatchItemDesc* pDesc = pItems->GetItem(MMatchCharItemParts(i))->GetDesc();
					if (!pDesc)
					{
						ZGetGameClient()->OutputMessage_Sub("%s == NULL.",GetCharItemPartsStr(MMatchCharItemParts(i)));
						continue;
					}

					if(MMatchCharItemParts(i) != MMCIP_FINGERL && MMatchCharItemParts(i) != MMCIP_FINGERR)
						ZGetGameClient()->OutputMessage_Sub("[%d]%s - %s",pDesc->m_nID,pDesc->m_pMItemName->Ref().m_szItemName,GetCharItemPartsStr(MMatchCharItemParts(i)));
				}
				break;
			}
		}
	}
}

void ChatCmd_SearchItems(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	string pszItemName = argv[1];
	std::transform(pszItemName.begin(),pszItemName.end(),pszItemName.begin(),::tolower);

	MMatchItemDescMgr::iterator it = MGetMatchItemDescMgr()->begin();
	MMatchItemDescMgr::const_iterator end = MGetMatchItemDescMgr()->end();
	MMatchItemDesc* pItemDesc = NULL;
	int nItemCount = 0;

	for( ; it != end; ++it )
	{
		pItemDesc = reinterpret_cast<MMatchItemDesc*>( it->second );
		if (!pItemDesc)
			continue;

		string szItemNameOrig = pItemDesc->m_pMItemName->Ref().m_szItemName;
		string szItemName = szItemNameOrig.c_str();
		int   nItemID    = pItemDesc->m_nID;

		std::transform(szItemName.begin(),szItemName.end(),szItemName.begin(),::tolower);

		if (strstr(szItemName.c_str(),pszItemName.c_str()))
		{
			ZGetGameClient()->OutputMessage_Sub("Found: [%d]%s",nItemID,szItemNameOrig.c_str());
			nItemCount++;
		}
	}

	if (nItemCount > 0)
		ZGetGameClient()->OutputMessage_Sub("Total Found: %d",nItemCount);
	else
		ZGetGameClient()->OutputMessage_Sub("No Results Found.");
}

void ChatCmd_Design(const char* line, const int argc, char **const argv)
{
	return;
}

void ChatCmd_SetTarget(const char* line, const int argc, char **const argv)
{
	if (!ZGetGameClient()->m_DevMode || !(ZGetMyInfo()->GetUGradeID() == MMUG_CODER || ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)) return;

	if (argc < 2) 
	{
		ZGetGame()->m_bTarget = false;
		ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),"Target mode disabled");
		return;
	}
	char* pszName = argv[1];

	(ZGetGame()->m_bTarget) ? ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),"Target mode enabled") : ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),"Target mode disabled");

	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		if(!stricmp(tInfo->CharInfo.szName, pszName))
		{
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			if (peerChar && !peerChar->IsDie())
			{
				if (!ZGetGame()->m_bTarget)
				{
					ZGetGame()->m_bTarget = true;
					ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),"Target mode enabled");
				}
				ZGetGame()->SetTarget(tInfo->uidChar);
				char newtarget[256];
				sprintf(newtarget,"New target selected: %s",peerChar->GetUserName());
				ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),newtarget);
				break;
			}

		}
	}
}

void ChatCmd_TeleTo(const char* line, const int argc, char **const argv)
{
	//if (ZGetMyInfo()->GetUGradeID() == MMUG_HIDDENGM || ZGetMyInfo()->GetUGradeID() == MMUG_GAMEMASTER || ZGetMyInfo()->GetUGradeID() == MMUG_TRIALGM || ZGetMyInfo()->GetUGradeID() == MMUG_DEVELOPER || ZGetMyInfo()->GetUGradeID() == MMUG_TESTER) return;
	if(!IsGunzHighAdminIncludeEMGM(ZGetMyInfo()->GetUGradeID()))
		return;


	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		if(!stricmp(tInfo->CharInfo.szName, pszName))
		{
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			ZGetGame()->m_pMyCharacter->SetPosition(peerChar->GetPosition());

			break;
		}
	}
}

void ChatCmd_Tele(const char* line, const int argc, char **const argv)
{
	if (argc < 4) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	rvector telepos;
	telepos.x = (float)atoi(argv[1]);
	telepos.y = (float)atoi(argv[2]);
	telepos.z = (float)atoi(argv[3]);
	ZGetGame()->m_pMyCharacter->SetPosition(telepos);
}

//unused 
void ChatCmd_Teleother(const char* line, const int argc, char **const argv)
{
	if (argc < 5) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	ZPostTeleportOtherPlayer(argv[1],(float)atoi(argv[2]),(float)atoi(argv[3]),(float)atoi(argv[4]));
}
void ChatCmd_Summon(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SUMMON, MCmdParamStr(pszName));
}

void ChatCmd_SummonAll(const char* line, const int argc, char **const argv)
{
	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar && !peerChar->IsDie())
		{
			ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SUMMON, MCmdParamStr(peerChar->GetUserName()));
		}
	}
}

void ChatCmd_Spawn(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszName = argv[1];
	ZGame *pGame = ZGetGame();
	D3DXVECTOR3 pos = pGame->m_pMyCharacter->GetPosition();
	D3DXVECTOR3 dir = pGame->m_pMyCharacter->GetDirection();

	/*if(!stricmp(pszName,ZGetMyInfo()->GetCharName()))
	{
		ZPostSpawn(pos,dir);
		return;
	}*/

	ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SPAWNP, MCmdParamStr(pszName));
}

void ChatCmd_Freeze(const char* line, const int argc, char **const argv)
{
	if (!ZGetGame() || !ZGetGame()->m_pMyCharacter) return;
	if (!IsDGAdminGrade(ZGetMyInfo()->GetUGradeID()))
		return;

	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;
	if(!bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER)
		return;

	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}	
	char* pszName = argv[1];
	int nSeconds;
	
	if ( argc == 2)
		nSeconds = 10;
	else
		nSeconds = atoi(argv[2]);

	MUID uidTarget = MUID(0,0);

	if(!stricmp(ZGetGame()->m_pMyCharacter->GetUserName(),pszName))
	{
		uidTarget = ZGetMyUID();
	}
	else
	{
		for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
		{
			MMatchPeerInfo * tInfo = (*itor).second;
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
			if (peerChar)
			{
				if (!stricmp(peerChar->GetUserNameA(),pszName))
				{
					uidTarget = peerChar->GetUID();
				}
			}
		}
	}

	if (ZGetCharacterManager()->Find(uidTarget))
	{
		ZGetGameClient()->OutputMessage_Sub("Freezing %s for %d seconds.",pszName,nSeconds);
		ZPOSTCMD2(MC_MATCH_GAME_REQUEST_FREEZE, MCmdParamUID(uidTarget), MCmdParamInt(nSeconds));
	}
	else
		ZGetGameClient()->OutputMessage_Sub("Character not found");
}

void ChatCmd_FreezeAll(const char* line, const int argc, char **const argv)
{
	if (!ZGetGame() || !ZGetGame()->m_pMyCharacter) return;
	if (!IsDGAdminGrade(ZGetMyInfo()->GetUGradeID()))
		return;

	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;
	if(!bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER)
		return;

	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}	
	int nSeconds = atoi(argv[1]);

	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar)
		{
			ZPOSTCMD2(MC_MATCH_GAME_REQUEST_FREEZE, MCmdParamUID(peerChar->GetUID()), MCmdParamInt(nSeconds));
		}
	}
	ZGetGameClient()->OutputMessage_Sub("Freezing everyone for %d seconds.",nSeconds);
}

void ChatCmd_SlapAll(const char* line, const int argc, char **const argv)
{
	if (!ZGetGame() || !ZGetGame()->m_pMyCharacter) return;
	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar && !peerChar->IsDie())
		{
			ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SLAP, MCmdParamUID(peerChar->GetUID()));
		}
	}
}

void ChatCmd_KillAll(const char* line, const int argc, char **const argv)
{
	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;

	if(bEvent)
	{
		if(!IsHighAdmin(ZGetMyInfo()->GetUGradeID()) && ZGetMyInfo()->GetUGradeID() != MMUG_EVENTMANAGER)
			return;
	}
	else if(!IsHighAdmin(ZGetMyInfo()->GetUGradeID()) && ZGetMyInfo()->GetUGradeID() != MMUG_CODER)
		return;

	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar && !peerChar->IsDie())
		{
			ZPOSTCMD1(MC_MATCH_GAME_REQUEST_BOMB, MCmdParamStr(peerChar->GetUserName()));
		}
	}
}

void ChatCmd_SpawnAll(const char* line, const int argc, char **const argv)
{
	for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
	{
		MMatchPeerInfo * tInfo = (*itor).second;
		ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		if (peerChar)
		{
			ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SPAWNP, MCmdParamStr(peerChar->GetUserName()));
		}
		ZPOSTCMD1(MC_MATCH_GAME_REQUEST_SPAWNP, MCmdParamStr(ZGetGame()->m_pMyCharacter->GetUserName()));
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////

void ChatCmd_Help(const char* line, const int argc, char **const argv)
{
	ZChatCmdManager* pCCM = ZGetGameInterface()->GetChat()->GetCmdManager();

	char szBuf[1024] = "";

	//////////////////////////////////////////////////////////

	GunzState state = ZApplication::GetGameInterface()->GetState();

	if( state==GUNZ_GAME ) {
		// 자신의 캐릭터가 레벨이 1-10 사이인 경우만..
		if(ZGetMyInfo()) {
			if(ZGetMyInfo()->GetLevel() < 10) {
				if( ZGetGame() ) {
					ZGetGame()->m_HelpScreen.ChangeMode();
					return;
				}
			}
		}
	}

	if (argc == 1)
	{
		
		ZChatCmdFlag nCurrFlag = CCF_NONE;

		switch (state)
		{
			case GUNZ_LOBBY: nCurrFlag = CCF_LOBBY; break;
			case GUNZ_STAGE: nCurrFlag = CCF_STAGE; break;
			case GUNZ_GAME: nCurrFlag = CCF_GAME; break;
		}

		sprintf(szBuf, "%s: ", ZMsg(MSG_WORD_COMMANDS));

		int nCnt=0;
		int nCmdCount = pCCM->GetCmdCount();

		for (ZChatCmdMap::iterator itor = pCCM->GetCmdBegin(); itor != pCCM->GetCmdEnd(); ++itor)
		{
			nCnt++;
			ZChatCmd* pCmd = (*itor).second;

			if ((pCmd->GetFlag() & CCF_ALL)) continue;
			if (!(pCmd->GetFlag() & nCurrFlag)) continue;

			strcat(szBuf, pCmd->GetName());

			if (nCnt != nCmdCount) strcat(szBuf, ", ");
		}
		switch (state)
		{
			/*case GUNZ_LOBBY:
				strcat( szBuf, "go");
				break;*/
			case GUNZ_STAGE:
				strcat( szBuf, "kick");
				break;
			case GUNZ_GAME:
				strcat( szBuf, "kick");
				break;
		}
		if (ZGetMyInfo()->IsAdminGrade())
		{
			strcat(szBuf,"\n-Staff Commands-\n");
			for (ZChatCmdMap::iterator itor = pCCM->GetCmdBegin(); itor != pCCM->GetCmdEnd(); ++itor)
			{
				nCnt++;
				ZChatCmd* pCmd = (*itor).second;

				if (!(pCmd->GetFlag() & CCF_ALL)) continue;
				if (!(pCmd->GetFlag() & nCurrFlag)) continue;
				if (pCmd->GetFlag() & CCF_DEV)		continue;

				strcat(szBuf, pCmd->GetName());

				if (nCnt != nCmdCount) strcat(szBuf, ", ");
			}
		}
		if (ZGetGameClient()->m_DevMode)
		{
			strcat(szBuf,"\n-Devmode Commands-\n");
			for (ZChatCmdMap::iterator itor = pCCM->GetCmdBegin(); itor != pCCM->GetCmdEnd(); ++itor)
			{
				nCnt++;
				ZChatCmd* pCmd = (*itor).second;

				if (!(pCmd->GetFlag() & CCF_DEV)) continue;

				strcat(szBuf, pCmd->GetName());

				if (nCnt != nCmdCount) strcat(szBuf, ", ");
			}
		}

		// 엄청난 하드코딩... 어쩔수 없다... -ㅇ-

		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);

		sprintf(szBuf, "%s: /h %s", ZMsg(MSG_WORD_HELP), ZMsg(MSG_WORD_COMMANDS));
		ZChatOutput(szBuf, ZChat::CMT_SYSTEM);
	}
	else if (argc == 2)
	{
		OutputCmdHelp(argv[1]);
		OutputCmdUsage(argv[1]);
	}
}

void ChatCmd_Go(const char* line, const int argc, char **const argv)
{
	if (argc < 2) return;

	ZRoomListBox* pRoomList = (ZRoomListBox*)ZApplication::GetGameInterface()->GetIDLResource()->FindWidget("Lobby_StageList");
	if (pRoomList == NULL)
		return;

	int nRoomNo = atoi(argv[1]);

	ZPostStageGo(nRoomNo);
}

void ChatCmd_Whisper(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
	{
		ZChatOutput( ZMsg(MSG_CANNOT_CHAT) );
		return;
	}


	char* pszSenderName = "Me";	// 아무거나 보내도 서버에서 채워넣음

	char szName[512] = "";
	char szRName[512] = "";

	MLex lex;
	char* pszMsg = lex.GetOneArg(argv[1], szName, szRName);

	if ( (int)strlen( pszMsg) > 127)
		return;

	// 욕필터링
	if (!ZGetGameInterface()->GetChat()->CheckChatFilter(pszMsg)) return;

	//귓속말 캐릭터 이름 글자 수 제한..jintriple3
	int nNameLen = (int)strlen(szName);
	if ( nNameLen < MIN_CHARNAME)		// 이름이 너무 짧다.
	{
		const char *str = ZErrStr( MERR_TOO_SHORT_NAME );
		if(str)
		{
			char text[1024];
			sprintf(text, "%s (E%d)", str, MERR_TOO_SHORT_NAME);
			ZChatOutput(MCOLOR(96,96,168), text, ZChat::CL_CURRENT);
		}
	}
	else if ( nNameLen > MAX_CHARNAME)		// 이름이 제한 글자수를 넘었다.
	{
		const char *str = ZErrStr( MERR_TOO_LONG_NAME );
		if(str)
		{
			char text[1024];
			sprintf(text, "%s (E%d)", str, MERR_TOO_LONG_NAME);
			ZChatOutput(MCOLOR(96,96,168), text, ZChat::CL_CURRENT);
		}
	}
	else
	{
		if (ZGetGameClient()->IsIgnored(szName))
		{
			ZGetGameClient()->OutputMessage("You cannot whisper a player you are ignoring");
			return;
		}

		ZPostWhisper(pszSenderName, szName, pszMsg);

		// loop back
		char szMsg[512];
		sprintf(szMsg, "(To %s) : %s", szRName, pszMsg);	//jintriple3 유저 네임은 그대로 출력되도록...
		ZChatOutput(MCOLOR(ZCOLOR_CHAT_WHISPER), szMsg, ZChat::CL_CURRENT);
		if (ZApplication::GetGameInterface()->GetState() != GUNZ_STAGE)
			ZChatOutput(MCOLOR(ZCOLOR_CHAT_WHISPER), szMsg, ZChat::CL_STAGE);
		if (ZApplication::GetGameInterface()->GetState() != GUNZ_LOBBY)
			ZChatOutput(MCOLOR(ZCOLOR_CHAT_WHISPER), szMsg, ZChat::CL_LOBBY);
	}
}

void ChatCmd_CreateChatRoom(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszCharName = argv[1];

	if( !MGetChattingFilter()->IsValidStr( pszCharName, 1) ){
		char szMsg[ 256 ];
		ZTransMsg( szMsg, MSG_WRONG_WORD_NAME, 1, MGetChattingFilter()->GetLastFilteredStr());
		ZApplication::GetGameInterface()->ShowMessage( szMsg, NULL, MSG_WRONG_WORD_NAME );
	}
	else
	{
		ZChatOutput( 
			ZMsg(MSG_LOBBY_REQUESTING_CREATE_CHAT_ROOM), 
			ZChat::CMT_SYSTEM );

		ZPostChatRoomCreate(ZGetMyUID(), pszCharName);
	}
}
void ChatCmd_JoinChatRoom(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	ZChatOutput( 
		ZMsg(MSG_LOBBY_REQUESTING_JOIN_CAHT_ROOM), 
		ZChat::CMT_SYSTEM );

	char* pszChatRoomName = argv[1];

	ZPostChatRoomJoin(pszChatRoomName);
}

void ChatCmd_LeaveChatRoom(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	ZChatOutput( 
		ZMsg(MSG_LOBBY_LEAVE_CHAT_ROOM), 
		ZChat::CMT_SYSTEM );

	char* pszRoomName = argv[1];

	ZPostChatRoomLeave(pszRoomName);
}

void ChatCmd_SelectChatRoom(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	ZChatOutput( 
		ZMsg(MSG_LOBBY_CHOICE_CHAT_ROOM), 
		ZChat::CMT_SYSTEM );

	char* pszRoomName = argv[1];

	ZPostSelectChatRoom(pszRoomName);
}

void ChatCmd_InviteChatRoom(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
	{
		ZChatOutput( ZMsg(MSG_CANNOT_CHAT) );
		return;
	}

	char* pszPlayerName = argv[1];

	char szLog[128];
	
	ZTransMsg( szLog, MSG_LOBBY_INVITATION, 1, pszPlayerName );

	ZChatOutput(szLog, ZChat::CMT_SYSTEM);

	ZPostInviteChatRoom(pszPlayerName);
}

void ChatCmd_VisitChatRoom(const char* line, const int argc, char **const argv)
{
	char* pszRoomName = const_cast<char*>(ZGetGameClient()->GetChatRoomInvited());
	ZPostChatRoomJoin(pszRoomName);
}

void ChatCmd_ChatRoomChat(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
	{
		ZChatOutput( ZMsg(MSG_CANNOT_CHAT) );
		return;
	}

	char* pszMsg = argv[1];

	// 욕필터링
	if (!ZGetGameInterface()->GetChat()->CheckChatFilter(pszMsg)) return;

	ZPostChatRoomChat(pszMsg);
}


void ChatCmd_CopyToTestServer(const char* line, const int argc, char **const argv)
{
	// 사용하지 않는다. - 테스트 서버로 정보 복사
	return;


	if (argc != 1) return;

	static unsigned long int st_nLastTime = 0;
	unsigned long int nNowTime = timeGetTime();

#define DELAY_POST_COPY_TO_TESTSERVER		(1000 * 60)		// 5분 딜레이

	if ((nNowTime - st_nLastTime) > DELAY_POST_COPY_TO_TESTSERVER)
	{
		ZPostRequestCopyToTestServer(ZGetGameClient()->GetPlayerUID());

		st_nLastTime = nNowTime;
	}
}


void ChatCmd_StageFollow(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszTarget = argv[1];

	ZPostStageFollow(pszTarget);
}

void ChatCmd_Where(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszTarget = argv[1];

	ZPostWhere(pszTarget);
}

void ChatCmd_Friend(const char* line, const int argc, char **const argv)
{
	//// ZFriendCommandHelper ////
	class ZFriendCommandHelper {
	public:
		enum ZFRIENDCMD {
			ZFRIENDCMD_ADD,
			ZFRIENDCMD_REMOVE,
			ZFRIENDCMD_LIST,
			ZFRIENDCMD_MSG,
			ZFRIENDCMD_UNKNOWN
		};
		ZFRIENDCMD GetSubCommand(const char* pszCmd) {
			if (stricmp(pszCmd, "add") == 0)
				return ZFRIENDCMD_ADD;
			else if (stricmp(pszCmd, "추가") == 0)
				return ZFRIENDCMD_ADD;
			else if (stricmp(pszCmd, "remove") == 0)
				return ZFRIENDCMD_REMOVE;
			else if (stricmp(pszCmd, "삭제") == 0)
				return ZFRIENDCMD_REMOVE;
			else if (stricmp(pszCmd, "list") == 0)
				return ZFRIENDCMD_LIST;
			else if (stricmp(pszCmd, "목록") == 0)
				return ZFRIENDCMD_LIST;
			else if (stricmp(pszCmd, "msg") == 0)
				return ZFRIENDCMD_MSG;
			else if (stricmp(pszCmd, "채팅") == 0)
				return ZFRIENDCMD_MSG;
			else return ZFRIENDCMD_UNKNOWN;
		}
	} friendHelper;

	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char szSubCmd[256] = "";
	char szArg[256] = "";

	MLex lex;
	char* pszMsg = lex.GetOneArg(argv[1], szSubCmd);
	
	//// Sub Command Handler ////
	switch(friendHelper.GetSubCommand(szSubCmd)) {
	case ZFriendCommandHelper::ZFRIENDCMD_ADD:
		{
			lex.GetOneArg(pszMsg, szArg);
			ZPostFriendAdd(szArg);
		}
		break;
	case ZFriendCommandHelper::ZFRIENDCMD_REMOVE:
		{
			lex.GetOneArg(pszMsg, szArg);
			ZPostFriendRemove(szArg);
		}
		break;
	case ZFriendCommandHelper::ZFRIENDCMD_LIST:
		{
			ZPostFriendList();
		}
		break;
	case ZFriendCommandHelper::ZFRIENDCMD_MSG:
		{
			lex.GetOneArg(pszMsg, szArg);
			ZPostFriendMsg(szArg);
		}
		break;
	default:
		OutputDebugString("Unknown Friend Command \n");
		break;
	};
}

void ChatCmd_Clan(const char* line, const int argc, char **const argv)
{
	

	//// ZClanCommandHelper ////
	class ZClanCommandHelper {
	public:
		enum ZCLANCMD {
			ZCLANCMD_CREATE,		// 클랜 생성
			ZCLANCMD_CLOSE,			// 클랜 폐쇄
			ZCLANCMD_JOIN,
			ZCLANCMD_LEAVE,
			ZCLANCMD_EXPEL_MEMBER,	// 강제탈퇴
			ZCLANCMD_LIST,
			ZCLANCMD_MSG,
			
			ZCLANCMD_CHANGE_GRADE,	// 멤버 권한 변경

			ZCLANCMD_UNKNOWN
		};

		ZCLANCMD GetSubCommand(const char* pszCmd) 
		{
			GunzState nGameState = ZApplication::GetGameInterface()->GetState();

			if ((stricmp(pszCmd, "생성") == 0) || (stricmp(pszCmd, "open") == 0))
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_CREATE;
			}
			else if ((stricmp(pszCmd, "폐쇄") == 0) || (stricmp(pszCmd, "해체") == 0) || (stricmp(pszCmd, "close") == 0))
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_CLOSE;
			}
			else if ( (stricmp(pszCmd, "초대") == 0) || (stricmp(pszCmd, "invite") == 0) )
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_JOIN;
			}
			else if ( (stricmp(pszCmd, "탈퇴") == 0) || (stricmp(pszCmd, "leave") == 0) )
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_LEAVE;
			}
			else if ( (stricmp(pszCmd, "권한변경") == 0) || (stricmp(pszCmd, "promote") == 0) )
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_CHANGE_GRADE;
			}
			else if ((stricmp(pszCmd, "강제탈퇴") == 0) || (stricmp(pszCmd, "방출") == 0) || (stricmp(pszCmd, "dismiss") == 0))
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_EXPEL_MEMBER;
			}
			else if ((stricmp(pszCmd, "list") == 0) || (stricmp(pszCmd, "목록") == 0))
			{
				if (nGameState == GUNZ_LOBBY) return ZCLANCMD_LIST;
			}
			else if ((stricmp(pszCmd, "msg") == 0) || (stricmp(pszCmd, "채팅") == 0))
			{
				return ZCLANCMD_MSG;
			}
			
			return ZCLANCMD_UNKNOWN;
		}
	} clanHelper;

	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char szSubCmd[256] = "";

	MLex lex;
	char* pszMsg = lex.GetOneArg(argv[1], szSubCmd);
	
	//// Sub Command Handler ////
	switch(clanHelper.GetSubCommand(szSubCmd)) 
	{
		case ZClanCommandHelper::ZCLANCMD_CREATE:
			{
				// clan 생성 클랜이름 발기인1 발기인2 발기인3 발기인4
				if (argc < 3)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if( !MGetChattingFilter()->IsValidStr( argv[2], 2, true) )
				{
					char szMsg[ 256 ];
					ZTransMsg( szMsg, MSG_WRONG_WORD_NAME, 1, MGetChattingFilter()->GetLastFilteredStr());
					ZGetGameInterface()->ShowMessage( szMsg, NULL, MSG_WRONG_WORD_NAME );
					break;
				}

				ZGetGameClient()->RequestCreateClan(argv[2]);
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_CLOSE:
			{
				// clan 폐쇄 클랜이름
				if (argc < 3)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if (ZGetMyInfo()->GetClanGrade() != MCG_MASTER)
				{
					ZChatOutput( 
						ZMsg(MSG_CLAN_ENABLED_TO_MASTER), 
						ZChat::CMT_SYSTEM );
					break;
				}

				// 클랜이름 확인
				if (stricmp(ZGetMyInfo()->GetClanName(), argv[2]))
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_WRONG_CLANNAME), 
						ZChat::CMT_SYSTEM );
					break;
				}

				ZApplication::GetGameInterface()->ShowConfirmMessage(
					ZMsg(MSG_CLAN_CONFIRM_CLOSE), 
					ZGetClanCloseConfirmListenter()	);
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_JOIN:
			{
				// clan 초대 가입자이름
				if (argc < 3)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if (!ZGetMyInfo()->IsClanJoined())
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_NOT_JOINED), 
						ZChat::CMT_SYSTEM );
					break;
				}

				if (!IsUpperClanGrade(ZGetMyInfo()->GetClanGrade(), MCG_ADMIN))
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_ENABLED_TO_MASTER_AND_ADMIN), 
						ZChat::CMT_SYSTEM );
					break;
				}

				char szClanName[256];
				strcpy(szClanName, 	ZGetMyInfo()->GetClanName());
				ZPostRequestJoinClan(ZGetGameClient()->GetPlayerUID(), szClanName, argv[2]);
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_LEAVE:
			{
				// clan 탈퇴
				if (argc < 2)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if (!ZGetMyInfo()->IsClanJoined())
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_NOT_JOINED), 
						ZChat::CMT_SYSTEM );
					break;
				}

				// 마스터는 탈퇴가 안된다.
				if (IsUpperClanGrade(ZGetMyInfo()->GetClanGrade(), MCG_MASTER))
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_MASTER_CANNOT_LEAVED), 
						ZChat::CMT_SYSTEM);
					break;
				}

				ZApplication::GetGameInterface()->ShowConfirmMessage(
					ZMsg(MSG_CLAN_CONFIRM_LEAVE), 
					ZGetClanLeaveConfirmListenter() );
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_CHANGE_GRADE:
			{
				// clan 권한변경 멤버이름 권한이름
				if (argc < 4)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if (!ZGetMyInfo()->IsClanJoined())
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_NOT_JOINED), 
						ZChat::CMT_SYSTEM );
					break;
				}

				if (!IsUpperClanGrade(ZGetMyInfo()->GetClanGrade(), MCG_MASTER))
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_ENABLED_TO_MASTER), 
						ZChat::CMT_SYSTEM );
					break;
				}

				char szMember[256];
				int nClanGrade = 0;

				strcpy(szMember, argv[2]);
				if ((strlen(szMember) < 0) || (strlen(szMember) > CLAN_NAME_LENGTH))
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if ((!stricmp(argv[3], "클랜운영자")) || (!stricmp(argv[3], "운영자")) || (!stricmp(argv[3], "영자")) || (!stricmp(argv[3], "admin")))
				{
					nClanGrade = (int)MCG_ADMIN;
				}
				else if ((!stricmp(argv[3], "클랜멤버")) || (!stricmp(argv[3], "멤버")) || (!stricmp(argv[3], "클랜원")) || (!stricmp(argv[3], "member")))
				{
					nClanGrade = (int)MCG_MEMBER;
				}
				else
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}


				ZPostRequestChangeClanGrade(ZGetGameClient()->GetPlayerUID(), szMember, nClanGrade);
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_EXPEL_MEMBER:
			{
				// clan 강제탈퇴 클랜멤버
				if (argc < 3)
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				if (!ZGetMyInfo()->IsClanJoined())
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_NOT_JOINED), 
						ZChat::CMT_SYSTEM );
					break;
				}

				if (!IsUpperClanGrade(ZGetMyInfo()->GetClanGrade(), MCG_ADMIN))
				{
					ZChatOutput(
						ZMsg(MSG_CLAN_ENABLED_TO_MASTER_AND_ADMIN), 
						ZChat::CMT_SYSTEM );
					break;
				}

				char szMember[256];
				int nClanGrade = 0;

				strcpy(szMember, argv[2]);
				if ((strlen(szMember) < 0) || (strlen(szMember) > CLAN_NAME_LENGTH))
				{
					OutputCmdWrongArgument(argv[0]);
					break;
				}

				ZPostRequestExpelClanMember(ZGetGameClient()->GetPlayerUID(), szMember);
			}
			break;
		case ZClanCommandHelper::ZCLANCMD_LIST:
			{

			}
			break;
		case ZClanCommandHelper::ZCLANCMD_MSG:
			{
				if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
				{
					ZChatOutput( ZMsg(MSG_CANNOT_CHAT) );
					break;
				}

				// clan msg 하고싶은말
				MLex lex;
			
				char szLine[512], szTemp1[256] = "", szTemp2[256] = "";
				strcpy(szLine, line);

				char* pszMsg = lex.GetTwoArgs(szLine, szTemp1, szTemp2);

				ZPostClanMsg(ZGetGameClient()->GetPlayerUID(), pszMsg);
			}
			break;
		default:
			ZChatOutput( ZMsg(MSG_WRONG_ARGUMENT) );
			break;
	};
}

void ChatCmd_RequestQuickJoin(const char* line, const int argc, char **const argv)
{
	ZGetGameInterface()->RequestQuickJoin();
}

void ChatCmd_Report119(const char* line, const int argc, char **const argv)
{
	ZPostLocalReport119();
}

void ChatCmd_AdminKickPlayer(const char* line, const int argc, char **const argv)
{
	if (argc < 2) {
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszPlayerName = argv[1];
	ZGetGameClient()->OutputMessage_Sub("Attempting to kick player %s...",pszPlayerName);
	ZPostAdminRequestKickPlayer(pszPlayerName);
}
void ChatCmd_ChangeTeam(const char* line, const int argc, char **const argv)
{
	PCHAR PlayerName = NULL;
	LONG TeamId = -1;
	if (argc < 3) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	PlayerName = argv[1];
	TeamId = strtol(argv[2],0,10);
	if(TeamId < 0 || TeamId > 3)
		return;
	ZGetGameClient()->OutputMessage_Sub("Changing teams..");
	ZPostChangeTeam(PlayerName,TeamId);
}

int GetDueHour(char* pszDue)
{
	int nLength = (int)strlen(pszDue);

	for(int i = 0; i < nLength - 1; i++ ) {
		if( pszDue[i] > '9' || pszDue[i] < '0') {
			return -1;
		}
	}

	int nDueType = toupper(pszDue[nLength - 1]);
	if( nDueType == toupper('d') ) {
		int nDay = atoi(pszDue);
		if( nDay < 365 * 10)	return nDay * 24;
		else					return -1;		
	} 
	else if( nDueType == toupper('h')) {
		int nHour = atoi(pszDue);
		return nHour;
	} 
	else {
		return -1;
	}
}

void ChatCmd_AdminEventBanPlayer(const char* line, const int argc, char **const argv)
{
	if (argc < 2) {
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszPlayerName = argv[1];
	ZPostAdminRequestEventBanPlayer(pszPlayerName,0);
}

void ChatCmd_AdminEventUnbanPlayer(const char* line, const int argc, char **const argv)
{
	if (argc < 2) {
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszPlayerName = argv[1];
	ZPostAdminRequestEventUnbanPlayer(pszPlayerName,0);
}

void ChatCmd_AdminPingToAll(const char* line, const int argc, char **const argv)
{
	ZPostAdminPingToAll();
}

void ChatCmd_AdminReloadClientHash(const char* line, const int argc, char **const argv)
{
	ZPostAdminReloadClientHash();
}


void ChatCmd_AdminResetAllHackingBlock( const char* line, const int argc, char **const argv )
{
	ZPostAdminResetAllHackingBlock();
}

void ChatCmd_AdminReloadGambleitem( const char* line, const int argc, char **const argv )
{
	ZPostAdminReloadGambleItem();
}


void ChatCmd_AdminDumpGambleitemLog( const char* line, const int argc, char **const argv )
{
	ZPostAdminDumpGambleItemLog();
}

void ChatCmd_AdminAssasin( const char* line, const int argc, char **const argv )
{
	ZPostAdminAssasin();
}

void ChatCmd_AdminGod( const char* line, const int argc, char **const argv )
{
	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;
	if(!bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER)
		return;

	if (argc == 2)
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	if (argc < 2)
	{
		ZGetGameClient()->m_bAdminGod = !ZGetGameClient()->m_bAdminGod;
		ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->m_bAdminGod == true ? "Godmode is now **ON**" : "Godmode is now **OFF**");

		if (!ZGetGameClient()->m_bAdminGod)
		{
			if(ZGetGameClient()->m_bForcefield)
			{
				ZGetGameClient()->m_bForcefield = false;
				ZGetGameClient()->OutputMessage_Sub("Forcefield is now **OFF**");
			}
		}
	}
	else
	{
		char* pszTargetName = argv[1];

		for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
		{
			MMatchPeerInfo * tInfo = (*itor).second;
			ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);
		
			if(!peerChar) continue;

			if(peerChar == ZGetGame()->m_pMyCharacter)
			{
				ZGetGameClient()->m_bAdminGod = !ZGetGameClient()->m_bAdminGod;
				ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->m_bAdminGod == true ? "Godmode is now **ON**" : "Godmode is now **OFF**");

				if (!ZGetGameClient()->m_bAdminGod)
				{
					if(ZGetGameClient()->m_bForcefield)
					{
						ZGetGameClient()->m_bForcefield = false;
						ZGetGameClient()->OutputMessage_Sub("Forcefield is now **OFF**");
					}
				}
				return;
			}

			if(!stricmp(peerChar->GetUserName(),pszTargetName))
			{
				bool bState = 0;

				if( !stricmp(argv[2], "1")) bState = true;
				if( !stricmp(argv[2], "0")) bState = false;
				if( !stricmp(argv[2], "on")) bState = true;
				if( !stricmp(argv[2], "off")) bState = false;
				ZPostAdminGod(peerChar->GetUID(),bState);
				ZGetGameClient()->OutputMessage_Sub("Godmode for %s is now %s",peerChar->GetUserName(),(bState)?"**ON**":"**OFF**");
				return;
			}
		}
	}
}

void ChatCmd_Reflect( const char* line, const int argc, char **const argv )
{
	if (!ZGetGameClient()->m_bAdminGod && !ZGetGameClient()->m_bForcefield)
	{
		ZGetGameClient()->m_bAdminGod = true;
		ZGetGameClient()->OutputMessage("Godmode is now **ON**");
	}
	else if(ZGetGameClient()->m_bAdminGod && ZGetGameClient()->m_bForcefield)
	{
		ZGetGameClient()->m_bAdminGod = false;
		ZGetGameClient()->OutputMessage("Godmode is now **OFF**");
	}

	ZGetGameClient()->m_bForcefield = !ZGetGameClient()->m_bForcefield;
	ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->m_bForcefield == true ? "Forcefield is now **ON**" : "Forcefield is now **OFF**");
}

void ChatCmd_DevMode( const char* line, const int argc, char **const argv )
{
	ZMyInfo *pInfo = ZGetMyInfo();
	bool bEvent = strcmp(ZGetGameClient()->GetChannelName(), "DG Event")==0;

	if(isDEV(ZGetMyInfo()->GetUGradeID()) || pInfo->IsHighAdmin() || (bEvent && ZGetMyInfo()->GetUGradeID() == MMUG_EVENTMANAGER))
	{
		ZGetGameClient()->m_DevMode =! ZGetGameClient()->m_DevMode;
		ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->m_DevMode == true ? "Developer Mode Enabled." : "Developer Mode Disabled.");
		
	}
}
void ChatCmd_GetUGrade( const char* line, const int argc, char **const argv )
{
	ZGetGameClient()->OutputMessage_Sub("%p",ZGetMyInfo()->GetUserGradeIdAddress());
}
void ChatCmd_Ghost( const char* line, const int argc, char **const argv )
{	
	if (ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)
	{
		ZGetGameClient()->m_GhostMode =! ZGetGameClient()->m_GhostMode;
		ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->m_GhostMode == true ? "Ghost Mode Enabled." : "Ghost Mode Disabled.");
	}
}

void ChatCmd_ClanInfo( const char* line, const int argc, char **const argv )
{	
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszName = argv[1];

	for(int i = 0;i<=ZGetCharacterManager()->GetCount();i++)
	{
		ZCharacter* pChar = ZGetCharacterManager()->Get(i);
		if(!pChar)
			continue;
		if(!stricmp(pszName,pChar->GetCharInfo()->szName))
		{
			ZPostRequestClanInfo(pChar->GetUID(),pChar->GetCharInfo()->szClanName);
			break;
		}
		if(i == ZGetCharacterManager()->GetCount())
			ZChatOutput("User not Found.");
	}
}

void ChatCmd_Check( const char* line, const int argc, char **const argv )
{	
	if (ZGetMyInfo()->GetUGradeID() != MMUG_CODER && ZGetMyInfo()->GetUGradeID() != MMUG_HCODER) return;

	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char* pszName = argv[1];

	//ZGetGame()->m_pMyCharacter->TestToggleCharacter();
	//RMesh* pMesh = ZGetMeshMgr()->Get(pszName);
	//if (pMesh)
	//	ZGetGame()->m_pMyCharacter->SetVisualMesh(pMesh->GetVisualMesh());

	for(int i = 0;i<=ZGetCharacterManager()->GetCount();i++)
	{
		ZCharacter* pChar = ZGetCharacterManager()->Get(i);
		if(!pChar)
			continue;
		if(!stricmp(pszName,pChar->GetCharInfo()->szName))
		{
			ZCharaterStatusBitPacking & uStatus = pChar->m_dwStatusBitPackingValue.Ref();
			int ping;
			ping = (pChar == ZGetGame()->m_pMyCharacter) ? 0 : ZGetGameClient()->FindPeer(pChar->GetUID())->GetPing(ZGetGame()->GetTickTime());
			ZGetGameClient()->OutputMessage_Sub("Name: %s, bDisconn: %i, nPing: %i, bDead: %i, nStateLower: %i",pChar->GetUserName(),uStatus.m_bLostConEffect,ping,uStatus.m_bDie,pChar->GetStateLower());
			break;
		}
		if(i == ZGetCharacterManager()->GetCount())
			ZGetGameClient()->OutputMessage_Sub("Character not Found.");
	}
}
void ChatCmd_Camera( const char* line, const int argc, char **const argv )
{
	if(!ZGetGame())
		return;

	if(!ZGetGame()->m_pMyCharacter)
		return;

	if(argc < 2)
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	float CameraDistance = atof(argv[1]);
	if(CameraDistance  >= 100.0f && CameraDistance <= 400.f)
	{
		ZGetGameInterface()->GetCamera()->m_fDist = CameraDistance;
	}

}
void ChatCmd_df( const char* line, const int argc, char **const argv )
{
	if(!ZGetGame())
		return;

	if(!ZGetGame()->m_pMyCharacter)
		return;

	if(!IsGunzHighAdminIncludeEMGM(ZGetMyInfo()->GetUGradeID()))
		return ;

	MUID Self = ZGetMyUID();
	
	ZPOSTCMD1(MC_ADMIN_REQUEST_DF,MCommandParameterUID(Self));
}
void ChatCmd_Start( const char* line, const int argc, char **const argv )
{
	if(ZGetGameClient()->GetMatchStageSetting()->GetMapName()[0]!=0)
	{
		ZApplication::GetStageInterface()->ChangeStageEnableReady( true);
		unsigned int crcspawn, crcbsp;
		char xmlname[_MAX_PATH];
		sprintf(xmlname,"%s.%s",ZGetGameClient()->GetMatchStageSetting()->GetMapName(),"rs.bsp");
		crcbsp = ZGetFileSystem()->GetCRC32(xmlname);

		char xmlname2[_MAX_PATH];
		sprintf(xmlname2,"%s.%s",ZGetGameClient()->GetMatchStageSetting()->GetMapName(),"rs.xml");
		crcspawn = ZGetFileSystem()->GetCRC32(xmlname2);

		ZPostStageStart(ZGetGameClient()->GetPlayerUID(), ZGetGameClient()->GetStageUID(), crcbsp, crcspawn);
	}
}

void ChatCmd_Clanwar( const char* line, const int argc, char **const argv )
{
	ZIDLResource* pResource = ZApplication::GetGameInterface()->GetIDLResource();

	MListBox* pListBox = NULL;

	MWidget *pDialog = pResource->FindWidget("ArrangedTeamGameDialog");

	/*if(pDialog)
		pListBox = (MListBox*)pResource->FindWidget("ArrangedTeamSelect");

	if(pListBox && pListBox->IsVisible())
	{
		pListBox->RemoveAll();
		for(CLANMEMBER ClanMember : ZGetGameClient()->GetClanMembers())
		{
			if (ClanMember.uid == ZGetMyUID()) continue;
			pListBox->Add(ClanMember.name);
		}
	}*/

	pDialog->Show(true);

	ZPlayerListBox* pWidget = (ZPlayerListBox*)ZApplication::GetGameInterface()->GetIDLResource()->FindWidget("LobbyChannelPlayerList");
	if (pWidget) pWidget->SetMode(ZPlayerListBox::PLAYERLISTMODE_CHANNEL_CLAN);
}

void ChatCmd_ChangeMaster(const char* line, const int argc, char **const argv)
{
	ZPostChangeMaster();
}

void ChatCmd_ChangePassword(const char* line, const int argc, char **const argv)
{
	char szPassword[STAGEPASSWD_LENGTH];
	strncpy(szPassword,line+15,STAGEPASSWD_LENGTH);

	if (szPassword[0] == '\0' || strlen(szPassword) == 0)
	{
		ZPostChangePassword("");
		ZGetGameClient()->OutputMessage("Password removed");
	}
	else
	{
		ZPostChangePassword(szPassword);
		ZGetGameClient()->OutputMessage_Sub("Password is now: %s",szPassword);
	}
}

void ChatCmd_ChangeTitle(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szNewName[STAGENAME_LENGTH];
	strncpy(szNewName,line+12,STAGENAME_LENGTH);

	ZPostChangeTitle(szNewName);
}

void ChatCmd_AdminHide(const char* line, const int argc, char **const argv)
{
	ZPostAdminHide();
}

void ChatCmd_AdminIncognito(const char* line, const int argc, char **const argv)
{
	char* pszNewName = argv[1];

	if (!pszNewName || strlen(pszNewName) == 0 || pszNewName[0] == '\0')
		ZPostAdminIncognito("");
	else
		ZPostAdminIncognito(pszNewName);
}

void ChatCmd_Time(const char* line, const int argc, char **const argv)
{
	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
	char szTime[128];
	bool pm = false;
	if (timeinfo->tm_hour >= 12)
		pm = true;
	if (timeinfo->tm_hour == 0)
		timeinfo->tm_hour = 12;
	if (timeinfo->tm_hour > 12)
		timeinfo->tm_hour -= 12;

	sprintf(szTime,"Time: %d:%.2d ",timeinfo->tm_hour,timeinfo->tm_min);
	strcat(szTime,(pm)?"PM":"AM");
	ZGetGameClient()->OutputMessage_Sub(szTime);
}

void ChatCmd_RequestJjang(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszTargetName = argv[1];

	ZPostAdminRequestJjang(pszTargetName);
}

void ChatCmd_RemoveJjang(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszTargetName = argv[1];

	ZPostAdminRemoveJjang(pszTargetName);
}

void ChatCmd_Test(const char* line, const int argc, char **const argv)
{
	ZChatOutput("Testing...", ZChat::CMT_SYSTEM);
	
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	char* pszCharName = argv[1];

	ZGameClient* pClient = ZGetGameClient();
	MMatchPeerInfoList* pList = pClient->GetPeers();
	for (MMatchPeerInfoList::iterator i=pList->begin(); i!= pList->end(); i++) {
		MMatchPeerInfo* pInfo = (*i).second;
		if(stricmp(pInfo->CharInfo.szName, pszCharName) == 0) {
			MCommand* pCmd = pClient->CreateCommand(MC_TEST_PEERTEST_PING, pInfo->uidChar);
			pClient->Post(pCmd);
		}
	}
}

void ChatCmd_StaffLobbyChat(const char* line, const int argc, char **const argv)
{
	if(ZGetGameClient()) 
	{
		ZGetGameClient()->SetLobbyChat(!ZGetGameClient()->GetLobbyChat());
		ZGetGameClient()->OutputMessage_Sub(ZGetGameClient()->GetLobbyChat() == true ? "Lobbychat is enabled." : "Lobbychat is disabled.");
	}
}

void ChatCmd_Macro(const char* line, const int argc, char **const argv)
{
// config 에 등록 저장 - 키입력 누를때처럼
// 
	if(argc != 3)
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	int mode = -1;

		 if( stricmp(argv[1], "1")==0 ) mode = 0;
	else if( stricmp(argv[1], "2")==0 ) mode = 1;
	else if( stricmp(argv[1], "3")==0 ) mode = 2;
	else if( stricmp(argv[1], "4")==0 ) mode = 3;
	else if( stricmp(argv[1], "5")==0 ) mode = 4;
	else if( stricmp(argv[1], "6")==0 ) mode = 5;
	else if( stricmp(argv[1], "7")==0 ) mode = 6;
	else if( stricmp(argv[1], "8")==0 ) mode = 7;
	else if( stricmp(argv[1], "9")==0 ) mode = 8;
	else 
		return;

	ZCONFIG_MACRO* pMacro = NULL;

	if(ZGetConfiguration())
		pMacro = ZGetConfiguration()->GetMacro();
	
	if( pMacro && argv[2] ) {
		strcpy( pMacro->szMacro[mode],argv[2] );
		ZGetConfiguration()->Save( Z_LOCALE_XML_HEADER);
	}
}

void ChatCmd_EmotionTaunt(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_TAUNT );
}

void ChatCmd_EmotionBow(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_BOW );
}

void ChatCmd_EmotionWave(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_WAVE );
}

void ChatCmd_EmotionLaugh(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_LAUGH );
}

void ChatCmd_EmotionCry(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_CRY );
}

void ChatCmd_EmotionDance(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_DANCE );
}


unsigned long ulWalkCheck = 0;

void ChatCmd_EmotionLoginWalk(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_LOGIN_WALK );
}

void ChatCmd_EmotionLoginIntro(const char* line,const int argc, char **const argv)
{
	if(ZGetGame())
		ZGetGame()->PostSpMotion( ZC_SPMOTION_LOGIN_INTRO );
}


void ChatCmd_RequestPlayerInfo(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	ZPostRequestCharInfoDetail(ZGetGameClient()->GetPlayerUID(), argv[1]);
}

void ChatCmd_AdminRequestLadderList(const char* line, const int argc, char **const argv)
{
	ZPostAdminRequestLadderList();
}

void ChatCmd_AdminRequestLadderJoin(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	int UID = atoi(argv[1]);

	if(!UID) return;

	ZPostAdminRequestJoinLadder(UID);
}

void ChatCmd_AdminHalt(const char* line,const int argc, char **const argv)
{
	if(ZGetMyInfo()->GetUGradeID() == MMUG_ADMIN || ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)
		ZPostAdminHalt(ZGetMyUID());
}

//Announcement
void ChatCmd_AdminAnnounce(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szMsg[256];
	//sprintf(szMsg, "^1[!]%s : ^2%s" , ZGetMyInfo()->GetCharName(), argv[1]);
	strcpy(szMsg,argv[1]);

	ZPostAdminAnnounce(ZGetGameClient()->GetPlayerUID(), szMsg, ZAAT_CHAT);
}
//Announcement
void ChatCmd_AdminBox(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szMsg[256];
	//sprintf(szMsg, "^1[!]%s : ^2%s" , ZGetMyInfo()->GetCharName(), argv[1]);
	strcpy(szMsg,argv[1]);

	ZPostAdminAnnounce(ZGetGameClient()->GetPlayerUID(), szMsg, ZAdminAnnounceType::ZAAT_MSGBOX);
}

void ChatCmd_AdminSwitchCreateLadderGame(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	bool bEnabled = true;
	if (!strcmp(argv[1], "0")) bEnabled = false;

	ZPostAdminRequestSwitchLadderGame(ZGetGameClient()->GetPlayerUID(), bEnabled);
}

void ChatCmd_Suicide(const char* line,const int argc, char **const argv)
{
	if(ZGetGame()->GetMatch()->GetMatchType() != MMATCH_GAMETYPE_DUEL) 
	{
		ZChatOutput("Killing you in approximately ten seconds...", ZChat::CMT_SYSTEM);
		ZGetGameClient()->RequestGameSuicide();
	}
}


void ChatCmd_LadderInvite(const char* line,const int argc, char **const argv)
{
	if (argc < 3) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	// 테스트로 우선 2명이 한팀
	char szNames[2][256];
	strcpy(szNames[0], argv[1]);
	strcpy(szNames[1], argv[2]);

	ZGetGameClient()->RequestProposal(MPROPOSAL_LADDER_INVITE, &argv[1], 2);
}

void ChatCmd_LadderTest(const char* line,const int argc, char **const argv)
{
	// 사용하는 부분이 없어서 디버그용으로 수정함. -by SungE 2007-04-02
//#ifdef _DEBUG
//	if (argc == 1)
//	{
//		char szPlayerName[MATCHOBJECT_NAME_LENGTH];
//		strcpy(szPlayerName, ZGetMyInfo()->GetCharName());
//		char* pName[1];
//		pName[0] = szPlayerName;
//
//		ZPostLadderRequestChallenge(pName, 1, 0);
//	} else if (argc == 2)
//	{
//		char szPlayerName[MATCHOBJECT_NAME_LENGTH], szTeamMember1[MATCHOBJECT_NAME_LENGTH];
//		strcpy(szPlayerName, ZGetMyInfo()->GetCharName());
//		strcpy(szTeamMember1, argv[1]);
//
//		char*pName[2];
//		pName[0] = szPlayerName;
//		pName[1] = szTeamMember1;
//
//		ZPostLadderRequestChallenge(pName, 2, 0);
//	}
//#endif
}

void ChatCmd_LaunchTest(const char* line,const int argc, char **const argv)
{
	// 사용하는 부분이 없어서 디버그용으로 수정함. -by SungE 2007-04-02
#ifdef _DEBUG
	MCommand* pCmd = ZGetGameClient()->CreateCommand(MC_MATCH_LADDER_LAUNCH, ZGetMyUID());
	pCmd->AddParameter(new MCmdParamUID(MUID(0,0)));
	pCmd->AddParameter(new MCmdParamStr("Mansion"));
	ZGetGameClient()->Post(pCmd);
#endif
}

void ChatCmd_Callvote(const char* line,const int argc, char **const argv)
{
	if ( (argv[1] == NULL) || (argv[2] == NULL) )
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	ZPOSTCMD2(MC_MATCH_CALLVOTE, MCmdParamStr(argv[1]), MCmdParamStr(argv[2]))
}

void ChatCmd_VoteYes(const char* line,const int argc, char **const argv)
{
	ZPOSTCMD0(MC_MATCH_VOTE_YES);
}

void ChatCmd_VoteNo(const char* line,const int argc, char **const argv)
{
	ZPOSTCMD0(MC_MATCH_VOTE_NO);
}

void ChatCmd_Kick(const char* line,const int argc, char **const argv)
{
	ZGetCombatInterface()->GetVoteInterface()->CallVote("kick");
}

void ChatCmd_PermKick(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szMsg[32];
	sprintf(szMsg, "%s", argv[1]);

	ZPOSTCMD1(MC_MATCH_PERMKICK, MCmdParamStr(szMsg));
}

void ChatCmd_RemoveKick(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szMsg[32];
	sprintf(szMsg, "%s", argv[1]);

	ZPOSTCMD1(MC_MATCH_REMOVEKICK, MCmdParamStr(szMsg));
}

void ChatCmd_StageSummon(const char* line, const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	char szMsg[32];
	sprintf(szMsg, "%s", argv[1]);

	ZPOSTCMD1(MC_MATCH_STAGE_SUMMON, MCmdParamStr(szMsg));
}

void ChatCmd_MouseSensitivity(const char* line,const int argc, char **const argv)
{
	if (argc == 1) 
	{
		ZChatOutputMouseSensitivityCurrent( ZGetConfiguration()->GetMouseSensitivityInInt());
	}
	else if (argc == 2)
	{
		// 감도 설정
		int original = ZGetConfiguration()->GetMouseSensitivityInInt();

		char* szParam = argv[1];
		int asked = atoi(szParam);
        int changed = ZGetConfiguration()->SetMouseSensitivityInInt(asked);

		ZChatOutputMouseSensitivityChanged(original, changed);
	}
	else
		OutputCmdWrongArgument(argv[0]);
}

void ChatCmd_AddFriend(const char* line,const int argc, char **const argv)
{
	if (argc < 2) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}
	ZPostFriendAdd(argv[1]);
}

void ChatCmd_AFK(const char* line,const int argc, char **const argv)
{
	//if (argc < 2) 
	//{
	//	OutputCmdWrongArgument(argv[0]);
	//	return;
	//}

	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED) return;

	ZGetGameClient()->m_bAFK = !ZGetGameClient()->m_bAFK;

	if (ZGetGameClient()->m_bAFK)
	{
		sprintf(ZGetGameClient()->m_szAFKMessage,"[AFK] %s",line+4);
		ZGetGameClient()->OutputMessage_Sub("You are now AFK: %s",line+4);
	}
	else
	{
		strcpy(ZGetGameClient()->m_szAFKMessage,"");
		ZGetGameClient()->OutputMessage_Sub("You are no longer AFK");
	}
}

unsigned long ulHPCheck = 0;

void ChatCmd_HPCheck(const char* line,const int argc, char **const argv)
{
	if (argc < 1) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	if(ulHPCheck <= timeGetTime() - 3000) 
	{
		float nHP = ZGetGame()->m_pMyCharacter->GetHP();
		float nAP = ZGetGame()->m_pMyCharacter->GetAP();

		char szMsg[108];
		sprintf(szMsg,"HP: %.0f | AP: %.0f",nHP,nAP);

		ZPostPeerChat(szMsg,0);
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting HP again...");
	}


	ulHPCheck = timeGetTime();

}

unsigned long ulTeamHPCheck = 0;

void ChatCmd_TeamHPCheck(const char* line,const int argc, char **const argv)
{
	if (argc < 1) 
	{
		OutputCmdWrongArgument(argv[0]);
		return;
	}

	if(ulTeamHPCheck <= timeGetTime() - 3000) 
	{
		float nHP = ZGetGame()->m_pMyCharacter->GetHP();
		float nAP = ZGetGame()->m_pMyCharacter->GetAP();

		char szMsg[108];
		sprintf(szMsg,"HP: %.0f | AP: %.0f",nHP,nAP);

		ZPostPeerChat(szMsg,ZGetGame()->m_pMyCharacter->GetTeamID());
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting HP again...");
	}

	ulTeamHPCheck = timeGetTime();

}


void ChatCmd_AutoQuest(const char* line,const int argc, char **const argv)
{
	/*if (ZGetMyInfo()->GetUGradeID() != MMUG_CODER) return;

	ZGetGameClient()->m_bAutoQuest = !ZGetGameClient()->m_bAutoQuest;

	if (ZGetGameClient()->m_bAutoQuest)
	{
		ZChatOutput( "AutoQuest Enabled!" );
	}
	else
	{
		ZChatOutput( "AutoQuest Disabled!" );
	}*/
}


// 퀘스트 테스트용 명령어 /////////////////////////////////////////////////////
unsigned long ulQT_GodCheck = 0;

void ChatCmd_QUESTTEST_God(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if (ulQT_GodCheck <= timeGetTime() - 3000)
	{
		bool bNowGod = ZGetQuest()->GetCheet(ZQUEST_CHEET_GOD);
		bNowGod = !bNowGod;

		ZGetQuest()->SetCheet(ZQUEST_CHEET_GOD, bNowGod);

		if (bNowGod)
		{
			ZChatOutput( "God mode enabled" );
		}
		else
		{
			ZChatOutput( "God mode disabled" );
		}
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting God again...");
	}
	ulQT_GodCheck = timeGetTime();
}

unsigned long ulQT_SpawnNPC = 0;
void ChatCmd_QUESTTEST_SpawnNPC(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;
	
	if(ulQT_SpawnNPC <= timeGetTime() - 3000) 
	{
		if (argc < 2) return;
		int nNPCID = 0;
		int nCount = 1;

		nNPCID = atoi(argv[1]);

		if(argv[2])
		{
			nCount = atoi(argv[2]);
			ZPostQuestTestNPCSpawn(nNPCID, nCount);
		}
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting SpawnNPC again...");
	}
	ulQT_SpawnNPC = timeGetTime();
}


unsigned long ulQT_LocalSpawnNPC = 0;
void ChatCmd_QUESTTEST_LocalSpawnNPC(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if(ulQT_LocalSpawnNPC <= timeGetTime() - 3000) 
	{
		int nNPCID = 0;
		int nCount = 1;

		nNPCID = atoi(argv[1]);
		if(argv[2]) nCount = atoi(argv[2]);

		MCommand* pCmd = ZNewCmd(MC_QUEST_NPC_LOCAL_SPAWN);
		pCmd->AddParameter(new MCmdParamUID(ZGetMyUID()));
	
		MUID uidLocal;
		uidLocal.High = 1;
		uidLocal.Low = (unsigned long)ZGetObjectManager()->size();

		pCmd->AddParameter(new MCmdParamUID(uidLocal));
		pCmd->AddParameter(new MCmdParamUChar((unsigned char)nNPCID));
		pCmd->AddParameter(new MCmdParamUChar((unsigned char)nCount));

		//ZGetGameClient()->OutputMessage_Sub("Spawned NPC: %d[%d|%d] Index: %d",nNPCID,uidLocal.Low,uidLocal.High,nCount);
		ZPostCommand(pCmd);
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting LocalSpawnNPC again...");
	}
	ulQT_LocalSpawnNPC = timeGetTime();
}


unsigned long ulQT_NPCClear = 0;
void ChatCmd_QUESTTEST_NPCClear(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if (ulQT_NPCClear <= timeGetTime() - 3000)
	{
		ZPostQuestTestClearNPC();
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting NPCClear again...");
	}
	ulQT_NPCClear = timeGetTime();
}

unsigned long ulQT_Reload = 0;
void ChatCmd_QUESTTEST_Reload(const char* line,const int argc, char **const argv)
{

	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if(ulQT_Reload <= timeGetTime() - 3000) 
	{
		ZGetObjectManager()->ClearNPC();
		ZGetQuest()->Reload();

		ZChatOutput( "Reloaded" );
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting Reload again...");
	}
	ulQT_Reload = timeGetTime();
}

unsigned long ulQT_SectorClear = 0;
void ChatCmd_QUESTTEST_SectorClear(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if(ulQT_SectorClear <= timeGetTime() - 3000) 
	{
		ZPostQuestTestSectorClear();
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting SectorClear again...");
	}
	ulQT_SectorClear = timeGetTime();
}

unsigned long ulQT_Finish = 0;
void ChatCmd_QUESTTEST_Finish(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	//if(ulQT_Finish <= timeGetTime() - 3000) 
	//{
		ZPostQuestTestFinish();
	//}
	//else
	//{
	//	ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting QTFinish again...");
	//}
	//ulQT_Finish = timeGetTime();
}

unsigned long ulQT_WeakNPCs = 0;
void ChatCmd_QUESTTEST_WeakNPCs(const char* line,const int argc, char **const argv)
{
	if (!ZGetMyInfo()->IsHighAdmin()) return;

	if(ulQT_WeakNPCs <= timeGetTime() - 3000)
	{
		bool bNow = ZGetQuest()->GetCheet(ZQUEST_CHEET_WEAKNPCS);
		bNow = !bNow;

		ZGetQuest()->SetCheet(ZQUEST_CHEET_WEAKNPCS, bNow);

		if (bNow)
		{
			ZChatOutput( "WeakNPC mode enabled" );

			// 지금 있는 NPC들의 HP를 1로 세팅
			for (ZObjectManager::iterator itor = ZGetObjectManager()->begin();
				itor != ZGetObjectManager()->end(); ++itor)
			{
				ZObject* pObject = (*itor).second;
				if (pObject->IsNPC())
				{
					ZActor* pActor = (ZActor*)pObject;
					ZModule_HPAP* pModule = (ZModule_HPAP*)pActor->GetModule(ZMID_HPAP);
					if (pModule)
					{
						pModule->SetHP(1);
					}
				}
			}
		}
		else
		{
			ZChatOutput( "WeakNPC mode disabled" );
		}
	}
	else
	{
		ZGetGameClient()->OutputMessage_Sub("^2Please wait before posting WeakNPCs again...");
	}
	ulQT_WeakNPCs = timeGetTime();
}

void ChatCmd_Info(const char* line, const int argc, char **const argv)
{
	MCOLOR chatClr = MCOLOR(0,255,0);

	ZChatOutput(chatClr, "-Roomtags-");
	ZChatOutput(chatClr, "[IA] - Infinite Ammo");
	ZChatOutput(chatClr, "[IB] - Infinite Block");
	ZChatOutput(chatClr, "[Tele] - Tele mode Alt+H / to save , Alt+G to teleport"); 
	ZChatOutput(chatClr, "[GH] - Hold G to become Spiderman");
	ZChatOutput(chatClr, "[G=#] and [S=#] - modifiers for move speed(S) and gravity(G)");
	ZChatOutput(chatClr, "[L] - Turn anti-lead system off");
	ZChatOutput(chatClr, "[T] - Training mode");
	ZChatOutput(chatClr, "[F] - Disable flips");
	ZChatOutput(chatClr, "[R] - Instant reload");
	ZChatOutput(chatClr, "[FPS] - First-person-shooter mode");
	ZChatOutput(chatClr, "[G] - Gladiator mode");
	ZChatOutput(chatClr, "[MS] - Med Style");
	ZChatOutput(chatClr, "[E] - Disable elements");
	ZChatOutput(chatClr, "[Float] - Float mode");
	ZChatOutput(chatClr, "[I] - 'Ijji' mode (vanilla)");
	ZChatOutput(chatClr, "[EQ] - Equality mode - IJJI HP/AP, every peer has the same delay,damage and accuracy");
	ZChatOutput(chatClr, "[S] - Shield mode \n");
	ZChatOutput(chatClr, "[ESP] - Show all player names \n");
	ZChatOutput(chatClr, "[GHOST] - All players are transparent \n");
	ZChatOutput(chatClr, "[ND] - Toggle Godmode (ALT + K) Training room only \n");
}

void ChatCmd_Report(const char* line, const int argc, char **const argv)
{
	ZGetGameInterface()->Show112Dialog(true);
}

///////////////////////////////////////////////////////////////////////////////