#include <winsock2.h>
#include <windows.h>
#include <stdio.h>

struct GEOIP_Params
{
	char *szCountry;
	char *peerip;
};

DWORD __stdcall GeoIP_GetCountry(LPVOID lpThreadParameter);