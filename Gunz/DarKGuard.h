#ifndef DARKGUARD_H
#define DARKGUARD_H

#pragma once

#include "ZPrerequisites.h"
#include "ZConfiguration.h"
#include "ZGameClient.h"
#include <windows.h>
#include <wingdi.h>
#include <mmsystem.h>
#include <shlwapi.h>
#include <shellapi.h>

#include "dxerr9.h"

#include "main.h"
#include "resource.h"
#include "VersionNo.h"

#include "Mint4R2.h"
#include "ZApplication.h"
#include "MDebug.h"
#include "ZMessages.h"
#include "MMatchNotify.h"
#include "RealSpace2.h"
#include "Mint.h"
#include "ZGameInterface.h"
#include "RFrameWork.h"
#include "ZButton.h"
#include "ZDirectInput.h"
#include "ZActionDef.h"
#include "MRegistry.h"
#include "ZInitialLoading.h"
#include "MDebug.h"
#include "MCrashDump.h"
#include "ZEffectFlashBang.h"
#include "ZMsgBox.h"
#include "ZSecurity.h"
#include "ZStencilLight.h"
#include "ZReplay.h"
#include "ZUtil.h"
#include "ZOptionInterface.h"
#include "HMAC_SHA1.h"

#include "HardDriveSerialNumer.h"

#pragma intrinsic(_ReturnAddress)

class MCommand;

typedef CONST LPBYTE LPCBYTE;

FARPROC WINAPI new_GetProcAddress(HMODULE hModule, LPCSTR lpszFunctionName);
HMODULE WINAPI new_GetModuleHandle(LPCSTR lpszModuleName);

string AES_Encrypt(string sString, BYTE* iv);
string AES_Decrypt(string sString, BYTE* iv);

RRESULT OnHackDetect(void* pParam);
DarKGuard* Init_Guard(ZApplication* pApp);

namespace DarK{
VOID InitHook();
VOID ExitHook();

BOOL CheckHooks();
VOID ExitHookThread();
}
using namespace DarK;

void ConvertCharOldDgzXor(char* pData,int _size);
void RecoveryCharOldDgzXor(char* pData,int _size);
void RecoveryRC4(char* pData,int _size);
void ConvertRC4(char* pData,int _size);
enum DARKGUARD_CALLTABLE
{
	CALLTABLE_START,
	GETPROCADDRESS,
	GETMODULEHANDLE,
	TIMEGETTIME,
	GETTICKCOUNT,
	GETLASTERROR,
	GETSYSTEMINFO,
	VIRTUALPROTECT,
	GETTHREADCONTEXT,
	SETTHREADCONTEXT,
	VIRTUALALLOC,
	VIRTUALFREE,
	ISDEBUGGERPRESENT,
	LOADLIBRARY,
	GETCURRENTTHREAD,
	GETCURRENTPROCESS,
	NTQUERYINFORMATIONPROCESS,
	NTQUERYSYSTEMINFORMATION,
	OUTPUTDEBUGSTRING,
	ZNEWCMD,
	ZPOSTCOMMAND,
	UUIDCREATESEQUENTIAL,
	CALLTABLE_MAX
};

enum VTABLE_CALLWRAPPER_BASE
{
	DESTRUCTOR,
	CALL_GETPROCADDRESS,
	CALL_GETMODULEHANDLE,
	CALL_DWORD,
	CALL_GETSYSTEMINFO,
	CALL_LOADLIBRARY,
	CALL_GETCURRENTTHREAD,
	CALL_GETCURRENTPROCESS,
	CALL_GETTHREADCONTEXT,
	CALL_SETTHREADCONTEXT,
	CALL_VIRTUALALLOC,
	CALL_VIRTUALFREE,
	CALL_VIRTUALPROTECT,
	CALL_ISDEBUGGERPRESENT,
	CALL_OUTPUTDEBUGSTRING,
	CALL_NTQUERYINFORMATIONPROCESS,
	CALL_NTQUERYSYSTEMINFORMATION,
	CALL_NEWCOMMAND,
	CALL_POSTCOMMAND,
	CALL_UUIDCREATESEQUENTIAL,
	VTABLE_CALLWRAPPER_MAX
};

class DarKGuard_CallWrapperBase
{
private:
	LPVOID								*m_pCallTable; //The array of function pointers
	MProtectValue<DWORD>				*m_pCallWrapperKey;

public:
	DarKGuard_CallWrapperBase();
	virtual ~DarKGuard_CallWrapperBase();

	DWORD GetCallWrapperKey() { return (*m_pCallWrapperKey).Ref(); }

	LPVOID *DarKGuard_CallWrapperBase::GetCallTableFunction(int index);
	virtual FARPROC Call_GetProcAddress(HMODULE hModule, LPCSTR lpszFunctionName);
	virtual HMODULE Call_GetModuleHandle(LPCSTR lpszModuleName);

	virtual DWORD Call_Return_DWORD(DARKGUARD_CALLTABLE nTableEntry);
	virtual VOID Call_GetSystemInfo(LPSYSTEM_INFO lpSystemInfo);
	virtual HMODULE Call_LoadLibrary(LPCTSTR lpFileName);
	virtual HANDLE Call_GetCurrentThread();
	virtual HANDLE Call_GetCurrentProcess();
	virtual BOOL Call_GetThreadContext(HANDLE hThread, LPCONTEXT lpContext);
	virtual BOOL Call_SetThreadContext(HANDLE hThread, CONST PCONTEXT lpContext);
	virtual LPVOID Call_VirtualAlloc(LPVOID lpAddress, DWORD dwSize, DWORD flAllocationType, DWORD flProtect);
	virtual BOOL Call_VirtualFree(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);
	virtual BOOL Call_VirtualProtect(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, LPDWORD lpflOldProtect);
	virtual BOOL Call_IsDebuggerPresent();
	virtual VOID Call_OutputDebugString(LPCTSTR lpOutputString);
	virtual NTSTATUS Call_NtQueryInformationProcess(HANDLE hProcess, UINT ProcessInfoClass, PVOID ProcessInfo, ULONG ProcessInfoLen, PULONG ReturnLen);
	virtual NTSTATUS Call_NtQuerySystemInformation(UINT SystemInfoClass, PVOID SystemInfo, ULONG SystemInfoLen, PULONG ReturnLen);

	virtual MCommand* Call_NewCommand(INT nID);
	virtual BOOL Call_PostCommand(MCommand* pCommand);

	virtual RPC_STATUS Call_UuidCreateSequential(UUID __RPC_FAR *Uuid);

	VOID SetCallTableEntry(DARKGUARD_CALLTABLE nTableEntry, LPVOID lpFuncPointer);

	LPDWORD GetVTable() { LPDWORD vTable = (LPDWORD)((LPDWORD)this)[0]; return vTable; }
};

class DarKGuard_CallWrapper : public DarKGuard_CallWrapperBase
{
public:
	DarKGuard_CallWrapper();
	virtual  ~DarKGuard_CallWrapper() { DarKGuard_CallWrapperBase::~DarKGuard_CallWrapperBase(); }
};

class DarKGuard_BaseClass
{
protected:
	MProtectValue<LPDWORD>*				m_pVTable_d3d; //The VTable pointer array will be stored in this variable
	MProtectValue<LPDWORD>*				m_pOffsetList_d3d; //Contains the pointers to the original functions
	MProtectValue<LPINT>*				m_pEntryList_d3d; //Contains the VTable entries 
	MProtectValue<LPBYTE>*				m_pByteList; //Contains the detour bytes

	MProtectValue<LPBYTE>*				m_pMemoryBuf; //Points to the game's .CODE section
	MProtectValue<DarKGuard_CallWrapper>* m_pCallWrapper;

	MProtectValue<BYTE>*				m_nINT3Byte;

	MProtectValue<string>*				m_pSzHWID; //The variable in which the system's HWID is stored
	MProtectValue<UINT>*				m_p_nHWIDCRC32;
	DWORD								m_nOldProtect;

	MProtectValue<INT>*					m_pHeartbeatCmdID;
	MProtectValue<BOOL>*				m_pbCaught;

	UINT								m_nSystemCRC32;

	INT OnCatch(INT nType); 
	BOOL CheckVTable_D3D();
	BOOL CheckDetour();
	BOOL CheckMemory();
	BOOL CheckBP();
	BOOL CheckINT3BP(LPVOID pMemAddress, SIZE_T nSize);
	BOOL CheckMemoryBP();
	INT	 CheckHardwareBP();
	BOOL CheckDebug();
	BOOL CheckIsDebuggerPresent();
	BOOL CheckProcessDebugFlags();
	BOOL CheckProcessDebugObject();
	BOOL CheckProcessDebugPort();
	BOOL CheckKernelDebugger();
	BOOL CheckDebugString();

	DWORD BuildCRC32();

	VOID MakeMemoryCRC32();
	BOOL CheckMemoryCRC32();

public:
	DarKGuard_BaseClass();
	virtual ~DarKGuard_BaseClass();

	virtual RRESULT OnDetect(LPVOID pParam) { return R_OK; }

	VOID SetHeartbeatCmdID(INT nID)		{	(*m_pHeartbeatCmdID).Set(nID);		}
	INT GetHeartbeatCmdID()				{	return (*m_pHeartbeatCmdID).Ref();		}

	VOID SetSystemCRC32(UINT nCRC32)	{	m_nSystemCRC32 = nCRC32;		}

	VOID SetCaught(BOOL bCaught)		{	(*m_pbCaught).Set(bCaught);		}
	BOOL GetCaught()					{	return (*m_pbCaught).Ref();		}
};

class DarKGuard : public DarKGuard_BaseClass //Polymorphism makes it a little harder for reversers
{
private:
	MProtectValue<DWORD>*			m_p_nTimer;
	MProtectValue<DWORD>*			m_p_nPeriod;

	DWORD CallWrapperVTables[VTABLE_CALLWRAPPER_MAX];

public:
	DarKGuard();
	virtual ~DarKGuard() { SAFE_DELETE(m_p_nTimer); SAFE_DELETE(m_p_nPeriod); DarKGuard_BaseClass::~DarKGuard_BaseClass(); }

	unsigned long codehash;

	VOID DarKGuard::InstallHooks();
	VOID* DarKGuard::Hook(void* pvDes,void* pvSrc,unsigned long ulPatchSize);
	INT DarKGuard::UnHook(void* pvRestore,void* pvSrc,unsigned long ulSize);

	VOID SendHeartbeat();
	virtual RRESULT OnDetect(LPVOID pParam);

	BOOL CheckVTable_CallWrapper();

	VOID SetVTableEntry(VTABLE_CALLWRAPPER_BASE nEntry, DWORD dwPTR) { CallWrapperVTables[nEntry] = dwPTR; }
	DWORD GetVTablePTR(VTABLE_CALLWRAPPER_BASE nEntry) { return CallWrapperVTables[nEntry]; }
	DWORD GetCurrentVTablePTR(VTABLE_CALLWRAPPER_BASE nEntry) { return (*m_pCallWrapper).Ref().GetVTable()[nEntry]; }

	VOID SetVTable(LPDWORD pTable) { (*m_pVTable_d3d).Set(pTable); }
	VOID SetOffsetList(LPDWORD pList) { CopyMemory((*m_pOffsetList_d3d).Ref(), pList, MAXFUNC * sizeof(DWORD)); }
	VOID SetEntryList(LPINT pList) { CopyMemory((*m_pEntryList_d3d).Ref(), pList, MAXFUNC * sizeof(INT)); }
	VOID SetByteList(LPBYTE pList) { CopyMemory((*m_pByteList).Ref(), pList, MAXFUNC * sizeof(BYTE)); }

	VOID SetMemory(LPBYTE pMemory) { (*m_pMemoryBuf).Set(pMemory); }

	VOID SetPeriod(DWORD dwPeriod) { (*m_p_nPeriod).Set_CheckCrc(dwPeriod); }
	VOID SetTimer(DWORD dwTime) { (*m_p_nTimer).Set_CheckCrc(dwTime); }
	
	VOID MakeMemCRC32() { MakeMemoryCRC32(); }

	virtual VOID SetCallTableEntry(DARKGUARD_CALLTABLE nTableEntry, LPVOID lpFuncPointer) { (*m_pCallWrapper).Ref().SetCallTableEntry(nTableEntry, lpFuncPointer); }
	FORCEINLINE DarKGuard_CallWrapper* GetCallWrapper() {	return &(*m_pCallWrapper).Ref();	}

	LPBYTE GetByteList() { return (*m_pByteList).Ref(); }

	VOID SetMemoryBPByte(BYTE bp) { (*m_nINT3Byte).Set(bp); }
	
	string* GetHWID() { return &m_pSzHWID->Ref(); }
	VOID GetHWID(PCHAR pBuffer);
	VOID SetHWIDCRC32(UINT nCRC32) { m_p_nHWIDCRC32->Set(nCRC32); }

	UINT GetHWIDCRC32() { return m_p_nHWIDCRC32->Ref(); }
	VOID SetHWID();

	static DarKGuard* InitGuard();
};

#endif