#include "stdafx.h"
#include "ZRuleDeathMatch.h"
#include "ZScreenEffectManager.h"
#include "MMatchUtil.h"

ZRuleSoloDeathMatch::ZRuleSoloDeathMatch(ZMatch* pMatch) : ZRule(pMatch)
{

}

ZRuleSoloDeathMatch::~ZRuleSoloDeathMatch()
{





}

ZRuleTeamDeathMatch::ZRuleTeamDeathMatch(ZMatch* pMatch) : ZRule(pMatch)
{

}

ZRuleTeamDeathMatch::~ZRuleTeamDeathMatch()
{

}

/////////////////////////////////////////////////////////////////////////////////////////
//ZRuleInfection
ZRuleInfection::ZRuleInfection(ZMatch* pMatch) : ZRule(pMatch)
{
	m_uidSource = MUID(0,0);
}

ZRuleInfection::~ZRuleInfection()
{

}

bool ZRuleInfection::OnCommand(MCommand* pCommand)
{
	if(pCommand->GetID() == MC_MATCH_GAME_SOURCE)
	{
		pCommand->GetParameter(&m_uidSource, 0, MPT_UID);
		return true;
	}
	return false;
}


/////////////////////////////////////////////////////////////////////////////////////////
//ZRuleGunGame

ZRuleGunGame::ZRuleGunGame(ZMatch* pMatch) : ZRule(pMatch)
{
	
}

ZRuleGunGame::~ZRuleGunGame()
{

}

MTD_CharInfo* ZRuleGunGame::GetGearSetLevel(int nLvl)
{
		int kills = nLvl;

	MTD_CharInfo* pInfoTemp = new MTD_CharInfo();

	switch(kills)
	{
		case 1:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500001;//Advencture Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4001;//Raptor 50
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 2:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 31;//  Iron Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4005;// Minic 567
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 3:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 51;// Giant Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4004;//Raptor 120 x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 5001;//Rendard
			break;
		case 4:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500004;//Riddic Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4008;//Zaurus A x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4004;//Raptor 120 x2
			break;
		case 5:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502007;//Golden Dragon Dual Swords
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5001;//Renard
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4502;//Dynax 7000 x2
			break;
		case 6:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502001;//Adventure Long Sword	
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5004;//Walcom S5 x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4005;//Minic 567
			break;
		case 7:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1;//Rusty Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4506;//Phantom Cruise
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 8:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502012;//Black Dragon Dual Swords
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5014;//Walcom XL x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4503;//Minic 567 Limited
			break;
		case 9:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502002;//Salamanda
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 5015;//Renard VI
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 5017;//Walcom XLS
			break;
		case 10:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 63;//Black Iron Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4516;//Dynax 800C x2
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 11:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 71;//Black Iron Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6002;//Breaker 5
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 12:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502003;//Dark Kharma
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6003;//Breaker 6
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6004;//Breaker 7
			break;
		case 13:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 500006;//Shadow Vane
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6006;//Breaker 8
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6008;//Breaker 9P
			break;
		case 14:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502014;//Justice
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 6009;//Iron Crow Shotgun Limited
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 6010;//Cruelizon Limited
			break;
		case 15:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502015;//Silver Fang
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7002;//Nico R5
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7003;//Walcom Warrior
			break;
		case 16:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1040;//Assassin Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7004;//maxwell LX30
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 17:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502016;//Harvester
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7005;//Nico R6
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7006;//Maxwell LX44
			break;
		case 18:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 502026;//Shark Fang
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8001;//Nico MG-K8
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7002;//Nico R5
			break;
		case 19:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333022;//Kingdom Key
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7003;//Walcom Warrior
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 8002;//Nico MG-K9
			break;
		case 20:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 1036;//Golden Ruby
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8004;//Nico MG-K11
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 21:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 190294;//Golden Chaos Axe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 190286;//P37
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287570;//Gun Blade (revolver)
			break;
		case 22:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 481518;//Raster's Edge
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 8000;//Donation Revolver
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333034;//District-9
			break;
		case 23:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 300040;//Infection Katana
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333033;//Dragonite
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333043;//LEGO Shooter
			break;
		case 24:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50018;//Mercury Hammer
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1019;//Cerberus
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1020;//Raging Bull
			break;
		case 25:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50000;//Valoran Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 7000;//Donation Rifle
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 26:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50001;//Fusion Kodachi
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287573;//XM8=Carbine
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 7025;//Donation Rocket
			break;
		case 27:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50002;//Sinister Blades
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287577;//Torgue Rocket Launcher
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 200132;//Jaw's Launcher
			break;
		case 28:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50003;//Ork Axes
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 2014;//Event Winner's Rocket
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 900022;//Bisfed3
			break;
		case 29:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50004;//Carbon Ripper
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 900004;//Body Counter
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1025;//TakeDown
			break;
		case 30:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50005;//Jarvin's Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 190289;//Locust
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 9456;//Donator Shotgun Black
			break;
		case 31:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50006;//Targon's Spear
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 9459;//Donator Shotgun Pink
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 9461;//Donator Shotgun Red
			break;
		case 32:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50007;//Viscero Spear
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71693;//Gauss Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 33:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50008;//Darkin Blade
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71692;//PPZ30 Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			break;
		case 34:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50009;//Noxian Axe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 40413;//River Death
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 40417;//Deathly Blow
			break;
		case 35:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50010;//Saltwater Cutlass
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1008;//Impulse
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 4567;//Dual Silver Shotguns
			break;
		case 36:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50011;//Glamdring
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 4568;//Dual Golden Shotguns
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1016;//Reflex
			break;
		case 37:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50012;//Blade of Justice
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1027;//Blunderbuss
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 71699;//The Marshall
			break;
		case 38:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50013;//Boneshiver
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 287572;//MX-26
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287578;//Carnage
			break;
		case 39:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50014;//Urukhai Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1022;//Fracture
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333052;//PULSE RX-A1
			break;
		case 40:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50015;//Wuju Blade
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 37862;//Hunting Rifle
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 40412;//Punk Shotgun
			break;
		case 41:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50016;//Nimbus
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333050;//ATLAS 77-A
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 2047;//Event Winner's Rifle
			break;
		case 42:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 50017;//Shadow Scythe
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 378650;//XEM-8 PT
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 378652;//Mutilator
			break;
		case 43:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333100;//Crystal Sword
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71694;//Krylov
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 71695;//CR320 Battle Rifle
			break;
		case 44:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333106;//Bakuya
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 333029;//Thumper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 333030;//RPG-7X-22A
			break;
		case 45:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333108;//Hidden Scorpion
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1009;//Omni
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 200132;//Jaw's Launcher
			break;
		case 46:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 200118;//Devil's Glare
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 71698;//Oriental Shotgun
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 287572;//MX-26
			break;
		case 47:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 333000;//Soul Edge
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1;
			break;
		case 48:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 900010;//Tensa Zangetsu Rebirth
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 13371;//Staff Sniper
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 1;
			break;
		case 49:
			pInfoTemp->nEquipedItemCount[MMCIP_MELEE] = 1;
			pInfoTemp->nEquipedItemCount[MMCIP_PRIMARY] = 0;
			pInfoTemp->nEquipedItemCount[MMCIP_SECONDARY] = 0;
			pInfoTemp->nEquipedItemCount[MMCIP_CUSTOM1] = 1;
			
			pInfoTemp->nEquipedItemDesc[MMCIP_MELEE] = 900017;//Admin Dagger
			pInfoTemp->nEquipedItemDesc[MMCIP_PRIMARY] = 0;
			pInfoTemp->nEquipedItemDesc[MMCIP_SECONDARY] = 0;
			pInfoTemp->nEquipedItemDesc[MMCIP_CUSTOM1] = 2034;//The Ultimate Nades
			break;
		case 50:
		default:
			break;
	}

	return pInfoTemp;
	//delete pInfoTemp;
}

bool ZRuleGunGame::OnCommand(MCommand* pCommand)
{
	if(pCommand->GetID() == MC_MATCH_NEWGEARSET)
	{
		MUID uidNew;
		MTD_CharInfo* pInfo = new MTD_CharInfo();
		
		pCommand->GetParameter(&uidNew, 0, MPT_UID);

		MCommandParameter* pInfoParam = pCommand->GetParameter(1);

		if (pInfoParam->GetType() != MPT_BLOB)
			return false;
		void *pInfoBlob = pInfoParam->GetPointer();

		if(!pInfoBlob)
			pInfo = (MTD_CharInfo*)pInfoBlob;

		ZCharacter* pNew = ZGetCharacterManager()->Find(uidNew);
		pNew->Create(pInfo);

		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////


ZRuleTeamTraining::ZRuleTeamTraining(ZMatch* pMatch) : ZRule(pMatch)
{

}

ZRuleTeamTraining::~ZRuleTeamTraining()
{

}

/////////////////////////////////////////////////////////////////////////////////////////
ZRuleTeamDeathMatch2::ZRuleTeamDeathMatch2(ZMatch* pMatch) : ZRule(pMatch)
{

}

ZRuleTeamDeathMatch2::~ZRuleTeamDeathMatch2()
{

}

bool ZRuleTeamDeathMatch2::OnCommand(MCommand* pCommand)
{
	if (!ZGetGame()) return false;

	switch (pCommand->GetID())
	{

	case MC_MATCH_GAME_DEAD:
		{
			MUID uidAttacker, uidVictim;

			pCommand->GetParameter(&uidAttacker, 0, MPT_UID);
			pCommand->GetParameter(&uidVictim, 2, MPT_UID);

			ZCharacter* pAttacker = ZGetGame()->m_CharacterManager.Find(uidAttacker);
			ZCharacter* pVictim = ZGetGame()->m_CharacterManager.Find(uidVictim);
			
			m_pMatch->AddTeamKills(pVictim->GetTeamID() == MMT_BLUE ? MMT_RED : MMT_BLUE);
		}
		break;
	}

	return false;
}

ZRuleTeamCTF::ZRuleTeamCTF(ZMatch* pMatch) : ZRule(pMatch)
{
	ZeroMemory(m_BlueFlagPos, sizeof(m_BlueFlagPos));
	ZeroMemory(m_RedFlagPos, sizeof(m_RedFlagPos));
}

ZRuleTeamCTF::~ZRuleTeamCTF()
{

}

bool ZRuleTeamCTF::OnCommand(MCommand* pCommand)
{
	if (!ZGetGame()) return false;

	switch (pCommand->GetID())
	{
		case MC_MATCH_FLAG_STATE:
		{
			int nItemID;
			MShortVector s_pos;
			int IsGone;
			MUID Carrier;
			pCommand->GetParameter(&nItemID, 0, MPT_INT);
			pCommand->GetParameter(&s_pos, 1, MPT_SVECTOR);
			pCommand->GetParameter(&IsGone, 2, MPT_INT);
			pCommand->GetParameter(&Carrier, 3, MPT_UID);
			switch(nItemID)
			{
			case CTF_RED_ITEM_ID:
				{
				SetRedCarrier(Carrier);
				SetRedFlagState(IsGone);
				SetRedFlagPos(rvector(s_pos.x, s_pos.y, s_pos.z));
				}
				break;
			case CTF_BLUE_ITEM_ID:
				{
				SetBlueCarrier(Carrier);
				SetBlueFlagState(IsGone);
				SetBlueFlagPos(rvector(s_pos.x, s_pos.y, s_pos.z));
				}
				break;
			}
		}
		break;

	case MC_MATCH_FLAG_CAP:
		{
			MMatchTeam nTeam;
			pCommand->GetParameter(&nTeam, 0, MPT_INT);
			m_pMatch->AddTeamKills(nTeam == MMT_BLUE ? MMT_BLUE : MMT_RED);

			if(nTeam == MMT_RED)
			{
				ZGetGameInterface()->PlayVoiceSound( VOICE_RED_TEAM_SCORE, 1600);
				ZGetScreenEffectManager()->AddScreenEffect("ctf_score_r");
				SetBlueFlagState(false);
				SetRedCarrier(MUID(0,0));
			}
			else if(nTeam == MMT_BLUE)
			{
				ZGetGameInterface()->PlayVoiceSound( VOICE_BLUE_TEAM_SCORE, 1600);
				ZGetScreenEffectManager()->AddScreenEffect("ctf_score_b");
				SetBlueCarrier(MUID(0,0));
				SetRedFlagState(true);
			}

			for (ZCharacterManager::iterator itor = ZGetGame()->m_CharacterManager.begin();
			itor != ZGetGame()->m_CharacterManager.end(); ++itor)
			{
				ZCharacter* pCharacter = (*itor).second;
				if(pCharacter)
				{
					if(pCharacter->GetTeamID() == nTeam)
					{
						pCharacter->SetTagger(false);
					}
				}
			}
		}
		break;
	case MC_MATCH_FLAG_EFFECT:
		{
			MUID uidOwner;
			int nTeam;
			pCommand->GetParameter(&uidOwner,		0, MPT_UID);
			pCommand->GetParameter(&nTeam,		1, MPT_INT);

			ZCharacter* pCapper = ZGetGame()->m_CharacterManager.Find(uidOwner);
			if(pCapper)
			{
				if(!pCapper->IsDie())
				{
					AssignFlagEffect(uidOwner, nTeam);
				}
				else
				{
					for (ZCharacterManager::iterator itor = ZGetGame()->m_CharacterManager.begin();
					itor != ZGetGame()->m_CharacterManager.end(); ++itor)
					{
						ZCharacter* pCharacter = (*itor).second;
						if(pCharacter)
						{
							if(pCharacter->GetTeamID() == nTeam)
							{
								pCharacter->SetTagger(false);
							}
						}
					}
					if(nTeam == MMT_RED)
					{
						SetRedCarrier(MUID(0,0));
						SetBlueFlagState(false);
						ZGetScreenEffectManager()->AddScreenEffect("ctf_flagdrop_b");
						ZGetGameInterface()->PlayVoiceSound( VOICE_BLUE_FLAG_RETURN, 1600);
					}
					else if(nTeam == MMT_BLUE)
					{
						SetBlueCarrier(MUID(0,0));
						SetRedFlagState(false);
						ZGetScreenEffectManager()->AddScreenEffect("ctf_flagdrop_r");
						ZGetGameInterface()->PlayVoiceSound( VOICE_RED_FLAG_RETURN, 1600);
					}
				}
			}
			else
			{
					for (ZCharacterManager::iterator itor = ZGetGame()->m_CharacterManager.begin();
					itor != ZGetGame()->m_CharacterManager.end(); ++itor)
					{
						ZCharacter* pCharacter = (*itor).second;
						if(pCharacter)
						{
							if(pCharacter->GetTeamID() == nTeam)
							{
								pCharacter->SetTagger(false);
							}
						}
					}
					if(nTeam == MMT_RED)
					{
					SetRedCarrier(MUID(0,0));
					SetBlueFlagState(false);
					ZGetScreenEffectManager()->AddScreenEffect("ctf_flagdrop_b");
					ZGetGameInterface()->PlayVoiceSound( VOICE_BLUE_FLAG_RETURN, 1600);
					}
					else if(nTeam == MMT_BLUE)
					{
					SetBlueCarrier(MUID(0,0));
					SetRedFlagState(false);
					ZGetScreenEffectManager()->AddScreenEffect("ctf_flagdrop_r");
					ZGetGameInterface()->PlayVoiceSound( VOICE_RED_FLAG_RETURN, 1600);
					}
			}
		}
		break;
	}

	return false;
}

void ZRuleTeamCTF::AssignFlagEffect(MUID& uidOwner, int nTeam)
{
	if (!ZGetGame()) return;

	for (ZCharacterManager::iterator itor = ZGetGame()->m_CharacterManager.begin();
	itor != ZGetGame()->m_CharacterManager.end(); ++itor)
	{
		ZCharacter* pCharacter = (*itor).second;
		if(pCharacter)
		{
			if(pCharacter->GetTeamID() == nTeam)
			{
				pCharacter->SetTagger(false);
			}
		}
	}

	ZCharacter* pFlagChar = ZGetGame()->m_CharacterManager.Find(uidOwner);
	if (pFlagChar)
	{		
			if(nTeam == MMT_BLUE)
			{
			ZGetEffectManager()->AddBerserkerIcon(pFlagChar);
			ZGetEffectManager()->AddRedFlagIcon(pFlagChar);
			pFlagChar->SetTagger(true);
			}        
			else if(nTeam == MMT_RED)
			{
            ZGetEffectManager()->AddBerserkerIcon(pFlagChar);
            ZGetEffectManager()->AddBlueFlagIcon(pFlagChar);
            pFlagChar->SetTagger(true);
			}  
			if(nTeam == MMT_BLUE)
			{
			SetBlueCarrier(uidOwner);
			SetRedFlagState(true);
			ZGetScreenEffectManager()->AddScreenEffect("ctf_taken_r");
			ZGetGameInterface()->PlayVoiceSound( VOICE_BLUE_HAS_FLAG, 1600);
			}
			else if (nTeam == MMT_RED)
			{
			SetRedCarrier(uidOwner);
			SetBlueFlagState(true);
			ZGetScreenEffectManager()->AddScreenEffect("ctf_taken_b");
			ZGetGameInterface()->PlayVoiceSound( VOICE_RED_HAS_FLAG, 1600);
			}
		}
}