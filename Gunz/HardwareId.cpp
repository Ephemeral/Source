#include "stdafx.h"
#include "HardwareId.h"

BOOLEAN GetPhysicalDriveProperties(_Out_ PSTORAGE_DEVICE_DESCRIPTOR* DriveProperties)
{
	BOOLEAN Status = FALSE;
	
	STORAGE_PROPERTY_QUERY QueryInformation;

	STORAGE_DESCRIPTOR_HEADER DeviceProperty = {0};

	ULONG SizeNeededForProperties = 0;

	HANDLE Device = INVALID_HANDLE_VALUE;
	
	if(!DriveProperties)
		return FALSE;
	
	Device = CreateFileW(L"\\\\.\\PhysicalDrive0",0,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
	
	if(Device == INVALID_HANDLE_VALUE)
		return FALSE;

	RtlSecureZeroMemory(&QueryInformation,sizeof(STORAGE_PROPERTY_QUERY));

	QueryInformation.PropertyId = StorageDeviceProperty;
	QueryInformation.QueryType = PropertyStandardQuery;

	Status = (BOOLEAN)DeviceIoControl(Device,IOCTL_STORAGE_QUERY_PROPERTY,&QueryInformation,sizeof(STORAGE_PROPERTY_QUERY),&DeviceProperty,sizeof(STORAGE_DESCRIPTOR_HEADER),&SizeNeededForProperties,NULL);

	if(!Status)
		goto CleanUp;

	*DriveProperties = (PSTORAGE_DEVICE_DESCRIPTOR)malloc(DeviceProperty.Size);
	
	if(!*DriveProperties)
		goto CleanUp;

	RtlSecureZeroMemory(*DriveProperties,DeviceProperty.Size);

	Status = (BOOLEAN)DeviceIoControl(Device,IOCTL_STORAGE_QUERY_PROPERTY,&QueryInformation,sizeof(STORAGE_PROPERTY_QUERY),(LPVOID)*DriveProperties,DeviceProperty.Size,&SizeNeededForProperties,NULL);
	
	if(!Status)
		goto CleanUp;
	
	if( ((PSTORAGE_DEVICE_DESCRIPTOR)*DriveProperties)->SerialNumberOffset )
		Status = TRUE;


CleanUp:
	CloseHandle(Device);

	if(!Status)
		if(*DriveProperties)
			DestroyDriveProperties(*DriveProperties);

	return Status;
}
VOID DestroyDriveProperties(_In_ PSTORAGE_DEVICE_DESCRIPTOR DriveProperties)
{
	if(!DriveProperties)
		return ;
	RtlSecureZeroMemory(DriveProperties,DriveProperties->Size);
	free(DriveProperties);

}