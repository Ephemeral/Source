//This is a work-in-progress !


//thats probably no longer needed ;D
#include "stdafx.h"

#include "ZGameObject.h"
#include <windows.h>

#include "MZFileSystem.h"
#include "RealSpace2.h"
#include "FileInfo.h"
#include "MDebug.h"
#include "MBlobArray.h"
#include "MObject.h"
#include "ZConsole.h"
#include "MCommandLogFrame.h"
#include "ZInterface.h"
#include "ZGameInterface.h"
#include "ZApplication.h"
#include "ZCommandTable.h"
#include "ZPost.h"
#include "ZPostLocal.h"
#include "MStrEx.h"
#include "MMatchItem.h"
#include "ZEffectManager.h"
#include "ZEffectBillboard.h"
#include "Config.h"
#include "MProfiler.h"
#include "MMatchItem.h"
#include "ZScreenEffectManager.h"
#include "RParticleSystem.h"
#include "RDynamicLight.h"
#include "ZConfiguration.h"
#include "ZLoading.h"
#include "Physics.h"
#include "zeffectflashbang.h"
#include "ZInitialLoading.h"
#include "RealSoundEffect.h"
#include "RLenzFlare.h"
#include "ZWorldItem.h"
#include "ZMyInfo.h"
#include "ZNetCharacter.h"
#include "ZSecurity.h"
#include "ZStencilLight.h"
#include "ZMap.h"
#include "ZEffectStaticMesh.h"
#include "ZEffectAniMesh.h"
#include "ZGameAction.h"
#include "ZSkyBox.h"
#include "ZFile.h"
#include "ZObject.h"
#include "ZCharacter.h"
#include "ZMapDesc.h"

#include "MXml.h"
#include "ZGameClient.h"
#include "MUtil.h"
#include "RMeshMgr.h"
#include "RVisualMeshMgr.h"
#include "RMaterialList.h"
#include "RAnimationMgr.h"
#include "ZCamera.h"
#include "Mint4R2.h"
//#include "RParticleSystem.h"
#include "ZItemDesc.h"

//#include "MObjectCharacter.h"
#include "MMath.h"
#include "ZQuest.h"
#include "MMatchUtil.h"
#include "ZReplay.h"
#include "ZRuleBerserker.h"
#include "ZRuleDuelTournament.h"
#include "ZApplication.h"
#include "ZGameConst.h"

#include "ZRuleDuel.h"
#include "ZMyCharacter.h"
#include "MMatchCRC32XORCache.h"
#include "MMatchObjCache.h"

#include "ZModule_HealOverTime.h"

_USING_NAMESPACE_REALSPACE2

ZGameObject::ZGameObject()
{
	m_nDesc = *(new ZGameObjectDesc);
	m_pVMesh = NULL;
}

ZGameObject::~ZGameObject()
{
	delete &m_nDesc;

	if(m_pVMesh != NULL) m_pVMesh = NULL;
	delete m_pVMesh;
}

void ZGameObject::LoadMesh()
{
	RMesh* pMesh;

	if(m_nDesc.m_nFlag.m_nTeam == m_nDesc.m_nFlag.TEAM_RED)
		strcpy(m_nDesc.szMesh, "FLAG_RED");
	else if(m_nDesc.m_nFlag.m_nTeam == m_nDesc.m_nFlag.TEAM_BLUE)
		strcpy(m_nDesc.szMesh, "FLAG_BLUE");

	pMesh = ZGetMeshMgr()->Get(m_nDesc.szMesh);

	if(!pMesh)
		mlog("AddGameObject : ERROR #1\n");

	int nVMID = ZGetGame()->m_VisualMeshMgr.Add(pMesh);

	if(nVMID==-1) 
		mlog("AddGameObject : ERROR #2\n");

	m_nVMID.Set_CheckCrc(nVMID);

	RVisualMesh* pVMesh = ZGetGame()->m_VisualMeshMgr.GetFast(nVMID);
	m_pVMesh = pVMesh; 
}

void ZGameObject::Draw()
{
	rboundingbox bb;

	bb.vmax=GetPosition()+rvector(50,50,190);
	bb.vmin=GetPosition()-rvector(50,50,0);

	if(!ZGetGame()->GetWorld()->GetBsp()->IsVisible(bb)) return ;
	if(!isInViewFrustum(&bb, RGetViewFrustum())) return ;

	m_pVMesh->Render(true);
}

void ZGameObject::Update()
{
	if(m_pVMesh) 
	{
		m_pVMesh->SetVisibility(1.f);//투명 버그 때문...
		m_pVMesh->Frame();

		D3DXMATRIX world;
		MakeWorldMatrix(&world,rvector(0,0,0),rvector(0,0,0),rvector(0,0,1));

		MakeWorldMatrix(&world,rvector(m_nDesc.m_fPosX, m_nDesc.m_fPosY, m_nDesc.m_fPosZ),rvector(0,0,0),rvector(0,0,1));
		m_pVMesh->SetWorldMatrix(world);
	}
}