#include "CheckReturnCallStack.h"
#include "stdafx.h"
PE_STRUCT __declspec(noinline) getStructure()
{
	PE_STRUCT peStruct = {0};
	IMAGE_DOS_HEADER IDH = {0};
	IMAGE_NT_HEADERS INH = {0};
	peStruct.base = (unsigned long)GetModuleHandleA(0);
	memcpy(&IDH,(void*)peStruct.base,sizeof(IDH));
	memcpy(&INH,(void*)(peStruct.base+IDH.e_lfanew),sizeof(IMAGE_NT_HEADERS));
	peStruct.size = INH.OptionalHeader.SizeOfCode;
	peStruct.base +=0x1000;
	peStruct.top = peStruct.base + peStruct.size;
	return peStruct;
}