#include "stdafx.h"

#include "ZGameInput.h"
#include "ZApplication.h"
#include "ZGameInterface.h"
#include "ZGame.h"
#include "ZConfiguration.h"
#include "ZActionDef.h"
#include "Mint.h"
#include "MEvent.h"
#include "MWidget.h"
#include "ZGameClient.h"
#include "ZCombatInterface.h"
#include "ZConsole.h"
//#include "MActionKey.h"
#include "ZPost.h"
#include "ZScreenEffectManager.h"
#include "ZMyInfo.h"
#include "ZMinimap.h"
#include "ZInput.h"
#include "ZBandiCapturer.h" // 동영상 캡쳐
#include "ZRuleDuel.h"

#undef _DONOTUSE_DINPUT_MOUSE

ZGameInput* ZGameInput::m_pInstance = NULL;

ZGameInput::ZGameInput()
{
	m_pInstance = this;
	m_bCTOff = false;

	// 이것들은 실행되는 내내 m_SequenceActions안에 참조되므로 static 으로 선언되어 있다.
	static ZKEYSEQUENCEITEM action_ftumble[]= { {true,ZACTION_FORWARD}, {false,ZACTION_FORWARD} , {true,ZACTION_FORWARD} };	// 앞 앞
	static ZKEYSEQUENCEITEM action_btumble[]= { {true,ZACTION_BACK}, {false,ZACTION_BACK} , {true,ZACTION_BACK} };	// 뒤 뒤
	static ZKEYSEQUENCEITEM action_rtumble[]= { {true,ZACTION_RIGHT}, {false,ZACTION_RIGHT} , {true,ZACTION_RIGHT} };
	static ZKEYSEQUENCEITEM action_ltumble[]= { {true,ZACTION_LEFT}, {false,ZACTION_LEFT} , {true,ZACTION_LEFT} };	

#define ADDKEYSEQUENCE(time,x) m_SequenceActions.push_back(ZKEYSEQUENCEACTION(time,sizeof(x)/sizeof(ZKEYSEQUENCEITEM),x));

	const float DASH_SEQUENCE_TIME = 0.2f;
	ADDKEYSEQUENCE(DASH_SEQUENCE_TIME,action_ftumble);
	ADDKEYSEQUENCE(DASH_SEQUENCE_TIME,action_btumble);
	ADDKEYSEQUENCE(DASH_SEQUENCE_TIME,action_rtumble);
	ADDKEYSEQUENCE(DASH_SEQUENCE_TIME,action_ltumble);
}

ZGameInput::~ZGameInput()
{
	m_pInstance = NULL;
}


unsigned long ulconsoletime;
bool bConsole = false;


bool ZGameInput::OnEvent(MEvent* pEvent)
{
	int sel = 0;
	char szMsg[137];

	if ((ZGetGameInterface()->GetState() != GUNZ_GAME)) return false;
	if ( ZGetGameInterface()->GetGame() == NULL ) return false;

	MWidget* pMenuWidget = ZGetGameInterface()->GetIDLResource()->FindWidget("CombatMenuFrame");
	if ((pMenuWidget) && (pMenuWidget->IsVisible())) return false;
	MWidget* pChatWidget = ZGetGameInterface()->GetIDLResource()->FindWidget("CombatChatInput");
	if ((pChatWidget) && (pChatWidget->IsVisible())) return false;
	MWidget* p112ConfirmWidget = ZGetGameInterface()->GetIDLResource()->FindWidget("112Confirm");
	if (p112ConfirmWidget->IsVisible()) return false;

//#ifndef _PUBLISH

//#endif

	ZMyCharacter* pMyCharacter = ZGetGameInterface()->GetGame()->m_pMyCharacter;
	if ((!pMyCharacter) || (!pMyCharacter->GetInitialized())) return false;

	if (ZGetGameClient()->CanVote() || ZGetGameInterface()->GetCombatInterface()->GetVoteInterface()->GetShowTargetList())
	{
		if (pEvent->nKey == VK_ESCAPE && ZGetGameInterface()->GetCombatInterface()->GetVoteInterface()->GetShowTargetList())
		{
			ZGetGameInterface()->GetCombatInterface()->GetVoteInterface()->CancelVote();
			return true;
		}
		else
		{
			ZGetGameInterface()->GetCombatInterface()->GetVoteInterface()->VoteInput(pEvent->nKey);
			//return true;
		}
		//return true;
	}

	////////////////////////////////////////////////////////////////////////////
	switch(pEvent->nMessage){
	case MWM_HOTKEY:
		{
			int nKey = pEvent->nKey;
			ZHOTKEY *hk=ZGetConfiguration()->GetHotkey(nKey);
			//if(ProcessLowLevelCommand(hk->command.c_str())==false)
			
			char buffer[256];
			strcpy(buffer,hk->command.c_str());
			ZApplication::GetGameInterface()->GetChat()->Input(buffer);

//			ConsoleInputEvent(hk->command.c_str());
		}break;

	case MWM_LBUTTONDOWN:
		{
			ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();

			if ( ZGetCombatInterface()->IsShowResult())
			{
				if ( ((ZGetCombatInterface()->m_nReservedOutTime - timeGetTime()) / 1000) < 13)
				{
					if(ZGetGameClient()->IsLadderGame() || ZGetGameClient()->IsDuelTournamentGame())
						ZChangeGameState(GUNZ_LOBBY);
					else
						ZChangeGameState(GUNZ_STAGE);

					return true;
				}
			}

			if (pCombatInterface->IsChat())
			{
				pCombatInterface->EnableInputChat(false);
			}

			if (pCombatInterface->GetObserver()->IsVisible())
			{
				bool bKillCam = pCombatInterface->GetObserver()->m_bKillCam;
				if (bKillCam)
					return true;
				pCombatInterface->GetObserver()->ChangeToNextTarget();
				return true;
			}

/*			if ((pMyCharacter) && (pMyCharacter->IsDie()))	//// 실서비스에서 스폰안되는 버그유발. 원인불명(_PUBLISH누락) 영구봉쇄.
			{
				// 혼자테스트할때 되살아나기
				if(g_pGame->m_CharacterManager.size()==1)
				{
#ifndef _PUBLISH
					ZGetGameInterface()->RespawnMyCharacter();
					return true;
#endif
				}
			}*/
			if (ZGetGameInterface()->IsCursorEnable())
				return false;
		}
		return true;
	case MWM_RBUTTONDOWN:
		{
			if (ZGetGameInterface()->GetCombatInterface()->IsChat())
			{
				ZGetGameInterface()->GetCombatInterface()->EnableInputChat(false);
			}

			ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
			if (pCombatInterface->GetObserver()->IsVisible())
			{
				pCombatInterface->GetObserver()->NextLookMode();
			}
		}
		return true;
	case MWM_MBUTTONDOWN:
		if (ZGetGameInterface()->GetCombatInterface()->IsChat())
		{
			ZGetGameInterface()->GetCombatInterface()->EnableInputChat(false);
		}
		return true;
	case MWM_ACTIONRELEASED:
		{
			switch(pEvent->nKey){
			case ZACTION_FORWARD:
			case ZACTION_BACK:
			case ZACTION_LEFT:
			case ZACTION_RIGHT:
				if (m_pInstance) 
					m_pInstance->m_ActionKeyHistory.push_back(ZACTIONKEYITEM(ZGetGame()->GetTime(),false,pEvent->nKey));
				return true;

			case ZACTION_DEFENCE:
				{
					if(ZGetGame()->m_pMyCharacter)
					{
						ZGetGame()->m_pMyCharacter->m_statusFlags.Ref().m_bGuardKey = false;
					}
				}
				return true;
			}
		}break;
	case MWM_ACTIONPRESSED:
		if ( !ZGetGame()->IsReservedSuicide())		// 자살 예정인 경우 대쉬를 할수없게 막는다
		{
			INT nDash = 4;
		switch(pEvent->nKey){
			case ZACTION_FORWARD: 
				nDash=0; 
			case ZACTION_BACK:
				if(nDash == 4) nDash=1;
			case ZACTION_LEFT: 
				if(nDash == 4) nDash=3;
			case ZACTION_RIGHT:
				if(nDash == 4) nDash=2;

				//if(ZGetGame()->m_bAimbot)
				//{
				//	if(m_pInstance)
				//		ZGetGame()->m_pMyCharacter->OnTumble(nDash);

				//	return true;
				//}
			case ZACTION_JUMP:
				if (m_pInstance) 
					m_pInstance->m_ActionKeyHistory.push_back(ZACTIONKEYITEM(ZGetGame()->GetTime(),true,pEvent->nKey));
				return true;
			case ZACTION_MELEE_WEAPON:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->ChangeWeapon(ZCWT_MELEE);
				}
				return true;
			case ZACTION_PRIMARY_WEAPON:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->ChangeWeapon(ZCWT_PRIMARY);
				}
				return true;
			case ZACTION_SECONDARY_WEAPON:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->ChangeWeapon(ZCWT_SECONDARY);
				}
				return true;
			case ZACTION_ITEM1:
			case ZACTION_ITEM2:
				{
					int nIndex = pEvent->nKey - ZACTION_ITEM1 + ZCWT_CUSTOM1;
					if ( !ZGetGame()->IsReplay()) {
						ZGetGameInterface()->ChangeWeapon(ZChangeWeaponType(nIndex));
					}
				}
				return true;
			case ZACTION_COMMUNITYITEM1:	mlog("Community Item1 Selected!\n"); return true;
			case ZACTION_COMMUNITYITEM2:	mlog("Community Item2 Selected!\n"); return true;
			case ZACTION_PREV_WEAPON:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->ChangeWeapon(ZCWT_PREV);
				}
				return true;
			case ZACTION_NEXT_WEAPON:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->ChangeWeapon(ZCWT_NEXT);
				}
				return true;
			case ZACTION_RELOAD:
				{
					if ( !ZGetGame()->IsReplay())
						ZGetGameInterface()->Reload();
				}
				return true;
			case ZACTION_DEFENCE:
				{
					if ( ZGetGame()->m_pMyCharacter && !ZGetGame()->IsReplay())
					{
						ZGetGame()->m_pMyCharacter->m_statusFlags.Ref().m_bGuardKey = true;
				}
				}
				return true;

			case ZACTION_TAUNT:		// 틸다키
			case ZACTION_BOW:
			case ZACTION_WAVE:
			case ZACTION_LAUGH:
			case ZACTION_CRY:
			case ZACTION_DANCE:
				{
					if ( ZGetGame()->IsReplay())
						break;
					if ( MEvent::GetShiftState())
						break;
					if(ZGetGameInterface()->GetCombatInterface()->GetObserverMode())
						break;

					ZC_SPMOTION_TYPE mtype;

						 if(pEvent->nKey == ZACTION_TAUNT) mtype = ZC_SPMOTION_TAUNT;
					else if(pEvent->nKey == ZACTION_BOW  ) mtype = ZC_SPMOTION_BOW;
					else if(pEvent->nKey == ZACTION_WAVE ) mtype = ZC_SPMOTION_WAVE;
					else if(pEvent->nKey == ZACTION_LAUGH) mtype = ZC_SPMOTION_LAUGH;
					else if(pEvent->nKey == ZACTION_CRY  ) mtype = ZC_SPMOTION_CRY;
					else if(pEvent->nKey == ZACTION_DANCE) mtype = ZC_SPMOTION_DANCE;
					else 
						return true;

					if(ZGetGame())
						ZGetGame()->PostSpMotion( mtype );	// ZPostSpMotion(mtype);
					
				}
				return true;

			case ZACTION_RECORD:
				{
					if ( ZGetGame() && !ZGetGame()->IsReplay())
						ZGetGame()->ToggleRecording();
				}
				return true;
			case ZACTION_MOVING_PICTURE:
				{	// 동영상 캡쳐...2008.10.02
					if (ZGetGameInterface()->GetBandiCapturer() != NULL)
						ZGetGameInterface()->GetBandiCapturer()->ToggleStart();
				}
				return true;
			case ZACTION_TOGGLE_CHAT:
				{
					if(ZGetCombatInterface()->IsShowUI())
					{ // UI토글이 켜져 있을때만 채팅토글을 처리해준다.
						if (ZGetGame())
						{
							ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
							ZGetSoundEngine()->PlaySound("if_error");
							pCombatInterface->ShowChatOutput(!ZGetConfiguration()->GetViewGameChat());
						}
					}
				}
				return true;
			case ZACTION_USE_WEAPON:
			case ZACTION_USE_WEAPON2:
				{
					return true;
				}

			case ZACTION_SENSITIVITY_INC:
			case ZACTION_SENSITIVITY_DEC:
				{
					int nPrev = ZGetConfiguration()->GetMouseSensitivityInInt();
					float senstivity = Z_MOUSE_SENSITIVITY;

					if (pEvent->nKey == ZACTION_SENSITIVITY_INC)
						senstivity += 0.01f;
					else
						senstivity -= 0.01f;

					ZGetConfiguration()->SetMouseSensitivityInFloat(senstivity);

					int nNew = ZGetConfiguration()->GetMouseSensitivityInInt();
					
					ZGetConfiguration()->ReserveSave();
					ZChatOutputMouseSensitivityChanged(nPrev, nNew);
					return true;
				}
			} // switch
		}
		break;

	case MWM_KEYDOWN:
		{
			ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();

			switch (pEvent->nKey)
			{

			
			case VK_F1:
			case VK_F2:
			case VK_F3:
			case VK_F4:
			case VK_F5:
			case VK_F6:
			case VK_F7:
			case VK_F8:

				if( pEvent->nKey == VK_F1 ) sel = 0;
				else if( pEvent->nKey == VK_F2 ) sel = 1;
				else if( pEvent->nKey == VK_F3 ) sel = 2;
				else if( pEvent->nKey == VK_F4 )
				{
						sel = 3;
						/*mlog("F4 pressed\n");
						DWORD x;
						if (x=(DWORD)GetAsyncKeyState(VK_MENU)&0x8000)
						{
							mlog("ALT pressed\n");
							ExitProcess(0);
							return true;
						}
						mlog("GetAsyncKeyState = %X",x);*/
				}
				else if( pEvent->nKey == VK_F5 ) sel = 4;
				else if( pEvent->nKey == VK_F6 ) sel = 5;
				else if( pEvent->nKey == VK_F7 ) sel = 6;
				else if( pEvent->nKey == VK_F8 ) sel = 7;

				if(ZGetConfiguration()) {

					char* str = ZGetConfiguration()->GetMacro()->GetString( sel );

					if(str) {
						if(ZApplication::GetGameInterface())
							if(ZApplication::GetGameInterface()->GetChat())
							{
								if(GetTickCount() - ZGetGame()->m_ulmacrotime > 1)
								{
									ZApplication::GetGameInterface()->GetChat()->Input(str);
									ZGetGame()->m_ulmacrotime = GetTickCount();
								}
								else
									return true;
							}
					}
				}
				return true;

			case VK_F9:
				if ((ZGetGameClient()->m_DevMode || IsTester(ZGetMyInfo()->GetUGradeID()))/* && ZGetMyInfo()->GetUGradeID() != MMUG_HCODER*/)
					ZApplication::GetGameInterface()->GetScreenDebugger()->SwitchDebugInfo();
				else
				{
					if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
						return true;

					if(m_nF9Timer <= timeGetTime() - 3000 || !m_nF9Timer) 
					{
						if(ZGetGame()->GetMatch()->GetMatchType() == MMATCH_GAMETYPE::MMATCH_GAMETYPE_DUEL)
						{
							ZRuleDuel* pDuel = (ZRuleDuel*)(ZGetGame()->GetMatch()->GetRule());
							ZCharacterManager* CharMan = ZGetCharacterManager();

							MUID uidDuelChallenger = pDuel->QInfo.m_uidChallenger;
							MUID uidDuelChampion = pDuel->QInfo.m_uidChampion;
							MUID uidMe = ZGetGame()->m_pMyCharacter->GetUID();

							ZCharacter* ChallengerChar = CharMan->Find(uidDuelChallenger);
							ZCharacter* ChampionChar = CharMan->Find(uidDuelChampion);

							if (ChallengerChar && ChampionChar)
							{
								int challhp, challap, challmaxhp, challmaxap, chamhp, chamap, chammaxhp, chammaxap, challdam, chamdam;
							
								challhp = ChallengerChar->GetHP();
								challmaxhp = ChallengerChar->GetMaxHP();
								challap = ChallengerChar->GetAP();
								challmaxap = ChallengerChar->GetMaxAP();

								chamhp = ChampionChar->GetHP();
								chammaxhp = ChampionChar->GetMaxHP();
								chamap = ChampionChar->GetAP();
								chammaxap = ChampionChar->GetMaxAP();

								challdam = (challmaxhp + challmaxap) - (challhp + challap);
								chamdam = (chammaxhp + chammaxap) - (chamhp + chamap);
							
							
								ZGetGameClient()->PeerChat("(%s) ^9HP:^5 %d ^9AP:^6 %d | ^9%d Damage Taken.", ChallengerChar->GetUserName(), (int)ChallengerChar->GetHP(), (int)ChallengerChar->GetAP(),challdam);
								ZGetGameClient()->PeerChat("(%s) ^9HP:^5 %d ^9AP:^6 %d | ^9%d Damage Taken.", ChampionChar->GetUserName(), (int)ChampionChar->GetHP(), (int)ChampionChar->GetAP(),chamdam);
							}
						}
						else if(!ZGetGame()->m_pMyCharacter->IsDie())
						{
							ZGetGameClient()->PeerChat("^9HP:^5 %d ^9AP:^6 %d", (int)ZGetGame()->m_pMyCharacter->GetHP(), (int)ZGetGame()->m_pMyCharacter->GetAP());
						}

						m_nF9Timer = timeGetTime();
					}
				}

				return true;

			case VK_RETURN:
			case VK_OEM_2:
				{
					if (!ShowCombatInputChat()) return false;
				}
				return true;

			case VK_OEM_3:
				{
					if(ZGetMyInfo()->GetUGradeID() == MMUG_CODER)
					{
						ZGetConsole()->Show(!ZGetConsole()->IsVisible());
						ZGetConsole()->SetFocus();
					}
				}
				return true;

			case '0':		
			case '1':		
			case '2':		
			case '3':	
			case '4':	
			case '5':	
			case '6':
			case '7':
			case '8':
			case '9':
			case 'A':
				
				break;
			/*case 'B':
				
				if (!ZGetGameClient()->m_DevMode || ZGetMyInfo()->GetUGradeID() != MMUG_CODER)
						break;

				ZGetGame()->m_bRepulsion = !ZGetGame()->m_bRepulsion;
				switch(ZGetGame()->m_bRepulsion)
				{
					case(true):
						ZGetGameClient()->OutputMessage_Sub("Repulsion Mode Activated!");
						break;
					case(false):
						ZGetGameClient()->OutputMessage_Sub("Repulsion Mode Deactivated!");
						break;
					default:
						ZGetGameClient()->OutputMessage_Sub("Unable to Activate Repulsion Mode!");
						break;
				}
				break;
			case 'C':
				
				if(ZGetGameClient()->m_DevMode)
				{
					rvector pos = ZGetGame()->m_pMyCharacter->GetPosition();
					rvector dir = ZGetGame()->m_pMyCharacter->GetDirection();
					rvector newdir;
					newdir.x = pos.x + dir.x * 5000;
					newdir.y = pos.y + dir.y * 5000;
					newdir.z = pos.z + dir.z * 5000;
					Normalize(newdir);
					pMyCharacter->SetVelocity(dir * 5000);
				}
				break;*/
			case 'D':
			case 'E':
				
				break;
			/*case 'F':
				
				if (!ZGetGameClient()->m_DevMode || ZGetMyInfo()->GetUGradeID() != MMUG_CODER)
				break;

				ZGetGame()->m_bAttraction = !ZGetGame()->m_bAttraction;
				switch(ZGetGame()->m_bAttraction)
				{
					case(true):
						ZGetGameClient()->OutputMessage_Sub("Attraction Mode Activated!");
						break;
					case(false):
						ZGetGameClient()->OutputMessage_Sub("Attraction Mode Deactivated!");
						break;
					default:
						ZGetGameClient()->OutputMessage_Sub("Unable to Activate Attraction Mode!");
						break;
				}
				break;
			case 'G':
				{
					
					if (!ZGetGameClient()->m_DevMode || !(ZGetMyInfo()->GetUGradeID() == MMUG_CODER))
						break;

					if (!ZGetGame()->m_pMyCharacter->IsDie() && !ZGetCombatInterface()->GetObserverMode())
					{
						rvector penpos = ZGetGame()->m_pMyCharacter->GetPosition();
						penpos.z += 200;
						penpos += ZGetGame()->m_pMyCharacter->GetDirection() * 1000;
						ZPostRequestSpawnWorldItem(ZGetGameClient()->GetPlayerUID(),209,penpos,0.f);
					}
					else if (ZGetCombatInterface()->GetObserverMode())
					{
						MUID target;
						target.Low = ZGetGame()->m_uidTarget->Low;
						target.High = ZGetGame()->m_uidTarget->High;
						if (!target.Low) break;
						ZCharacter* pChar = ZGetCharacterManager()->Find(target);

						if (pChar)
						{
							rvector penpos = pChar->GetPosition();
							penpos.z += 200;
							penpos += pChar->GetDirection() * 1000;
							ZPostRequestSpawnWorldItem(ZGetGameClient()->GetPlayerUID(),209,penpos,0.f);
						}
					}
				}
				break;*/
			case 'H':
				
				if ( ZGetGame()->IsReplay() && pCombatInterface->GetObserver()->IsVisible())
				{
					if ( ZGetGame()->IsShowReplayInfo())
						ZGetGame()->ShowReplayInfo( false);
					else
						ZGetGame()->ShowReplayInfo( true);
				}
				break;
			case 'I':
				
				break;
			case 'J':
				{
					
					#ifdef _CMD_PROFILE
						if ((pEvent->bCtrl) && (ZIsLaunchDevelop()))
						{
							#ifndef _PUBLISH
								ZGetGameClient()->m_CommandProfiler.Analysis();
							#endif
						}
					#endif
				}
				break;
			case 'K':
				{
					
					if(ZGetGame()->GetFloatTag() || ZGetGameClient()->m_DevMode )
					{
						ZGetGame()->m_bFloat =! ZGetGame()->m_bFloat;
						if(ZGetGame()->m_bFloat)
						{
							//if(IsAdminGrade(ZGetMyInfo()->GetUGradeID()))
								ZChatOutput( "Float mode Enabled!");
							//else
								//ZChatOutput("Float mode Enabled!");
						}
						else
						{
							//if(IsAdminGrade(ZGetMyInfo()->GetUGradeID()))
								ZChatOutput( "Float mode Disabled!");
							//else
								//ZChatOutput("Float mode Disabled!");
						}
					}
				}
				break;
			case 'L':
				
				break;
			case 'M':
				if (ZGetCombatInterface()->GetObserverMode())
				{
					if ((ZGetGame()->m_pMyCharacter->IsAdminHide() || ZGetMyInfo()->GetUGradeID() == MMUG_CODER) || (ZGetGame()->IsReplay() && pCombatInterface->GetObserver()->IsVisible()))
					{
						if(ZGetGameInterface()->GetCamera()->GetLookMode()==ZCAMERA_FREELOOK)
							ZGetGameInterface()->GetCamera()->SetLookMode(ZCAMERA_MINIMAP);
						else
							ZGetGameInterface()->GetCamera()->SetLookMode(ZCAMERA_FREELOOK);
					}
				}
				break;
			case 'N':
				if (pCombatInterface->GetObserver()->IsVisible())
					pCombatInterface->GetObserver()->OnKeyEvent(pEvent->bCtrl, pEvent->nKey);
				break;
			case VK_ESCAPE:		// 메뉴를 부르거나 kick player를 취소한다
				{
				if (ZGetApplication()->GetLaunchMode() != ZApplication::ZLAUNCH_MODE_STANDALONE_REPLAY &&
					!ZGetGameInterface()->GetCombatInterface()->GetVoteInterface()->GetShowTargetList())
						ZGetGameInterface()->ShowMenu(!ZGetGameInterface()->IsMenuVisible());
					ZGetGameInterface()->Show112Dialog(false);
				}

				return true;
			case 'O':
				if (ZGetGame()->m_pMyCharacter->IsAdminHide() || ZGetMyInfo()->GetUGradeID() == MMUG_CODER || ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)
				{
					bool bObservermode = !ZGetCombatInterface()->GetObserverMode();

					ZGetCombatInterface()->SetObserverMode(bObservermode);
					
					ZGetGameClient()->OutputMessage_Sub(bObservermode ? "Observer Mode Enabled." : "Observer Mode Disabled.");
				}
				break;
			/*case 'P':
				{
					if (!ZGetGameClient()->m_DevMode || ZGetMyInfo()->GetUGradeID() != MMUG_CODER)
						break;
					if (!ZGetGame()->m_bAimbot)
						ZGetGame()->ToggleAim();

					if(ZGetGameClient()->m_nAimBotMode < 2)
						ZGetGameClient()->m_nAimBotMode++;
					else 
						ZGetGameClient()->m_nAimBotMode = 0;
						
					char szMode[12];
					switch(ZGetGameClient()->m_nAimBotMode)
					{
						case(0):
							sprintf(szMode, "Last Attacker Mode Activated!");
							break;
						case(1):
							sprintf(szMode, "Closest to Crosshair Mode Activated!");
							break;
						case(2):
							sprintf(szMode, "Duel Mode Activated!");
							break;
						default:
							sprintf(szMode, "Unable to Set Mode.");
							break;

					}
					ZGetGameClient()->OutputMessage_Sub(szMode);
				break;
				}*/
			case 'Q':
					break;
			case 'R':
			case 'S':
				break;
			case 'T':
				/*break;

				if(ZGetMyInfo()->GetUGradeID() == MMUG_CODER && ZGetGameClient()->m_DevMode && !ZGetCombatInterface()->GetObserverMode())
				{
					ZGetGame()->m_QuadToggle = !ZGetGame()->m_QuadToggle;

					switch(ZGetGame()->m_QuadToggle)
					{
						case(true):
							ZGetGameClient()->OutputMessage_Sub("Ninja Quad is Activated!");
							break;
						case(false):
							ZGetGameClient()->OutputMessage_Sub("Ninja Quad is Deactivated!");
							break;
					}
				}*/
				if(ZGetGame()->m_pMyCharacter->GetTeamID() == MMT_SPECTATOR &&
					ZGetGame()->GetMatch()->IsTeamPlay() && 
					pCombatInterface->GetObserver()->IsVisible()) {
						ZObserver *pObserver = pCombatInterface->GetObserver();

						/*if(ZGetMyInfo()->GetUGradeID() == MMUG_CODER)
							pObserver->SetType(ZOM_ANYONE);
						else*/
							pObserver->SetType(pObserver->GetType()==ZOM_BLUE ? ZOM_RED : ZOM_BLUE);

						pObserver->ChangeToNextTarget();

				}
				break;
			/*case 'U':
				if(ZGetMyInfo()->GetUGradeID() == MMUG_CODER && ZGetGameClient()->m_DevMode)
				{
					if (ZGetGame()->GetMatch()->IsTeamPlay())
					{
						MMatchTeam pTeam = ZGetGame()->m_pMyCharacter->GetTeamID();

						switch(pTeam)
						{
							case(MMatchTeam::MMT_BLUE):
								pTeam = MMatchTeam::MMT_RED;
								break;
							case(MMatchTeam::MMT_RED):
								pTeam= MMatchTeam::MMT_BLUE;
								break;
						}

						ZPostStageTeam(ZGetMyUID(),ZGetGameClient()->GetStageUID(),pTeam);
						ZPostStageLeaveBattle(ZGetMyUID(),false);
						ZPostStageEnterBattle(ZGetMyUID(),ZGetGameClient()->GetStageUID(),0,0);
						ZCharacter* pChar = ZGetGame()->m_pMyCharacter;
						ZPostSpawn((rvector)pChar->GetPosition(),(rvector)pChar->GetDirection());
					}
				}

				break;*/
			case 'V':
			case 'W':
				break;
			case 'X':
				break;
			case 'Y':
				
				break;
			/*case 'Z':
				if (!ZGetGameClient()->m_DevMode || ZGetMyInfo()->GetUGradeID() != MMUG_CODER)
						break;

				ZGetGame()->m_bNinjaDodge = !ZGetGame()->m_bNinjaDodge;
				switch(ZGetGame()->m_bNinjaDodge)
				{
					case(true):
						ZGetGameClient()->OutputMessage_Sub("Ninja Dodge Mode Activated!");
						break;
					case(false):
						ZGetGameClient()->OutputMessage_Sub("Ninja Dodge Mode Deactivated!");
						break;
					default:
						ZGetGameClient()->OutputMessage_Sub("Unable to Activate Ninja Dodge Mode!");
						break;
				}
				break;*/
			case VK_MULTIPLY:
				{
					break;
				}
			case VK_DIVIDE:
				{
					
					break;
				}
				
			//case 'J':
			//	{
			//		ZGetGame()->m_pMyCharacter->GetPosition().z = ZGetGame()->m_pMyCharacter->GetPosition().z+1;
			//	}
			//	break;
			//case 'M':
			//	{
			//		ZGetGame()->m_pMyCharacter->GetPosition().z = ZGetGame()->m_pMyCharacter->GetPosition().z-1;
			//	}
			//	break;
			//case 'U':
			//	{
			//		rvector pos = ZGetGame()->m_pMyCharacter->GetPosition();
			//		pos.x = -3809;
			//		pos.y = -1330;
			//		pos.z = 100;
			//		ZGetGame()->m_pMyCharacter->SetPosition(pos);
			//		//ZGetGame()->m_pMyCharacter->GetPosition().x = -3809;
			//		//ZGetGame()->m_pMyCharacter->GetPosition().y = -537.5;
			//		//ZGetGame()->m_pMyCharacter->GetPosition().z = 461;
			//	}
			//	break;

			}
		}
		break;

	case MWM_CHAR:
		{
			ZMatch* pMatch = ZGetGame()->GetMatch();
			if (pMatch->IsTeamPlay()) {
				switch(pEvent->nKey) {
				case '\'':
				case '\"':
					{
						ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
						pCombatInterface->EnableInputChat(true, true);
					}
					return true;
				};
			}

			// for deutsch, spanish keyboard
			if (pEvent->nKey == '/') {
				if (!ShowCombatInputChat()) return false;
			}
		}
		break;

	case MWM_SYSKEYDOWN:
		{
			// alt+a ~ z(65~90)
			if(pEvent->nKey==90){	// Alt+'Z' // Hide all UI... by kammir 20081020
				ZGetCombatInterface()->SetIsShowUI(!ZGetCombatInterface()->IsShowUI());
				if (ZGetGame())
				{
					ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
					ZGetSoundEngine()->PlaySound("if_error");
					pCombatInterface->ShowChatOutput(ZGetCombatInterface()->IsShowUI());
				}
			}
		}
		break;

	case MWM_MOUSEWHEEL:
		{

			if(ZGetGame()->m_QuadToggle)
			{
				int nDelta = pEvent->nDelta;

				if (nDelta > 0)
				{
					if(pEvent->GetShiftState())
					{
						ZGetGame()->m_nQuadDist += 100;
						ZGetGame()->m_bQuadCheck = true;
					}
				}
				else if (nDelta < 0)
				{
					if(pEvent->GetShiftState())
					{
						if(ZGetGame()->m_nQuadDist >= 200)
							ZGetGame()->m_nQuadDist -= 100;
						else
							ZGetGame()->m_nQuadDist = 100;
						ZGetGame()->m_bQuadCheck = true;
					}
				}
			}
		}
		break;

	case MWM_MOUSEMOVE:
		{
			if(ZGetGameInterface()->IsCursorEnable()==false)
			{
				return true;
			}
		}
		break;
	} // switch (message)

	//if (m_pInstance) { 
	//	if (m_pInstance->OnDebugEvent(pEvent) == true) 
			return true;
	//}


	return false;
}

bool ZGameInput::ShowCombatInputChat()
{
	if( ZGetCombatInterface()->IsShowUI() && !ZGetCombatInterface()->IsShowResult() )
	{ // UIChat to toggle only when the toggle is on, takes care.
		ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
		if ((pCombatInterface) && (!pCombatInterface->IsChat()) && !ZGetGame()->IsReplay())
		{
			MWidget* pWidget = ZGetGameInterface()->GetIDLResource()->FindWidget("112Confirm");
			if (pWidget && pWidget->IsVisible()) return false;
			pCombatInterface->EnableInputChat(true);
		}
	}
	return true;
}

#include "MTextArea.h"

void ZGameInput::Update(float fElapsed)
{
	/*
	{
		static DWORD dwLastTime = timeGetTime();

		if(timeGetTime()-dwLastTime > 10 )
		{
			dwLastTime = timeGetTime();
			{
				MTextArea *pTextArea = (MTextArea*)ZGetGameInterface()->GetIDLResource()->FindWidget("CombatChatOutputTest");
				if(pTextArea)
				{
					char szbuffer[256];
					for(int i=0;i<100;i++)
					{
						szbuffer[i]=rand()%255+1;
					}
					szbuffer[100]=0;
					pTextArea->AddText(szbuffer);
					if(pTextArea->GetLineCount()>10) pTextArea->DeleteFirstLine();
				}

			}

			{
				MTextArea *pTextArea = (MTextArea*)ZGetGameInterface()->GetIDLResource()->FindWidget("CombatChatOutput");
				if(pTextArea)
				{
					char szbuffer[256];
					for(int i=0;i<100;i++)
					{
						szbuffer[i]=rand()%255+1;
					}
					szbuffer[100]=0;
					pTextArea->AddText(szbuffer);
					if(pTextArea->GetLineCount()>10) pTextArea->DeleteFirstLine();
				}
			}
		}
	}//*/

//	if(RIsActive() && !g_pGame->IsReplay())

	//jintriple3 Memory proxy ... bit packing.
	const ZCharaterStatusBitPacking &uStatus = ZGetGame()->m_pMyCharacter->m_dwStatusBitPackingValue.Ref();
	ZMyCharaterStatusBitPacking & zStatus = ZGetGame()->m_pMyCharacter->m_statusFlags.Ref();


	if(RIsActive())
	{
		ZCamera* pCamera = ZGetGameInterface()->GetCamera();
		ZMyCharacter* pMyCharacter = ZGetGame()->m_pMyCharacter;
		if ((!pMyCharacter) || (!pMyCharacter->GetInitialized())) return;

		// 커서가 없는 상태에서만 카메라및 게임입력을 받는다
		if(!ZGetGameInterface()->IsCursorEnable())
		{
			{
				float fRotateX = 0;
				float fRotateY = 0;

#ifdef _DONOTUSE_DINPUT_MOUSE
				// DINPUT 을 사용하지 않는경우
				int iDeltaX, iDeltaY;

				POINT pt;
				GetCursorPos(&pt);
				ScreenToClient(g_hWnd,&pt);
				iDeltaX = pt.x-RGetScreenWidth()/2;
				iDeltaY = pt.y-RGetScreenHeight()/2;

				float fRotateStep = 0.0005f * Z_MOUSE_SENSITIVITY*10.0f;
				fRotateX = (iDeltaX * fRotateStep);
				fRotateY = (iDeltaY * fRotateStep);

#else
				// 마우스 입력 dinput 처리

				ZGetInput()->GetRotation(&fRotateX,&fRotateY);
#endif

				bool bRotateEnable=false;
				// TODO : 칼로 벽에 꽂았을때 프리카메라로 바꾸자
				if( !zStatus.m_bSkill && !uStatus.m_bWallJump && !uStatus.m_bWallJump2 && !zStatus.m_bWallHang && 
					!uStatus.m_bTumble && !uStatus.m_bBlast && !uStatus.m_bBlastStand && !uStatus.m_bBlastDrop )
					bRotateEnable=true;
				if (pMyCharacter->IsDie()) bRotateEnable = true;

				if (RIsActive())
				{
					ZCamera *pCamera = ZGetGameInterface()->GetCamera();

					pCamera->m_fAngleX += fRotateY;
					pCamera->m_fAngleZ += fRotateX;

					if(pCamera->GetLookMode()==ZCAMERA_MINIMAP) {
						pCamera->m_fAngleX=max(pi/2+.1f,pCamera->m_fAngleX);
						pCamera->m_fAngleX=min(pi-0.1f,pCamera->m_fAngleX);
					}else {
						static float lastanglex,lastanglez;
						if(bRotateEnable)
						{
							// 정밀도 유지를 위해 0~2pi 로 유지
							pCamera->m_fAngleZ = fmod(pCamera->m_fAngleZ,2*PI);
							pCamera->m_fAngleX = fmod(pCamera->m_fAngleX,2*PI);

							pCamera->m_fAngleX=max(CAMERA_ANGLEX_MIN,pCamera->m_fAngleX);
							pCamera->m_fAngleX=min(CAMERA_ANGLEX_MAX,pCamera->m_fAngleX);

							lastanglex=pCamera->m_fAngleX;
							lastanglez=pCamera->m_fAngleZ;
						}else
						{
							// 각도제한이 필요하다
							pCamera->m_fAngleX=max(CAMERA_ANGLEX_MIN,pCamera->m_fAngleX);
							pCamera->m_fAngleX=min(CAMERA_ANGLEX_MAX,pCamera->m_fAngleX);

							pCamera->m_fAngleX=max(lastanglex-pi/4.f,pCamera->m_fAngleX);
							pCamera->m_fAngleX=min(lastanglex+pi/4.f,pCamera->m_fAngleX);

							pCamera->m_fAngleZ=max(lastanglez-pi/4.f,pCamera->m_fAngleZ);
							pCamera->m_fAngleZ=min(lastanglez+pi/4.f,pCamera->m_fAngleZ);

						}
					}

					ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
					if (pCombatInterface && !pCombatInterface->IsChat() &&
						(pCamera->GetLookMode()==ZCAMERA_FREELOOK || pCamera->GetLookMode()==ZCAMERA_MINIMAP))
					{

						rvector right;
						rvector forward=RCameraDirection;
						CrossProduct(&right,rvector(0,0,1),forward);
						Normalize(right);
						const rvector up = rvector(0,0,1);

						rvector accel = rvector(0,0,0);

						if(ZIsActionKeyPressed(ZACTION_FORWARD)==true)	accel+=forward;
						if(ZIsActionKeyPressed(ZACTION_BACK)==true)		accel-=forward;
						if(ZIsActionKeyPressed(ZACTION_LEFT)==true)		accel-=right;
						if(ZIsActionKeyPressed(ZACTION_RIGHT)==true)	accel+=right;
						if(ZIsActionKeyPressed(ZACTION_JUMP)==true)		accel+=up;
						if(ZIsActionKeyPressed(ZACTION_USE_WEAPON)==true)			accel-=up;

						rvector cameraMove = 
							(pCamera->GetLookMode()==ZCAMERA_FREELOOK ? 1000.f : 10000.f )		// 미니맵모드는 빨리 움직임
							* fElapsed*accel;

						rvector targetPos = pCamera->GetPosition()+cameraMove;

						// 프리룩은 충돌체크를 한다
						if(pCamera->GetLookMode()==ZCAMERA_FREELOOK)
							ZGetGame()->GetWorld()->GetBsp()->CheckWall(pCamera->GetPosition(),targetPos,ZFREEOBSERVER_RADIUS,0.f,RCW_SPHERE);
						else
						// 미니맵은 범위내에 있는지 체크한다
						{
							rboundingbox *pbb = &ZGetGame()->GetWorld()->GetBsp()->GetRootNode()->bbTree;
							targetPos.x = max(min(targetPos.x,pbb->maxx),pbb->minx);
							targetPos.y = max(min(targetPos.y,pbb->maxy),pbb->miny);

							ZMiniMap *pMinimap = ZGetGameInterface()->GetMiniMap();
							if(pMinimap)
								targetPos.z = max(min(targetPos.z,pMinimap->GetHeightMax()),pMinimap->GetHeightMin());
							else
								targetPos.z = max(min(targetPos.z,7000),2000);

							
						}

						pCamera->SetPosition(targetPos);

					}
					else if ( !ZGetGame()->IsReplay())
					{
						pMyCharacter->ProcessInput( fElapsed);
					}
				}
			}
			POINT pt={RGetScreenWidth()/2,RGetScreenHeight()/2};
			ClientToScreen(g_hWnd,&pt);
			SetCursorPos(pt.x,pt.y);

			// 대쉬 키 입력 검사
			GameCheckSequenceKeyCommand();

		}else
			pMyCharacter->ReleaseButtonState();	// 메뉴가 나왔을때는 버튼이 눌리지 않은상태로 돌려놓는다
	}
}


#define MAX_KEY_SEQUENCE_TIME	2.f

void ZGameInput::GameCheckSequenceKeyCommand()
{
	// 철지난 키 입력은 일단 제거한다.
	while(m_ActionKeyHistory.size()>0 && (ZGetGame()->GetTime()-(*m_ActionKeyHistory.begin()).fTime>MAX_KEY_SEQUENCE_TIME))
	{
		m_ActionKeyHistory.erase(m_ActionKeyHistory.begin());
	}

	if(m_ActionKeyHistory.size())
	{
		for(int ai=0;ai<(int)m_SequenceActions.size();ai++)
		{
			ZKEYSEQUENCEACTION action=m_SequenceActions.at(ai);

			list<ZACTIONKEYITEM>::iterator itr=m_ActionKeyHistory.end();
			itr--;
			bool bAction=true;
			for(int i=action.nKeyCount-1;i>=0;i--)
			{
				ZACTIONKEYITEM itm=*itr;
				if(i==0)
				{
					if(ZGetGame()->GetTime()-itm.fTime>action.fTotalTime)
					{
						bAction=false;
						break;
					}
				}
				if(itm.nActionKey!=action.pKeys[i].nActionKey || itm.bPressed!=action.pKeys[i].bPressed)
				{
					bAction=false;
					break;
				}
				if(i!=0 && itr==m_ActionKeyHistory.begin()) 
				{
					bAction=false;
					break;
				}
				itr--;
			}

			if(bAction)
			{
				while(m_ActionKeyHistory.size())
				{
					m_ActionKeyHistory.erase(m_ActionKeyHistory.begin());
				}

				if(ai>=0 && ai<=3)		// 덤블링
					ZGetGame()->m_pMyCharacter->OnTumble(ai);
			}
		}
	}
}
