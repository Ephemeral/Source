//Work-in-progress
#ifndef _ZGAMEOBJECT_H
#define _ZGAMEOBJECT_H

#include "ZPrerequisites.h"

#include "MMatchClient.h"
#include "MDataChecker.h"

#include "RTypes.h"
#include "RBspObject.h"
#include "ZMatch.h"
#include "ZGameTimer.h"
#include "ZWater.h"
#include "ZClothEmblem.h"
#include "ZEffectManager.h"
#include "ZWeaponMgr.h"
#include "ZHelpScreen.h"
#include "ZCharacterManager.h"
#include "ZObjectManager.h"
#include "ZWorld.h"

_USING_NAMESPACE_REALSPACE2

class MZFileSystem;

class ZLoading;
class ZGameAction;
class ZSkyBox;
class ZFile;
class ZObject;
class ZCharacter;
class ZMyCharacter;
class ZMiniMap;
class ZMsgBox;
class ZInterfaceBackground;
class ZCharacterSelectView;
class ZScreenEffectManager;
class ZPlayerMenu;
class ZGameClient;
class ZMapDesc;
class ZReplayLoader;

struct ZFlag
{
	bool m_bIsFlag;

	enum ZFlag_Team
	{
		TEAM_RED,
		TEAM_BLUE
	};

	ZFlag_Team m_nTeam;
};

struct ZGameObjectDesc
{
	char szName[32];
	char szMesh[256];

	float m_fPosX;
	float m_fPosY;
	float m_fPosZ;

	ZFlag m_nFlag;
};//

class ZGameObject
{
protected:
	ZGameObjectDesc			m_nDesc;

	rvector GetPosition() { return rvector(m_nDesc.m_fPosX, m_nDesc.m_fPosY, m_nDesc.m_fPosZ); }
public:
	ZGameObject();
	~ZGameObject();

	RVisualMesh*			m_pVMesh;
	MProtectValue<int>		m_nVMID;

	void LoadMesh();
	void Draw();
	void Update();
};

#endif