#ifndef _CHECK_RETURN_CALLSTACK_
#define _CHECK_RETURN_CALLSTACK_
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif
#include "stdafx.h"
typedef struct pe_struct
{
        unsigned long base;
        unsigned long top;
        unsigned long size;
}PE_STRUCT;
PE_STRUCT __declspec(noinline) getStructure();
#define CHECK_RETURN_CALLSTACK()					DWORD ret_ptr_dlrjsTmwlakdy     = 0; \
													__asm {pushad} \
													__asm {mov eax, ebp} \
													__asm {add eax, 4} \
													__asm {mov eax, dword ptr ds:[eax]}     \
													__asm {mov ret_ptr_dlrjsTmwlakdy, eax}  \
													__asm {popad}; \
													PE_STRUCT peStruct              = getStructure(); \
													if( (ret_ptr_dlrjsTmwlakdy < peStruct.base ) || (ret_ptr_dlrjsTmwlakdy > peStruct.top) ) { \
													MCommand* pC=ZNewCmd(MC_REQUEST_GIVE_ONESELF_UP); ZPostCommand(pC); }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CHECK_RETURN_CALLSTACK_ESP()		DWORD ret_ptr_dlrjsTmwlakdy	= 0; \
													__asm {pushad} \
													__asm {mov eax, esp} \
													__asm {add eax, 4} \
													__asm {mov eax, dword ptr ds:[eax]}	\
													__asm {mov ret_ptr_dlrjsTmwlakdy, eax}	\
													__asm {popad}; \
													PE_STRUCT peStruct              = getStructure(); \
													if( (ret_ptr_dlrjsTmwlakdy < peStruct.base ) || (ret_ptr_dlrjsTmwlakdy > peStruct.top) ) { \
													MCommand* pC=ZNewCmd(MC_REQUEST_GIVE_ONESELF_UP); ZPostCommand(pC); } 

#endif