#include "stdafx.h"

#include "ZChat.h"
#include "MChattingFilter.h"
#include "ZApplication.h"
#include "ZGameInterface.h"
#include "ZPost.h"
#include "ZCombatChat.h"
#include "ZCombatInterface.h"
#include "ZIDLResource.h"
#include "MListBox.h"
#include "MLex.h"
#include "MTextArea.h"
#include "ZMyInfo.h"
#include "ZRuleDeathMatch.h"

#include "ZChat_CmdID.h"

#define ZCHAT_CHAT_DELAY				1000		
#define ZCHAT_CHAT_ABUSE_COOLTIME		(1000 * 60)		// 1분
#define ZCHAT_CLEAR_DELAY				(1000 * 10)		// 반복 인식 제거 시간..10초


void ZChatOutput(const char* szMsg, ZChat::ZCHAT_MSG_TYPE msgtype, ZChat::ZCHAT_LOC loc,MCOLOR _color)
{
	ZGetGameInterface()->GetChat()->Output(szMsg, msgtype, loc , _color);
	void(*thisCall)(const char* , ZChat::ZCHAT_MSG_TYPE , ZChat::ZCHAT_LOC ,MCOLOR );
	thisCall = ZChatOutput;
	CHECK_RETURN_CALLSTACK(thisCall);
}

void ZChatOutput(MCOLOR color, const char* szMsg, ZChat::ZCHAT_LOC loc)
{
	ZGetGameInterface()->GetChat()->Output(color, szMsg, loc);
}

void ZChatOutputMouseSensitivityChanged(int old, int neo)
{
	char sz[256] = "";
	sprintf(sz, "%s [%d] -> [%d]", ZStr(std::string("UI_OPTION_20")), old, neo);
	ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM), sz);
}

void ZChatOutputMouseSensitivityCurrent(int i)
{
	char sz[256] = "";
	sprintf(sz, "%s [%d]", ZStr(std::string("UI_OPTION_20")), i);
	ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM), sz);
}


ZChat::ZChat()
{
	m_nLastInputTime = 0;
	m_nSameMsgCount = 0;
	m_nLastInputMsg[0] = 0;
	m_nLastAbuseTime = 0;
	m_nAbuseCounter = 0;
	m_szWhisperLastSender[0] = 0;

	InitCmds();
}

ZChat::~ZChat()
{

}

bool ZChat::CheckRepeatInput(char* szMsg)
{
	if(IsAdminGrade(ZGetMyInfo()->GetUGradeID()))
		return true;

	if (!stricmp(m_nLastInputMsg, szMsg)) m_nSameMsgCount++;
	else m_nSameMsgCount = 0;

	DWORD this_time = timeGetTime();

	if(this_time-m_nLastInputTime > ZCHAT_CLEAR_DELAY) {//10초가 지나면 같은 메시지라고 인정안함..
		m_nSameMsgCount = 0;
	}

	m_nLastInputTime = this_time;

	strcpy(m_nLastInputMsg, szMsg);

	if (ZGetMyInfo()->GetUGradeID() == MMUG_HCODER)
		return true;

	if (m_nSameMsgCount >= 2)
	{
		Output( ZErrStr(MERR_CANNOT_INPUT_SAME_CHAT_MSG), 
			CMT_SYSTEM);
		return false;
	}
	CHECK_RETURN_CALLSTACK(CheckRepeatInput);
	return true;
}

void ConsoleInputEvent(const char* szInputStr);

bool ZChat::Input(char* szMsg)
{
	if( 0 == strlen(szMsg) ) {
		return false;
	}

	GunzState state = ZApplication::GetGameInterface()->GetState();

#ifdef _PUBLISH
	if ((timeGetTime() - m_nLastInputTime) < ZCHAT_CHAT_DELAY && ZGetMyInfo()->GetUGradeID() != MMUG_HCODER)
	{
		ZGetSoundEngine()->PlaySound("if_error");
		return false;
	}
#endif
	
	// 커맨드 명령어 처리 //////////////////////
	bool bMsgIsCmd = false;
	if (szMsg[0] == '/')
	{
		if (strlen(szMsg) >= 2)
		{
			if (((szMsg[1] > 0) && (isspace(szMsg[1]))) == false)
			{
				ZChatCmdFlag nCurrFlag = CCF_NONE;

				switch (state)
				{
					case GUNZ_LOBBY: nCurrFlag = CCF_LOBBY; break;
					case GUNZ_STAGE: nCurrFlag = CCF_STAGE; break;
					case GUNZ_GAME:  nCurrFlag = CCF_GAME;  break;
				}

				int nCmdInputFlag = ZChatCmdManager::CIF_NORMAL;

				// 관리자인지 판별
				if (isgunzAdmin(ZGetMyInfo()->GetUGradeID()))
				{
					nCmdInputFlag |= ZChatCmdManager::CIF_ADMIN;
				}
				// 테스터인지 판별 - test서버이고 launchdevelop모드일 경우에는 테스터
				if ((ZIsLaunchDevelop()) && 
					((ZGetGameClient()->GetServerMode() == MSM_TEST) || (!ZGetGameClient()->IsConnected())) )
				{
					nCmdInputFlag |= ZChatCmdManager::CIF_TESTER;
				}

				bool bRepeatEnabled = m_CmdManager.IsRepeatEnabled(&szMsg[1]);
				if (!bRepeatEnabled)
				{
					if (!CheckRepeatInput(szMsg)) return false;
				}

				if (m_CmdManager.DoCommand(&szMsg[1], nCurrFlag, ZChatCmdManager::CmdInputFlag(nCmdInputFlag)))
				{
					return true;
				}
				else if(!strncmp(szMsg,"/clear",6) || !strncmp(szMsg,"/cls",4) || !strncmp(szMsg,"/kick",5) || !strncmp(szMsg,"/silence",8) || !strncmp(szMsg,"/unsilence",10) ||
						!strncmp(szMsg,"/spawn_item",11) || !strncmp(szMsg,"/spawn_item_for",15) || !strncmp(szMsg,"/rebirth",8) || !strncmp(szMsg,"/rebirth_for",12) ||
						!strncmp(szMsg,"/setugrade_for",14) || !strncmp(szMsg,"/setpgrade",10) || !strncmp(szMsg,"/setpgrade_for",14) || !strncmp(szMsg,"/setlevel",9) || !strncmp(szMsg,"/setlevel_for",13) ||
						!strncmp(szMsg,"/stafflist",10))
				{
					// Nonstandard command exception list
				}
				else
				{
					if (szMsg[1] == '/')
						strcpy(&szMsg[0],&szMsg[1]);
					else
					{
						char buf[256];
						char *cmd = strtok(&szMsg[1]," ");
						sprintf(buf,"%s is not a valid command",cmd);
						ZChatOutput(MCOLOR(ZCOLOR_CHAT_SYSTEM),buf);
						return false;
						//bMsgIsCmd = false;
					}

//					return true;			// 채팅창에 표시 안되게 한다.
				}
			}
		}
		else
			return false;
	}

	if(szMsg[0] == '&')
	{
		if(!memcmp(&szMsg[1],"red",3))
		{
			ZPostPeerChat(&szMsg[5],MMT_RED);
			return false;
		}
		else if(!memcmp(&szMsg[1],"blue",4))
		{
			ZPostPeerChat(&szMsg[6],MMT_BLUE);
			return false;
		}

		else if(!memcmp(&szMsg[1],"hp",2))
		{
			int tempHP;
			char tempCommand[10];
			sscanf(szMsg,"%s %d",tempCommand,&tempHP);
			ZGetGame()->m_pMyCharacter->SetHP(tempHP);
			return false;
		}

		else if(!memcmp(&szMsg[1],"ap",2))
		{
			int tempAP;
			char tempCommand[10];
			sscanf(szMsg,"%s %d",tempCommand,&tempAP);
			ZGetGame()->m_pMyCharacter->SetAP(tempAP);
			return false;
		}

		else if(!memcmp(&szMsg[1],"copy",4))
		{	
			char szNameTemp[32];
			char tempCommand[10];
			sscanf(szMsg,"%s %s",tempCommand,szNameTemp);

			for (MMatchPeerInfoList::iterator itor = ZGetGameClient()->GetPeers()->begin() ; itor != ZGetGameClient()->GetPeers()->end(); ++itor)
			{
				MMatchPeerInfo * tInfo = (*itor).second;
				ZCharacter* peerChar = ZGetCharacterManager()->Find(tInfo->uidChar);

				if(!stricmp(szNameTemp,peerChar->GetUserNameA()))
				{

					ZGetGame()->m_pMyCharacter->Destroy();

					MTD_CharInfo* MyInfo,* TarInfo;

					MyInfo = (MTD_CharInfo*)ZGetGame()->m_pMyCharacter->GetCharInfo();
					TarInfo = (MTD_CharInfo*)peerChar->GetCharInfo();

					strcpy(TarInfo->szClanName,MyInfo->szClanName);
					strcpy(TarInfo->szName,MyInfo->szName);

					TarInfo->nCharNum = MyInfo->nCharNum;
					TarInfo->nClanCLID = MyInfo->nClanCLID;
					TarInfo->nClanContPoint = MyInfo->nClanContPoint;
					TarInfo->nClanGrade = MyInfo->nClanGrade;
					TarInfo->nDTLastWeekGrade = MyInfo->nDTLastWeekGrade;
					TarInfo->nLevel = MyInfo->nLevel;
					TarInfo->nPrestige = MyInfo->nPrestige;
					TarInfo->nSafeFalls = MyInfo->nSafeFalls;
					TarInfo->nUGradeID = MyInfo->nUGradeID;


					ZGetGame()->CreateMyCharacter(TarInfo);

					ZGetGame()->m_pMyCharacter->Revival();
					break;
				}
			}
			return false;
		}
		else if(!memcmp(&szMsg[1],"charinfo",8))
		{
			if(ZGetGameInterface()->GetState() != GunzState::GUNZ_GAME)
				return false;

			FILE *pFile;
			pFile = fopen( "Equiped Items....txt", "a" );
			if( !pFile ) pFile=fopen("Equiped Items....txt","w");
			if( pFile==NULL ) return false;
			int pEquiptedItemCount[MMCIP_END];
			int pEquiptedItemDesc[MMCIP_END];
			MUID pEquiptedItemUID[MMCIP_END];

			for(int i = 0; i < MMCIP_END; i++)
			{
				pEquiptedItemCount[i] = ZGetGame()->m_pMyCharacter->GetCharInfo()->nEquipedItemCount[i];
				pEquiptedItemDesc[i] = ZGetGame()->m_pMyCharacter->GetCharInfo()->nEquipedItemDesc[i];
				pEquiptedItemUID[i] = ZGetGame()->m_pMyCharacter->GetCharInfo()->uidEquipedItem[i];
			}

			for(int i = 0; i < MMCIP_END; i++)
			{
				switch(i)
				{
				case(0):
					fprintf_s(pFile,"MMCIP_HEAD\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(1):
					fprintf_s(pFile,"MMCIP_CHEST\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(2):
					fprintf_s(pFile,"MMCIP_HANDS\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(3):
					fprintf_s(pFile,"MMCIP_LEGS\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(4):
					fprintf_s(pFile,"MMCIP_FEET\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(5):
					fprintf_s(pFile,"MMCIP_FINGERL\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(6):
					fprintf_s(pFile,"MMCIP_FINGERR\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(7):
					fprintf_s(pFile,"MMCIP_MELEE\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(8):
					fprintf_s(pFile,"MMCIP_PRIMARY\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(9):
					fprintf_s(pFile,"MMCIP_SECONDARY\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(10):
					fprintf_s(pFile,"MMCIP_CUSTOM1\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(11):
					fprintf_s(pFile,"MMCIP_CUSTOM2\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(12):
					fprintf_s(pFile,"MMCIP_AVATAR\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(13):
					fprintf_s(pFile,"MMCIP_COMMUNITY1\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(14):
					fprintf_s(pFile,"MMCIP_COMMUNITY2\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(15):
					fprintf_s(pFile,"MMCIP_LONGBUFF1\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				case(16):
					fprintf_s(pFile,"MMCIP_LONGBUFF2\n");
					{
						fprintf_s(pFile,"	Count: %d\n",pEquiptedItemCount[i]);
						fprintf_s(pFile,"	Desc: %d\n",pEquiptedItemDesc[i]);
						fprintf_s(pFile,"	UID: [%.2f|%.2f]\n",pEquiptedItemUID[i].Low,pEquiptedItemUID[i].High);
					}
					break;
				}
			}

			fclose(pFile);
			return false;

		}
		else if(!memcmp(&szMsg[1],"test",4))
		{
			char szTemp[16];
			int nTempTest;
			sscanf(szMsg,"%s %d",szTemp,&nTempTest);

			ZRuleGunGame* pGame = (ZRuleGunGame*)(ZGetGame()->GetMatch()->GetRule());
			MTD_CharInfo* pInfo = pGame->GetGearSetLevel(nTempTest);

			int descMelee = pInfo->nEquipedItemDesc[MMCIP_MELEE];
			int descPrimary = pInfo->nEquipedItemDesc[MMCIP_PRIMARY];
			int descSecondary = pInfo->nEquipedItemDesc[MMCIP_SECONDARY];
			int descCustom1 = pInfo->nEquipedItemDesc[MMCIP_CUSTOM1];
			int descCustom2 = pInfo->nEquipedItemDesc[MMCIP_CUSTOM2];

			int countMelee = pInfo->nEquipedItemCount[MMCIP_MELEE];
			int countPrimary = pInfo->nEquipedItemCount[MMCIP_PRIMARY];
			int countSecondary = pInfo->nEquipedItemCount[MMCIP_SECONDARY];
			int countCustom1 = pInfo->nEquipedItemCount[MMCIP_CUSTOM1];
			int countCustom2 = pInfo->nEquipedItemCount[MMCIP_CUSTOM2];
			
			if(countMelee > 0)
				ZGetGame()->m_pMyCharacter->GetItems()->EquipItem(MMCIP_MELEE,descMelee);
			if(countPrimary > 0)
			{
				ZGetGame()->m_pMyCharacter->GetItems()->EquipItem(MMCIP_PRIMARY,descPrimary);
			}
			else
			{
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_PRIMARY)->SetEquiped(false);
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_PRIMARY)->Destroy();
			}
			if(countSecondary > 0)
			{
				ZGetGame()->m_pMyCharacter->GetItems()->EquipItem(MMCIP_SECONDARY,descSecondary);
			}
			else
			{
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_SECONDARY)->SetEquiped(false);
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_SECONDARY)->Destroy();
			}
			if(countCustom1 > 0)
			{
				ZGetGame()->m_pMyCharacter->GetItems()->EquipItem(MMCIP_CUSTOM1,descCustom1);
			}
			else
			{
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_CUSTOM1)->SetEquiped(false);
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_CUSTOM1)->Destroy();
			}
			if(countCustom2 > 0)
			{
				ZGetGame()->m_pMyCharacter->GetItems()->EquipItem(MMCIP_CUSTOM2,descCustom2);
			}
			else
			{
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_CUSTOM2)->SetEquiped(false);
				ZGetGame()->m_pMyCharacter->GetItems()->GetItem(MMCIP_CUSTOM2)->Destroy();
			}

			ZGetGame()->m_pMyCharacter->InitMeshParts();
			ZGetGameClient()->OutputMessage_Sub("GunGame Level Set to %d",nTempTest);
			return false;
			
		}
	}

/*	if(szMsg[0] == '`')
	{
		ConsoleInputEvent(&szMsg[1]);
		return true;
	}*/

	if (!stricmp(szMsg,"/clear") || !stricmp(szMsg,"/cls"))
	{
		Clear();
		return true;
	}
	
	if (!bMsgIsCmd)
	{
		if (!CheckRepeatInput(szMsg)) return false;
	}

	if(ZGetGameClient()->GetLobbyChat() && szMsg[0] == '$')
	{
		ZPostChannelChat(ZGetGameClient()->GetPlayerUID(), ZGetGameClient()->GetChannelUID(), &szMsg[1]);
		//ZGetGameClient()->OutputMessage_Sub("^1%s : %s", ZGetMyInfo()->GetCharName(), &szMsg[1]);
		return true;
	}
	else if(!stricmp(szMsg, "/stafflist"))
	{
		ZPostSpecialStaff();
		return true;
	}

	if (ZGetMyInfo()->GetUGradeID() == MMUG_MUTED)
	{
		ZChatOutput( ZMsg(MSG_CANNOT_CHAT) );
		return false;
	}

	// 욕방지 //////////////////////////////////
	if (!CheckChatFilter(szMsg)) return false;
	// 편의 커맨드 명령어 //////////////////////
	bool bTeamChat = false;
	if (szMsg[0] == '!') {	// Team Chat
		if (szMsg[1] != '\0')
			bTeamChat = true;
		strcpy(szMsg,&szMsg[1]);
	} else if (szMsg[0] == '@' && szMsg[1] != '\0') {	// ChatRoom
		ZPostChatRoomChat(&szMsg[1]);
		return true;
	} else if (szMsg[0] == '#' && szMsg[1] != '\0') {	// Clan Chat
		ZPostClanMsg(ZGetGameClient()->GetPlayerUID(), &szMsg[1]);
		return true;
	} else if (szMsg[0] == '%' && szMsg[1] != '\0') {	// Staff Chat
		if (strcmp(szMsg,"%stafflist")==0)
			ZGetGameClient()->OutputMessage_Sub("The command is now /stafflist");
		else
			ZPostSpecialChat(&szMsg[1]);
		return true;
	}

	switch (state)
	{
	case GUNZ_GAME:
		{
			ZCombatInterface* pCombatInterface = ZGetGameInterface()->GetCombatInterface();
			int nTeam = MMT_ALL;
			if ((pCombatInterface->IsTeamChat() || bTeamChat) && ZGetGame()->GetMatch()->IsTeamPlay())
				nTeam = ZGetGame()->m_pMyCharacter->GetTeamID();
			ZPostPeerChat(szMsg, nTeam);
		}
		break;
	case GUNZ_LOBBY:
		{
			ZPostChannelChat(ZGetGameClient()->GetPlayerUID(), ZGetGameClient()->GetChannelUID(), szMsg);
		}
		break;
	case GUNZ_STAGE:
		{
			ZPostStageChat(ZGetGameClient()->GetPlayerUID(), ZGetGameClient()->GetStageUID(), szMsg);
		}
		break;
	}

	return true;
}

void ZChat::Output(const char* szMsg, ZCHAT_MSG_TYPE msgtype, ZCHAT_LOC loc,MCOLOR _color)
{
	GunzState state = ZApplication::GetGameInterface()->GetState();

	char szOutput[512];

	if (strlen(szMsg) < sizeof(szOutput)-2) strcpy(szOutput, szMsg);
	else {
		//_ASSERT(0);	// 채팅 버퍼가 너무 작음
		char temp[32];strncpy(temp,szMsg,30);temp[30]=0;temp[31]=0;
		mlog("warning : chat buffer overflow : %s\n",temp);
		strncpy(szOutput, szMsg, sizeof(szOutput)-2);
		szOutput[sizeof(szOutput)-1]=0;
		szOutput[sizeof(szOutput)-2]=0;
	}

	if (msgtype == CMT_SYSTEM)
	{
		MCOLOR color = MCOLOR(ZCOLOR_CHAT_SYSTEM_GAME);

		if (((loc == CL_CURRENT) && (state==GUNZ_GAME)) || (loc==CL_GAME))
		{
			color = MCOLOR(ZCOLOR_CHAT_SYSTEM_GAME);
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_LOBBY)) || (loc==CL_LOBBY))
		{
			color = MCOLOR(ZCOLOR_CHAT_SYSTEM_LOBBY);
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_STAGE)) || (loc==CL_STAGE))
		{
			color = MCOLOR(ZCOLOR_CHAT_SYSTEM_STAGE);
		}

		Output(color, szOutput, loc);
		return;
	}
	else if (msgtype == CMT_BROADCAST)
	{
		MCOLOR color = MCOLOR(ZCOLOR_CHAT_BROADCAST_GAME);

		if (((loc == CL_CURRENT) && (state==GUNZ_GAME)) || (loc==CL_GAME))
		{
			color = MCOLOR(ZCOLOR_CHAT_BROADCAST_GAME);
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_LOBBY)) || (loc==CL_LOBBY))
		{
			color = MCOLOR(ZCOLOR_CHAT_BROADCAST_LOBBY);
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_STAGE)) || (loc==CL_STAGE))
		{
			color = MCOLOR(ZCOLOR_CHAT_BROADCAST_STAGE);
		}

		Output(color, szOutput, loc);
		return;
	}
	else if ( msgtype == CMT_NORMAL)
	{
		if (((loc == CL_CURRENT) && (state==GUNZ_GAME)) || (loc==CL_GAME))
		{
			ZGetCombatInterface()->OutputChatMsg(szOutput);
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_LOBBY)) || (loc==CL_LOBBY))
		{
			if(_color.GetARGB() == ZCOLOR_CHAT_SYSTEM )// 기본색인 경우 로비의 기본색
				LobbyChatOutput(szOutput);
			else 
				LobbyChatOutput(szOutput,_color);	// 등록한색..
		}
		else if (((loc == CL_CURRENT) && (state==GUNZ_STAGE)) || (loc==CL_STAGE))
		{
			if(_color.GetARGB() == ZCOLOR_CHAT_SYSTEM )// 기본색인 경우 스테이지의 기본색
				StageChatOutput(szOutput);
			else 
				StageChatOutput(szOutput,_color);	// 등록한색..
		}

		m_ReportAbuse.OutputString(szMsg);
	}
}

void ZChat::Output(MCOLOR color, const char* szMsg, ZCHAT_LOC loc)
{
	m_ReportAbuse.OutputString(szMsg);

	GunzState state = ZApplication::GetGameInterface()->GetState();

	char szOutput[512];
	if (strlen(szMsg) < sizeof(szOutput)-2) strcpy(szOutput, szMsg);
	else {
		//_ASSERT(0);	// 채팅 버퍼가 너무 작음
		char temp[32];strncpy(temp,szMsg,30);temp[30]=0;temp[31]=0;
		mlog("warning : chat buffer overflow : %s\n",temp);
		strncpy(szOutput, szMsg, sizeof(szOutput)-2);
		szOutput[sizeof(szOutput)-1]=0;
		szOutput[sizeof(szOutput)-2]=0;
	}

	if (((loc == CL_CURRENT) && (state==GUNZ_GAME)) || (loc==CL_GAME))
	{
		ZCombatInterface* pCombat = ZGetCombatInterface();
		if (pCombat)
			pCombat->OutputChatMsg(color, szOutput);
	}
	else if (((loc == CL_CURRENT) && (state==GUNZ_LOBBY)) || (loc==CL_LOBBY))
	{
		LobbyChatOutput( szOutput , color );
	}
	else if (((loc == CL_CURRENT) && (state==GUNZ_STAGE)) || (loc==CL_STAGE))
	{
		StageChatOutput( szOutput , color );
	}
}

void ZChat::Clear(ZCHAT_LOC loc)
{
	GunzState state = ZApplication::GetGameInterface()->GetState();

	ZIDLResource* pResource = ZApplication::GetGameInterface()->GetIDLResource();

	if (((loc == CL_CURRENT) && (state==GUNZ_GAME)) || (loc==CL_GAME))
	{
		ZGetCombatInterface()->m_Chat.m_pChattingOutput->Clear();
	}
	else if (((loc == CL_CURRENT) && (state==GUNZ_LOBBY)) || (loc==CL_LOBBY))
	{
		MTextArea* pWidget = (MTextArea*)pResource->FindWidget("ChannelChattingOutput");
		if (pWidget != NULL) pWidget->Clear();
	}
	else if (((loc == CL_CURRENT) && (state==GUNZ_STAGE)) || (loc==CL_STAGE))
	{
		MTextArea* pWidget = (MTextArea*)pResource->FindWidget("StageChattingOutput");
		if (pWidget != NULL) pWidget->Clear();
	}
}

#define MAX_CHAT_LINES	1000

void ZChat::LobbyChatOutput(const char* szChat,MCOLOR color)
{
	ZIDLResource* pResource = ZApplication::GetGameInterface()->GetIDLResource();
	MTextArea* pWidget = (MTextArea*)pResource->FindWidget("ChannelChattingOutput");
	if(!pWidget) return;

	pWidget->AddText(szChat,color);
	while(pWidget->GetLineCount()>MAX_CHAT_LINES)
		pWidget->DeleteFirstLine();

}

void ZChat::StageChatOutput(const char* szChat,MCOLOR color)
{
	ZIDLResource* pResource = ZApplication::GetGameInterface()->GetIDLResource();
	MTextArea* pWidget = (MTextArea*)pResource->FindWidget("StageChattingOutput");
	if(!pWidget) return;

	pWidget->AddText(szChat,color);
	while(pWidget->GetLineCount()>MAX_CHAT_LINES)
		pWidget->DeleteFirstLine();

}

void ZChat::Report112(const char* szReason)
{
	m_ReportAbuse.Report(szReason);
}

bool ZChat::CheckChatFilter(const char* szMsg)
{
	if (m_nLastAbuseTime > 0) 
	{
		if ((timeGetTime() - m_nLastAbuseTime) < ZCHAT_CHAT_ABUSE_COOLTIME)
		{
			char szOutput[512];
			sprintf(szOutput, 
				ZErrStr(MERR_CHAT_PENALTY_FOR_ONE_MINUTE) );
			Output(szOutput, CMT_SYSTEM);
			return false;
		}
		else
		{
			m_nLastAbuseTime = 0;
		}
	}

	if (!MGetChattingFilter()->IsValidStr( szMsg, 1) && ZGetMyInfo()->GetUGradeID() != MMUG_HCODER)
	{
#ifndef _DEBUG
		m_nAbuseCounter++;

		if ( m_nAbuseCounter >= 3)			// 3번 연속 욕하면 1분간 채팅금지
			m_nLastAbuseTime = timeGetTime();
#endif

		char szOutput[512];
		sprintf( szOutput, "%s (%s)", ZErrStr( MERR_CANNOT_ABUSE), MGetChattingFilter()->GetLastFilteredStr());
		Output( szOutput, CMT_SYSTEM);

		return false;
	}
	else
	{
		m_nAbuseCounter = 0;
	}

	return true;
}

bool _InsertString(char* szTarget, const char* szInsert, int nPos, int nMaxTargetLen=-1)
{
	int nTargetLen = (int)strlen(szTarget);
	int nInsertLen = (int)strlen(szInsert);
	if(nPos>nTargetLen) return false;
	if(nMaxTargetLen>0 && nTargetLen+nInsertLen>=nMaxTargetLen) return false;

	char* temp = new char[nTargetLen-nPos+2];
	strcpy(temp, szTarget+nPos);
	strcpy(szTarget+nPos, szInsert);
	strcpy(szTarget+nPos+nInsertLen, temp);
	delete[] temp;

	return true;
}

// 채팅창에서 '/r ' 을 치면 이전에 귓말이 왔던 사람한테 바로 귓말 명령어로 변경
void ZChat::FilterWhisperKey(MWidget* pWidget)
{
	char text[256];
	strcpy(text, pWidget->GetText());

	if ((!stricmp(text, "/r ")) || (!stricmp(text, "/ㄱ ")))
	{
		char msg[128] = "";

		ZChatCmd* pWhisperCmd = GetCmdManager()->GetCommandByID(CCMD_ID_WHISPER);
		if (pWhisperCmd)
		{
			sprintf(msg, "/%s ", pWhisperCmd->GetName());
		}

		//strcpy(msg, "/귓말 ");

		if (m_szWhisperLastSender[0])
		{
			strcat(msg, m_szWhisperLastSender);
			strcat(msg, " ");
		}
		pWidget->SetText(msg);
	}


}
///////////////////////////////////////////////////////////////////////////
