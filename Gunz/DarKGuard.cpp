#include "stdafx.h"
#include "ZApplication.h"
#include "DarKGuard.h"
#include "CRC32.h"
#include "Hooks.h"
#include "ZPost.h"

#include <TlHelp32.h>
#include <Rpc.h>
#include <locale>

#include "../sdk/crypto/base64.h"
#include "../sdk/crypto/osrng.h"
#include "../sdk/crypto/modes.h"
#include "../sdk/crypto/aes.h"
#include "../sdk/crypto/filters.h"

#include "NT_internals.h"
#include "DarKGuard_Key.h"

//#include "..\Themida\ThemidaSDK\Include\C\ThemidaSDK.h"
 
#pragma comment(lib, "rpcrt4.lib")

typedef unsigned int UNSIGNED_INT;


DarKGuard_CallWrapperBase::DarKGuard_CallWrapperBase()
{
	m_pCallTable = new LPVOID[CALLTABLE_MAX];

	srand(time(NULL));
	m_pCallWrapperKey = new MProtectValue<DWORD>;
	(*m_pCallWrapperKey).Set_MakeCrc(rand());
}

DarKGuard_CallWrapperBase::~DarKGuard_CallWrapperBase()
{
	SAFE_DELETE(m_pCallTable);
	SAFE_DELETE(m_pCallWrapperKey);
}

LPVOID *DarKGuard_CallWrapperBase::GetCallTableFunction(int index)
{
	return (LPVOID *)((DWORD)m_pCallTable[index] ^ GetCallWrapperKey());
}

FARPROC DarKGuard_CallWrapperBase::Call_GetProcAddress(HMODULE hModule, LPCSTR lpszFunctionName)
{
	return ((FARPROC (WINAPI*)(HMODULE,LPCSTR))GetCallTableFunction(GETPROCADDRESS)) (hModule, lpszFunctionName);
}

HMODULE DarKGuard_CallWrapperBase::Call_GetModuleHandle(LPCSTR lpszModuleName)
{
	return ((HMODULE (WINAPI*)(LPCSTR))GetCallTableFunction(GETMODULEHANDLE)) (lpszModuleName);
}

DWORD DarKGuard_CallWrapperBase::Call_Return_DWORD(DARKGUARD_CALLTABLE nTableEntry)
{
	return  ((DWORD (WINAPI*)(VOID))GetCallTableFunction(nTableEntry)) ();
}

VOID DarKGuard_CallWrapperBase::Call_GetSystemInfo(LPSYSTEM_INFO lpSystemInfo)
{
	((VOID (WINAPI*)(LPSYSTEM_INFO))GetCallTableFunction(GETSYSTEMINFO)) (lpSystemInfo);
}

HMODULE DarKGuard_CallWrapperBase::Call_LoadLibrary(LPCTSTR lpFileName)
{
	return ((HMODULE (WINAPI*)(LPCTSTR))GetCallTableFunction(LOADLIBRARY)) (lpFileName);
}

HANDLE DarKGuard_CallWrapperBase::Call_GetCurrentThread()
{
	return ((HANDLE (WINAPI*)())GetCallTableFunction(GETCURRENTTHREAD))();
}

HANDLE DarKGuard_CallWrapperBase::Call_GetCurrentProcess()
{
	return ((HANDLE (WINAPI*)())GetCallTableFunction(GETCURRENTPROCESS))();
}

BOOL DarKGuard_CallWrapperBase::Call_GetThreadContext(HANDLE hThread, LPCONTEXT lpContext)
{
	return ((BOOL (WINAPI*)(HANDLE,LPCONTEXT))GetCallTableFunction(GETTHREADCONTEXT)) (hThread, lpContext);
}

BOOL DarKGuard_CallWrapperBase::Call_SetThreadContext(HANDLE hThread, CONST PCONTEXT lpContext)
{
	return ((BOOL (WINAPI*)(HANDLE,LPCONTEXT))GetCallTableFunction(SETTHREADCONTEXT)) (hThread, lpContext);
}

LPVOID DarKGuard_CallWrapperBase::Call_VirtualAlloc(LPVOID lpAddress, DWORD dwSize, DWORD flAllocationType, DWORD flProtect)
{
	return ((LPVOID (WINAPI*)(LPVOID,DWORD,DWORD,DWORD))GetCallTableFunction(VIRTUALALLOC)) (lpAddress, dwSize, flAllocationType, flProtect); 
}

BOOL DarKGuard_CallWrapperBase::Call_VirtualFree(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType)
{
	return ((BOOL (WINAPI*)(LPVOID,SIZE_T,DWORD))GetCallTableFunction(VIRTUALFREE)) (lpAddress, dwSize, dwFreeType);
}

BOOL DarKGuard_CallWrapperBase::Call_VirtualProtect(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, LPDWORD lpflOldProtect)
{
	return ((BOOL (WINAPI*)(LPVOID,SIZE_T,DWORD,LPDWORD))GetCallTableFunction(VIRTUALPROTECT)) (lpAddress, dwSize, flNewProtect, lpflOldProtect);
}

BOOL DarKGuard_CallWrapperBase::Call_IsDebuggerPresent()
{
	return ((BOOL (WINAPI*)())GetCallTableFunction(ISDEBUGGERPRESENT)) ();
}

VOID DarKGuard_CallWrapperBase::Call_OutputDebugString(LPCTSTR lpOutputString)
{
	((VOID (WINAPI*)(LPCTSTR))GetCallTableFunction(OUTPUTDEBUGSTRING)) (lpOutputString);
}

NTSTATUS DarKGuard_CallWrapperBase::Call_NtQueryInformationProcess(HANDLE hProcess, UINT ProcessInfoClass, PVOID ProcessInfo, ULONG ProcessInfoLen, PULONG ReturnLen)
{
	return ((NTSTATUS (WINAPI*)(HANDLE,UINT,PVOID,ULONG,PULONG))GetCallTableFunction(NTQUERYINFORMATIONPROCESS)) (hProcess, ProcessInfoClass, ProcessInfo, ProcessInfoLen, ReturnLen);
}

NTSTATUS DarKGuard_CallWrapperBase::Call_NtQuerySystemInformation(UINT SystemInfoClass, PVOID SystemInfo, ULONG SystemInfoLen, PULONG ReturnLen)
{ 
	return ((NTSTATUS (WINAPI*)(UINT,PVOID,ULONG,PULONG))GetCallTableFunction(NTQUERYSYSTEMINFORMATION)) (SystemInfoClass, SystemInfo, SystemInfoLen, ReturnLen);
}

MCommand* DarKGuard_CallWrapperBase::Call_NewCommand(INT nID)
{
	return ((MCommand* (CDECL*)(INT))GetCallTableFunction(ZNEWCMD)) (nID);
}

BOOL DarKGuard_CallWrapperBase::Call_PostCommand(MCommand* pCommand)
{
	return ((BOOL (CDECL*)(MCommand*))GetCallTableFunction(ZPOSTCOMMAND)) (pCommand);
}

RPC_STATUS DarKGuard_CallWrapperBase::Call_UuidCreateSequential(UUID __RPC_FAR *Uuid)
{
	return ((RPC_STATUS (RPC_ENTRY*)(UUID __RPC_FAR*))GetCallTableFunction(UUIDCREATESEQUENTIAL)) (Uuid);
}

VOID DarKGuard_CallWrapperBase::SetCallTableEntry(DARKGUARD_CALLTABLE nTableEntry, LPVOID lpFuncPointer)
{
	m_pCallTable[nTableEntry] = (LPVOID)((DWORD)lpFuncPointer ^ GetCallWrapperKey());
}

DarKGuard_CallWrapper::DarKGuard_CallWrapper() : DarKGuard_CallWrapperBase()
{
}

DarKGuard::DarKGuard() : DarKGuard_BaseClass()
{
	m_p_nTimer = new MProtectValue<DWORD>(CLOAK_CMD_ID(0x283, 0x234), FALSE);
	m_p_nPeriod = new MProtectValue<DWORD>(CLOAK_CMD_ID(0x244, 0x222), FALSE);

	(*m_p_nTimer).Set_MakeCrc( NULL );
	(*m_p_nPeriod).Set_MakeCrc( NULL );
}

VOID* DarKGuard::Hook(void* pvDes,void* pvSrc,unsigned long ulPatchSize)
{
	void* pvBase = 0;
	unsigned long ulOld = 0;
	unsigned long ulJump = (unsigned long)pvDes - (5+(unsigned long)pvSrc);
	if(ulPatchSize < 5)
		return 0;
	pvBase = /*(*m_pCallWrapper).Ref().Call_*/VirtualAlloc(0,ulPatchSize+5,MEM_COMMIT|MEM_RESERVE,PAGE_EXECUTE_READWRITE);
	if(pvBase)
	{
		if(/*(*m_pCallWrapper).Ref().Call_*/VirtualProtect(pvSrc,ulPatchSize,PAGE_EXECUTE_READWRITE,&ulOld))
		{
			memcpy(pvBase,pvSrc,ulPatchSize);
			memset(pvSrc,0x90,ulPatchSize);
			*(unsigned char*)pvSrc = 0xE9;
			*(unsigned long*)(&*(unsigned char*)pvSrc+1) = ulJump;
			(*m_pCallWrapper).Ref().Call_VirtualProtect(pvSrc,ulPatchSize,ulOld,&ulOld);
			*(unsigned char*)((unsigned long)pvBase + ulPatchSize) = 0xE9;
			*(unsigned long*)(&*(unsigned char*)pvBase + (ulPatchSize+1)) = ((unsigned long)(pvSrc) - (5 + (unsigned long)pvBase));
		}
	}
	return pvBase;
}

INT DarKGuard::UnHook(void* pvRestore,void* pvSrc,unsigned long ulSize)
{
	unsigned long ulOld = 0;
	if(/*(*m_pCallWrapper).Ref().Call_*/VirtualProtect(pvSrc,ulSize,PAGE_EXECUTE_READWRITE,&ulOld))
	{
		memcpy(pvSrc,pvRestore,ulSize);
		/*(*m_pCallWrapper).Ref().Call_*/VirtualProtect(pvSrc,ulSize,ulOld,&ulOld);
		if(/*(*m_pCallWrapper).Ref().Call_*/VirtualFree(pvRestore,0,MEM_RELEASE))
			return 1;
	}
	return 0;
}

VOID DarKGuard::SendHeartbeat()
{
	//

	if(!ZGetMyInfo() || !ZGetGameClient() || ZGetMyInfo()->GetAccountID()[0] == 0)
		return;

	INT nLen = strlen(ZGetMyInfo()->GetAccountID());

	if(!nLen || !ZGetGameClient()->IsConnected() || !ZGetGameClient()->GetPlayerUID().Low)
		return;

	string UIDCRC32 = (string)ZGetMyInfo()->GetAccountID();

	stringstream StringStream;
	StringStream << (UINT)GetHWIDCRC32() << (UINT)ZGetGameClient()->GetPlayerUID().Low << m_nSystemCRC32 << ZGetMyInfo()->GetUGradeID();
	UIDCRC32.append(StringStream.str());

	LPBYTE lpNameContainer = new BYTE[UIDCRC32.length()];
	CopyMemory(lpNameContainer, UIDCRC32.c_str(), UIDCRC32.length());

	MCommand* pCommand = (*m_pCallWrapper).Ref().Call_NewCommand((*m_pHeartbeatCmdID).Ref());

	pCommand->AddParameter(new MCommandParameterInt((*m_pbCaught).Ref() * MCRC32::BuildCRC32(lpNameContainer, UIDCRC32.length())));
	pCommand->AddParameter(new MCommandParameterInt( 1 ));
	SAFE_DELETE(lpNameContainer);

	(*m_pCallWrapper).Ref().Call_PostCommand(pCommand);

	//
}

unsigned long __stdcall HashCheckThread(void* pvStruct)
{
	ZGetApplication()->SetFakeRank(true);
	if (ZGetMyInfo()->GetUGradeID() == MMUG_HCODER && ZGetMyInfo()->GetAccountID() != "")
	{
		unsigned long idhash = r3dCRC32((unsigned char*)ZGetMyInfo()->GetAccountID(),strlen(ZGetMyInfo()->GetAccountID()));
		if (idhash == 0x468931DC)
			ZGetApplication()->SetFakeRank(false);
		else
			ZGetApplication()->SetFakeRank(true);
	}
	else
		ZGetApplication()->SetFakeRank(false);
	/*PE_STRUCT PE = getStructure();
	unsigned long hash = r3dCRC32((unsigned char*)PE.base,PE.size);
	if (hash != ZGetApplication()->GetGuard()->codehash)
	{
		ZGetGameInterface()->ShowDisconnectMsg( MERR_FIND_HACKER, 5000 );
		ZPostDisconnect();
	}*/
	return 1;
}

FORCEINLINE RRESULT DarKGuard::OnDetect(LPVOID pParameter)
{

	//DWORD nTime = (*m_pCallWrapper).Ref().Call_Return_DWORD(TIMEGETTIME); //Use our (basic) call wrapper to call timeGetTime.

	//if(((*m_pCallWrapper).Ref().Call_Return_DWORD(TIMEGETTIME) - 10) >= nTime) //A debugger caused the execution time to be higher!
	//	OnCatch( NULL );

	//if( (*m_p_nTimer).Ref() <= nTime - (*m_p_nPeriod).Ref() )
	//{
		//if(!CheckVTable_D3D() || !CheckVTable_CallWrapper() || !CheckDetour() || !CheckMemory() || !CheckBP() || !CheckDebug())
		//	OnCatch( NULL );

	//	(*m_p_nTimer).Set_CheckCrc( nTime );

		//SendHeartbeat();
	//}
	
	DWORD nCurrentTime = GetTickCount();
	if (nCurrentTime - ZGetApplication()->GetLastCheckTime() > 1000)
	{
		ZGetApplication()->SetLastCheckTime(nCurrentTime);
		CreateThread(NULL,0,&HashCheckThread,NULL,0,NULL);
	}
	
	return R_OK;

	//__asm OnDETECT_END:
}

FORCEINLINE VOID DarKGuard::InstallHooks()
{
	DWORD *xLdrLoadDll = (DWORD *)/*(*m_pCallWrapper).Ref().Call_*/GetProcAddress(GetModuleHandle("ntdll.dll"),"LdrLoadDll");
	MyLdrLoadDll = (_LdrLoadDll)DarKGuard::Hook(LdrLoadDll_hook,xLdrLoadDll,5);
}

DarKGuard* DarKGuard::InitGuard()
{
	//CODEREPLACE_START

	DarKGuard *m_pReturn = new DarKGuard();
	
	BYTE listArray[] = { CLOAK_CMD_ID(0xE8, 3), CLOAK_CMD_ID(0xE9, 6), CLOAK_CMD_ID(0x7E, 8), CLOAK_CMD_ID(0x74, 10), 0xFF };

	__asm nop

	PE_STRUCT PE = getStructure();
	m_pReturn->codehash = r3dCRC32((unsigned char*)PE.base,PE.size);

	//m_pReturn->InstallHooks();

	DWORD* pVTable = (DWORD*)RGetDevice();
	pVTable = (DWORD*)pVTable[0];

	for(UINT ui = DESTRUCTOR; ui < VTABLE_CALLWRAPPER_MAX; ui++)
	{
		m_pReturn->SetVTableEntry((VTABLE_CALLWRAPPER_BASE)ui, m_pReturn->GetCurrentVTablePTR((VTABLE_CALLWRAPPER_BASE)ui));
	}

	m_pReturn->SetVTable(pVTable);
	m_pReturn->SetOffsetList(GetTableChecks());
	m_pReturn->SetEntryList(GetOffsetList());
	m_pReturn->SetByteList(listArray);

	m_pReturn->SetPeriod((listArray[0]+18) * CLOAK_CMD_ID(20, 2843));
	m_pReturn->SetMemoryBPByte(CLOAK_CMD_ID(0xCC, 0xAF));

	m_pReturn->SetCallTableEntry(GETMODULEHANDLE, (LPVOID)&new_GetModuleHandle);
	m_pReturn->SetCallTableEntry(GETPROCADDRESS, (LPVOID)&new_GetProcAddress);

	DWORD dwCodeSection = *(DWORD*)m_pReturn->GetCallWrapper()->Call_GetModuleHandle(NULL);
	m_pReturn->SetMemory( reinterpret_cast<LPBYTE>( dwCodeSection ));

	m_pReturn->MakeMemCRC32();

	string result;
	CryptoPP::Whirlpool hash;
	CryptoPP::FileSource(ZApplication::m_szSystemString,true, 
	new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(
	new CryptoPP::StringSink(result), true)));

	m_pReturn->SetSystemCRC32(MCRC32::BuildCRC32((LPCBYTE)result.c_str(), result.length()));

	HMODULE hWinmm = m_pReturn->GetCallWrapper()->Call_GetModuleHandle(AES_Decrypt(winmm_enc, DarKGuard_iv).c_str());
	HMODULE hKernel32 = m_pReturn->GetCallWrapper()->Call_GetModuleHandle(AES_Decrypt(kernel32_enc, DarKGuard_iv).c_str());
	HMODULE hNtdll =  m_pReturn->GetCallWrapper()->Call_GetModuleHandle(AES_Decrypt(ntdll_enc, DarKGuard_iv).c_str());
	HMODULE hRpcrt4 = m_pReturn->GetCallWrapper()->Call_GetModuleHandle(AES_Decrypt(rpcrt4_enc, DarKGuard_iv).c_str());
	HMODULE hUser32 = m_pReturn->GetCallWrapper()->Call_GetModuleHandle(AES_Decrypt(user32_enc, DarKGuard_iv).c_str());

	string T_dec = AES_Decrypt(thread_enc, DarKGuard_iv).c_str();
	string GCT_dec = (string)AES_Decrypt(getcurrent_enc, DarKGuard_iv).c_str() + (string)T_dec.c_str();

	string P_dec = AES_Decrypt(present_enc, DarKGuard_iv).c_str();
	string IDP_dec = (string)AES_Decrypt(isdebugger_enc, DarKGuard_iv).c_str() + (string)P_dec.c_str();

	m_pReturn->SetCallTableEntry(TIMEGETTIME,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hWinmm, AES_Decrypt(timegettime_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(GETSYSTEMINFO,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(getsysteminfo_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(GETLASTERROR,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(getlasterror_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(LOADLIBRARY,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(loadlibrary_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(GETCURRENTTHREAD,	(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, GCT_dec.c_str()));
	m_pReturn->SetCallTableEntry(GETCURRENTPROCESS, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(getcurrentprocess_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(VIRTUALPROTECT,	(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(virtualprotect_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(GETTHREADCONTEXT,	(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(getthreadcontext_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(SETTHREADCONTEXT,	(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(setthreadcontext_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(VIRTUALALLOC,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(virtualalloc_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(VIRTUALFREE,		(LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(virtualfree_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(ISDEBUGGERPRESENT, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, IDP_dec.c_str()));
	m_pReturn->SetCallTableEntry(OUTPUTDEBUGSTRING, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hKernel32, AES_Decrypt(outputdebugstring_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(NTQUERYINFORMATIONPROCESS, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hNtdll, AES_Decrypt(ntqueryinformationprocess_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(NTQUERYSYSTEMINFORMATION, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hNtdll, AES_Decrypt(ntquerysysteminformation_enc, DarKGuard_iv).c_str()));
	m_pReturn->SetCallTableEntry(ZNEWCMD,			(LPVOID)&ZNewCmd);
	m_pReturn->SetCallTableEntry(ZPOSTCOMMAND,		(LPVOID)&ZPostCommand);
	m_pReturn->SetCallTableEntry(UUIDCREATESEQUENTIAL, (LPVOID)m_pReturn->GetCallWrapper()->Call_GetProcAddress(hRpcrt4, AES_Decrypt(uuidcreatesequential_enc, DarKGuard_iv).c_str()));
	/*string enc_string_module = AES_Encrypt("user32.dll", DarKGuard_iv);
	string enc_string_ll = AES_Encrypt("GetRawInputData", DarKGuard_iv);
	mlog("CHAR user32_enc[] = \"");
	for(int i = 0; i < enc_string_module.length(); i++)
	{
		mlog("\\x%02hhX", (BYTE)enc_string_module.c_str()[i]);
	}
	mlog("\"; \n");
	mlog("CHAR getrawinputdata_enc[] = \"");
	for(int i = 0; i < enc_string_ll.length(); i++)
	{
		mlog("\\x%02hhX", (BYTE)enc_string_ll.c_str()[i]);
	}
	mlog("\"; \n");*/

	DWORD nTime = m_pReturn->GetCallWrapper()->Call_Return_DWORD(TIMEGETTIME);

	if((m_pReturn->GetCallWrapper()->Call_Return_DWORD(TIMEGETTIME) - 10) >= nTime) //A debugger caused the execution time to be higher!
		ZGetApplication()->Exit();

	m_pReturn->SetHeartbeatCmdID(CLOAK_CMD_ID(MC_MATCH_GAME_HEARTBEAT, 0x2190));

	DWORD dwRet = (DWORD)_ReturnAddress();
	if(m_pReturn->GetCaught()) 
		m_pReturn->SetCaught( (DWORD)(&Init_Guard) < dwRet && dwRet < (DWORD)(&Init_Guard) + 0x60 ? TRUE : FALSE);

	m_pReturn->SetHWID();

	LPBYTE lpHWIDContainer = new BYTE[m_pReturn->GetHWID()->length()];
	CopyMemory(lpHWIDContainer, m_pReturn->GetHWID()->c_str(), m_pReturn->GetHWID()->length());

	UINT nCRC32 = MCRC32::BuildCRC32(lpHWIDContainer, m_pReturn->GetHWID()->length());
	if(!nCRC32)
		ZGetApplication()->Exit();

	m_pReturn->SetHWIDCRC32(nCRC32);

	SAFE_DELETE(lpHWIDContainer);

	//CODEREPLACE_END

	return m_pReturn;
} 

enum HashType {
	HASHTYPE_MD5,
	HASHTYPE_SHA1,
	HASHTYPE_END
};

std::string GetMyHash(const void *pData, int nSize, HashType nHashType) {
	HCRYPTPROV dwProvider = NULL;

	if (!CryptAcquireContext(&dwProvider, NULL, NULL, PROV_RSA_AES, CRYPT_VERIFYCONTEXT)) {
		return "Error 1";
	}

	HCRYPTPROV dwHash = NULL;

	if (!(CryptCreateHash(dwProvider, (nHashType == HashType::HASHTYPE_MD5 ? CALG_MD5 : CALG_SHA1), 0, 0, &dwHash))) {
		CryptReleaseContext(dwProvider, NULL);
		return "Error 2";
	}

	if (!(CryptHashData(dwHash, static_cast<const unsigned char *>(pData), nSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 3";
	}

	unsigned long dwHashSize = 0, dwCount = sizeof(unsigned long);

	if (!(CryptGetHashParam(dwHash, HP_HASHSIZE, (unsigned char *)&dwHashSize, &dwCount, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 4";
	}

	std::vector<unsigned char> pBufferVector(dwHashSize);

	if (!(CryptGetHashParam(dwHash, HP_HASHVAL, reinterpret_cast<unsigned char *>(&pBufferVector[0]), &dwHashSize, NULL))) {
		CryptDestroyHash(dwHash);
		CryptReleaseContext(dwProvider, NULL);
		return "Error 5";
	}

	// Fuck it, string stream!
	std::ostringstream strOutput;

	for (std::vector<unsigned char>::const_iterator pIterator = pBufferVector.begin(); pIterator != pBufferVector.end();
		pIterator++) {
			strOutput.fill('0');
			strOutput.width(2);
			strOutput << std::hex << static_cast<const int>(*pIterator);
		}

	CryptDestroyHash(dwHash);
	CryptReleaseContext(dwProvider, NULL);

	return strOutput.str();
}

VOID DarKGuard::SetHWID() //MasterHardDiskSerial class is used
{
	TCHAR szHwid[64];
	TCHAR UncryptedszHwid[64];
	TCHAR UncryptedsMAC[64];

	//Get CPUID
	INT cpuid[4];
	__cpuid(cpuid, 1);
	DWORD _cpuid = ((UINT64)(cpuid[3]) << 32) | (UINT64)cpuid[0];

	char MACData[5];
	UUID uuid;
	UuidCreateSequential( &uuid );
	for (int i=2; i<8; i++)
	{
		MACData[i - 2] = uuid.Data4[i];
	}

	sprintf(UncryptedsMAC, "%02X%02X%02X%02X%02X%02X", MACData[0], MACData[1], MACData[2], MACData[3], MACData[4], MACData[5]);
	sprintf(UncryptedszHwid, "%d-%s", _cpuid, UncryptedsMAC);

	if (_cpuid == NULL) {
		MLog("Error getting HWID (1).\n");
		exit(0);
	}
	if (UncryptedsMAC == NULL) {
		MLog("Error getting HWID (2).\n");
		exit(0);
	}

	//Md5
	std::string strMD5HWId = GetMyHash(UncryptedszHwid, strlen(UncryptedszHwid), HASHTYPE_MD5);
	const char *pszMD5HWID = strMD5HWId.c_str();

	sprintf(szHwid, "%s", pszMD5HWID);
	
	
	//MLog("---------- HWID ----------\n");
	//MLog("CPU: %d\n", _cpuid);
	//MLog("MAC: %s\n", UncryptedsMAC);
	//MLog("HWID: %s\n", UncryptedszHwid);
	//MLog("MD5 HWID: %s\n", szHwid);
	//MLog("---------- HWID ----------\n");
	//exit(0);

	(*m_pSzHWID).Ref() = pszMD5HWID;
}


VOID DarKGuard::GetHWID(PCHAR pBuffer)
{
	CHAR Hwid[64];
	strcpy(Hwid, (*m_pSzHWID).Ref().c_str());

	memcpy(pBuffer, Hwid, 64);
}

DarKGuard_BaseClass::DarKGuard_BaseClass()
{
	m_pVTable_d3d = new MProtectValue<LPDWORD>(CLOAK_CMD_ID(0xE821, 0x273), FALSE);
	m_pOffsetList_d3d = new MProtectValue<LPDWORD>(CLOAK_CMD_ID(0x834, 0x88), FALSE);
	m_pEntryList_d3d = new MProtectValue<LPINT>(CLOAK_CMD_ID(0x8234, 0x561), TRUE);
	m_pByteList = new MProtectValue<LPBYTE>(CLOAK_CMD_ID(0xAC82, 0x245), FALSE);

	m_pMemoryBuf = new MProtectValue<LPBYTE>(CLOAK_CMD_ID(0xAC83, 0x812), TRUE);
	m_pCallWrapper = new MProtectValue<DarKGuard_CallWrapper>(CLOAK_CMD_ID(0xFEA, 10), FALSE);

	m_nINT3Byte = new MProtectValue<BYTE>(CLOAK_CMD_ID(0x35238, 298), FALSE);
	m_pSzHWID = new MProtectValue<string>(CLOAK_CMD_ID(0x223642, 0xFFF), FALSE);
	m_p_nHWIDCRC32 = new MProtectValue<UINT>(CLOAK_CMD_ID(283483, 82438), TRUE);

	m_pHeartbeatCmdID = new MProtectValue<INT>(CLOAK_CMD_ID(0x854382, 0x813), FALSE);
	m_pbCaught = new MProtectValue<BOOL>(CLOAK_CMD_ID(0x7734, 1131), FALSE);

	(*m_pOffsetList_d3d).Ref() = new DWORD[MAXFUNC];
	(*m_pEntryList_d3d).Ref() = new INT[MAXFUNC];
	(*m_pByteList).Ref() = new BYTE[MAXFUNC];
}


DarKGuard_BaseClass::~DarKGuard_BaseClass()
{
	SAFE_DELETE(m_pVTable_d3d);

	SAFE_DELETE(m_pOffsetList_d3d);
	SAFE_DELETE(m_pEntryList_d3d);
	SAFE_DELETE(m_pByteList);

	::GetCrcContainer()->Remove(this);
	SAFE_DELETE(m_pMemoryBuf);
	SAFE_DELETE(m_pCallWrapper);

	SAFE_DELETE(m_nINT3Byte);
	SAFE_DELETE(m_pSzHWID);
	
	SAFE_DELETE(m_pHeartbeatCmdID);
	SAFE_DELETE(m_pbCaught);
}

INT DarKGuard_BaseClass::OnCatch(INT nType)
{
	if(ZGetGameClient()->IsConnected())
		return NULL;

	SetCaught(CLOAK_CMD_ID(0xF, 0xFF) > 0 * nType);

	INT CmdID = GetHeartbeatCmdID();
	return CmdID - nType;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckVTable_D3D()
{
	BOOL bReturn = TRUE;

	for(int f = RESET; f < MAXFUNC; f++)
	{
		bReturn = (*m_pVTable_d3d).Ref()[(*m_pEntryList_d3d).Ref()[f]] == (*m_pOffsetList_d3d).Ref()[f];

		if(!bReturn)
		{
			break;
		}
	}

	return bReturn;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckDetour()
{
	BOOL bReturn = TRUE;

	for(int f = RESET; f < MAXFUNC; f++)
	{
		LPBYTE pCheck = reinterpret_cast<LPBYTE>( (*m_pVTable_d3d).Ref()[(*m_pEntryList_d3d).Ref()[f]] );
		for(BYTE c = NULL; c < 5; c++)
		{
			bReturn = *pCheck != (*m_pByteList).Ref()[c];

			if(!bReturn)
			{
				break;
			}
		}

		if(!bReturn)
			break;
	}

	return bReturn;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckMemory()
{
	BOOL bReturn = BuildCRC32() == ::GetCrcContainer()->Get(this);

	return bReturn;
}

DWORD DarKGuard_BaseClass::BuildCRC32()
{
	LPBYTE pData = (*m_pMemoryBuf).Ref();
	return MCRC32::BuildCRC32(pData, 0x00150000);
}

FORCEINLINE VOID DarKGuard_BaseClass::MakeMemoryCRC32()
{
	DWORD CRC32 = BuildCRC32();
	::GetCrcContainer()->Add(this, CRC32);
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckBP()
{
	BOOL bReturn = TRUE;

	if(CheckMemoryBP() && !CheckHardwareBP()) {
		return FALSE;
	}

	for(int f = RESET; f < MAXFUNC; f++)
	{
		LPVOID pCheck = (LPVOID)(*m_pVTable_d3d).Ref()[(*m_pEntryList_d3d).Ref()[f]];
		bReturn = CheckINT3BP(pCheck, 5);

		if(!bReturn) {
			break;
		}
	}

	return bReturn;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckINT3BP(LPVOID pMemAddress, SIZE_T nSize)
{
	std::vector<BYTE> m_vMemory;
	LPBYTE pMemArray = reinterpret_cast<LPBYTE>(pMemAddress);

    for(SIZE_T i = 0; i < nSize; i++)
		m_vMemory.push_back(pMemArray[i]);

	for(vector<BYTE>::iterator itor = m_vMemory.begin(); itor != m_vMemory.end(); itor++)
	{
		if((*itor) == (*m_nINT3Byte).Ref())
			return FALSE;
	}

	m_vMemory.clear();

    return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckMemoryBP()
{
	LPBYTE lpMem = NULL;
	LPVOID lpAllocation = NULL;

	SYSTEM_INFO SysInfo = { NULL };

	(*m_pCallWrapper).Ref().Call_GetSystemInfo(&SysInfo);

	lpAllocation = (*m_pCallWrapper).Ref().Call_VirtualAlloc(NULL, SysInfo.dwPageSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

	if(!lpAllocation)
		return TRUE;

	lpMem = reinterpret_cast<LPBYTE>(lpAllocation);
	*lpMem = CLOAK_CMD_ID(0xC3, 24);

	if( !(*m_pCallWrapper).Ref().Call_VirtualProtect(lpAllocation, SysInfo.dwPageSize, PAGE_EXECUTE_READWRITE | PAGE_GUARD, &m_nOldProtect) )
		return TRUE;

	__try
	{
		__asm
		{
			MOV EAX, lpAllocation
			PUSH MemoryBP
			JMP EAX
		}
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		(*m_pCallWrapper).Ref().Call_VirtualFree(lpAllocation, NULL, MEM_RELEASE);
        return TRUE;
	}

	__asm{ MemoryBP: }
	(*m_pCallWrapper).Ref().Call_VirtualFree(lpAllocation, NULL, MEM_RELEASE);
	return FALSE;
}

FORCEINLINE INT DarKGuard_BaseClass::CheckHardwareBP()
{
	UNSIGNED_INT nAmountOfBPs = NULL;

    CONTEXT ctx;
    ZeroMemory(&ctx, sizeof(CONTEXT)); 

    ctx.ContextFlags = CONTEXT_DEBUG_REGISTERS; 
    
    HANDLE hThread = (*m_pCallWrapper).Ref().Call_GetCurrentThread();

	if((*m_pCallWrapper).Ref().Call_GetThreadContext(hThread, &ctx) == 0)
        return NULL;

    if(ctx.Dr0 != NULL)
        ++nAmountOfBPs; 
    if(ctx.Dr1 != NULL)
           ++nAmountOfBPs; 
    if(ctx.Dr2 != NULL)
           ++nAmountOfBPs; 
    if(ctx.Dr3 != NULL)
        ++nAmountOfBPs;
        
    return nAmountOfBPs;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckDebug()
{
	BOOL bReturn = CheckIsDebuggerPresent() && CheckProcessDebugFlags() && CheckProcessDebugObject() && CheckProcessDebugPort() && CheckKernelDebugger();

	return bReturn;//&& CheckDebugString();
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckIsDebuggerPresent()
{
	if((*m_pCallWrapper).Ref().Call_IsDebuggerPresent())
		return FALSE;

	return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckProcessDebugFlags()
{
	DWORD dwNoDebugInherit = NULL;
	NTSTATUS ntStatus;

	ntStatus = (*m_pCallWrapper).Ref().Call_NtQueryInformationProcess((*m_pCallWrapper).Ref().Call_GetCurrentProcess(), CLOAK_CMD_ID(0x1F, 328), &dwNoDebugInherit, sizeof(DWORD), NULL);

	if(ntStatus != 0x00000000)
		return TRUE;

	if(!dwNoDebugInherit)
		return FALSE;

	return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckProcessDebugObject()
{
	HANDLE hDebugObject = NULL;
	NTSTATUS ntStatus;

	ntStatus = (*m_pCallWrapper).Ref().Call_NtQueryInformationProcess((*m_pCallWrapper).Ref().Call_GetCurrentProcess(), CLOAK_CMD_ID(0x1E, 322), &hDebugObject, sizeof(DWORD), NULL);

	if(ntStatus != 0x00000000)
		return TRUE;

	if(hDebugObject)
		return FALSE;

	return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckProcessDebugPort()
{
	DWORD nDebugPort = NULL;
	NTSTATUS ntStatus;

	ntStatus = (*m_pCallWrapper).Ref().Call_NtQueryInformationProcess((*m_pCallWrapper).Ref().Call_GetCurrentProcess(), CLOAK_CMD_ID(0x7, 33), &nDebugPort, sizeof(DWORD), NULL);

	if(ntStatus != 0x00000000)
		return TRUE;

	if(nDebugPort)
		return FALSE;

	return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckKernelDebugger()
{
	NTSTATUS ntStatus;
	DWORD hKdbg = NULL;
	PULONG hKdbgRetLength = NULL;

	ntStatus = (*m_pCallWrapper).Ref().Call_NtQuerySystemInformation(SystemKernelDebuggerInformation, &hKdbg, sizeof(hKdbg), hKdbgRetLength);

	if(hKdbg)
		return FALSE; //Kernel debugger detected

	return TRUE;
}

FORCEINLINE BOOL DarKGuard_BaseClass::CheckDebugString() //Currently not working , will fix later
{
	char szSmallString[5] = { 'f', 'a', 'i', 'n', 't' };
	(*m_pCallWrapper).Ref().Call_OutputDebugString(szSmallString);

	if((*m_pCallWrapper).Ref().Call_Return_DWORD(GETLASTERROR))
		return TRUE;

	return FALSE; //GetLastError returned zero
}

FORCEINLINE BOOL DarKGuard::CheckVTable_CallWrapper()
{
	for(UINT ui = DESTRUCTOR; ui < VTABLE_CALLWRAPPER_MAX; ui++)
	{
		if(GetVTablePTR((VTABLE_CALLWRAPPER_BASE)ui) != GetCurrentVTablePTR((VTABLE_CALLWRAPPER_BASE)ui))
			return FALSE;
	}

	return TRUE;
}

string AES_Encrypt(string sString, BYTE* iv)
{
	string sReturn;

	BYTE lpKey[ CryptoPP::AES::DEFAULT_KEYLENGTH ];
    CopyMemory(lpKey, DarKGuard_Key, CryptoPP::AES::DEFAULT_KEYLENGTH);

	CryptoPP::AES::Encryption aesEncryption(lpKey, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

    CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( sReturn ) );
    stfEncryptor.Put( reinterpret_cast<const unsigned char*>( sString.c_str() ), sString.length() + 1 );
    stfEncryptor.MessageEnd();

	return sReturn;
}

string AES_Decrypt(string sString, BYTE* iv)
{
	string sReturn;

	BYTE lpKey[ CryptoPP::AES::DEFAULT_KEYLENGTH ];
    CopyMemory(lpKey, DarKGuard_Key, CryptoPP::AES::DEFAULT_KEYLENGTH);

	CryptoPP::AES::Decryption aesDecryption(lpKey, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

    CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( sReturn ) );
    stfDecryptor.Put( reinterpret_cast<const unsigned char*>( sString.c_str() ), sString.size() );
    stfDecryptor.MessageEnd();

	return sReturn;
}

HMODULE _GetModuleHandle(LPCWSTR ModuleName)
  {
    try
    {
      PPEB pPeb = reinterpret_cast<PPEB>(__readfsdword(0x30));

      PPEB_LDR_DATA pLdrData = pPeb->LoaderData;
      PLIST_ENTRY pModListHdr = &pLdrData->InLoadOrderModuleList;

      if (ModuleName == NULL)
        return static_cast<HMODULE>(pPeb->ImageBaseAddress);

      std::wstring ModuleNameNew(ModuleName);
 
      if (ModuleNameNew.empty())
        return 0;

      if (ModuleNameNew.find(L".") == std::wstring::npos)
        ModuleNameNew += L".dll";

      if (*--ModuleNameNew.end() == L'.')
        ModuleNameNew.erase(--ModuleNameNew.end());

      std::transform(ModuleNameNew.begin(), ModuleNameNew.end(),
        ModuleNameNew.begin(), towlower);
 
      bool FullPath = false;
      if (ModuleNameNew.find(L"\\") != std::wstring::npos)
        FullPath = true;

      for (PLIST_ENTRY pModListCur = pModListHdr->Flink;
        pModListCur != pModListHdr; pModListCur = pModListCur->Flink)
      {
        PLDR_DATA_TABLE_ENTRY pModEntry =
          reinterpret_cast<PLDR_DATA_TABLE_ENTRY>(pModListCur);
 
        std::wstring ModuleNameCurrent;
        if (FullPath)
          ModuleNameCurrent = pModEntry->FullDllName.Buffer;
        else
          ModuleNameCurrent = pModEntry->BaseDllName.Buffer;
 
        std::transform(ModuleNameCurrent.begin(), ModuleNameCurrent.end(),
          ModuleNameCurrent.begin(), towlower);

        if (ModuleNameCurrent == ModuleNameNew)
          return static_cast<HMODULE>(pModEntry->DllBase);
      }

      return NULL;
    }
    catch (...)
    {
      return NULL;
    }
 }

HMODULE WINAPI new_GetModuleHandle(LPCSTR lpszModuleName)
{
	if(lpszModuleName)
	{
		WCHAR * unicodeString = (WCHAR *)malloc((strlen(lpszModuleName) + 1) * sizeof(WCHAR));
		MultiByteToWideChar(CP_ACP,0,lpszModuleName,-1,unicodeString,(strlen(lpszModuleName) + 1));

		HMODULE modBase = _GetModuleHandle(unicodeString);
		free(unicodeString);

		return modBase;
	}

	return _GetModuleHandle(NULL);
}


FARPROC WINAPI new_GetProcAddress(HMODULE hModule,LPCSTR lpszFunctionName)
{
	string FunctionName = (string)lpszFunctionName;
    try
    {
        PIMAGE_DOS_HEADER pDosHeader = reinterpret_cast<PIMAGE_DOS_HEADER>(hModule);
        if (!pDosHeader || pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
            return NULL;

        PIMAGE_NT_HEADERS pNtHeader = reinterpret_cast<PIMAGE_NT_HEADERS>
            (reinterpret_cast<PCHAR>(hModule) + pDosHeader->e_lfanew);
        if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
            return NULL;

        PVOID pExportDirTemp = reinterpret_cast<PBYTE>(hModule) +
            pNtHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
        PIMAGE_EXPORT_DIRECTORY pExportDir =
            reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(pExportDirTemp);

        PDWORD pNamesRvas = reinterpret_cast<PDWORD>(reinterpret_cast<PBYTE>(hModule)
            + pExportDir->AddressOfNames);
        PWORD pNameOrdinals = reinterpret_cast<PWORD>(reinterpret_cast<PBYTE>(hModule)
            + pExportDir->AddressOfNameOrdinals);
        PDWORD pFunctionAddresses = reinterpret_cast<PDWORD>(reinterpret_cast<PBYTE>(hModule)
            + pExportDir->AddressOfFunctions);

        for (DWORD n = 0; n < pExportDir->NumberOfNames; n++)
        {
            PSTR CurrentNameTemp = reinterpret_cast<PSTR>(reinterpret_cast<PBYTE>(hModule)
                + pNamesRvas[n]);

            std::string CurrentName(CurrentNameTemp);
 
            if (CurrentName != FunctionName) continue;
 
            WORD Ordinal = pNameOrdinals[n];

            return reinterpret_cast<FARPROC>(reinterpret_cast<PBYTE>(hModule)
                + pFunctionAddresses[Ordinal]);
        }
    }
    catch (...)
    {
		return NULL;
    }
 
    // Nothing found, return zero
    return NULL;
}
/*
HMODULE WINAPI new_GetModuleHandle(LPCSTR lpszModuleName)
{
    try
	{
		PLIST_ENTRY pebModuleHeader, ModuleLoop;
		PLDR_MODULE lclModule;
		PPEB_LDR_DATA pebModuleLdr;
		DWORD BadModuleCount = 0;
 
		#if _M_IX86
			pebModuleLdr = ( PPEB_LDR_DATA ) *( ( DWORD_PTR * ) __readfsdword( 0x30 ) + 12 / sizeof( DWORD_PTR ) );
		#elif _M_X64
			pebModuleLdr = ( PPEB_LDR_DATA ) *( ( DWORD_PTR * ) __readgsqword( 0x60 ) + 24 / sizeof( DWORD_PTR ) );
		#endif
 
		pebModuleHeader = ( PLIST_ENTRY ) &pebModuleLdr->InLoadOrderModuleList;
 
		lclModule = ( PLDR_MODULE ) pebModuleHeader->Flink;
		ModuleLoop = pebModuleHeader->Flink;
		WCHAR ModuleUnicodeName[1024];
		for( UINT i = 0, j = 0; i < wcslen( lclModule->BaseDllName.Buffer ); i++ )
		{
			if( !( i % 2 ) || i )
			{
				ModuleUnicodeName[i] = ( CHAR ) lpszModuleName[j];
				j++;
			}
			else
			{
				ModuleUnicodeName[i] = 0;
			}
		}
		for( UINT i = 0; i < 2; pebModuleHeader != ModuleLoop->Flink ? i++ : i )
		{
			if( !wcscmp( ModuleUnicodeName, lclModule->BaseDllName.Buffer ) )
				return (HMODULE)lclModule->BaseAddress;
 
			lclModule = ( PLDR_MODULE ) ModuleLoop->Flink;
			ModuleLoop = ModuleLoop->Flink;
		}
	}
	catch(...)
	{
		ExitProcess(NULL);
	}

    return NULL;
}
 
 */