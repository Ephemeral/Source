#include "DarKGuard.h"
#include "NT_internals.h"

typedef NTSTATUS (NTAPI* _LdrLoadDll)(PWCHAR PathToFile,ULONG Flags,PUNICODE_STRING ModuleFileName,PHANDLE ModuleHandle);
_LdrLoadDll MyLdrLoadDll = NULL;

NTSTATUS NTAPI LdrLoadDll_hook(PWCHAR PathToFile,ULONG Flags,PUNICODE_STRING ModuleFileName,PHANDLE ModuleHandle)
{
	FILE *f = fopen("C:\\injectlog.txt","a");
	fwprintf(f,L"New DLL: %s\n",ModuleFileName->Buffer);
	fclose(f);
	return MyLdrLoadDll(PathToFile,Flags,ModuleFileName,ModuleHandle);
}