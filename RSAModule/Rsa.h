#ifndef _RSA_H_
#define _RSA_H_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <wincrypt.h>
#include <stdlib.h>
#include <stdio.h>
#define MAX_BUFFER_SIZE_FOR_ENCRYPTION 256
#define SHA1_HASH_LENGTH 20
#ifdef __cplusplus
extern "C" {
#endif

	BOOLEAN GetCryptoContext(_Out_ HCRYPTPROV* CryptContext);

	BOOLEAN RSAGenerateKeyPair(_In_ HCRYPTPROV CryptContext,_Out_ HCRYPTKEY* KeyContext);

	BOOLEAN RSAExportPrivateKeyToBlob(_In_ HCRYPTKEY KeyContext,_Out_ PBYTE* PrivateKeyBlob,_Out_ PULONG PrivateKeyBlobSize);

	BOOLEAN RSAExportPublicKeyToBlob(_In_ HCRYPTKEY KeyContext,_Out_ PBYTE* PublicKeyBlob,_Out_ PULONG PublicKeyBlobSize);

	VOID RSADestroyExportedBlob(_In_ PBYTE Blob);
	
	BOOLEAN RSACreateKeyHandleFromBlob(_In_ HCRYPTPROV CryptoContext,_In_ PBYTE Blob,_In_ ULONG BlobSize,_Out_ HCRYPTKEY* KeyContext);
	
	VOID RSADestroyKeyHandle(_In_ HCRYPTKEY KeyContext);

	VOID DestroyCryptoContext(_In_ HCRYPTPROV CryptoContext);

	VOID RSADestroyAllocatedBuffer(_In_ PBYTE Buffer);

	BOOLEAN RSAEncryptUsingKeyHandle(_In_ HCRYPTKEY KeyContext,_In_ PBYTE BufferToBeEncrypted, _In_ ULONG BufferSize /*,_Out_ PBYTE* EncryptedBuffer*/ ,_Out_ PULONG EncryptedBufferSize);

	/*
	If the blob is stored in a buffer, call this function to perform encryption using the blob skipping unnecessary steps
	*/
	BOOLEAN RSAEncryptUsingPublicKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
	(_In_ PBYTE PublicKeyBlob,_In_ ULONG PublicKeyBlobSize,_In_ PBYTE BufferToBeEncrypted,_In_ ULONG BufferSize /*,_Out_ PBYTE* EncryptedBuffer*/ ,_Out_ PULONG EncryptedBufferSize);

	BOOLEAN RSADecryptUsingKeyHandle(_In_ HCRYPTKEY KeyContext,_Inout_ PBYTE BufferToBeDecrypted,_Out_ PULONG DecryptedBufferSize);
	/*
	If the blob is stored in a buffer, call this function to perform decryption using the blob skipping unnecessary steps
	*/
	BOOLEAN RSADecryptUsingPrivateKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
	(_In_ PBYTE PrivateKeyBlob,_In_ ULONG PrivateKeyBlobSize,_Inout_ PBYTE BufferToBeDecrypted,_Out_ PULONG DecryptedBufferSize);

	BOOLEAN WriteShellToFile(PCHAR HeaderFileName,PCHAR SourceFileName,PCHAR ShellName,PCHAR PageGuardEpilogue,PBYTE Shellcode,ULONG ShellSize);

	/*
		Buffer: Buffer to be hashed
		BufferSize: Size of the buffer to be hashed
		Hash: Buffer which will contain the SHA1 hash upon completion (Needs to be at least SHA1_HASH_LENGTH bytes)
		HashBufferSize: OPTIONAL - In-Size of the hash buffer, Out size of hash (SHA1_HASH_LENGTH)
	*/
	BOOLEAN CalculateSHA1(_In_ PBYTE Buffer,_In_ ULONG BufferSize,_Out_ PBYTE Hash,__inout_opt PULONG HashBufferSize);

	VOID FORCEINLINE Dump(PBYTE Buffer,ULONG Size)
	{
		PBYTE Pointer = Buffer;
		for( ; Pointer < Buffer+Size; ++Pointer )
			printf("%02X,",*(BYTE*)(Pointer));
		printf("\n");
	}

#ifdef __cplusplus
}
#endif


#endif