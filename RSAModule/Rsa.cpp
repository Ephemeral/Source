

#include "Rsa.h"
#pragma comment(lib,"Advapi32.lib")

#define MAX_BUFFER_SIZE_FOR_ENCRYPTION 256

BOOLEAN GetCryptoContext(_Out_ HCRYPTPROV* CryptContext)
{
	BOOLEAN Status = TRUE;
	
	if(!CryptContext)
		return FALSE;

	if(!CryptAcquireContextA(CryptContext,NULL,MS_ENHANCED_PROV_A,PROV_RSA_FULL,CRYPT_VERIFYCONTEXT))
	{
		Status = FALSE;
		if(GetLastError() == NTE_BAD_KEYSET)
		{
			if(CryptAcquireContextA(
				CryptContext, 
				NULL,
				MS_ENHANCED_PROV_A, 
				PROV_RSA_FULL, 
				CRYPT_NEWKEYSET))
			{
				Status = TRUE;
			}
		}
	}
	return Status;
}

BOOLEAN RSAGenerateKeyPair(_In_ HCRYPTPROV CryptContext,_Out_ HCRYPTKEY* KeyContext)
{
	if(CryptGenKey(CryptContext,CALG_RSA_KEYX, 0x08000000 | CRYPT_EXPORTABLE,KeyContext))
		return TRUE;

	*KeyContext = 0;
	return FALSE;
	
}

BOOLEAN RSAExportKey(_In_ LONG ExportType,_In_ HCRYPTKEY KeyContext,PBYTE* Blob,PULONG BlobSize)
{
	BOOLEAN Status = FALSE;

	if(!Blob)
		return FALSE;

	if(!CryptExportKey(KeyContext,0,ExportType,0,NULL,BlobSize))
		return FALSE;

	*Blob = (PBYTE)malloc(*BlobSize);

	if(!*Blob)
		return FALSE;

	if(CryptExportKey(KeyContext,0,ExportType,0,*Blob,BlobSize))
		return TRUE;

	free(*Blob);
	
	*Blob = NULL;
	
	*BlobSize = 0;

	return FALSE;
}

BOOLEAN RSAExportPublicKeyToBlob(_In_ HCRYPTKEY KeyContext,_Out_ PBYTE* PublicKeyBlob,_Out_ PULONG PublicKeyBlobSize)
{
	BOOLEAN Status = RSAExportKey(PUBLICKEYBLOB,KeyContext,PublicKeyBlob,PublicKeyBlobSize);
	return Status;
}

BOOLEAN RSAExportPrivateKeyToBlob(_In_ HCRYPTKEY KeyContext,_Out_ PBYTE* PrivateKeyBlob,_Out_ PULONG PrivateKeyBlobSize)
{
	BOOLEAN Status = RSAExportKey(PRIVATEKEYBLOB,KeyContext,PrivateKeyBlob,PrivateKeyBlobSize);
	return Status;
}

VOID RSADestroyExportedBlob(_In_ PBYTE Blob)
{
	if(Blob)
		free(Blob);
}

BOOLEAN RSACreateKeyHandleFromBlob(_In_ HCRYPTPROV CryptoContext,_In_ PBYTE Blob,_In_ ULONG BlobSize,_Out_ HCRYPTKEY* KeyContext)
{
	if(!Blob)
		return FALSE;

	if(!KeyContext)
		return FALSE;

	if(CryptImportKey(CryptoContext,Blob,BlobSize,0,0,KeyContext))
		return TRUE;

	*KeyContext = 0;

	return FALSE;
}

VOID RSADestroyKeyHandle(_In_ HCRYPTKEY KeyContext)
{
	CryptDestroyKey(KeyContext);
}

VOID DestroyCryptoContext(_In_ HCRYPTPROV CryptoContext)
{
	CryptReleaseContext(CryptoContext,0);
}

BOOLEAN RSAEncryptUsingKeyHandle(_In_ HCRYPTKEY KeyContext,_In_ PBYTE BufferToBeEncrypted, _In_ ULONG BufferSize /*,_Out_ PBYTE* EncryptedBuffer*/ ,_Out_ PULONG EncryptedBufferSize)
{

	if(!BufferToBeEncrypted)
		return FALSE;
	
	//if(!EncryptedBuffer)
	//	return FALSE;
	
	if(!EncryptedBufferSize)
		return FALSE;

	if(BufferSize >= MAX_BUFFER_SIZE_FOR_ENCRYPTION)
		return FALSE;

	//*EncryptedBuffer = (PBYTE)malloc(MAX_BUFFER_SIZE_FOR_ENCRYPTION);

	//if(!*EncryptedBuffer)
	//	return FALSE;

	/*RtlSecureZeroMemory(*EncryptedBuffer,MAX_BUFFER_SIZE_FOR_ENCRYPTION);*/

	/*memcpy(*EncryptedBuffer,BufferToBeEncrypted,BufferSize);*/

	*EncryptedBufferSize = BufferSize;

	if(CryptEncrypt(KeyContext,0,1,0,/* *EncryptedBuffer */ BufferToBeEncrypted ,EncryptedBufferSize,MAX_BUFFER_SIZE_FOR_ENCRYPTION))
		return TRUE;


	//free(*EncryptedBuffer);

	//*EncryptedBuffer = NULL;

	*EncryptedBufferSize = 0;

	return FALSE;

}

VOID RSADestroyAllocatedBuffer(_In_ PBYTE Buffer)
{
	RSADestroyExportedBlob(Buffer);
}

BOOLEAN RSAEncryptUsingPublicKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
	(_In_ PBYTE PublicKeyBlob,_In_ ULONG PublicKeyBlobSize,_In_ PBYTE BufferToBeEncrypted,_In_ ULONG BufferSize  /*,_Out_ PBYTE* EncryptedBuffer*/  ,_Out_ PULONG EncryptedBufferSize)
{
	HCRYPTKEY KeyContext = 0;
	HCRYPTPROV CryptoContext = 0;
	
	BOOLEAN Status = FALSE;

	if(!PublicKeyBlob)
		return FALSE;

	if(!GetCryptoContext(&CryptoContext))
		return FALSE;

	if(!RSACreateKeyHandleFromBlob(CryptoContext,PublicKeyBlob,PublicKeyBlobSize,&KeyContext))
		goto CleanUp;

	if(RSAEncryptUsingKeyHandle(KeyContext,BufferToBeEncrypted,BufferSize /*,EncryptedBuffer*/ ,EncryptedBufferSize))
		Status = TRUE;

CleanUp:
	
	if(KeyContext)
		RSADestroyKeyHandle(KeyContext);

	if(CryptoContext)
		DestroyCryptoContext(CryptoContext);

	return Status;
}

BOOLEAN RSADecryptUsingKeyHandle(_In_ HCRYPTKEY KeyContext,_Inout_ PBYTE BufferToBeDecrypted,_Out_ PULONG DecryptedBufferSize)
{
	if(!BufferToBeDecrypted)
		return FALSE;

	if(!DecryptedBufferSize)
		return FALSE;

	*DecryptedBufferSize = MAX_BUFFER_SIZE_FOR_ENCRYPTION;

	if(CryptDecrypt(KeyContext,0,1,0,BufferToBeDecrypted,DecryptedBufferSize))
		return TRUE;

	*DecryptedBufferSize = 0;

	return FALSE;
}

BOOLEAN RSADecryptUsingPrivateKeyBlobWithoutTheNeedForManuallyCreatingACryptoContextFunctionInspiredByMicrosoftNamingConvention
	(_In_ PBYTE PrivateKeyBlob,_In_ ULONG PrivateKeyBlobSize,_Inout_ PBYTE BufferToBeDecrypted,_Out_ PULONG DecryptedBufferSize)
{
	HCRYPTPROV CryptoContext = 0;
	HCRYPTKEY KeyContext = 0;

	BOOLEAN Status = FALSE;

	if(!PrivateKeyBlob)
		return FALSE;
	
	if(!GetCryptoContext(&CryptoContext))
		return FALSE;

	if(!RSACreateKeyHandleFromBlob(CryptoContext,PrivateKeyBlob,PrivateKeyBlobSize,&KeyContext))
		goto CleanUp;

	if(RSADecryptUsingKeyHandle(KeyContext,BufferToBeDecrypted,DecryptedBufferSize))
		Status = TRUE;

CleanUp:

	if(KeyContext)
		RSADestroyKeyHandle(KeyContext);

	if(CryptoContext)
		DestroyCryptoContext(CryptoContext);
	return Status;
}
BOOLEAN CalculateSHA1(_In_ PBYTE Buffer,_In_ ULONG BufferSize,_Out_ PBYTE Hash,__inout_opt PULONG HashBufferSize)
{
	HCRYPTPROV CryptoContext = 0;
	HCRYPTHASH HashContext = 0;
	BOOLEAN Status = FALSE;
	ULONG Temporary = 0;

	if(!Buffer)
		return FALSE;

	if(!Hash)
		return FALSE;

	if(!GetCryptoContext(&CryptoContext))
		return FALSE;

	if(!CryptCreateHash(CryptoContext,CALG_SHA1,0,0,&HashContext))
		goto CleanUp;

	if(!CryptHashData(HashContext,Buffer,BufferSize,0))
		goto CleanUp;

	if(CryptGetHashParam(HashContext,HP_HASHVAL,Hash,HashBufferSize ? HashBufferSize : &Temporary,0))
		Status = TRUE;

CleanUp:
	
	if(HashContext)
		CryptDestroyHash(HashContext);

	if(CryptoContext)
		DestroyCryptoContext(CryptoContext);


	return Status;

}
#pragma warning(disable:4996)

BOOLEAN WriteShellToFile(PCHAR HeaderFileName,PCHAR SourceFileName,PCHAR ShellName,PCHAR PageGuardEpilogue,PBYTE Shellcode,ULONG ShellSize)
{
	FILE* Shell = NULL;
	ULONG Index = 0;

	if(!HeaderFileName)
		return FALSE;

	if(!SourceFileName)
		return FALSE;

	if(!ShellName)
		return FALSE;

	if(!Shellcode)
		return FALSE;

	if(!PageGuardEpilogue)
		return FALSE;

	fopen_s(&Shell,HeaderFileName,"wb");
	if(!Shell)
		return FALSE;

	fprintf(Shell,"#ifndef _%s_H_\r\n#define _%s_H_\r\nextern unsigned char %s[0x%X];",PageGuardEpilogue,PageGuardEpilogue,ShellName,ShellSize);
	fprintf(Shell,"\r\n#endif\r\n");
	fclose(Shell);

	fopen_s(&Shell,SourceFileName,"wb");
	if(!Shell)
		return FALSE;

	fprintf(Shell,"#include \"%s\"\r\nunsigned char %s[0x%X] = \r\n{\r\n\t",HeaderFileName,ShellName,ShellSize);
	for(; Index < ShellSize;++Index)
	{
		if(Index % 1 == 0 && Index != 0)
			fprintf(Shell,",");
		if(Index % 15 == 0 && Index != 0)
			fprintf(Shell,"\r\n\t");
		fprintf(Shell,"0x%02X",Shellcode[Index]);
	}
	
	fprintf(Shell,"\r\n};\r\n");
	fclose(Shell);

	return TRUE;
}